<?php
class model_admin_menu extends CI_Model {
	function __construct()
	{
		$this->db = $this->load->database("default",TRUE);
		parent::__construct();
	}
	
	function cekAccessMenu($ref_menu)
	{
		$this->db->where("filez",$ref_menu);
		$query = $this->db->get("menu_admin");
		//die($this->db->last_query());
		return $query;
	}
	
	function cekLogin($username,$userpass)
	{
		$this->db->where("username",$username);
		$this->db->where("userpass",$userpass);
		$query=$this->db->get("admin");
		return $query;
	}
}
?>