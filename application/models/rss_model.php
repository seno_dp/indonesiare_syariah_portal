<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rss_model extends CI_Model {

	function getNews()
	{
		$this->db->order_by('create_date', 'desc');
		$query = $this->db->get('kg_news', 5);

		return $query;
	}

	function getPublications()
	{
		$this->db->order_by('create_date', 'desc');
		$query = $this->db->get('kg_publication', 5);

		return $query;
	}

	function getProjects()
	{
		$this->db->order_by('create_date', 'desc');
		$query = $this->db->get('kg_projects', 5);

		return $query;
	}

	function getServices()
	{
		$this->db->order_by('create_date', 'desc');
		$query = $this->db->get('kg_services', 5);

		return $query;
	}



}

/* End of file posts_model.php */
/* Location: ./application/models/posts_model.php */