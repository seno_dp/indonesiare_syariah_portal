<!-- PENGADUAN -->
<?php 
    $image_field = ($controller_name == "article") ? 'thumbnail' : 'image';
?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="cc_single_post">
                    <div class="title-list-page">                        
                        <h3><?php echo ($menu_title) ? ucfirst($menu_title) : "Article"?></h3>                    
                    </div>                
                    <div class="post_text">
                        <p>
                            Halaman ini disediakan untuk sebanyak-banyaknya menampung masukan dari semua karyawan ReINDO
                            demi transparansi dan peningkatan kinerja perusahaan 
                        </p>
                        
                        <form method="POST" action="<?php echo base_url();?>id/pengaduan/main">
                            <div class="form-rent-car">
                                <label>Nama</label>
                                <input type="text" name="name" value="<?php echo set_value('name');?>">
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-rent-car">
                                        <h6>Divisi</h6>
                                        <label class="select">
                                        <?php echo form_dropdown('divisi', $opt_divisi, set_value('divisi'));?>
                                        </label>
                                    </div>                                    
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="form-rent-car">
                                        <h6>Bag. Pengaduan</h6>
                                        <label class="select">
                                        <?php echo form_dropdown('bag_pengaduan', $opt_bag_pengaduan, set_value('bag_pengaduan'));?>
                                        <?php echo form_error('bag_pengaduan');?>
                                        </label>                      
                                    </div>                                    
                                </div>
                            </div>
                            <div class="clear"></div>
                            
                            <p class="small">*Anda bisa mengosongkan nama dan divisi jika ingin memberikan laporan secara anonim</p>
                            <div class="form-rent-car">
                                <h6>Perihal</h6>
                                <label class="select">
                                <?php echo form_dropdown('perihal', $opt_ref_perihal, set_value('perihal'));?>
                                <?php echo form_error('perihal');?>
                                </label>              
                            </div>

                            <div class="form-rent-car">
                                <label>Subject</label>
                                <input type="text" name="subject" value="<?php echo set_value('subject');?>">
                                <?php echo form_error('subject');?>
                            </div>

                            <div class="clear"></div>
                            
                            <div class="form-rent-car">
                                <label>Isi Aduan</label>
                                <textarea name="isi_aduan"><?php echo set_value('isi_aduan');?></textarea>
                                <?php echo form_error('isi_aduan');?>
                            </div>
                            
                            <div class="submit-pengaduan">
                                <button class="btn-pengaduan" type="submit" value="submit">Kirim</button>
                            </div>
                            <div class="flex_66">
                                <h4 class="info_pengaduan"><?php echo $info;?></h4>
                            </div>
                        </form>
                    </div>                    
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="home_sidebar">
                    <div class="follow_us_side">
                        <?php echo $this->load->view('detail_sidebar')?> 
                    </div>
                </div>                  
            </div>
        </div>
    </div>
</section>

<!-- PENGADUAN -->

<div style="clear: both"></div>
<?php if($controller_name == 'kliping') { ?>
<script>
 jQuery(document).ready(function() {
    
            var config = {
              '.chzn-select'           : {},
              '.chzn-select-deselect'  : {allow_single_deselect:true},
              '.chzn-select-no-single' : {disable_search_threshold:10},
              '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
              '.chzn-select-width'     : {width:"100%"}
            }

            for (var selector in config) {
              jQuery(selector).chosen(config[selector]);
            }

            //jQuery("#datepicker").datepicker();

            jQuery("#submit_cari_kliping").click(function(){
                alert(jQuery("#date_kliping").val());
                    //alert($('input[name="totalsemua"]').val());
                        var data = {
                            id_koran:jQuery("#id_koran").val(), 
                            date_kliping:jQuery("#date_kliping").val(), 
                            text_kliping:jQuery("#text_kliping").val() 
                        };
                        //$("#shipping").hide();
                        jQuery.ajax({
                                type: "POST",
                                url : "<?php echo site_url('kliping/search')?>",
                                data: data,
                                success: function(msg){

                                    //var variable_returned_from_php = msg.biaya_kirim;
                                    //$("#shipping").show();
                                    //jQuery("#comment").val(''); 
                                    jQuery("#search_kliping_result").html(msg);
                                },
                                error: function(){
                                    alert('failure');
                                }
                            });                         
                }); 

        }); 
</script>
<?php } ?>