<!-- KRITERION PAGE -->
            <div class="flex_66_11">
                <div class="post" id="">
                	<?php 
							if($member->num_rows() > 0){
								$v = $member->row_array();
						?>
                    <div class="wrapper-admin">
                        <div id="" class="">

							<div class="cover-area">
								<?php if($v['cover']) { ?>
								     <img class="cover_photo_admin" src="<?php echo base_url().'uploads/'.$v['cover']?>" style="max-width: 960px; max-height: 300px;"/>
					            <?php }else{ ?>
					                <img class="cover_photo_admin" src="<?php echo base_url()?>uploads/2013/06/cover-photo-admin.jpg" />
					            <?php } ?>
							</div>		
							<div style="clear:both"></div>
								<div class="nametag-admin">
									<div class="admin-photo-profile">
										<?php if($v['image']) { ?>
						                    <img src="<?php echo base_url().'uploads/'.getThumb($v['image'])?>" />
						                <?php }else{ ?>
						                    <img src="<?php echo base_url().'assets/theme/images/180x180.jpg'?>" />
						                <?php } ?>
									</div>
									<div class="fullname-admin">
										<h2><?php echo $v['title']?></h2>
										<div class="admin-edit">
											<?php if($this->uri->segment(4) == $this->session->userdata('user_id_sess')) { ?>
												<a href="<?php echo site_url('member/profile')?>"><img src="<?php echo base_url()?>uploads/2013/06/edit_icon.png" />Edit Profile</a>
											<?php } ?>
										</div>
									</div>
								</div> 							
							<div class="wrapper-left-profile">
								<div class="top-about-administrator"></div>	
								
								<div class="about-administrator">
									<?php if($v['content']) { ?>
										<p><strong>About Me : </strong><?php echo $v['content']?></p>
									<?php } ?>
								</div>
								
								<div class="latest-activity">
									<h4>Latest Activity</h4>
									<?php if($ma->num_rows() > 0){?>
										<ul>
	                    					<?php foreach($ma->result_array() as $mval) {?>
												<?php 
													if($mval['activity'] == 'login' || $mval['activity'] == 'logout' || $mval['activity'] == 'update_cover' || $mval['activity'] == 'update_avatar' || $mval['activity'] == 'update_profile' || $mval['activity'] == 'delete_blog'){
														$url = current_url();
													}else{
														$url = site_url($mval['url']);
													}
												?>
												<li><a href="<?php echo $url ?>"><?php echo $mval['title']?></a><span class="date-list"><?php echo date('d M Y H:i:s', strtotime($mval['create_date'])).' WIB'?></span></li>
											<?php } ?>
										</ul>
									<?php } ?>
								</div>								
							</div>	
							<div class="wrapper-right-profile">
								<div class="sidebar-blog-post">
										<div class="head-blog-post">
											<h4>Blog Post</h4>
											<?php if($this->uri->segment(4) == $this->session->userdata('user_id_sess')) { ?>
												<a href="<?php echo site_url('blog/post')?>"><img src="<?php echo base_url()?>uploads/2013/06/write_icon.png" />Posting Blog</a>
											<?php } ?>
										</div>								
									<div class="list-post-blog">
										<?php if($qp->num_rows() > 0){?>
											<ul>
		                    					<?php foreach($qp->result_array() as $qval) {?>
													<li><a href="<?php echo site_url('blog/detail/'.$qval['id'].'/'.url_title($qval['title']))?>"><?php echo $qval['title']?></a><span class="date-list"><?php echo date('d M Y', strtotime($qval['create_date']))?></span></li>
												<?php } ?>
											</ul>
										<?php } ?>
									</div>
								</div>	
								<!-- <div class="sidebar-thread-post">
										<div class="head-blog-post">
											<h4>Thread Post</h4>
											<a href="#"><img src="<?php echo base_url()?>uploads/2013/06/write_icon.png" />Posting Thread</a>
										</div>								
									<div class="list-post-blog">
										<ul>
											<li><a href="#">Update : Dept Undwriting Fakultatif Marine & Aviation Reindo Reinsurance 2014</a><span class="date-list">04 Mei 2014</span></li>
											<li><a href="#">Badan Pengelola Pusat Data Asuransi Nasional (BPPDAN) adalah sebagai pusat data statistik jenis asuransi kebakaran Indonesi</a><span class="date-list">04 Mei 2014</span></li>
											<li><a href="#">Special Award-Most Innovative Reinsurance Company 2006</a><span class="date-list">04 Mei 2014</span></li>
										</ul>
									</div>
								</div>	 -->								
							</div>
                        </div>
                        <div style="clear:both"></div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div style="clear: both"></div>