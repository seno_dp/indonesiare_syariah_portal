<script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/uploading-file.js'></script>
<script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/uploading-file-ava.js'></script>

<!-- EDIT PROFILE -->
    <section>
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="cc_single_post">
                    <div class="post_text">
                        <div class="wrap-of-editorial">
                                <div class="breadcum_c_left">
                                    Administrator : <a href="#">Edit Profile</a>
                                </div>                        
                        </div>
                    </div> 
                    <div class="forum-detail">
                        <div id="respond" class="cc_single_post">
                            <?php 
                                if($member->num_rows() > 0){
                                    $v = $member->row_array();
                            ?>      
                                <form action="<?php echo site_url('member/edit')?>" method="post" class="comment-form" enctype="multipart/form-data">

                                <div class="row">
                                    <div class="wrap-overflow">
                                        <div class="col-lg-12 col-md-12 col-md-12 col-xs-12">
                                            <div class="every-form">
                                                <label class="label" for="email">Cover Picture (1400x353)
                                                    <span class="required">:</span>
                                                </label>                                              
                                            </div>                                         
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <div class="wrap-blank-avatar">
                                                <?php echo form_hidden('file_old_cover',set_value('file_old_cover',$v['cover']));?>
                                                <?php
                                                    if($v['cover']){
                                                        echo '<div class="thumb_image_cover">';
                                                        echo '<img class="img-responsive" src="'.base_url().'uploads/'.getThumb($v['cover']).'">';
                                                        echo '<div class="delete_cover">Delete</div></div>';
                                                    }
                                                ?>                                            
                                            </div>   
                                        </div>    
                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">                                  
                                            
                                            <div class="input-file-container">
                                                    <input class="input-file-ava" id="my-file-ava" type="file" maxlength="50" name="attachedfilecover">                                                
                                                     
                                                    <label tabindex="0" for="my-file-ava" class="input-file-trigger-ava">Browse Cover</label>
                                                    <?php echo form_hidden('file_old',set_value('file_old',$v['image']));?>
                                                    
                                                    <span class="file-return-ava"></span> 
                                                </div>                                              
                                        </div>                                          
                                    </div>                                 
                                </div>
                                <div class="row">
                                    <div class="wrap-overflow">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="every-form">
                                                <label class="label" for="email">Avatar
                                                    <span class="required">:</span>
                                                </label>                                                  
                                            </div>                                    
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <div class="wrap-blank-cover">
                                                <?php
                                                    if($v['image']){
                                                        echo '<div class="thumb_image_avatar">';
                                                        echo '<img src="'.base_url().'uploads/'.getThumb($v['image']).'"><br/>';
                                                        echo '<div class="delete_avatar">Delete</div></div>';
                                                    }
                                                ?>                                            
                                            </div>
                                        </div>
                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">                                   
                                                <div class="input-file-container">                                             
                                                <input class="input-file" id="my-file" type="file" maxlength="50" allow="text/*" name="attachedfile"> 
                                                <label tabindex="0" for="my-file" class="input-file-trigger">Browse Avatar</label>
                                                <span class="required">:</span>
                                                <!-- <input id="file_old" name="file_old" type="hidden" value="<?php echo $val['image']?>"/> -->
                                                <?php echo form_hidden('file_old_cover',set_value('file_old_cover',$v['cover']));?>
                                                <span class="file-return"></span>  
                                            </div>   
                                      
                                        </div>                                         
                                    </div>                                   
                                </div>
<!--                                     <p class="comment-form-email">
                                        <label for="email">Cover Picture (1400x353)
                                            <span class="required">:</span>
                                        </label>
                                        <?php echo form_hidden('file_old_cover',set_value('file_old_cover',$v['cover']));?>
                                        <input type="file" maxlength="50" allow="text/*" name="attachedfilecover"></input>
                                        <?php
                                            if($v['cover']){
                                                echo '<div class="thumb_image_cover">';
                                                echo '<img src="'.base_url().'uploads/'.getThumb($v['cover']).'"><br/>';
                                                echo '<div class="delete_cover">Delete</div></div>';
                                            }
                                        ?>
                                    </p> -->

<div class="col-lg-12">
    <div class="wrap-editorial">
        <div class="every-form">
            <label class="label" for="author">Name
                <span class="required">:</span>
            </label>
            <!--input id="post-title" name="author" type="text" value="" size="30" aria-required='true' value="<?php echo $v['title']?>"/-->
            <?php echo form_input(array('name'=>'title','aria-required'=>'true','value'=>set_value('title',$v['title'])));?>
            <?php echo form_error('title'); ?>
        </div>

        <div class="every-form">
            <label class="label" for="author">Email
                <span class="required">:</span> </label>
                <!--input id="post-title" name="author" type="text" value="" size="30" aria-required='true' value="<?php echo $v['email']?>"/-->
                <?php echo form_input(array('name'=>'email','aria-required'=>'true','value'=>set_value('email',$v['email']),'readonly'=>'true'));?>
                <?php echo form_error('email'); ?>
        </div>

        <div class="every-form">
            <label class="label" for="author">Pangkat
                <span class="required">:</span> </label>
                <?php
                $filter = array(
                        "id"=>"where/".$v['pangkat_id'],
                );

                $pangkat = GetAll('kg_pangkat',$filter);
                if($pangkat->num_rows() > 0)
                {
                    $q_pangkat = $pangkat->row_array();
                    $v_pangkat_id = $q_pangkat['title'];
                }else{
                    $v_pangkat_id = 1;
                }
                ?>
                <!--input id="post-title" name="author" type="text" value="" size="30" aria-required='true' value="<?php echo $v['email']?>"/-->
                <?php echo form_input(array('name'=>'pangkat_id','aria-required'=>'true','value'=>set_value('pangkat_id',$v_pangkat_id),'readonly'=>'true'));?>
                <?php echo form_error('pangkat_id'); ?>
        </div>

        <div class="every-form">
            <label class="label" for="author">Phone
                <span class="required">:</span> </label>
                <?php echo form_input(array('name'=>'phone','aria-required'=>'true','value'=>set_value('phone',$v['phone'])));?>
                <?php echo form_error('phone'); ?>
                <!--input id="post-title" name="author" type="text" value="" size="30" aria-required='true' value="<?php echo $v['phone']?>"/-->
        </div>

        <div class="every-form">
            <label class="label" for="author">Address
                <span class="required">:</span> </label>
                <!--textarea id="address" name="address" cols="31" rows="2" aria-required="true"><?php echo $v['address']?></textarea-->
                <?php echo form_textarea(array('name'=>'address','aria-required'=>'true','value'=>set_value('address',$v['address']),'rows'=>'3', 'style'=>'width:100%'));?>    
        </div> 

        <div class="every-form">
            <label class="label" for="author">Tentang saya
                <span class="required">:</span> </label>
                <?php echo form_textarea(array('name'=>'content','aria-required'=>'true','value'=>set_value('content',$v['content']),'rows'=>'5', 'style'=>'width:100%'));?>
        </div>                                      
            
        <div class="every-form">
                <input name="submit" type="submit" id="submit" value="Cancel" />
                <input name="submit" type="submit" id="submit" value="Save" />
                <?php echo form_hidden('id',set_value('id',$v['id']));?>    
        </div>
                                              
    </div>    
</div>
                                </form>                            
                            <?php } ?>                  
                        </div>
                    </div>
                                      
                </div>
            </div>
        </div>
    </section>
<!-- EDIT PROFILE -->


<div style="clear: both"></div>