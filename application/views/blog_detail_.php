<div class="flex_66">
                <div class="box">
                    <div class="listarticle-title">
                        <b>Blog Post
                            <span class="arrows">&raquo;</span>
                        </b>
                    </div>
                </div>
                <br/>
                <br/>
                <?php if($blog->num_rows() > 0) {?>
    			<?php $val = $blog->row_array();?>
                <div class="post-<?php echo $val['id']?> post type-post status-publish">
                    <h1 id="post-<?php echo $val['id']?>">
                        <a href="#" title="<?php echo $val['title']?>">
                            <?php echo $val['title']?></a>
                    </h1>
                    <div class="entry">
                    	<?php if($val['image']) {?>
                        	<img class="thumb-large" src="<?php echo base_url()?>uploads/<?php echo $val['image']?>" class="attachment-blog-image wp-post-image" alt="<?php echo $val['title']?>"/>
                       
                        <?php } ?>
                        <div>
                        	
                        	<?php echo $val['content']?>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <br/>
                    <div class="postmetadata">
                        <!-- KRITERION META -->
                        <div class="meta">
                            <span class="time"><?php echo date('M d, Y',strtotime($val['create_date']))?></span>
                            <em>by </em>
                            <?php echo '<a href="'.site_url('member/userprofile/'.$val['create_user_id'].'/'.url_title($val['title'])).'">'.GetUserName('kg_member','title',$val['create_user_id']).'</a>'?>
                            
						</div>
                        <span class="categories">Filed Under:
                            <a href="#" title="View all posts in <?php echo $val['category']?>" rel="category tag"><?php echo $val['category']?></a>
                        </span>
                        Tags:
                        <?php echo explodetags($val['tags']) ?>
                        <?php if($comment->num_rows() > 0) { 
                        	$num_comment = $comment->num_rows()." comments";
                        }else{
                        	$num_comment = "No comment yet";
                        }
                        ?>
                        <a href="##respond" class="post-comments" title="Comment on <?php echo $val['title']?>"><?php echo $num_comment;?> </a>
                    </div>
					<div class="area-commenters" id="area-commenters">
						<?php if($comment->num_rows() > 0) {?>
						<?php foreach ($comment->result_array() as $comm) { ?>
						<div class="list-commenters">
							<div class="display-commenters">
								<?php if($comm['image_member']){ ?>
									<img class="thum-commenters" src="<?php echo base_url()?>uploads/<?php echo getThumb($comm['image_member'])?>" />
								<?php }else{ ?>
									<img class="thum-commenters" src="<?php echo base_url()?>uploads/2013/06/commenters.png" />
								<?php } ?>
								<span><p class="name-commenters"><?php echo $comm['name_member']?></p></span>
								<p class="date-comments"><?php echo date('M d, Y',strtotime($comm['create_date']))?></p>
							</div>
							<div class="box-commenters">
								<p class="message-commenters"><?php echo $comm['content']?></p>
							</div>						
						</div>
						<?php } ?>
						<?php }else{ ?>
						<div class="list-commenters">
							<div class="display-commenters">
								<img class="thum-commenters" src="<?php echo base_url()?>uploads/2013/06/commenters.png" />
								<span><p class="name-commenters">Anonymous</p></span>
								<p class="date-comments">dd mm, yyyy</p>
							</div>
							<div class="box-commenters">
								<p class="message-commenters">No comment</p>
							</div>						
						</div>
						<?php } ?>
					</div>
					<div class="post-comment-blog">
						<h3>Post Comment</h3>
						<input type="hidden" id="blog_id" value="<?php echo $this->uri->segment(4)?>" name="blog_id">
						<input type="hidden" id="member_id" value="<?php echo $this->session->userdata('user_id_sess')?>" name="member_id">
						<textarea class="blog-comment" name="comment" id="comment"></textarea>
						<input id="submit-comment-blog" type="button" value="Post Comment" name="submit"></input>
					</div>
                </div>
                <?php } ?>

                
                
               
                
            </div>
            <div class="flex_33">
                <div id="sidebar">
                    <!-- begin widget sidebar -->
                    <div class="sidebar_100">
					  <script type="text/javascript">
						$(document).ready( function() {
						  $('#tab-container').easytabs();
						});
					  </script>
					
					<div class="box">
						<div id="tab-container" class='tab-container'>
							<ul class='etabs'>
								<?php if($recent->num_rows() > 0 ) {?>
								<li class='tab'>
									<a href="#tabs-recent">Recent</a>
								</li>
                                <?php } ?>
                                <?php if($popular->num_rows() > 0 ) {?>
								<li class='tab'>
									<a href="#tabs-popular">Popular</a>
								</li>
                                <?php } ?>
                                <?php if($mostcomm->num_rows() > 0 ) {?>
								<li class='tab'>
									<a href="#tabs-comment">Comments</a>
								</li>
                                <?php } ?>
								<!-- <li class='tab'>
									<a href="#tabs-comment">Comments</a>
								</li> -->
							</ul>
							<div class='panel-container'>
								<?php if($recent->num_rows() > 0 ) { ?>
                                <div id="tabs-recent">
									<ul>
                                        <?php foreach ($recent->result_array() as $valrec) {?>
                                            <li><a href="<?php echo site_url('blog/detail/'.$valrec['id'].'/'.url_title($valrec['title']))?>"><?php echo $valrec['title']?></a></li>
                                        <?php } ?>
									</ul>
								</div>
                                <?php } ?>
								<?php if($popular->num_rows() > 0 ) { ?>
								<div id="tabs-popular">
									<ul>
                                        <?php foreach ($popular->result_array() as $valpop) {?>
                                            <li><a href="<?php echo site_url('blog/detail/'.$valpop['id'].'/'.url_title($valpop['title']))?>"><?php echo $valpop['title']?></a></li>
                                        <?php } ?>
									</ul>
								</div>
                                <?php } ?>
                                <?php if($mostcomm->num_rows() > 0 ) { ?>
								<div id="tabs-comment">
									<ul>
                                        <?php foreach ($mostcomm->result_array() as $valcomm) {?>
                                            <li>
                                            	<div class="commenters">
                                            		<img src="<?php echo base_url()?>uploads/<?php echo getThumb($valcomm['image_member'])?>" style="width: 41px; "/>
                                            	</div>
												<a href="<?php echo site_url('blog/detail/'.$valcomm['id_blog'].'/'.url_title($valcomm['title_blog']))?>"><?php echo $valcomm['title_blog']?></a>
											</li>
                                        <?php } ?>
									</ul>
								</div>
                                <?php } ?>
								<!-- <div id="tabs-comment">
									<ul>
										<li>
											<div class="commenters"><img src="theme/images/thumbnail_admin.jpg"/></div>
											<a href="#">Penghargaan Reindo Reinsurance Indonesia 2014 </a>
										</li>
										<li>
											<div class="commenters"><img src="theme/images/thumbnail_admin.jpg"/></div>
											<a href="#">Rapat Umum Pemegang Saham Luar Biasa PT Reasuransi Internasional</a>
										</li>
										<li>
											<div class="commenters"><img src="theme/images/thumbnail_admin.jpg"/></div>
											<a href="#">Penghargaan Reindo Reinsurance Indonesia 2014 </a>
										</li>
										<li>
											<div class="commenters"><img src="theme/images/thumbnail_admin.jpg"/></div>
											<a href="#">Penghargaan Reindo Reinsurance Indonesia 2014 </a>
										</li>
										<li>
											<div class="commenters"><img src="theme/images/thumbnail_admin.jpg"/></div>
											<a href="#">Penghargaan Reindo Reinsurance Indonesia 2014 </a>
										</li>										
									</ul>
								</div> -->
							</div>
						</div>
					
					</div>
                        <!-- /.awesome-weather-wrap -->
                    </div>                    <div class="clear"></div>
                    <br/>
                </div>
            </div>
            <div style="clear: both"></div>