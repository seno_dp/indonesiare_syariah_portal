<!-- REINDO LIST EVENTS -->
<?php 
    $image_field = ($controller_name == "article" || $controller_name == "jadwal_closing" || $controller_name == "it_support" || $controller_name == "board_news" || $controller_name == "baporsi" || $controller_name == "csr" || $controller_name == "sp_reindo" || $controller_name == "board_management") ? 'thumbnail' : 'image';
?>
<!-- REINDO LIST EVENTS -->
<section class="main_news_wrapper cc_single_post_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="cc_single_post">
                    <div class="title-list-page">
                        <h3><?php echo ($menu_title) ? ucfirst($menu_title) : "Article"?></h3>
                    </div>                
                    <?php if($qp->num_rows() > 0) {?>
                    <?php foreach($qp->result_array() as $qval) {?>
                        <div class="post-<?php echo $qval['id']?> list-of-article">
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                   <?php if($qval[$image_field]) {?>
                                    <div class="thumb-arhive-image">
                                        <a href="<?php echo site_url($controller_name.'/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" rel="bookmark">
                                            <img width="263" src="<?php echo base_url()?>uploads/<?php echo GetThumb($qval[$image_field])?>" class="attachment-blog-image wp-post-image" alt="<?php echo $qval['title']?>"/>
                                        </a>
                                    </div>
                                    <?php }else{ ?>
                                     <div class="thumb-arhive-image">
                                        <a href="<?php echo site_url($controller_name.'/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" rel="bookmark">
                                            <img width="263" src="<?php echo base_url()?>assets/theme/images/no_image.jpg" class="attachment-blog-image" alt="<?php echo $qval['title']?>"/>
                                        </a>
                                    </div>
                                    <?php } ?>                                    
                                </div>
                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                    <h4 id="post-<?php echo $qval['id']?>">
                                        <a href="<?php echo site_url($controller_name.'/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" title="<?php echo $qval['title']?>">
                                            <?php echo $qval['title']?>
                                        </a>
                                    </h4>
                                    <!-- POST TIME AND AUTHORE -->
                                        <div class="post_meta post-meta-list">
                                            <ul>
                                                <li>
                                                    <a href=""><i class="fa fa-clock-o"></i><?php echo date('M d, Y',strtotime($qval['create_date']))?></a>
                                                </li>
                                                <li><a href="">by </a></li>
                                                <li><a href=""><i class="fa fa-user"></i><?php echo GetUserName('kg_admin','name',$qval['create_user_id'])?></a></li>
                                            </ul>
                                        </div> 
                                    <!-- POST TIME AND AUTHORE -->                                     
                                    <div class="excerptarhive"><?php echo $qval['headline']?></div>
                                    <div class="clear"></div>                                     
                                </div>                              
                            </div>
                        </div>
                        <?php } ?>
                    <?php }else{ ?>
                    <?php } ?>
                    <!-- START PAGINATION AREA -->               
                    <div class="news_pagination">
                        <ul class="news_pagi">
                            <?php echo $pagination?>
                        </ul>
                    </div>     
                    <!-- END PAGINATION AREA -->               
                </div>
            </div>
            <!-- START SIDEBAR RIGHT -->
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="home_sidebar">
                    <div class="follow_us_side">
                        <?php echo $this->load->view('detail_sidebar')?>   
                    </div>
                </div>                
            </div>
            <!-- END SIDEBAR RIGHT -->
        </div>
    </div>
</section>
<!-- REINDO LIST EVENTS -->