<!--

<div class="top-nav">
    <div class="menu-top-container">
        <div class="adsheader">
            SEARCH BLOCK 
            <div class="search-block">
                <form method="post" id="searchform" action="<?php echo site_url('home/keyword')?>">
                    <input class="search-button" type="submit" value="" />
                    <input type="text" id="s" name="s" value="Search..." onfocus="if (this.value == 'Search...') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Search...';}" />
                </form>
            </div>
        </div>
    <?php 
    if($this->session->userdata('user_id_sess')) {
        $id = $this->session->userdata('user_id_sess');
        $filter = array(
                "id"=>"where/".$id,
            );

        $member = GetAll('kg_member',$filter);
        if($member->num_rows() > 0){
        $v = $member->row_array();
    ?>
    <div class="admin-thumb">
        <div  class="left-text">
            <p><a href="<?php echo site_url('blog/my_profile')?>"><?php echo $v['title']?></a></p>
            <span><a href="<?php echo site_url('member/logout')?>">Log Out</a> </span>           
        </div>
        <?php if($v['image']) { ?>
            <div class="thumbnail-ava">
                <img src="<?php echo base_url().'uploads/'.getThumb($v['image'])?>" style="width:100%"/>
            </div>
        <?php }else{ ?>
            <div class="thumbnail-ava">
                <img src="<?php echo base_url().'assets/theme/images/180x180.jpg'?>" style="width:100%"/>
            </div>
        <?php } ?>
    </div>
    <?php } }else{ ?>
    <div class="admin-thumb">
        <div  class="left-text">
            <a href="<?php echo site_url('member/login')?>">Login</a> 
            <span><a href="<?php echo site_url('member/register')?>"> | Register</a></span>           
        </div>
        
    </div>


    <?php } ?>

    </div>
</div>
<div id="header" class="flex_100">
     LOGO 
    <div class="logo">
        <a href="<?php echo site_url('home')?>">
            <img src="<?php echo base_url()?>assets/theme/images/logo.png" alt="Reindo Reinsurance" border="0" />
        </a>
    </div>
</div>
-->

<!-- ~~~=| Header START |=~~~ -->
<header class="header_area">
  <div class="header_top">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-12">
        </div>
        <div class="col-md-6 col-sm-6 col-xs-12">
          <div class="header_top_right">
            <div class="social_header">
				<ul>
					<li><a href="https://web.facebook.com/PT-Reasuransi-Indonesia-Utama-Persero-1226629397355581/?ref=page_internal" target="_BLANK"><img width="26" height="26" src="<?php echo base_url() ?>assets/theme/images/fb-icon.png"></a></li>
					<li><a href="https://twitter.com/indonesia_re" target="_BLANK"><img width="26" height="26" src="<?php echo base_url() ?>assets/theme/images/twitter-icon.png"></a></li>
					<li><a href="https://www.youtube.com/channel/UCvUvsrVFUWJizR5V1E2fcuw" target="_BLANK"><img width="26" height="26" src="<?php echo base_url() ?>assets/theme/images/youtube-icon.png"></a></li>
					<li><a href="https://linkedin.com/company/pt.-reasuransi-indonesia-utama-persero-" target="_BLANK"><img width="26" height="26"  src="<?php echo base_url() ?>assets/theme/images/linkedin-icn.png"></a></li>
					<li><a></a></li>
					<li><a>&nbsp;</a></li>					
				</ul>
            </div>
            <div class="header_search_box">
              <form class="header_search hidden-xs" method="POST" action="<?php echo site_url('home/keyword')?>">
                <input type="text" placeholder="Search" name="s">
                <input type="submit" value="">
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  
  
  <div class="header_logo_area">
    <div class="container">
      <div class="row">
        <!-- ~~~=| Logo Area START |=~~~ -->  
        <div class="col-md-4 col-sm-4 col-xs-12">
            <div class="logo">
              <a href="<?php echo site_url('home')?>">
                <img src="<?php echo base_url()?>assets/theme/images/logo.png" alt="Reindo Reinsurance" border="0" />
                </a>
            </div>
        </div>
        <!-- ~~~=| Logo Area END |=~~~ -->   
        <div class="col-md-8 col-sm-8 col-xs-12">
            <div class="header_add">
                <?php 
                if($this->session->userdata('user_id_sess')) {
                    $id = $this->session->userdata('user_id_sess');
                    $filter = array(
                            "id"=>"where/".$id,
                        );

                    $member = GetAll('kg_member',$filter);
                    if($member->num_rows() > 0){
                    $v = $member->row_array();
                ?>
                <div class="admin-thumb">
					<div class="rating-company">
						<ul>
							<li><img width="95" height="76" src="<?php echo base_url() ?>assets/theme/images/fitch-rating.png"/></li>
							<li><img width="95" height="76" src="<?php echo base_url() ?>assets/theme/images/pefindo-rating.png"/></li>
						</ul>
					</div>
                    <div  class="left-text">
                        <p><a href="<?php echo site_url('blog/my_profile')?>"><?php echo $v['title']?></a></p>
                        <span><a href="<?php echo site_url('member/logout')?>">Log Out</a> </span>           
                    </div>
                    <?php if($v['image']) { ?>
                        <div class="thumbnail-ava">
                            <img src="<?php echo base_url().'uploads/'.getThumb($v['image'])?>" style="width:100%"/>
                        </div>
                    <?php }else{ ?>
                        <div class="thumbnail-ava">
                            <img src="<?php echo base_url().'assets/theme/images/180x180.jpg'?>" style="width:100%"/>
                        </div>
                    <?php } ?>
                </div>
                <?php } }else{ ?>
                <div class="admin-thumb">
                    <div  class="left-text">
                        <a href="<?php echo site_url('member/login')?>">Login</a> 
                        <span><a href="<?php echo site_url('member/register')?>"> | Register</a></span>           
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <!--<div class="border-rainbow"></div>-->  
      </div>
    </div>
  </div>
  
  
  <!-- ~~~=| Main Navigation START |=~~~ -->

  <!-- ~~~=| Main Navigation END |=~~~ --> 
  
</header>
