<!-- START LIST VIDEO -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="cc_single_post">
                    <!-- REINDO BREAD CRUMB -->
                    <!-- <div class="bread-crumb">REINDO Reinsurance Indonesia
                        <a href="#">Portal Reindo</a>/
                        <a href="#" title="Reindo Reinsurance Indonesia" rel="category tag">Reindo Reinsurance</a>/ Article</div> -->
                    <div class="title-list-page">
                        <h3><?php echo ($menu_title) ? ucfirst($menu_title) : "Galeri foto"?></h3>
                    </div>    
                    <div class="entry">
                        <div class="list-of-gallery">
                            <div class="row">
                                <div class="list-of-gallery">
                                    <?php if($qp->num_rows() > 0) {?>
                                        <ul>
                                            <?php foreach($qp->result_array() as $qval) {?>
                                            <li class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                                <div class="square-photo">
                                                    <a href="<?php echo site_url('video/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" rel="bookmark">
                                                        <img src="<?php echo base_url()?>uploads/<?php echo $qval['image']?>" class="img-responsive" alt="<?php echo $qval['title']?>"/>
                                                    </a>                                            
                                                </div>
                                                <div class="title-gallery">
                                                    <a href="<?php echo site_url('video/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" rel="bookmark">
                                                        <h5><?php echo $qval['title']?></h5>
                                                    </a>
                                                </div>
                                            </li>
                                            <?php } ?>  
                                        </ul>
                                    <?php } ?>                                      
                                </div>                              
                            </div>
                        </div>
                    </div>
                    <!-- SOCIAL ICONS SHARE -->
                    <div class="postmetadata"></div>
                </div>            
                <!-- START PAGINATION AREA --> 
                <div class="news_pagination">
                    <ul class="news_pagi">
                        <?php echo $pagination?>
                    </ul>
                </div>     
                <!-- END PAGINATION AREA -->                   
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="home_sidebar">
                    <div class="follow_us_side">
                        <?php echo $this->load->view('detail_sidebar')?>  
                    </div>
                </div>                 
            </div>
        </div>
    </div>
</section>
<!-- END LIST VIDEO -->
