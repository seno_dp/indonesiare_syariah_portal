<!-- START PAGE LIST GALLERY -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="cc_single_post">
                        <div class="title-list-page">
                            <h3><?php echo ($menu_title) ? ucfirst($menu_title) : "Galeri foto"?></h3>
                        </div>
                        <div class="entry">
                            <div class="row">
                                <div class="list-of-gallery">
                                    <ul>
                                        <!--<li>
                                            <a href="http://reindo.co.id/foto/14/02/tu2014/index.html" rel="bookmark" target="_BLANK">
                                                <img width="180" height="140" src="<?php echo base_url()?>uploads/IMG_0673.jpg" class="attachment-blog-image wp-post-image" alt="temu underwriter 2014"/>
                                            </a>
                                            <div class="title-gallery">
                                                <a href="<?php echo site_url('http://reindo.co.id/foto/14/02/tu2014/index.html')?>" rel="bookmark" target="_BLANK">
                                                    <h5>Temu Underwriter 2014</h5>
                                                </a>
                                            </div>
                                        </li>-->
                                        <?php if($qpj->num_rows() > 0) {?>
                                        <?php foreach($qpj->result_array() as $qvalj) {?>
                                        <li class="col-md-4 col-sm-6 col-xs-12 min-225">
                                            <div class="square-photo">
                                                <a href="<?php echo $qvalj['url_link']?>" rel="bookmark" target="_BLANK">
                                                    <img src="<?php echo base_url()?>uploads/<?php echo $qvalj['image']?>" class="img-responsive" alt="<?php echo $qvalj['title']?>"/>
                                                </a>                                                
                                            </div>
                                            <div class="title-gallery">
                                                <a href="<?php echo $qvalj['url_link']?>" rel="bookmark" target="_BLANK">
                                                    <h5><?php echo $qvalj['title']?></h5>
                                                </a>
                                            </div>
                                        </li>
                                        <?php } ?> 
                                        <?php } ?> 
                                        <?php if($qp->num_rows() > 0) {?>
                                        <?php foreach($qp->result_array() as $qval) {?>
                                        <li class="col-md-4 col-sm-6 col-xs-12 square-photo">
                                            <div class="square-photo">
                                                <a href="<?php echo site_url('foto/album/'.$qval['id'].'/'.url_title($qval['title']))?>" rel="bookmark">
                                                    <img src="<?php echo base_url()?>uploads/<?php echo $qval['image']?>" class="img-responsive" alt="<?php echo $qval['title']?>"/>
                                                </a>                                                
                                            </div>
                                            <div class="title-gallery">
                                                <a href="<?php echo site_url('foto/album/'.$qval['id'].'/'.url_title($qval['title']))?>" rel="bookmark">
                                                    <h5><?php echo $qval['title']?></h5>
                                                </a>
                                            </div>
                                        </li>
                                        <?php } ?>  
									</ul>
                                    <?php } ?>
                                </div>                                
                            </div>
                        </div>                
                        <!-- START PAGINATION AREA --> 
                        <div class="news_pagination">
                            <ul class="news_pagi">
                                <?php echo $pagination?>
                            </ul>
                        </div>     
                        <!-- END PAGINATION AREA -->                         
                    </div>                     
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="home_sidebar">
                        <div class="follow_us_side">
                            <?php echo $this->load->view('detail_sidebar')?>  
                        </div>
                    </div>                       
                </div>
            </div>
        </div>
    </section>
<!-- END PAGE LIST GALLERY -->    
