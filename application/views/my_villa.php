<!--DAFTAR PENGAJUAN VILA -->
                <div class="row">
                    <?php 
                            if($member->num_rows() > 0){
                                $v = $member->row_array();
                        ?>
                        <div id="" class="">
                            <div class="cover-area-profile">
                                <?php if($v['cover']) { ?>
                                     <img class="cover_photo_admin" src="<?php echo base_url().'uploads/'.$v['cover']?>"/>
                                <?php }else{ ?>
                                    <!--<img class="cover_photo_admin" src="<?php echo base_url()?>uploads/2013/06/cover-photo-admin.jpg" />-->
                                <?php } ?>
                            </div>      
                            <div style="clear:both"></div>
                            <div class="container">
                                <div class="nametag-admin-blog">
                                    <div class="admin-photo-profile">
                                        <?php if($v['image']) { ?>
                                            <img src="<?php echo base_url().'uploads/'.getThumb($v['image'])?>" />
                                        <?php }else{ ?>
                                            <img src="<?php echo base_url().'assets/theme/images/180x180.jpg'?>" />
                                        <?php } ?>
                                    </div>
                                    <div class="fullname-admin-blog">
                                        <h2><?php echo $v['title']?></h2>
                                        <div class="admin-edit">
                                            <?php if($this->uri->segment(4) == $this->session->userdata('user_id_sess')) { ?>
                                                <a href="<?php echo site_url('member/profile')?>"><img width='24' height='24' src="<?php echo base_url()?>assets/theme/images/edit_icon.png" />Edit Profile</a>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>                                 
                            </div>       
                            <div class="container">
                                <div class="row">
                                    <!-- SIDEBAR LEFT -->
                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                        <div class="wrapper-left-profile">
                                            <div class="top-about-administrator"></div> 
                                            <div class="about-administrator">
                                                <?php if($v['content']) { ?>
                                                    <p><strong>About Me : </strong><?php echo $v['content']?></p>
                                                <?php } ?>
                                            </div>
                                            <div class="latest-activity">
												<div class="title-list-page">
													<h3>Daftar Pengajuan Villa</h3>
												</div>
                                                <?php if($ma->num_rows() > 0){?>
                                                    <ul>
                                                        <?php foreach($ma->result_array() as $mval) {?>
                                                            <li class="list_pengajuan_villa">
                                                                <h3 class="title_villa">
                                                                    <?php echo $mval['no_surat']?> - <?php echo strtoupper($mval['title'])?>
                                                                </h3>
                                                                <div class="list-of-pengajuan">
                                                                    <?php echo 'Villa : '.$mval['villa_title'];?><br/>
                                                                    <?php
                                                                        $date1 = date_create($mval['start_date']);
                                                                        $date2 = date_create($mval['end_date']);
                                                                        $interval = date_diff($date1, $date2);
                                                                    ?>
                                                                    <?php echo 'Tanggal : '.date('d M Y', strtotime($mval['start_date'])).' - '.date('d M Y', strtotime($mval['end_date'])).''?><br/>
                                                                    <?php echo 'Lama Istirahat : '.$interval->format('%a hari')?><br/>
                                                                    <?php echo ($mval['remark_approve'] != NULL) ? 'Keterangan : '.$mval['remark_approve'] : '';?>&nbsp;&nbsp;
                                                                </div>
																<div class="list-of-pengajuan-right">
																	<!--<a href="<?php echo site_url('member/pdf_villa/'.$mval['id'])?>" target="_BLANK"><img src="<?php echo base_url().'/assets/theme/images/pdfprint.png'?>"></a>-->
																	<a href="<?php echo site_url('member/pdf_villa/'.$mval['id'])?>" target="_BLANK"><i class="fa fa-print" aria-hidden="true"></i>PRINT</a>
																</div>
                                                            </li>
                                                        <?php } ?>
                                                    </ul>
                                                <?php } ?>
                                            </div>                              
                                        </div>                                          
                                    </div>
                                    <!-- SIDEBAR LEFT -->
                                    <!-- SIDEBAR RIGHT -->
                                    <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                        <div class="cc_single_post">
                                            <div class="home_sidebar">
                                                <div class="follow_us_side">
                                                    <h2>Blog Post</h2>
                                                    <?php if($this->uri->segment(4) == $this->session->userdata('user_id_sess')) { ?>
                                                            <div class="button-post-first">
                                                                <a href="<?php echo site_url('blog/post')?>"><img width="24" height="24" src="<?php echo base_url()?>assets/theme/images/post-icn.png" />Posting Blog</a>
                                                            </div>
                                                    <?php } ?>
                                                </div>  
                                                <div class="tab-container">
                                                    <div class="panel-container">
                                                        <div id="tabs-recent">
                                                            <?php if($qp->num_rows() > 0){?>
                                                                <ul>
                                                                    <?php foreach($qp->result_array() as $qval) {?>
                                                                        <li>
                                                                            <a href="<?php echo site_url('blog/detail/'.$qval['id'].'/'.url_title($qval['title']))?>"><?php echo $qval['title']?>
                                                                                <div class="mr-grey"> 
                                                                                    <i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?php echo date('d M Y', strtotime($qval['create_date']))?>
                                                                                </div>                              
                                                                            </a>
                                                                        </li>    
                                                                    <?php } ?>
                                                                </ul>
                                                            <?php } ?>    
                                                        </div>
                                                    </div>                                                     
                                                </div>                            
                                                <!-- <div class="sidebar-thread-post">
                                                        <div class="head-blog-post">
                                                            <h4>Thread Post</h4>
                                                            <a href="#"><img src="<?php echo base_url()?>uploads/2013/06/write_icon.png" />Posting Thread</a>
                                                        </div>                              
                                                    <div class="list-post-blog">
                                                        <ul>
                                                            <li><a href="#">Update : Dept Undwriting Fakultatif Marine & Aviation Reindo Reinsurance 2014</a><span class="date-list">04 Mei 2014</span></li>
                                                            <li><a href="#">Badan Pengelola Pusat Data Asuransi Nasional (BPPDAN) adalah sebagai pusat data statistik jenis asuransi kebakaran Indonesi</a><span class="date-list">04 Mei 2014</span></li>
                                                            <li><a href="#">Special Award-Most Innovative Reinsurance Company 2006</a><span class="date-list">04 Mei 2014</span></li>
                                                        </ul>
                                                    </div>
                                                </div>   -->                                
                                            </div><!-- home_sidebar -->                                         
                                        </div>
                                    </div>
                                    <!-- SIDEBAR RIGHT -->
                                </div>
                            </div>                  
                        </div>
                    <?php } ?>
                </div>
<!-- DAFTAR PENGAJUAN VILA-->