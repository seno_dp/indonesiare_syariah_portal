<!-- MY BLOG -->  
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
    <div class="latest-activity">                                                
        <h4>Blog Post</h4>                                                             
    </div>                
    <?php
        $flashmessage = $this->session->flashdata('message');
        if($flashmessage)
        {
        ?>
            <p>
                <?php echo $flashmessage;?>
            </p>
        <?php
        }
    ?>
    <?php if($qp->num_rows() > 0){?>
        <?php foreach($qp->result_array() as $qval) {?>
            <div class="post-<?php echo $qval['id']?> list-of-article">
                <div class="excerptarhive">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-12">
                            <div class="thumb-arhive-book">
                                    <?php if($qval['image']) { ?>
                                    <div class="thumb-arhive">
                                        <a href="<?php echo site_url('blog/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" rel="bookmark">
                                            <img class="img-responsive" src="<?php echo base_url()?>uploads/<?php echo $qval['image']?>" class="attachment-blog-image wp-post-image" alt="<?php echo $qval['title']?>"/>
                                        </a>
                                    </div>
                                    <?php } ?>
                            </div>
                        </div>
                        <div class="col-lg-9">
                            <div class="row">
                                <div class="list-of-article">
                                    <h4 id="post-<?php echo $qval['id']?>">
                                        <a href="<?php echo site_url('blog/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" title="<?php echo $qval['title']?>">
                                            <?php echo $qval['title']?></a>
                                    </h4> 
                                    <div class="excerptarhive"><?php echo $qval['headline']?></div>
                                    <div class="postmetadatablog-comment">
                                        <div class="post-meta-blog">
                                            <ul>
                                                <li><a href=""><i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?php echo date('M d, Y',strtotime($qval['create_date']))?></a></li>
                                                <li><a href="">by</a></li>
                                                <li><i class="fa fa-user"></i>&nbsp;&nbsp;admin | <a href="<?php echo site_url('blog/edit/'.$qval['id'])?>">Edit</a>&nbsp;|&nbsp;<a href="<?php echo site_url('blog/delete/'.$qval['id'])?>">Delete</a></li>
                                            </ul>
                                        </div>    
                                        <div class="post-meta-blog">
                                            <ul>
                                                <li>
                                                    <i class="fa fa-folder"></i>&nbsp;&nbsp;Filed Under:&nbsp;&nbsp;
                                                    <a href="#" title="" rel="category tag"><?php echo $qval['category']?></a>
                                                </li>
                                                <li>
                                                    <a href=""><i class="fa fa-tag"></i>Tags:&nbsp;&nbsp;</a>
                                                    <?php echo explodetags($qval['tags']) ?>
                                                    <?php 
                                                    $filtercomment = array("is_publish"=>"where/publish","id_blog"=>"where/".$qval['id'],"id"=>"order/desc");
                                                    $comment = GetAll('kg_view_comment',$filtercomment);
                                                    if($comment->num_rows() > 0) { 
                                                        $num_comment = $comment->num_rows()." comments";
                                                    }else{
                                                        $num_comment = "No comment yet";
                                                    }
                                                    ?>
                                                </li>
                                                <li><i class="fa fa-comment"></i>&nbsp;&nbsp;<a href="#" title="Comment on <?php echo $qval['title']?>"><?php echo $num_comment;?></a></li>
                                            </ul>
                                        </div>                             
                                    </div>                                      
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    <?php }else{ ?>
    <div class="list-of-article">
        <div class="col-lg-3 col-md-3 col-sm-12">
            <div class="thumb-arhive-book">
                <a href="#" rel="bookmark">
                    <img src="<?php echo base_url()?>uploads/2013/06/listimage.jpg" class="attachment-blog-image wp-post-image" alt="2905135770_c3c86b41d9_z" width="150px" />
                </a>
            </div>
        </div> 
        <div class="col-lg-9 col-md-9- col-sm-12">
            <div class="row">
                <div class="list-of-article">
                    <h4>
                        <a href="#" title="Reindo Reinsurance">Silakan posting blog</a>
                    </h4> 
                    <div class="excerptarhive">Silakan posting blog ...</div>
					<div class="postmetadatablogroll">
						<div class="post-meta-blog">
							<ul>
								<li><a href=""><i class="fa fa-clock-o"></i>Apr 04, 2017</a></li>
								<li><a href="">by</a></li>
								<li><a href=""><i class="fa fa-user"></i>admin</a></li>
							</ul>
						</div>
						<div class="post-meta-blog">
							<ul>
								<li><i class="fa fa-folder"></i>&nbsp;&nbsp;Filed Under:
									<a href="#" title="" rel="category tag">&nbsp;&nbsp;reindo</a> </li>
								<li>
									<a href=""> <i class="fa fa-tag"></i>Tags:</a>
									<a href="http://localhost/reindo_intranet/id/searching/tags/kerja">admin</a>
								</li>
								<li> <i class="fa fa-comment"></i>&nbsp;<a href="" class="">No comment yet</a>
								</li>
							</ul>
						</div>
					</div>					
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    
    <div class="navigation flex_66">
        <div class="pagination">
            <?php echo $pagination?>
        </div>
    </div>    
</div>

<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
    <div id="sidebar">
        <!-- begin widget sidebar -->
          <script type="text/javascript">
            $(document).ready( function() {
              $('#tab-container').easytabs();
            });
          </script>
    
            <div id="tab-container" class='tab-container'>
                <ul class='etabs'>
                    <?php if($recent->num_rows() > 0 ) {?>
                    <li class='tab'>
                        <a href="#tabs-recent">Recent</a>
                    </li>
                    <?php } ?>
                    <?php if($popular->num_rows() > 0 ) {?>
                    <li class='tab'>
                        <a href="#tabs-popular">Popular</a>
                    </li>
                    <?php } ?>
                    <?php if($mostcomm->num_rows() > 0 ) {?>
                    <li class='tab'>
                        <a href="#tabs-comment">Comments</a>
                    </li>
                    <?php } ?>
                    <!-- <li class='tab'>
                        <a href="#tabs-comment">Comments</a>
                    </li> -->
                </ul>
                <div class='panel-container'>
                    <?php if($recent->num_rows() > 0 ) { ?>
                    <div id="tabs-recent">
                        <ul>
                            <?php foreach ($recent->result_array() as $valrec) {?>
                                <li><a href="<?php echo site_url('blog/detail/'.$valrec['id'].'/'.url_title($valrec['title']))?>"><?php echo $valrec['title']?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php } ?>
                    <?php if($popular->num_rows() > 0 ) { ?>
                    <div id="tabs-popular">
                        <ul>
                            <?php foreach ($popular->result_array() as $valpop) {?>
                                <li><a href="<?php echo site_url('blog/detail/'.$valpop['id'].'/'.url_title($valpop['title']))?>"><?php echo $valpop['title']?></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php } ?>
                    <?php if($mostcomm->num_rows() > 0 ) { ?>
                    <div id="tabs-comment">
                        <ul>
                            <?php foreach ($mostcomm->result_array() as $valcomm) {?>
                                <li>
                                    <div class="commenters">
                                        <img src="<?php echo base_url()?>uploads/<?php echo getThumb($valcomm['image_member'])?>" style="width: 41px; "/>
                                    </div>
                                    <a href="<?php echo site_url('blog/detail/'.$valcomm['id_blog'].'/'.url_title($valcomm['title_blog']))?>"><?php echo $valcomm['title_blog']?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php } ?>
                </div>
            </div>
    </div>
</div>
            <div style="clear: both"></div>
<!-- MY BLOG -->            