
<div class="top-nav">
    <div class="menu-top-container">
        <div class="adsheader">
            <!--SEARCH BLOCK -->
            <div class="search-block">
                <form method="get" id="searchform" action="#">
                    <input class="search-button" type="submit" value="" />
                    <input type="text" id="s" name="s" value="Search..." onfocus="if (this.value == 'Search...') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Search...';}" />
                </form>
            </div>
        </div>
    <?php 
    if($this->session->userdata('user_id_sess')) {
        $id = $this->session->userdata('user_id_sess');
        $filter = array(
                "id"=>"where/".$id,
            );

        $member = GetAll('kg_member',$filter);
        if($member->num_rows() > 0){
        $v = $member->row_array();
    ?>
    <div class="admin-thumb">
        <div  class="left-text">
            <p><a href="<?php echo site_url('blog/my_profile')?>"><?php echo $v['title']?></a></p>
            <span><a href="<?php echo site_url('member/logout')?>">Log Out</a> </span>           
        </div>
        <div class="thumbnail-ava">
            <img src="<?php echo base_url().'uploads/'.getThumb($v['image'])?>" style="width:100%"/>
        </div>
    </div>
    <?php } }else{ ?>
    <div class="admin-thumb">
        <div  class="left-text">
            <a href="<?php echo site_url('member/login')?>">Login</a> 
            <span><a href="<?php echo site_url('member/register')?>"> | Register</a></span>           
        </div>
        
    </div>


    <?php } ?>

    </div>
</div>
<div id="header" class="flex_100">
    <!-- LOGO -->
    <div class="logo">
        <a href="<?php echo site_url('home')?>">
            <img src="<?php echo base_url()?>assets/theme/images/logo.png" alt="Reindo Reinsurance" border="0" />
        </a>
    </div>
    

</div>
