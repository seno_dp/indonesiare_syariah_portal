<!--<div id="footer-border"></div>-->
            <!-- DYNAMIC SIDEBAR -->
<!--
            <div class="flex_33_1">
                <div class="widget-footer">Kontak
                    <span class="arrows">&raquo;</span>
                </div>
                <div class="social-profile-icons">
                    <div class="textwidget">Jl. Salemba Raya No 30 Jakarta Pusat 10430 INDONESIA
                        <br>
                        <br>Kontak
                        <br/>Telp: 62-21 3920101,334208 Fax: 62-21 3143828
                        <br/>Email : cosecretary@reindo.co.id
                        <br/>
                    </div>
                </div>
            </div>
-->

<!--
            <div class="flex_33_1">
                <div class="widget-footer">
                </div>
                
            </div>
-->
            <!-- TRACKING CODE -->
            <!-- COPYRIGHT CODE -->
<!--
            <div class="copyrigt">
                <span class="right-copy">Copyright&copy; 2013 - PT Reasuransi Internasional Indonesia - Alright reserved</span>
            </div>
-->
<footer class="footer_area">
  <div class="footer_bottom">
    <div class="container">
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="footer_bottom_box">
            <div class="lf-col">
              <p> Copyright&copy; 2017 - PT Reasuransi Internasional Indonesia - Alright reserved</p>
            </div>
            <div class="rf-col">
                <ul>
                    <li>
                        <a href="https://web.facebook.com/PT-Reasuransi-Indonesia-Utama-Persero-1226629397355581/?ref=page_internal" target="_BLANK"><img src="<?php echo base_url() ?>assets/theme/images/fb-icon.png"></a>
                    </li>
                    <li>
                        <a href="https://twitter.com/indonesia_re" target="_BLANK"><img src="<?php echo base_url() ?>assets/theme/images/twitter-icon.png"></a>
                    </li>
                    <li>
                        <a href="https://www.youtube.com/channel/UCvUvsrVFUWJizR5V1E2fcuw" target="_BLANK"><img src="<?php echo base_url() ?>assets/theme/images/youtube-icon.png"></a>
                    </li>
                    <!--<li>                            <a href="#" target="_BLANK"><img src="http://www.indonesiare.co.id/assets/theme/img/instagram-icon.png" /></a>                        </li>-->
                    <li>
                        <a href="https://linkedin.com/company/pt.-reasuransi-indonesia-utama-persero-" target="_BLANK"><img src="<?php echo base_url() ?>assets/theme/images/linkedin-icn.png"></a>
                    </li>
                </ul>
            </div>            
          </div>
        </div>
      </div>
    </div>
  </div>
</footer>

