<?php 
    $image_field = ($controller_name == "article") ? 'thumbnail' : 'image';
?>
<!-- KRITERION ARCHIVE -->
<div class="flex_66">
    <div class="box">
        <div class="listarticle-title">
            <b><?php echo ($menu_title) ? ucfirst($menu_title) : "Article"?>
                <span class="arrows">&raquo;</span>
            </b>
        </div>
		<div id="pengaduan" class="flex_100">
            <p>
				Halaman ini disediakan untuk sebanyak-banyaknya menampung masukan dari semua karyawan ReINDO
				demi transparansi dan peningkatan kinerja perusahaan 
			</p>
			
			<form method="POST" action="<?php echo base_url();?>id/pengaduan/main">
				<div class="flex_50">
					<label>Nama</label>
					<input type="text" name="name" value="<?php echo set_value('name');?>">
				</div>
				<div class="flex_50">
					<label>Divisi</label>
					<?php echo form_dropdown('divisi', $opt_divisi, set_value('divisi'));?>
				</div>
				<div class="clear"></div>
				
				<p class="small">
					*Anda bisa mengosongkan nama dan divisi jika ingin memberikan laporan secara anonim</p>
				
				<div class="flex_100">
					<label>Perihal</label>
					<?php echo form_dropdown('perihal', $opt_ref_perihal, set_value('perihal'));?>
					<?php echo form_error('perihal');?>
				</div>
				
				<div class="flex_100">
					<label>Isi Aduan</label>
					<textarea name="isi_aduan"><?php echo set_value('isi_aduan');?></textarea>
					<?php echo form_error('isi_aduan');?>
				</div>
				
				<div class="flex_33">
					<button type="submit" value="submit">Kirim</button>
				</div>
				<div class="flex_66">
					<h4 class="info_pengaduan"><?php echo $info;?></h4>
				</div>
			</form>
			
		</div>
       
        <div class="clear"></div>
    </div>
    
</div>
<div class="flex_33">
    <div id="sidebar">
        <?php echo $this->load->view('detail_sidebar')?>
    </div>
</div>
<div style="clear: both"></div>
<?php if($controller_name == 'kliping') { ?>
<script>
 jQuery(document).ready(function() {
    
            var config = {
              '.chzn-select'           : {},
              '.chzn-select-deselect'  : {allow_single_deselect:true},
              '.chzn-select-no-single' : {disable_search_threshold:10},
              '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
              '.chzn-select-width'     : {width:"100%"}
            }

            for (var selector in config) {
              jQuery(selector).chosen(config[selector]);
            }

            //jQuery("#datepicker").datepicker();

            jQuery("#submit_cari_kliping").click(function(){
                alert(jQuery("#date_kliping").val());
                    //alert($('input[name="totalsemua"]').val());
                        var data = {
                            id_koran:jQuery("#id_koran").val(), 
                            date_kliping:jQuery("#date_kliping").val(), 
                            text_kliping:jQuery("#text_kliping").val() 
                        };
                        //$("#shipping").hide();
                        jQuery.ajax({
                                type: "POST",
                                url : "<?php echo site_url('kliping/search')?>",
                                data: data,
                                success: function(msg){

                                    //var variable_returned_from_php = msg.biaya_kirim;
                                    //$("#shipping").show();
                                    //jQuery("#comment").val(''); 
                                    jQuery("#search_kliping_result").html(msg);
                                },
                                error: function(){
                                    alert('failure');
                                }
                            });                         
                }); 

        }); 
</script>
<?php } ?>