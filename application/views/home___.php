 <!-- ~~~=| Banner START |=~~~ -->
<section class="hp_banner_area section_padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="hp_banner_box">
                    <div class="hp_banner_left">
                        <div class="bl_single_news">
                            <img src="<?php echo base_url()?>uploads/blt-banner1.jpg" alt="Banner" />
                            <div class="bl_single_text">
                                <a href="blog-single-slider-post.html">
                                    <h4>20 Myths About Mobile Devices</h4>
                                </a> <span><i class="fa fa-clock-o"></i> 3 mins ago</span>
                            </div>
                        </div>
                        <div class="bl_single_news"> 
                            <img src="<?php echo base_url()?>uploads/blt-banner2.jpg" alt="Banner" />
                            <div class="bl_single_text">
                                <a href="blog-single-slider-post.html">
                                    <h4>What Do Need To Make a Business ?</h4>
                                </a> <span><i class="fa fa-clock-o"></i> 4 hours ago</span>
                            </div>
                        </div>
                        <div class="bl_single_news">
                            <img src="<?php echo base_url()?>uploads/bann2.jpg" alt="" />
                            <div class="bl_single_text">
                                <a href="blog-single-slider-post.html">
                                    <h4>The Ultimate Cheat Sheet On Ecology</h4>
                                </a> <span><i class="fa fa-clock-o"></i> 5 hours ago</span>
                            </div>
                        </div>
                    </div>
                    <div class="hp_banner_right">
                        <div class="br_single_news">
                            <img src="<?php echo base_url()?>uploads/brt-banner1.jpg" alt="" />
                            <div class="br_single_text"> <span class="green_hp_span">Photo</span>
                                <a href="blog-single-slider-post.html">
                                    <h4>The Ultimate Cheat Sheet On Ecology</h4>
                                </a>
                            </div>
                            <div class="br_cam">
                                <a href="" class="fa fa-camera"></a>
                            </div>
                        </div>
                        <div class="br_single_news">
                            <img src="<?php echo base_url()?>uploads/brt-banner2.jpg" alt="" />
                            <div class="br_single_text">
                                <span class="blue_hp_span">Video</span>
                                <a href="blog-single-slider-post.html">
                                    <h4>Notes from a vacation photographer</h4>
                                </a>
                            </div>
                            <div class="br_cam">
                                <a href="" class="fa fa-camera"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ~~~=| Banner END |=~~~ -->

<!-- ~~~=| GRID2 START |=~~~ -->
<section class="main_news_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">  
                <!--START HOME_SIDEBAR-->
                <div class="home_sidebar">
                    <div class="all_news_right"><!-- all_news_right EVENT -->  
                        <div class="follow_us_side">
                            <h2>Reindo Event</h2>
                        </div>              
                        <!-- START REINDO EVENT -->   
                        <?php 
                        $i=0;
                        if($event->num_rows() > 0) { 
                            foreach($event->result_array() as $eval) { 
                             $i = ++$i; 
                            if($i == 1){
                        ?>
                        <div class="">
                            <div class="single_fs_news_left_text">
                                <div class="fs_news_left_img">
                                    <a href="<?php echo site_url('event/detail/'.$eval['id'].'/'.url_title($eval['title']))?>" rel="bookmark">
                                        <img width="285" src="<?php echo base_url()?>uploads/<?php echo getThumb($eval['image'])?>" alt="<?php echo $eval['title']?>"/>
                                    </a>
                                </div>                                  
                            </div>                          
                        </div>
                        <?php } ?>
                        <div class="fs_news_right">
                            <div class="single_fs_news_right_text"><!-- super-item -->
                            <!-- title event  -->
                                <h4><a href="<?php echo site_url('event/detail/'.$eval['id'].'/'.url_title($eval['title']))?>" rel="bookmark"><?php echo $eval['title']?></a></h4>
                            <!-- title event -->
                            <!-- intro-event -->
                                <div class="<?php echo ($i == 2) ? 'intro-event' : 'intro' ?>">
                                    <p><?php echo character_limiter($eval['headline'],190)?></p>
                                </div>
                            <!-- intro-event -->   
                            <!-- Readmore -->    
                                <p><a class="gad_color" href="<?php echo site_url('event/detail/'.$eval['id'].'/'.url_title($eval['title']))?>">Readmore </a></p>
                            <!-- Readmore -->   
                                <?php if($i == 2){?>
                                <a href="<?php echo site_url('event')?>" class="more-items">More Info »</a>
                                                    <?php } ?>
                            <!-- date-post -->     
                                <p><i class="fa fa-clock-o"></i>&nbsp;<?php echo date('M d, Y',strtotime($eval['date_event']))?></p>
                            <!-- date-post -->    
                            </div><!-- super-item -->                            
                        </div>
                        <?php } ?>
                        <?php }else{ ?>
                        <div class="">
                            <div class="single_fs_news_left_text">
                                <div class="fs_news_left_img">
                                    <a href="#" rel="bookmark">
                                        <img width="285" height="120" src="<?php echo base_url()?>uploads/2013/06/Event.jpg" class="attachment-feat-thumb wp-post-image" alt="pool-103014_640" width="285px" />
                                    </a>                                    
                                </div>
                            </div>                            
                        </div>
                        <div class="fs_news_right">
                            <div class="single_fs_news_right_text"><!-- super-item -->
                            <!-- title-bookmark -->
                                <h4><a href="blog-single-slider-post.html">Internet trolls take pleasure in making you suffer</a></h4>
                            <!-- title-bookmark -->
                            <!-- intro-event -->
                                <div class="intro-of-event">
                                    <p>Fusce aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique ut, consectetur non erat.</p>
                                </div>
                            <!-- intro-event -->   
                            <!-- Readmore -->    
                                <p><a class="gad_color" href="">Readmore </a></p>
                            <!-- Readmore -->
                            <!-- date-post -->    
                                <p>| <i class="fa fa-clock-o"></i> 1 hour ago </p>
                            <!-- date-post -->    
                            </div><!-- super-item -->
                        </div>
                        <div class="fs_news_right">
                            <div class="single_fs_news_right_text"><!-- super-item -->
                            <!-- title-bookmark -->
                                <h4><a href="blog-single-slider-post.html">Internet trolls take pleasure in making you suffer</a></h4>
                            <!-- title-bookmark -->
                            <!-- intro-event -->
                                <div class="intro-event">
                                    <p>Fusce aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique ut, consectetur non erat.</p>
                                </div>
                            <!-- intro-event -->   
                            <!-- Readmore -->    
                                <p><a class="gad_color" href="">Readmore </a></p>
                            <!-- Readmore -->
                            <!-- date-post -->  
                            <a href="#">More Info »</a> 
                                <p><i class="fa fa-clock-o"></i> 1 hour ago </p>
                            <!-- date-post -->    
                            </div><!-- super-item -->
                        </div>
                        <?php } ?> 
                        <!-- END REINDO EVENT -->                                       
                    </div><!-- all_news_right EVENT -->                                  
                </div><!--END HOME_SIDEBAR-->    
                <div class="home_sidebar"><!--START HOME_SIDEBAR-->
                    <!-- START BLOG ITEM -->            
                    <div class="all_news_right"><!-- all_news_right EVENT -->
                            <div class="follow_us_side">
                                <h2>Blog Roll</h2>
                            </div>              
                            <div class="blog-of-item">
                                <?php if($blog->num_rows() > 0) { ?>
                                <ul>
                                <?php foreach ($blog->result_array() as $bval) { ?>
                                        <li>
                                            <a href="<?php echo site_url('blog/detail/'.$bval['id'].'/'.url_title($bval['title']))?>"><?php echo word_limiter($bval['title'],30)?></a>
                                        </li>
                                <?php } ?>
                                </ul>
                                    <a href="<?php echo site_url('blog')?>" class="more-items">More Info »</a>
                                <?php }else{ ?>
                                <ul>
                                    <li>
                                        <a href="#">aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat.</a>
                                    </li>
                                    <li>
                                        <a href="#">Half Day aliquet dolor sed dolor feugiat fermentum</a>
                                    </li>
                                    <li>
                                        <a href="#">Donec dignissim tellus non ante volutpat Meeting Reindo</a>
                                    </li>
                                </ul>
                                <a href="#" class="more-items">More Info »</a>
                                <?php } ?>
                            </div>
                        <!-- END BLOG ITEM -->               
                    </div><!-- all_news_right EVENT -->                     
                </div><!--END HOME_SIDEBAR-->      

            </div><!-- END COL 4 -->
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="home_sidebar">
                    <div class="all_news_right">
                        <div class="follow_us_side">
                            <h2>Latest Update</h2>
                        </div>    
                        <?php if($article->num_rows() > 0) { ?>
                        <?php foreach($article->result_array() as $arval) {?>
                        <?php if($arval['image']) {?>   
                        <!-- IMAGE LATEST --> 
                        <div class="">
                            <div class="single_fs_news_left_text">
                                <div class="fs_news_left_img">
                                    <a href="<?php echo site_url($arval['module_detail'].$arval['id'].'/'.url_title($arval['title']))?>" rel="bookmark">
                                        <!-- <img width="300" src="<?php echo base_url()?>uploads/<?php echo getThumb($arval['image'])?>" alt="<?php echo $arval['title']?>"/>-->
                                        <img width="300" src="http://localhost/reindo_intranet/uploads/20161108135510.POSTER_final_thumb.jpg" alt="<?php echo $arval['title']?>"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                                <div class="fs_news_right">
                                    <div class="single_fs_news_right_text"><!-- super-item -->
                                        <h4><a href="<?php echo site_url($arval['module_detail'].$arval['id'].'/'.url_title($arval['title']))?>" rel="bookmark"><?php echo $arval['title']?></a></h4>
                                        <div class="intro">
                                            <p><?php echo character_limiter($arval['headline'],190)?></p>
                                        </div>  
                                        <p><a class="gad_color" href="<?php echo site_url($arval['module_detail'].$arval['id'].'/'.url_title($arval['title']))?>" rel="bookmark" title="Read more">Readmore </a></p> 
                                    <!-- date-post -->     
                                        <p><i class="fa fa-clock-o"></i>&nbsp;<?php echo date('M d, Y',strtotime($arval['create_date']))?></p>
                                    <!-- date-post -->    
                                    </div>                          
                                </div>                                 
                        <?php } ?>
                        <?php } ?>
                        <?php } ?>
                        <!-- IMAGE LATEST -->               
                    </div>                    
                </div>
            </div><!-- END COL 4 -->
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="home_sidebar">
                    <div class="all_news_right">
                        <div class="follow_us_side">
                            <h2>Pengumuman</h2>
                        </div>
                        <div class="blog-of-item">
                            <ul>
                                <?php if($announcement->num_rows() > 0) { ?>
                                <?php foreach($announcement->result_array() as $anval){ ?>
                                    <li>
                                    <h4><?php echo $anval['title']?></h4>
                                    <p><?php echo ((strlen($anval['headline']) == 0) || ($anval['headline'] == 'NULL')) ? $anval['title'] : $anval['headline']?>
                                        <span>
                                            <a href="<?php echo site_url('announcement/detail/'.$anval['id'].'/'.url_title($anval['title']))?>" title="Read more">...read more</a>
                                        </span>
                                    </p>
                                    <div class="announcement-time"><i class="fa fa-clock-o"></i>&nbsp;<?php echo date('M d, Y',strtotime($anval['date_announcement']))?></div>
                                </li>
                                <?php } ?>
                                <?php }else{ ?>
                                <li>
                                    <h4>Title dummmy here</h4>
                                    <p>Fusce aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique
                                        <span><a href="#" title="Read more">...read more</a></span>
                                    </p>
                                </li>
                                <li>
                                    <h4>Title dummmy here</h4>
                                    <p>Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique ut, consectetur non erat. Donec dignissim tellus
                                        <span><a href="#" title="Read more">...read more</a></span>
                                    </p>
                                </li>
                                <?php } ?>                   
                            </ul>                            
                        </div>                             
                    </div>                  
                </div>
                <!--NEXT EVENT-->
                <div class="home_sidebar">
                    <div class="all_news_right">
                        <div class="follow_us_side">
                            <h2>Reindo Next Event</h2>
                        </div>
                    <div class="calender-box-list">
                        <ul>
                            <?php if($calendar->num_rows() > 0) {?>
                            <?php foreach($calendar->result_array() as $cval){?>
                            <li>
                                <div class="fs_news_right v-middle-calendar">                   
                                    <span class="date-box-blues"><?php echo strtoupper(date('M d',strtotime($cval['date_calendar'])))?></span>                        
                                    <p><a href="<?php echo site_url('calendar/detail/'.$cval['id'].'/'.url_title($cval['title']))?>"><?php echo character_limiter($cval['title'],50)?></a></p>                
                                </div>                                   
                            </li>
                            <?php } ?>
                            <?php }else{ ?>
                            <li>
                                <div class="fs_news_right v-middle-calendar">                   
                                    <span class="date-box-blues">DES 21</span>                        
                                    <p><a href="#">Title Here</a></p>                
                                </div>                            
                            </li>
                            <li>
                                <div class="fs_news_right v-middle-calendar">                   
                                    <span class="date-box-blues">DES 21</span>                        
                                    <p><a href="#">Title Here</a></p>                
                                </div>                            
                            </li>
                            <li>
                                <div class="fs_news_right v-middle-calendar">                   
                                    <span class="date-box-blues">DES 21</span>                        
                                    <p><a href="#">Title Here</a></p>                
                                </div>                            
                            </li>
                            <?php }?>
                        </ul>
                        <a href="<?php echo site_url('calendar')?>" class="more-items">More Info »</a>
                    </div>                                                  
                    </div>                  
                </div>
                <!--NEXT EVENT-->
                <!--MEETING ROOM-->
                <div class="home_sidebar">
                    <div class="all_news_right">
                        <div class="follow_us_side">
                            <h2>Ruang Meeting</h2>
                        </div>
                        <div class="meeting-box">
                            <ul>
                                <?php if($meeting->num_rows() > 0) {?>
                                    <?php foreach($meeting->result_array() as $mval) {?>
                                        <li>
                                            <div class="date-box">
                                                <h4><?php echo strtoupper(date('M d',strtotime($mval['date_meeting'])))?></h4>
                                            </div>
                                            <div class="meeting-info">
                                                <strong>Ruang <?php echo $mval['title']?> | <?php echo $mval['time_meeting']?></strong><br/>
                                                <span></span><?php echo $mval['headline']?>
                                            </div>
                                            <div style="clear: both"></div>
                                        </li>
                                    <?php } ?>
                                <?php }else{ ?>
                                <li>
                                <div class="fs_news_right v-middle-calendar">                   
                                    <span class="date-box-blues">MM DD</span>                        
                                    <p><a href="#"><strong>Belum ada jadwal meeting</strong> | 00:00</a>
                                    <br/><span>Dummy text for content</span>
                                    </p>
                                </div> 
                                    <div style="clear: both"></div>
                                </li>
                                <?php } ?>
                            </ul>                            
                        </div>
                    </div>
                </div>                    
                <!--MKEETING ROOM-->
                <!--ABSENSI-->
                <div class="home_sidebar">
                    <div class="all_news_right">
                        <div class="follow_us_side">
                            <h2>Absensi Karyawan</h2>
                        </div>
                        <div class="absensi-list">
                            <ul>
                                <?php if($absence->num_rows() > 0) { ?>
                                    <?php foreach($absence->result_array() as $abval) {?>
                                        
                                        <li>
                                            <!-- <a href="<?php echo site_url('absence/detail/'.$abval['id'].'/'.url_title($abval['title']))?>"><?php echo $abval['title']?></a> -->
                                            <a href="<?php echo site_url('absence/download/'.$abval['id'].'/'.url_title($abval['title']))?>" title="Download <?php echo $abval['title']?>"><?php echo $abval['title']?></a>
                                        </li>
                                        
                                    <?php } ?>
                                <?php }else{ ?>
                                <li>
                                    <a href="#"><img src="<?php echo base_url()?>assets/theme/images/employe-icon.png" alt="employe absence">Title absence here</a>
                                </li>
                                <?php } ?>
                            </ul>
                            <a href="<?php echo site_url('absence')?>" class="more-items">More Info »</a>
                        </div>                        
                    </div>                        
                </div>
                <!--ABSENSI-->
                <!--VOTE-->
                <div class="home_sidebar">
                    <div class="all_news_right">
                        <?php 
                            if($vote->num_rows() > 0) {
                                $voting = $vote->row_array();
                        ?>
                        <form id="vote_form" action="<?php echo site_url('home/vote')?>" method="POST">
                            <div class="follow_us_side">
                                <h2>Berikan Masukan Anda</h2>
                            </div>       
                            <p><?php echo $voting['content']; ?></p>    
                            <div class="custome-vertical-rb">
                                <div class="rb-vote">
                                    <input type="radio" name="answerit" value='option1' id="test1">
                                    <label for="test1"><?php echo $voting['option1']?></label>        
                                </div>
                            </div> 

                            <div class="custome-vertical-rb">
                                <div class="rb-vote">
                                    <input type="radio" id="test2" name="answerit" value='option1' id="test2">
                                    <label for="test2"><?php echo $voting['option2']?></label>        
                                </div>
                            </div> 

                            <div class="custome-vertical-rb">
                                <div class="rb-vote">
                                    <input type="radio" name="answerit" value='option3' id="test3">
                                    <label for="test3"><?php echo $voting['option3']?></label>        
                                </div>
                            </div> 

                            <div class="custome-vertical-rb">
                                <div class="rb-vote">
                                    <input type="radio" id="test4" name="answerit" value='option4' id="test4">
                                    <label for="test4"><?php echo $voting['option4']?></label>        
                                </div>
                            </div> 

                            <div class="wrap-btn comments_form">
                                <?php
                                $filterpollingmember = array("id_polling"=>"where/".$voting['id'],"id_member"=>"where/".$this->session->userdata('user_id_sess'));
                                $pollingmember = getAll('kg_polling_member',$filterpollingmember);
                                if($pollingmember->num_rows() > 0){
                                    echo "<a href='".site_url('vote/detail/'.$voting['id'].'/'.url_title($voting['title']))."' class='btn-default view_polling'>View voting</a>";
                                }
                                ?>
                                <button class="btn-default-vote" type="button">Batal</button>
                                <button class="btn-default-vote" type="submit">Submit</button>
                                <input type="hidden" name="id_polling" value="<?php echo $voting['id'];?>">
                                <input type="hidden" name="title" value="<?php echo $voting['title'];?>">
                            </div>                            
                                 
                        <!-- <div class="vote-item">
                            <div class="follow_us_side">
                                <h2>Berikan Masukan Anda</h2>
                            </div>
                            <p><?php echo $voting['content']; ?></p>
                            
                            <span class="optional">
                                <input type="radio" name="answerit" value='option1'><?php echo $voting['option1']?>
                            </span>
                            <span class="optional">
                                <input type="radio" name="answerit" value='option2'><?php echo $voting['option2']?>
                            </span>
                            <?php 
                                if($voting['option3']){
                                    echo "<span class='optional'>";
                                    echo "<input type='radio' name='answerit' value='option3'>".$voting['option3'];
                                    echo "</span>";
                                }
                            ?>
                            
                             <?php 
                                if($voting['option3']){
                                    echo "<span class='optional'>";
                                    echo "<input type='radio' name='answerit' value='option4'>".$voting['option4'];
                                    echo "</span>";
                                }
                            ?>
                            <div class="wrap-btn">
                                <?php
                                $filterpollingmember = array("id_polling"=>"where/".$voting['id'],"id_member"=>"where/".$this->session->userdata('user_id_sess'));
                                $pollingmember = getAll('kg_polling_member',$filterpollingmember);
                                if($pollingmember->num_rows() > 0){
                                    echo "<a href='".site_url('vote/detail/'.$voting['id'].'/'.url_title($voting['title']))."' class='btn-default view_polling'>View voting</a>";
                                }
                                ?>
                                <button class="btn-default" type="button">Batal</button>
                                <button class="btn-default" type="submit">Submit</button>
                                <input type="hidden" name="id_polling" value="<?php echo $voting['id'];?>">
                                <input type="hidden" name="title" value="<?php echo $voting['title'];?>">
                            </div>
                            <div style="clear:both"></div>
                        </div> -->
                        </form>
                        <?php } ?>                   
                    </div>
                </div> 
                <!--VOTE-->   
                <div class="home_sidebar">
                    <div class="all_news_right">
                        <!-- EMPLOYEE SELF-SERVICE -->
                        <div class="self-service">
                            <h3><span class="h3-orange">EMPLOYEE</span> SELF-SERVICE</h3>
                            <ul>
                                <li><a data-toggle="modal" href="#vila-form-mdl" class="">Booking Vila</a></li>
                                <li><a class="opencar" data-toggle="modal" href="#car-form-mdl">Peminjaman Mobil Perusahaan</a></li>
                                <li><a href="<?php echo base_url();?>id/pengaduan">Pengaduan</a></li>
                            </ul>
                        </div>                        
                    </div>  
                </div>                                  
            </div><!-- END COL 4 -->                        
        </div>
    </div>  
</section>
<!-- ~~~=| GRID2 END |=~~~ --> 

<!--  BOOKING VILA -->
<div class="modal" id="vila-form-mdl">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Booking Villa</h4>
        </div>
        <div class="modal-body">
            <?php   
                if($this->session->userdata('user_id_sess')) {
                $id = $this->session->userdata('user_id_sess');
                $filter = array(
                        "id"=>"where/".$id,
                    );
                $member = GetAll('kg_member',$filter);
                if($member->num_rows() > 0){
                $v = $member->row_array();
            ?>
            <div class="comments_form">
                <div class="contact_cc_name">
                    <form class="rz-form2" method="post" action="<?php echo base_url();?>id/home/booking_villa">
                        <input type="hidden" name="member" value="<?php echo $v['id'];?>">
                        <input type="hidden" name="session_villa_id" value="<?php echo getLastSessionVilla();?>">
                <?php }} ?>
                        <div class="info-villa">
                            *Mohon maaf, pengajuan tanggal booking villa hanya dapat dilakukan 60 hari dari tanggal sekarang !
                        </div>
                        <div class="form-rent-car" style="margin-right:6px">
                            <label>Check In</label>
                            <input type="text" class="tgl-villa datebook" name="start_date" placeholder="---- / -- / --    --:--" readonly="readonly" required>
                        </div>
                        <div class="form-rent-car">
                        <label>Check Out</label>
                        <input type="text" class="tgl-villa datebook" name="end_date" placeholder="---- / -- / --    --:--" readonly="readonly" required>
                        </div>
                        
                        <label>Tipe Villa</label>
                        <?php echo form_dropdown('tipe_villa', $opt_villa, set_value('tipe_villa'), 'required');?>
                        
                        <label>Keperluan / Acara</label>
                        <input type="text" name="title" value="<?php echo set_value('title');?>" required>

                        <label>Jumlah Peserta</label>
                        <input type="text" name="jumlah" value="<?php echo set_value('jumlah');?>" required>
                        
                        <div class="submit-rent-car">
                            <button class="btn-sbmt" type="submit">Pesan Sekarang</button>
                        </div>                        
                    </form>                    
                </div>                
            </div>
        </div>
      </div>
    </div>
</div>
<!--  BOOKING VILA -->

<!-- RENT CAR  -->
<div class="modal" id="car-form-mdl">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Peminjaman Mobil</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <?php   
                        if($this->session->userdata('user_id_sess')) {
                        $id = $this->session->userdata('user_id_sess');
                        $filter = array(
                                "id"=>"where/".$id,
                            );
                        $member = GetAll('kg_member',$filter);
                        if($member->num_rows() > 0){
                        $v = $member->row_array();
                    ?>
                    <form method="post" action="<?php echo base_url();?>id/home/pengajuan_mobil">
                        <div class="form-rent-car">
                            <label>Nama</label>
                            <input type="text" name="name" value="<?php echo $v['title'];?>" disabled>
                            <input type="hidden" name="member" value="<?php echo $v['id'];?>">                        
                        </div>
                <?php }} ?> 
                        <div class="form-rent-car">
                            <h6>Keperluan</h6>
                            <!--<?php echo form_dropdown('keperluan', array('Dinas'=>'Dinas','Pribadi'=>'Pribadi'), set_value('keperluan'), 'required style="width:100%;"');?>  -->   
                            <label class="select">
                                <select name="metode">
                                    <option value="0" selected="">Kepeluan</option>
                                    <option value="1">Dinas</option>
                                    <option value="2">Pribadi</option>
                                </select>
                                <i></i>
                            </label>                                            
                        </div>
                        <div class="form-rent-car">
                            <label>Tanggal dan waktu peminjaman</label>
                            <input type="text" class="tgl-pengajuan" name="date_pinjam" value="<?php echo gmdate('Y-m-d H:i', time()+60*60*7);?>" required>
                        </div>
                        <div class="form-rent-car">
                            <label>Tujuan</label>
                            <input type="text" class="" name="tujuan" value="<?php echo set_value('tujuan');?>" required>
                        </div>
                        
                        <div class="form-rent-car">
                            <label>Catatan</label>
                            <textarea name="description" required><?php echo set_value('description');?></textarea>
                        </div>
                        <div class="submit-rent-car">
                            <button class="btn-sbmt" type="submit">AJUKAN</button>
                        </div>
                    </form>                    
                </div>
            </div>
        </div>
      </div>
    </div>
</div>
<!-- RENT CAR  -->

<script type="text/javascript">
    $(document).ready(function() {
        $('#openBtn').click(function(){
            $('#vila-form-mdl').modal({show:true})
        }); 

        $('#openBtn').click(function(){
            $('#car-form-mdl').modal({show:true})
        });               
    });
</script>    