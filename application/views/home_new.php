            <!-- REINDO START SLIDER -->
            <div class="alpha flex_98">
                <!-- REINDO SLIDER -->
                <div class="box"></div>
            </div>
            <div id="slider">
                <ul class="ui-tabs-nav">
                    <?php if($slide->num_rows() > 0){ ?>
                    <?php foreach($slide->result_array() as $sval){ ?>
                    <li class="ui-tabs-nav-item ui-tabs-selected" id="nav-fragment-<?php echo $sval['id']?>">
                        <a href="#fragment-<?php echo $sval['id']?>">
                            <img width="86" height="57" src="<?php echo base_url()?>uploads/<?php echo getThumb($sval['image'])?>" alt="<?php echo $sval['title']?>"/>
                            <span><?php echo word_limiter($sval['title'],20)?></span><br/>
                            <!--<?php echo word_limiter(str_replace("<p>","",str_replace("</p>","",$sval['content'])),10)?>-->
                        </a>
                    </li>
                    <?php } ?>
                    <?php } ?>
                </ul>

                <?php if($slide->num_rows() > 0){ ?>
                <?php foreach($slide->result_array() as $sval){ ?>
                    <div id="fragment-<?php echo $sval['id']?>" class="ui-tabs-panel" style="">
                        <a href="<?php echo $sval['url_link']?>" rel="bookmark">
                            <img width="590" height="290" src="<?php echo base_url()?>uploads/<?php echo $sval['image']?>" alt="<?php echo $sval['title']?>"/>
                        </a>
                        <div class="info">
                            <a class="hideshow" href="#">Hide</a>
                            <h2>
                                <a href="<?php echo $sval['url_link']?>" rel="bookmark"><?php echo $sval['title']?></a>
                            </h2>
                            <p>
                                <?php echo str_replace("<p>","",str_replace("</p>","",$sval['content']))?>
                                <a href="<?php echo $sval['url_link']?>" rel="bookmark">read more</a>
                            </p>
                        </div>
                    </div>
                <?php } ?>
                <?php } ?>
            </div>
            <!-- REINDO THEME STYLE -->
            <!-- REINDO START RECENT POST -->
            <div class="box">
                <!-- REINDO START CAROUSEL -->
                <script type="text/javascript">
                    jQuery(document).ready(function($) {
                        $('#kr-carousel').carousel({
                            itemsPerPage: 4, // number of items to show on each page
                            itemsPerTransition: 1, // number of items moved with each transition
                            noOfRows: 1, // number of rows (see demo)
                            nextPrevLinks: true, // whether next and prev links will be included
                            pagination: false, // whether pagination links will be included
                            speed: 600, // animation speed
                            easing: 'swing' // supports the jQuery easing plugin
                        });
                    });
                </script>
                <!-- REINDO CAROUSEL -->

                 <?php if($this->session->userdata('user_id_sess')) {?>
                 <!--<div class="box">
                    <div class="slider-title">
                        <b>Decision Support System (DSS)
                            <span class="arrows">&raquo;</span>
                        </b>
                    </div>
                </div>
                <div class="alpha flex_98">
                    <div id="kr-carousel">
                        <ul>-->
                            <?php if($analisis->num_rows() > 0) { ?>
                            <?php foreach($analisis->result_array() as $aval) {?>
                           <!-- <li>
                                <a href="<?php echo $aval['url_link']?>" rel="bookmark" target="_BLANK">
                                    <img width="148" height="120" src="<?php echo base_url()?>uploads/<?php echo $aval['image']?>" alt="<?php echo $aval['title']?>"/>
                                </a>
                                <div class="title-car">
                                    <a href="<?php echo $aval['url_link']?>"  target="_BLANK" rel="bookmark"><?php echo $aval['title']?></a>
                                </div>
                            </li> -->
                            <?php } ?>
                            <?php } ?>
                        <!--</ul>
                    </div>
                </div> -->
                <?php }?>

                <div class="item-wrapper-1">
                    <div class="event-title">
                        <b>Reindo Event
                            <span class="arrows">&raquo;</span>
                        </b>
                    </div>
                    <?php 
                    $i=0;
                    if($event->num_rows() > 0) { 
                        foreach($event->result_array() as $eval) { 
                         $i = ++$i; 
                        if($i == 1){
                    ?>

                    <div class="view effect">
                        <a href="<?php echo site_url('event/detail/'.$eval['id'].'/'.url_title($eval['title']))?>" rel="bookmark">
                            <img width="285" src="<?php echo base_url()?>uploads/<?php echo getThumb($eval['image'])?>" alt="<?php echo $eval['title']?>"/>
                        </a>
                        <div class="mask"></div>
                        <div class="content">
                            <a href="<?php echo site_url('event/detail/'.$eval['id'].'/'.url_title($eval['title']))?>" rel="bookmark" class="info" title="Read more">Read more</a>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="super-item">
                        <h5>
                            <a href="<?php echo site_url('event/detail/'.$eval['id'].'/'.url_title($eval['title']))?>" rel="bookmark"><?php echo $eval['title']?></a>
                        </h5>
                        <div class="info"><?php echo date('M d, Y',strtotime($eval['date_event']))?></div>
                        <div class="<?php echo ($i == 2) ? 'intro-event' : 'intro' ?>"><?php echo character_limiter($eval['headline'],190)?>
                            <a href="<?php echo site_url('event/detail/'.$eval['id'].'/'.url_title($eval['title']))?>" rel="bookmark" title="Read more">read more</a>
                            <?php if($i == 2){?>
                            <br/>
                            <a href="<?php echo site_url('event')?>" class="more-items">More Info »</a>
                            <?php } ?>
                        </div>
                    </div>
                    <?php } ?>
                    <?php }else{ ?>
                    <div class="view effect">
                        <a href="#" rel="bookmark">
                            <img width="285" height="120" src="<?php echo base_url()?>uploads/2013/06/Event.jpg" class="attachment-feat-thumb wp-post-image" alt="pool-103014_640" width="285px" />
                        </a>
                        <div class="mask"></div>
                        <div class="content">
                            <a href="#" rel="bookmark" class="info" title="Read more">Read more</a>
                        </div>
                    </div>
                    <div class="super-item">
                        <h5>
                            <a href="#" rel="bookmark">Title Here</a>
                        </h5>
                        <div class="info">Jun 12th, 2013&nbsp;&nbsp;&nbsp;&nbsp;</div>
                        <div class="intro">Fusce aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique ut, consectetur non erat. Donec dignissim tellus non ante volutpat...
                            <a href="#" rel="bookmark" title="Read more">read more</a>
                        </div>
                    </div>
                    <div class="super-item">
                        <h5>
                            <a href="#" rel="bookmark">Title Here</a>
                        </h5>
                        <div class="info">Jun 12th, 2013&nbsp;&nbsp;&nbsp;&nbsp;</div>
                        <div class="intro-event">Fusce aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique ut, consectetur non erat.
                            <a href="#" rel="bookmark" title="Read more">read more</a><br/>
                            <a href="list_events.html" class="more-items">More Info »</a>
                        </div>
                    </div>
                    <?php } ?>

                    <div class="blog-item">
                        <div class="blog-title">
                            <b>Blog Roll
                                <span class="arrows">&raquo;</span>
                            </b>
                        </div>
                        <?php if($blog->num_rows() > 0) { ?>
                        <ul>
                        <?php foreach ($blog->result_array() as $bval) { ?>
                                <li>
                                    <a href="<?php echo site_url('blog/detail/'.$bval['id'].'/'.url_title($bval['title']))?>"><?php echo word_limiter($bval['title'],30)?></a>
                                </li>
                        <?php } ?>
                        </ul>
                            <a href="<?php echo site_url('blog')?>" class="more-items">More Info »</a>
                        <?php }else{ ?>
                        <ul>
                            <li>
                                <a href="#">aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat.</a>
                            </li>
                            <li>
                                <a href="#">Half Day aliquet dolor sed dolor feugiat fermentum</a>
                            </li>
                            <li>
                                <a href="#">Donec dignissim tellus non ante volutpat Meeting Reindo</a>
                            </li>
                        </ul>
                        <a href="#" class="more-items">More Info »</a>
                        <?php } ?>
                    </div>

                    <!-- <div class="topik-item">
                        <div class="topik-title">
                            <b>Topik Diskusi Terkini
                                <span class="arrows">&raquo;</span>
                            </b>
                        </div>
                        <ul>
                            <li>
                                <a href="#">aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat.</a>
                            </li>
                            <li>
                                <a href="#">Half Day aliquet dolor sed dolor feugiat fermentum</a>
                            </li>
                            <li>
                                <a href="#">Donec dignissim tellus non ante volutpat Meeting Reindo</a>
                            </li>
                        </ul>
                        <a href="#" class="more-items">More Info »</a>
                    </div> -->
                </div>

                <div class="item-wrapper-2">
                    <div class="latest-update-title">
                        <b>Latest Update
                            <span class="arrows">&raquo;</span>
                        </b>
                    </div>
                    <?php if($article->num_rows() > 0) { ?>
                    <?php foreach($article->result_array() as $arval) {?>
                    <?php if($arval['image']) {?>
                    <div class="view2 effect">
                        <a href="<?php echo site_url($arval['module_detail'].$arval['id'].'/'.url_title($arval['title']))?>" rel="bookmark">
                            <img width="300" src="<?php echo base_url()?>uploads/<?php echo getThumb($arval['image'])?>" alt="<?php echo $arval['title']?>"/>
                        </a>
                        <div class="mask"></div>
                        <div class="content">
                            <a href="<?php echo site_url($arval['module_detail'].$arval['id'].'/'.url_title($arval['title']))?>" rel="bookmark" class="info" title="Read more">Read more</a>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="super-item">
                        <h5>
                            <a href="<?php echo site_url($arval['module_detail'].$arval['id'].'/'.url_title($arval['title']))?>" rel="bookmark"><?php echo $arval['title']?></a>
                        </h5>
                        <div class="info"><?php echo date('M d, Y',strtotime($arval['create_date']))?></div>
                        <div class="intro"><?php echo character_limiter($arval['headline'],190)?>
                            <a href="<?php echo site_url($arval['module_detail'].$arval['id'].'/'.url_title($arval['title']))?>" rel="bookmark" title="Read more">read more</a>
                        </div>
                    </div>
                    <?php } ?>
                    <?php }else{ ?>
                    <div class="view2 effect">
                        <a href="#" rel="bookmark">
                            <img class="news-thumb" width="300" height="120" src="<?php echo base_url()?>uploads/2013/06/news2.jpg" class="attachment-feat-thumb wp-post-image" alt="2905135770_c3c86b41d9_z" width="285px" />
                        </a>
                        <div class="mask"></div>
                        <div class="content">
                            <a href="#" rel="bookmark" class="info" title="Read more">Read more</a>
                        </div>
                    </div>
                    <div class="super-item">
                        <h5>
                            <a href="#" rel="bookmark">Title Here</a>
                        </h5>
                        <div class="info">Jun 12th, 2013&nbsp;&nbsp;&nbsp;&nbsp;</div>
                        <div class="intro">Fusce aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique ut, consectetur non erat. Donec dignissim tellus non ante volutpat...
                            <a href="#" rel="bookmark" title="Read more">read more</a>
                        </div>
                    </div>

                    <div class="view2 effect">
                        <a href="#" rel="bookmark">
                            <img class="news-thumb" width="300" height="120" src="<?php echo base_url()?>uploads/2013/06/news3.jpg" class="attachment-feat-thumb wp-post-image" alt="2905135770_c3c86b41d9_z" width="285px" />
                        </a>
                        <div class="mask"></div>
                        <div class="content">
                            <a href="#" rel="bookmark" class="info" title="Read more">Read more</a>
                        </div>
                    </div>
                    <div class="super-item">
                        <h5>
                            <a href="#" rel="bookmark">Title Here</a>
                        </h5>
                        <div class="info">Jun 12th, 2013&nbsp;&nbsp;&nbsp;&nbsp;</div>
                        <div class="intro">Fusce aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique ut, consectetur non erat. Donec dignissim tellus non ante volutpat...
                            <a href="#" rel="bookmark" title="Read more">read more</a>
                        </div>
                    </div>

                    <div class="view2 effect">
                        <a href="#" rel="bookmark">
                            <img class="news-thumb" width="300" height="120" src="<?php echo base_url()?>uploads/2013/06/news4.jpg" class="attachment-feat-thumb wp-post-image" alt="2905135770_c3c86b41d9_z" width="285px" />
                        </a>
                        <div class="mask"></div>
                        <div class="content">
                            <a href="#" rel="bookmark" class="info" title="Read more">Read more</a>
                        </div>
                    </div>
                    <div class="super-item">
                        <h5>
                            <a href="#" rel="bookmark">Title Here</a>
                        </h5>
                        <div class="info">Jun 12th, 2013&nbsp;&nbsp;&nbsp;&nbsp;</div>
                        <div class="intro">Fusce aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique ut, consectetur non erat. Donec dignissim tellus non ante volutpat...
                            <a href="#" rel="bookmark" title="Read more">read more</a>
                        </div>
                    </div>

                    <div class="view2 effect">
                        <a href="#" rel="bookmark">
                            <img class="news-thumb" width="300" height="120" src="<?php echo base_url()?>uploads/2013/06/news4.jpg" class="attachment-feat-thumb wp-post-image" alt="2905135770_c3c86b41d9_z" width="285px" />
                        </a>
                        <div class="mask"></div>
                        <div class="content">
                            <a href="#" rel="bookmark" class="info" title="Read more">Read more</a>
                        </div>
                    </div>
                    <div class="super-item">
                        <h5>
                            <a href="#" rel="bookmark">Title Here</a>
                        </h5>
                        <div class="info">Jun 12th, 2013&nbsp;&nbsp;&nbsp;&nbsp;</div>
                        <div class="intro">Fusce aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique ut, consectetur non erat. Donec dignissim tellus non ante volutpat...
                            <a href="#" rel="bookmark" title="Read more">read more</a>
                        </div>
                    </div>
                    <?php } ?>
                </div>
                <div class="item-wrapper-3">

                    <div class="blog-title">
                            <b>Pengumuman
                                <span class="arrows">&raquo;</span>
                            </b>
                        </div>

                    <div class="pengumuman-item">
                        
                        <ul>
                            <?php if($announcement->num_rows() > 0) { ?>
                            <?php foreach($announcement->result_array() as $anval){ ?>
                                <li>
                                <h4><?php echo $anval['title']?></h4>
                                <div class="info" style="margin-left:8px;"><?php echo date('M d, Y',strtotime($anval['date_announcement']))?></div>
                                
                                <p><?php echo ((strlen($anval['headline']) == 0) || ($anval['headline'] == 'NULL')) ? $anval['title'] : $anval['headline']?>
                                    <a href="<?php echo site_url('announcement/detail/'.$anval['id'].'/'.url_title($anval['title']))?>" title="Read more">...read more</a>
                                </p>
                            </li>
                            <?php } ?>
                            <?php }else{ ?>
                            <li>
                                <p>Fusce aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique
                                    <a href="#" title="Read more">...read more</a>
                                </p>
                            </li>
                            <li>
                                <p>Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique ut, consectetur non erat. Donec dignissim tellus
                                    <a href="#">...read more</a>
                                </p>
                            </li>
                            <?php } ?>                   
                        </ul>
                        <a href="<?php echo site_url('announcement')?>" class="more-items">More Info »</a>
                    </div>


                    <div class="calender-title">
                        <b>Reindo Next Event
                            <span class="arrows">&raquo;</span>
                        </b>
                    </div>
                    <div class="calender-box">
                        <ul>
                            <?php if($calendar->num_rows() > 0) {?>
                            <?php foreach($calendar->result_array() as $cval){?>
                            <li>
                                <div class="date-box">
                                    <h4><?php echo strtoupper(date('M d',strtotime($cval['date_calendar'])))?></h4>
                                </div>
                                <div class="date-info">
                                    <a href="<?php echo site_url('calendar/detail/'.$cval['id'].'/'.url_title($cval['title']))?>"><?php echo character_limiter($cval['title'],50)?></a>
                                </div>
                            </li>
                            <?php } ?>
                            <?php }else{ ?>
                            <li>
                                <div class="date-box">
                                    <h4>DES 21</h4>
                                </div>
                                <div class="date-info">
                                    <a href="#">Title Here</a>
                                </div>
                            </li>
                            <li>
                                <div class="date-box">
                                    <h4>DES 21</h4>
                                </div>
                                <div class="date-info">
                                    <a href="#">Title Here</a>
                                </div>
                            </li>
                            <li>
                                <div class="date-box">
                                    <h4>DES 21</h4>
                                </div>
                                <div class="date-info">
                                    <a href="#">Title Here</a>
                                </div>
                            </li>
                            <?php }?>
                        </ul>
                        <a href="<?php echo site_url('calendar')?>" class="more-items">More Info »</a>
                    </div>

                    

                    <div class="meetroom-item">
                        <div class="blog-title">
                            <b>Ruang Meeting
                                <span class="arrows">&raquo;</span>
                            </b>
                        </div>
                        <div class="meeting-box">
                        <ul>
                            <?php if($meeting->num_rows() > 0) {?>
                                <?php foreach($meeting->result_array() as $mval) {?>
                                    <li>
                                        <div class="date-box">
                                            <h4><?php echo strtoupper(date('M d',strtotime($mval['date_meeting'])))?></h4>
                                        </div>
                                        <div class="meeting-info">
                                            <strong>Ruang <?php echo $mval['title']?> | <?php echo $mval['time_meeting']?></strong><br/>
                                            <span></span><?php echo $mval['headline']?>
                                        </div>
                                        <div style="clear: both"></div>
                                    </li>
                                <?php } ?>
                            <?php }else{ ?>
                            <li>
                                <div class="date-box">
                                    <h4>MM DD</h4>
                                </div>
                                <div class="meeting-info">
                                    <strong>Belum ada jadwal meeting</strong><br/>
                                </div>
                                <div style="clear: both"></div>
                            </li>
                            <!-- <li>
                                <div class="date-box">
                                    <h4>DES 21</h4>
                                </div>
                               <div class="meeting-info">
                                    <strong>Time meeting here</strong><br/>
                                    Meeting description here
                                </div>
                                <div style="clear: both"></div>
                            </li>
                            <li>
                                <div class="date-box">
                                    <h4>DES 21</h4>
                                </div>
                                <div class="meeting-info">
                                    <strong>Time meeting here</strong><br/>
                                    Meeting description here
                                </div>
                                <div style="clear: both"></div>
                            </li>
                            <li>
                                <div class="date-box">
                                    <h4>DES 21</h4>
                                </div>
                                <div class="meeting-info">
                                    <strong>Time meeting here</strong><br/>
                                    Meeting description here
                                </div>
                                <div style="clear: both"></div>
                            </li> -->
                            <?php } ?>
                        </ul>
                        <div style="clear: both"></div>
                        <a href="<?php echo site_url('meeting_room')?>" class="more-items">More Info »</a>
                            </div>
                    </div>

                    <div class="absensi-item">
                        <div class="blog-title">
                            <b>Absensi Karyawan
                                <span class="arrows">&raquo;</span>
                            </b>
                        </div>
                        <ul>
                            <?php if($absence->num_rows() > 0) { ?>
                                <?php foreach($absence->result_array() as $abval) {?>
                                    
                                    <li>
                                        <!-- <a href="<?php echo site_url('absence/detail/'.$abval['id'].'/'.url_title($abval['title']))?>"><?php echo $abval['title']?></a> -->
                                        <a href="<?php echo site_url('absence/download/'.$abval['id'].'/'.url_title($abval['title']))?>" title="Download <?php echo $abval['title']?>"><?php echo $abval['title']?></a>
                                    </li>
                                    
                                <?php } ?>
                            <?php }else{ ?>
                            <li>
                                <a href="#">Title absence here</a>
                            </li>
                            <?php } ?>
                        </ul>
                        <a href="<?php echo site_url('absence')?>" class="more-items">More Info »</a>
                        
                    </div>

                    <?php 
                        if($vote->num_rows() > 0) {
                            $voting = $vote->row_array();
                    ?>
                    <form id="vote_form" action="<?php echo site_url('home/vote')?>" method="POST">
                        <div class="vote-item">
                            <div class="blog-title">
                                <b>Silahkan Berikan Masukan Anda
                                    <span class="arrows">&raquo;</span>
                                </b>
                            </div>
                            <p><?php echo $voting['content']; ?></p>
                            
                            <span class="optional">
                                <input type="radio" name="answerit" value='option1'><?php echo $voting['option1']?>
                            </span>
                            <span class="optional">
                                <input type="radio" name="answerit" value='option2'><?php echo $voting['option2']?>
                            </span>
                            <?php 
                                if($voting['option3']){
                                    echo "<span class='optional'>";
                                    echo "<input type='radio' name='answerit' value='option3'>".$voting['option3'];
                                    echo "</span>";
                                }
                            ?>
                            
                             <?php 
                                if($voting['option3']){
                                    echo "<span class='optional'>";
                                    echo "<input type='radio' name='answerit' value='option4'>".$voting['option4'];
                                    echo "</span>";
                                }
                            ?>
                            <div class="wrap-btn">
                                <?php
                                $filterpollingmember = array("id_polling"=>"where/".$voting['id'],"id_member"=>"where/".$this->session->userdata('user_id_sess'));
                                $pollingmember = getAll('kg_polling_member',$filterpollingmember);
                                if($pollingmember->num_rows() > 0){
                                    echo "<a href='".site_url('vote/detail/'.$voting['id'].'/'.url_title($voting['title']))."' class='btn-default view_polling'>View voting</a>";
                                }
                                ?>
                                <button class="btn-default" type="button">Batal</button>
                                <button class="btn-default" type="submit">Submit</button>
                                <input type="hidden" name="id_polling" value="<?php echo $voting['id'];?>">
                                <input type="hidden" name="title" value="<?php echo $voting['title'];?>">
                            </div>
                            <div style="clear:both"></div>
                        </div>
                    </form>
                    <?php } ?>
						
						<!-- EMPLOYEE SELF-SERVICE -->
						<div class="self-service">
                            <h3><span class="h3-orange">EMPLOYEE</span> SELF-SERVICE</h3>
							<ul>
								<li><a class="fancybox" href="#villa">Booking Vila</a></li>
								<li><a class="fancybox" href="#pengajuanmobil">Peminjaman Mobil Perusahaan</a></li>
								<li><a href="<?php echo base_url();?>id/pengaduan">Pengaduan</a></li>
							<ul>
						</div>
				</div>
				
            </div>
            <div style="clear:both"></div>
            <div class="box">
            </div>
            <div style="clear:both"></div>

            <!-- REINDO RECENT CATEGORY -->
            <div class="box">
                <!--here 3-->


                <!--here 3-->
            </div>
			
	<div id="pengajuanmobil" class="frame-pengajuan">
	<?php	
		if($this->session->userdata('user_id_sess')) {
        $id = $this->session->userdata('user_id_sess');
        $filter = array(
                "id"=>"where/".$id,
            );
		$member = GetAll('kg_member',$filter);
        if($member->num_rows() > 0){
        $v = $member->row_array();
	?>
		<h2>Peminjaman Mobil</h2>
	
		<form class="rz-form" momethod="post" action="<?php echo base_url();?>id/home/pengajuan_mobil">
			<label>Nama</label>
			<input type="text" name="name" value="<?php echo $v['title'];?>" disabled>
			<input type="hidden" name="member" value="<?php echo $v['id'];?>">
	<?php }} ?>
			
			<label>Tanggal dan waktu peminjaman</label>
			<input type="text" class="tgl-pengajuan" name="date_pinjam" value="<?php echo gmdate('Y-m-d H:i', time()+60*60*7);?>" required>
			
			<label>Catatan</label>
			<textarea name="description" required><?php echo set_value('description');?></textarea>
			
			<button class="btn-sbmt" type="submit">AJUKAN</button>
		</form>
	</div>
	
	
	<!-- Booking Villa -->
	<div id="villa" class="frame-villa">
	<?php	
		if($this->session->userdata('user_id_sess')) {
        $id = $this->session->userdata('user_id_sess');
        $filter = array(
                "id"=>"where/".$id,
            );
		$member = GetAll('kg_member',$filter);
        if($member->num_rows() > 0){
        $v = $member->row_array();
	?>
		<h2>Booking Villa</h2>
	
		<form class="rz-form2" method="post" action="<?php echo base_url();?>id/home/booking_villa">
			<input type="hidden" name="member" value="<?php echo $v['id'];?>">
			<input type="hidden" name="session_villa_id" value="<?php echo getLastSessionVilla();?>">
	<?php }} ?>
			<div class="flex_50a" style="margin-right:6px">
				<label>Check In</label>
				<input type="text" class="tgl-pengajuan datebook" name="start_date" placeholder="---- / -- / --    --:--" required>
			</div>
			<div class="flex_50a">
			<label>Check Out</label>
			<input type="text" class="tgl-pengajuan datebook" name="end_date" placeholder="---- / -- / --    --:--" required>
			</div>
			
			<label>Tipe Villa</label>
			<?php echo form_dropdown('tipe_villa', $opt_villa, set_value('tipe_villa'), 'required');?>
			<label>Keterangan</label>
			<textarea name="title" required><?php echo set_value('title');?></textarea>
			
			<button class="btn-sbmt" type="submit">Pesan Sekarang</button>
		</form>
	</div>

<script type="text/javascript">
	$(document).ready(function() {
		$('.fancybox').fancybox();
		
		$('.rz-form').validate({
			rules: {
				date_pinjam: "required", //pengajuan mobil
				description: "required"
			},
			messages: { 
				date_pinjam: "*Tanggal dan waktu peminjaman is required",
				description: "*Catatan peminjaman is required"
				}
		});
		
		$('.rz-form2').validate({
			rules: {
				start_date: "required",
			    end_date: "required",
				tipe_villa: "required",
				title: "required",
			},
			messages: { 
				start_date: "*Check In is required",
			    end_date: "*Check Out is required",
				tipe_villa: "*Tipe Villa is required",
				title: "*Keterangan is required"
				}
		});
	});
	
	$(":submit").click(function(e){

		if(!$(this).closest("form").valid()){
			e.preventDefault();
			var errText="";
			$(".error").each(function(i,j){
			 errText+=$(j).text()+"<br/>";           
			});
			$("#errDiv").html(errText).css({color:'Red'});  
			return false;
			}
		$("#errDiv").empty();
		
	});
	
	$('.tgl-pengajuan').datetimepicker();
</script>

