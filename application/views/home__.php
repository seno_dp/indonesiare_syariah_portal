<!-- ~~~=| Banner START |=~~~ -->
<section class="hp_banner_area section_padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="hp_banner_box">
                    <div class="hp_banner_left">
                        <div class="bl_single_news">
                            <img src="<?php echo base_url()?>uploads/blt-banner1.jpg" alt="Banner" />
                            <div class="bl_single_text">
                                <a href="blog-single-slider-post.html">
                                    <h4>20 Myths About Mobile Devices</h4>
                                </a> <span><i class="fa fa-clock-o"></i> 3 mins ago</span>
                            </div>
                        </div>
                        <div class="bl_single_news"> 
                            <img src="<?php echo base_url()?>uploads/blt-banner2.jpg" alt="Banner" />
                            <div class="bl_single_text">
                                <a href="blog-single-slider-post.html">
                                    <h4>What Do Need To Make a Business ?</h4>
                                </a> <span><i class="fa fa-clock-o"></i> 4 hours ago</span>
                            </div>
                        </div>
                        <div class="bl_single_news">
                            <img src="<?php echo base_url()?>uploads/bann2.jpg" alt="" />
                            <div class="bl_single_text">
                                <a href="blog-single-slider-post.html">
                                    <h4>The Ultimate Cheat Sheet On Ecology</h4>
                                </a> <span><i class="fa fa-clock-o"></i> 5 hours ago</span>
                            </div>
                        </div>
                    </div>
                    <div class="hp_banner_right">
                        <div class="br_single_news">
                            <img src="<?php echo base_url()?>uploads/brt-banner1.jpg" alt="" />
                            <div class="br_single_text"> <span class="green_hp_span">Photo</span>
                                <a href="blog-single-slider-post.html">
                                    <h4>The Ultimate Cheat Sheet On Ecology</h4>
                                </a>
                            </div>
                            <div class="br_cam">
                                <a href="" class="fa fa-camera"></a>
                            </div>
                        </div>
                        <div class="br_single_news">
                            <img src="<?php echo base_url()?>uploads/brt-banner2.jpg" alt="" />
                            <div class="br_single_text">
                                <span class="blue_hp_span">Video</span>
                                <a href="blog-single-slider-post.html">
                                    <h4>Notes from a vacation photographer</h4>
                                </a>
                            </div>
                            <div class="br_cam">
                                <a href="" class="fa fa-camera"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- ~~~=| Banner END |=~~~ -->

<!-- ~~~=| GRID2 START |=~~~ -->
<section class="main_news_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">  
                <!--START HOME_SIDEBAR-->
                <div class="home_sidebar">
                    <div class="all_news_right"><!-- all_news_right EVENT -->  
                        <div class="follow_us_side">
                            <h2>Reindo Event</h2>
                        </div>              
                        <!-- START REINDO EVENT -->   
                        <?php 
                        $i=0;
                        if($event->num_rows() > 0) { 
                            foreach($event->result_array() as $eval) { 
                             $i = ++$i; 
                            if($i == 1){
                        ?>
                        <div class="">
                            <div class="single_fs_news_left_text">
                                <div class="fs_news_left_img">
                                    <a href="<?php echo site_url('event/detail/'.$eval['id'].'/'.url_title($eval['title']))?>" rel="bookmark">
                                        <img width="285" src="<?php echo base_url()?>uploads/<?php echo getThumb($eval['image'])?>" alt="<?php echo $eval['title']?>"/>
                                    </a>
                                </div>                                  
                            </div>                          
                        </div>
                        <?php } ?>
                        <div class="fs_news_right">
                            <div class="single_fs_news_right_text"><!-- super-item -->
                            <!-- title event  -->
                                <h4><a href="<?php echo site_url('event/detail/'.$eval['id'].'/'.url_title($eval['title']))?>" rel="bookmark"><?php echo $eval['title']?></a></h4>
                            <!-- title event -->
                            <!-- intro-event -->
                                <div class="<?php echo ($i == 2) ? 'intro-event' : 'intro' ?>">
                                    <p><?php echo character_limiter($eval['headline'],190)?></p>
                                </div>
                            <!-- intro-event -->   
                            <!-- Readmore -->    
                                <p><a class="gad_color" href="<?php echo site_url('event/detail/'.$eval['id'].'/'.url_title($eval['title']))?>">Readmore </a></p>
                            <!-- Readmore -->   
                                <?php if($i == 2){?>
                                <a href="<?php echo site_url('event')?>" class="more-items">More Info »</a>
                                                    <?php } ?>
                            <!-- date-post -->     
                                <p><i class="fa fa-clock-o"></i>&nbsp;<?php echo date('M d, Y',strtotime($eval['date_event']))?></p>
                            <!-- date-post -->    
                            </div><!-- super-item -->                            
                        </div>
                        <?php } ?>
                        <?php }else{ ?>
                        <div class="">
                            <div class="single_fs_news_left_text">
                                <div class="fs_news_left_img">
                                    <a href="#" rel="bookmark">
                                        <img width="285" height="120" src="<?php echo base_url()?>uploads/2013/06/Event.jpg" class="attachment-feat-thumb wp-post-image" alt="pool-103014_640" width="285px" />
                                    </a>                                    
                                </div>
                            </div>                            
                        </div>
                        <div class="fs_news_right">
                            <div class="single_fs_news_right_text"><!-- super-item -->
                            <!-- title-bookmark -->
                                <h4><a href="blog-single-slider-post.html">Internet trolls take pleasure in making you suffer</a></h4>
                            <!-- title-bookmark -->
                            <!-- intro-event -->
                                <div class="intro-of-event">
                                    <p>Fusce aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique ut, consectetur non erat.</p>
                                </div>
                            <!-- intro-event -->   
                            <!-- Readmore -->    
                                <p><a class="gad_color" href="">Readmore </a></p>
                            <!-- Readmore -->
                            <!-- date-post -->    
                                <p>| <i class="fa fa-clock-o"></i> 1 hour ago </p>
                            <!-- date-post -->    
                            </div><!-- super-item -->
                        </div>
                        <div class="fs_news_right">
                            <div class="single_fs_news_right_text"><!-- super-item -->
                            <!-- title-bookmark -->
                                <h4><a href="blog-single-slider-post.html">Internet trolls take pleasure in making you suffer</a></h4>
                            <!-- title-bookmark -->
                            <!-- intro-event -->
                                <div class="intro-event">
                                    <p>Fusce aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique ut, consectetur non erat.</p>
                                </div>
                            <!-- intro-event -->   
                            <!-- Readmore -->    
                                <p><a class="gad_color" href="">Readmore </a></p>
                            <!-- Readmore -->
                            <!-- date-post -->  
                            <a href="#">More Info »</a> 
                                <p><i class="fa fa-clock-o"></i> 1 hour ago </p>
                            <!-- date-post -->    
                            </div><!-- super-item -->
                        </div>
                        <?php } ?> 
                        <!-- END REINDO EVENT -->                                       
                    </div><!-- all_news_right EVENT -->                                  
                </div><!--END HOME_SIDEBAR-->    
                <div class="home_sidebar"><!--START HOME_SIDEBAR-->
                    <!-- START BLOG ITEM -->            
                    <div class="all_news_right"><!-- all_news_right EVENT -->
                            <div class="follow_us_side">
                                <h2>Blog Roll</h2>
                            </div>              
                            <div class="blog-of-item">
                                <?php if($blog->num_rows() > 0) { ?>
                                <ul>
                                <?php foreach ($blog->result_array() as $bval) { ?>
                                        <li>
                                            <a href="<?php echo site_url('blog/detail/'.$bval['id'].'/'.url_title($bval['title']))?>"><?php echo word_limiter($bval['title'],30)?></a>
                                        </li>
                                <?php } ?>
                                </ul>
                                    <a href="<?php echo site_url('blog')?>" class="more-items">More Info »</a>
                                <?php }else{ ?>
                                <ul>
                                    <li>
                                        <a href="#">aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat.</a>
                                    </li>
                                    <li>
                                        <a href="#">Half Day aliquet dolor sed dolor feugiat fermentum</a>
                                    </li>
                                    <li>
                                        <a href="#">Donec dignissim tellus non ante volutpat Meeting Reindo</a>
                                    </li>
                                </ul>
                                <a href="#" class="more-items">More Info »</a>
                                <?php } ?>
                            </div>
                        <!-- END BLOG ITEM -->               
                    </div><!-- all_news_right EVENT -->                     
                </div><!--END HOME_SIDEBAR-->      

            </div><!-- END COL 4 -->
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="home_sidebar">
                    <div class="all_news_right">
                        <div class="follow_us_side">
                            <h2>Latest Update</h2>
                        </div>    
                        <?php if($article->num_rows() > 0) { ?>
                        <?php foreach($article->result_array() as $arval) {?>
                        <?php if($arval['image']) {?>   
                        <!-- IMAGE LATEST --> 
                        <div class="">
                            <div class="single_fs_news_left_text">
                                <div class="fs_news_left_img">
                                    <a href="<?php echo site_url($arval['module_detail'].$arval['id'].'/'.url_title($arval['title']))?>" rel="bookmark">
                                        <!-- <img width="300" src="<?php echo base_url()?>uploads/<?php echo getThumb($arval['image'])?>" alt="<?php echo $arval['title']?>"/>-->
                                        <img width="300" src="http://localhost/reindo_intranet/uploads/20161108135510.POSTER_final_thumb.jpg" alt="<?php echo $arval['title']?>"/>
                                    </a>
                                </div>
                            </div>
                        </div>
                                <div class="fs_news_right">
                                    <div class="single_fs_news_right_text"><!-- super-item -->
                                        <h4><a href="<?php echo site_url($arval['module_detail'].$arval['id'].'/'.url_title($arval['title']))?>" rel="bookmark"><?php echo $arval['title']?></a></h4>
                                        <div class="intro">
                                            <p><?php echo character_limiter($arval['headline'],190)?></p>
                                        </div>  
                                        <p><a class="gad_color" href="<?php echo site_url($arval['module_detail'].$arval['id'].'/'.url_title($arval['title']))?>" rel="bookmark" title="Read more">Readmore </a></p> 
                                    <!-- date-post -->     
                                        <p><i class="fa fa-clock-o"></i>&nbsp;<?php echo date('M d, Y',strtotime($arval['create_date']))?></p>
                                    <!-- date-post -->    
                                    </div>                          
                                </div>                                 
                        <?php } ?>
                        <?php } ?>
                        <?php } ?>
                        <!-- IMAGE LATEST -->               
                    </div>                    
                </div>
            </div><!-- END COL 4 -->
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="home_sidebar">
                    <div class="all_news_right">
                        <div class="follow_us_side">
                            <h2>Pengumuman</h2>
                        </div>
                        <div class="blog-of-item">
                            <ul>
                                <?php if($announcement->num_rows() > 0) { ?>
                                <?php foreach($announcement->result_array() as $anval){ ?>
                                    <li>
                                    <h4><?php echo $anval['title']?></h4>
                                    <p><?php echo ((strlen($anval['headline']) == 0) || ($anval['headline'] == 'NULL')) ? $anval['title'] : $anval['headline']?>
                                        <span>
                                            <a href="<?php echo site_url('announcement/detail/'.$anval['id'].'/'.url_title($anval['title']))?>" title="Read more">...read more</a>
                                        </span>
                                    </p>
                                    <div class="announcement-time"><i class="fa fa-clock-o"></i>&nbsp;<?php echo date('M d, Y',strtotime($anval['date_announcement']))?></div>
                                </li>
                                <?php } ?>
                                <?php }else{ ?>
                                <li>
                                    <h4>Title dummmy here</h4>
                                    <p>Fusce aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique
                                        <span><a href="#" title="Read more">...read more</a></span>
                                    </p>
                                </li>
                                <li>
                                    <h4>Title dummmy here</h4>
                                    <p>Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique ut, consectetur non erat. Donec dignissim tellus
                                        <span><a href="#" title="Read more">...read more</a></span>
                                    </p>
                                </li>
                                <?php } ?>                   
                            </ul>                            
                        </div>                             
                    </div>                  
                </div>
                <!--NEXT EVENT-->
                <div class="home_sidebar">
                    <div class="all_news_right">
                        <div class="follow_us_side">
                            <h2>Reindo Next Event</h2>
                        </div>                        
                    </div>
                    <div class="calender-box">
                        <ul>
                            <?php if($calendar->num_rows() > 0) {?>
                            <?php foreach($calendar->result_array() as $cval){?>
                            <li>
                                <div class="date-box">
                                    <h4><?php echo strtoupper(date('M d',strtotime($cval['date_calendar'])))?></h4>
                                </div>
                                <div class="date-info">
                                    <a href="<?php echo site_url('calendar/detail/'.$cval['id'].'/'.url_title($cval['title']))?>"><?php echo character_limiter($cval['title'],50)?></a>
                                </div>
                            </li>
                            <?php } ?>
                            <?php }else{ ?>
                            <li>
                                <div class="date-box">
                                    <h4>DES 21</h4>
                                </div>
                                <div class="date-info">
                                    <a href="#">Title Here</a>
                                </div>
                            </li>
                            <li>
                                <div class="date-box">
                                    <h4>DES 21</h4>
                                </div>
                                <div class="date-info">
                                    <a href="#">Title Here</a>
                                </div>
                            </li>
                            <li>
                                <div class="date-box">
                                    <h4>DES 21</h4>
                                </div>
                                <div class="date-info">
                                    <a href="#">Title Here</a>
                                </div>
                            </li>
                            <?php }?>
                        </ul>
                        <a href="<?php echo site_url('calendar')?>" class="more-items">More Info »</a>
                    </div>                    
                </div>
                <!--NEXT EVENT-->
                <!--MKEETING ROOM-->
                <div class="home_sidebar">
                    <div class="all_news_right">
                        <div class="follow_us_side">
                            <h2>Ruang Meeting</h2>
                        </div>
                        <div class="meeting-box">
                            <ul>
                                <?php if($meeting->num_rows() > 0) {?>
                                    <?php foreach($meeting->result_array() as $mval) {?>
                                        <li>
                                            <div class="date-box">
                                                <h4><?php echo strtoupper(date('M d',strtotime($mval['date_meeting'])))?></h4>
                                            </div>
                                            <div class="meeting-info">
                                                <strong>Ruang <?php echo $mval['title']?> | <?php echo $mval['time_meeting']?></strong><br/>
                                                <span></span><?php echo $mval['headline']?>
                                            </div>
                                            <div style="clear: both"></div>
                                        </li>
                                    <?php } ?>
                                <?php }else{ ?>
                                <li>
                                    <div class="date-box">
                                        <h4>MM DD</h4>
                                    </div>
                                    <div class="meeting-info">
                                        <strong>Belum ada jadwal meeting</strong><br/>
                                    </div>
                                    <div style="clear: both"></div>
                                </li>
                                <!-- <li>
                                    <div class="date-box">
                                        <h4>DES 21</h4>
                                    </div>
                                   <div class="meeting-info">
                                        <strong>Time meeting here</strong><br/>
                                        Meeting description here
                                    </div>
                                    <div style="clear: both"></div>
                                </li>
                                <li>
                                    <div class="date-box">
                                        <h4>DES 21</h4>
                                    </div>
                                    <div class="meeting-info">
                                        <strong>Time meeting here</strong><br/>
                                        Meeting description here
                                    </div>
                                    <div style="clear: both"></div>
                                </li>
                                <li>
                                    <div class="date-box">
                                        <h4>DES 21</h4>
                                    </div>
                                    <div class="meeting-info">
                                        <strong>Time meeting here</strong><br/>
                                        Meeting description here
                                    </div>
                                    <div style="clear: both"></div>
                                </li> -->
                                <?php } ?>
                            </ul>                            
                        </div>
                    </div>
                </div>                    
                <!--MKEETING ROOM-->
            </div><!-- END COL 4 -->                        
        </div>
    </div>
</section>
<!-- ~~~=| GRID2 END |=~~~-->