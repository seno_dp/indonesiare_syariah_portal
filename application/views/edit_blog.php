<script language="JavaScript" src="<?php echo base_url();?>assets/theme/js/ckeditor/ckeditor.js" type="text/javascript"></script>
<script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/uploading-file.js'></script>
<!-- EDIT BLOG -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="post_text">
                    <div class="wrap-of-editorial">
                        <div> 
                            <!-- REINDO BREAD CRUMB -->
                            <div class="breadcum_c_left">
                                Posting blog
                            </div>
                            <div style="clear:both"></div>                          
                            <div class="forum-detail">          
                                <div id="respond" class="cc_single_post">
                                    <form action="<?php echo site_url('blog/update')?>" method="post" id="commentform" class="comment-form" enctype="multipart/form-data">
                                        <?php
                                        $flashmessage = $this->session->flashdata('message');
                                        if($flashmessage)
                                        {
                                        ?>
                                            <p>
                                                <?php echo $flashmessage;?>
                                            </p>
                                        <?php
                                        }
                                        ?>
                                        <?php
                                            if($qp->num_rows() > 0){
                                            $val = $qp->row_array();
                                        ?>
                                        <div class="row">
                                            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                <div class="wrap-blank-thumb">
                                                    <?php
                                                        if($val['image']){
                                                            echo '<div class="thumb_image_blog">';
                                                            echo '<img class="img_blog" width="260px;" src="'.base_url().'uploads/'.getThumb($val['image']).'"><br/>';
                                                            echo '<div class="delete_image">Delete</div></div>';
                                                        }
                                                    ?>                                                     
                                                </div>  
                                            </div>
                                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                                <!-- <div class="browse-thumb-img-edit-blog">
                                                    <label for="email">Upload Image      
                                                    </label>
                                                    <input type="file" ></input>                   
                                                </div>   -->
                                                <div class="input-file-container">  
                                                <input class="input-file" id="my-file" type="file" maxlength="50" name="attachedfile">
                                                <label tabindex="0" for="my-file" class="input-file-trigger">Upload Image</label>
                                                <span class="required">:</span>
                                                <input id="file_old" name="file_old" type="hidden" value="<?php echo $val['image']?>"/>
                                                <span class="file-return"></span>  
                                                </div>  
												<div class="wrap-editorial">
													<div class="every-form">
														<label class="label" for="author">Title
															<span class="required">:</span>
														</label>
														<input id="post-title" name="title" type="text" value="<?php echo $val['title']?>" size="30" aria-required='true' />
														<?php echo form_error('title', '<span class="error">', '</span>'); ?>
													</div> 
													<div class="every-form">
														<label for="author" class="label">Headline
															<span class="required">:</span>
														</label>
														<textarea class="" id="headline" name="headline"><?php echo $val['headline']?></textarea>
													</div> 
													<div class="every-form">
													<div class="label">
														<p>Content :</p>
													</div>        
													</div>
													<div class="editor-area">
														<textarea class="editor1" id="editor1" name="editor1" ><?php echo $val['content']?></textarea>
														<script>
															CKEDITOR.replace( 'editor1',
																	{
																skin : 'office2003',
																filebrowserBrowseUrl : '<?php echo base_url()?>assets/theme/js/ckfinder/ckfinder.html',
																 filebrowserImageBrowseUrl : '<?php echo base_url()?>assets/theme/js/ckfinder/ckfinder.html?Type=Images',
																 filebrowserFlashBrowseUrl : '<?php echo base_url()?>assets/theme/js/ckfinder/ckfinder.html?Type=Flash',
																 filebrowserUploadUrl : '<?php echo base_url()?>assets/theme/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
																 filebrowserImageUploadUrl : '<?php echo base_url()?>assets/theme/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
																 filebrowserFlashUploadUrl : '<?php echo base_url()?>assets/theme/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
																toolbar :
																[
																	['Source','-','Preview','Templates','Cut','Copy','Paste'],['Bold','Italic','Underline','Strike','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','NumberedList','BulletedList','Subscript','Superscript','-'],
																	'/',
																	['Link','Unlink','-','Image','Flash','Table','HorizontalRule','Smiley','SpecialChar'],['TextColor','BGColor','-','Font','FontSize','PasteFromWord']
																]
															});
														</script>
													</div>  
													<div class="every-form">
														<label for="author" class="label">Tags ( delimiter "," )
															<span class="required">:</span>
														</label>
														<input id="post-title" name="tags" type="text" value="<?php echo $val['tags']?>" size="30" aria-required='true' />
														<?php echo form_error('tags', '<span class="error" style="width: 50% !important; margin-left: 136px">', '</span>'); ?>
													</div>   
													
													<div class="every-form">
														<input name="submit" type="submit" id="submit" value="Submit Post" />
														<input id="id_blog" name="id_blog" type="hidden" value="<?php echo $val['id']?>"/>
													</div>
													<?php } ?>    
												</div>                                                          
                                            </div><!-- COL-LG-9 -->                                                                                 
                                        </div><!-- ROW -->   
                                    </form>
                                </div>
                                <!-- #respond -->
                            </div>    
                            <div style="clear:both"></div>  
 
                            <div style="clear:both"></div>  
                                    
                            <div style="clear:both"></div>  
                                                        
                        </div>
                        <!-- #product-523 -->
                        <div style="clear:both"></div>
                    </div>
                </div>                    
                </div>
            </div>
        </div>
    </section>
<!-- EDIT BLOG -->
