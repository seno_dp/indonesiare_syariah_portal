 <!-- ~~~=| Banner START |=~~~ -->
<link href="<?php echo base_url()?>assets/theme/css/blue.monday/jplayer.blue.monday.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/theme/js/jquery.jplayer.min.js"></script>
<section class="hp_banner_area section_padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="hp_banner_box">
                    <div class="hp_banner_left">
                        <div class="bl_single_news">
                            <img src="<?php echo base_url()?>uploads/blt-banner1.jpg" alt="Banner" />
                            <div class="bl_single_text">
                                <a href="http://192.168.1.10/foto/2017/rapRJ/index.html">
                                    <h4>Rapat Kerja RJ, Januari 2017</h4>
                                </a> <span><i class="fa fa-clock-o"></i> 3 month ago</span>
                            </div>
                        </div>
                        <div class="bl_single_news"> 
                            <img src="<?php echo base_url()?>uploads/blt-banner2.jpg" alt="Banner" />
                            <div class="bl_single_text">
                                <a href="http://192.168.1.10/foto/2017/syukhu2016/">
                                    <h4>Syukuran Hasil Usaha 2016</h4>
                                </a> <span><i class="fa fa-clock-o"></i> 1 years ago</span>
                            </div>
                        </div>
                        <div class="bl_single_news">
                            <img src="<?php echo base_url()?>uploads/bann2.jpg" alt="" />
                            <div class="bl_single_text">
                                <a href="http://192.168.1.10/foto/2017/pkscpasei/">
                                    <h4>PKS Client Portal Indonesia Re - AAI</h4>
                                </a> <span><i class="fa fa-clock-o"></i> 4 month ago</span>
                            </div>
                        </div>
                        <div class="bl_single_news"> 
                            <img src="<?php echo base_url()?>uploads/thumbnail-homepage-banner-dssa.jpg" alt="Banner DSSA" />
                            <div class="bl_single_text">
                                <a target="_BLANK" href="192.168.1.10/dss_new/">
                                    <h4>DSSA Gross Premi By Treaty/ Facultative</h4>
                                </a> <span><i class="fa fa-clock-o"></i> 4 hours ago</span>
                            </div>
                        </div>                        
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- ~~~=| Banner END |=~~~ -->

<!-- ~~~=| GRID2 START |=~~~ -->
<section class="main_news_wrapper">
    <div class="container">
        <div class="row gutter-10">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">  
                <!--START HOME_SIDEBAR-->
                <div class="home_sidebar">
                    <div class="all_news_right"><!-- all_news_right EVENT -->  
                        <div class="follow_us_side">
                            <h2><img width="20" height="20" src="<?php echo base_url() ?>assets/theme/images/re-dot.png"/>Reindo Event</h2>
              <span class="bar-red"></span>
                        </div>              
                        <!-- START REINDO EVENT -->   
                        <?php 
                        $i=0;
                        if($event->num_rows() > 0) { 
                            foreach($event->result_array() as $eval) { 
                             $i = ++$i; 
                            if($i == 1){
                        ?>
                        <div class="">
                            <div class="single_fs_news_left_text">
                                <div class="fs_news_left_img">
                                    <a href="<?php echo site_url('event/detail/'.$eval['id'].'/'.url_title($eval['title']))?>" rel="bookmark">
                                        <img width="285" src="<?php echo base_url()?>uploads/<?php echo getThumb($eval['image'])?>" alt="<?php echo $eval['title']?>"/>
                                    </a>
                                </div>                                  
                            </div>                          
                        </div>
                        <?php } ?>
                        <div class="fs_news_right">
                            <div class="single_fs_news_right_text"><!-- super-item -->
                            <!-- title event  -->
                                <h4><a href="<?php echo site_url('event/detail/'.$eval['id'].'/'.url_title($eval['title']))?>" rel="bookmark"><?php echo $eval['title']?></a></h4>
                            <!-- title event -->
                            <!-- intro-event -->
                                <div class="<?php echo ($i == 2) ? 'intro-event' : 'intro' ?>">
                                    <p><?php echo character_limiter($eval['headline'],190)?></p>
                                </div>
                            <!-- intro-event -->   
                            <!-- Readmore -->    
                                <div class="more-and-date">
                                    <p><a class="" href="<?php echo site_url('event/detail/'.$eval['id'].'/'.url_title($eval['title']))?>">Readmore </a><span><i class="fa fa-clock-o"></i>&nbsp;<?php echo date('M d, Y',strtotime($eval['date_event']))?></span></p>  
                                                                                                      
                                </div>
                            <!-- Readmore -->                                       
                            </div><!-- super-item -->                                                       
                        </div>
                        <!-- Readmore ALL-->   
                            <?php if($i == 3){?>
                                <div class="more-for-all-page">
                                    <a href="<?php echo site_url('event')?>" class="more-items">More Info</a>        
                                </div>   
                            <?php } ?>  
                        <!-- Readmore ALL -->                           
                        <?php } ?>
                        <?php }else{ ?>
                        <div class="">
                            <div class="single_fs_news_left_text">
                                <div class="fs_news_left_img">
                                    <a href="#" rel="bookmark">
                                        <img width="285" height="120" src="<?php echo base_url()?>uploads/2013/06/Event.jpg" class="attachment-feat-thumb wp-post-image" alt="pool-103014_640" width="285px" />
                                    </a>                                    
                                </div>
                            </div>                            
                        </div>
                        <div class="fs_news_right">
                            <div class="single_fs_news_right_text"><!-- super-item -->
                            <!-- title-bookmark -->
                                <h4><a href="blog-single-slider-post.html">Internet trolls take pleasure in making you suffer</a></h4>
                            <!-- title-bookmark -->
                            <!-- intro-event -->
                                <div class="intro-of-event">
                                    <p>Fusce aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique ut, consectetur non erat.</p>
                                </div>
                            <!-- intro-event -->   
                            <!-- Readmore -->    
                                <p><a class="gad_color" href="">Readmore </a></p>
                            <!-- Readmore -->
                            <!-- date-post -->    
                                <p>| <i class="fa fa-clock-o"></i> 1 hour ago </p>
                            <!-- date-post -->    
                            </div><!-- super-item -->
                        </div>
                        <div class="fs_news_right">
                            <div class="single_fs_news_right_text"><!-- super-item -->
                            <!-- title-bookmark -->
                                <h4><a href="blog-single-slider-post.html">Internet trolls take pleasure in making you suffer</a></h4>
                            <!-- title-bookmark -->
                            <!-- intro-event -->
                                <div class="intro-event">
                                    <p>Fusce aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique ut, consectetur non erat.</p>
                                </div>
                            <!-- intro-event -->   
                            <!-- Readmore -->    
                                <p><a class="gad_color" href="">Readmore </a></p>
                            <!-- Readmore -->
                            <!-- date-post -->  
                            <a href="#">More Info »</a> 
                                <p><i class="fa fa-clock-o"></i> 1 hour ago </p>
                            <!-- date-post -->    
                            </div><!-- super-item -->
                        </div>
                        <?php } ?> 
                        <!-- END REINDO EVENT -->                                       
                    </div><!-- all_news_right EVENT -->                                  
                </div><!--END HOME_SIDEBAR-->    
                <div class="home_sidebar"><!--START HOME_SIDEBAR-->
                    <!-- START BLOG ITEM -->            
                    <div class="all_news_right"><!-- all_news_right EVENT -->
                            <div class="follow_us_side">
                                <h2><img width="20" height="20" src="<?php echo base_url() ?>assets/theme/images/green-dot.png"/>Blog Roll</h2>
                              <span class="bar-green"></span>
                            </div>              
                            <div class="blog-of-item">
                                <?php if($blog->num_rows() > 0) { ?>
                                <ul>
                                <?php foreach ($blog->result_array() as $bval) { ?>
                                        <li>
                                            <a href="<?php echo site_url('blog/detail/'.$bval['id'].'/'.url_title($bval['title']))?>"><?php echo word_limiter($bval['title'],30)?></a>
                                        </li>
                                <?php } ?>
                                </ul>
                                <div class="more-for-all-page">
                                    <a href="<?php echo site_url('blog')?>" class="more-items">More Info</a>
                                </div>                                
                                    
                                <?php }else{ ?>
                                <ul>
                                    <li>
                                        <a href="#">aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat.</a>
                                    </li>
                                    <li>
                                        <a href="#">Half Day aliquet dolor sed dolor feugiat fermentum</a>
                                    </li>
                                    <li>
                                        <a href="#">Donec dignissim tellus non ante volutpat Meeting Reindo</a>
                                    </li>
                                </ul>
                                <div class="more-for-all-page">
                                    <a href="#" class="more-items">More Info</a>
                                </div>
                                <?php } ?>
                            </div>
                        <!-- END BLOG ITEM -->               
                    </div><!-- all_news_right EVENT -->                     
                </div><!--END HOME_SIDEBAR-->  

                <!--VIDEO PLAYER-->  
                <div class="home_sidebar"><!--START HOME_SIDEBAR--> 
                  <div class="all_news_right">
                      <div class="follow_us_side">
                          <h2><img width="20" height="20" src="<?php echo base_url() ?>assets/theme/images/blue-dot.png"/>Videos</h2>
                        <span class="bar-blue"></span>
                      </div> 
                      <div class="fs_news_left_img">
                        <!--<video width="100%" controls>
                          <source src="<?php echo base_url() ?>uploads/20161110172538.hut19jogja.mp4" type="video/mp4">
                        </video>-->    
						
						<script type="text/javascript">
						$(document).ready(function(){

						  $("#jquery_jplayer_1").jPlayer({
							ready: function () {
							  $(this).jPlayer("setMedia", {
								title: "<?php echo $qval['title']?>",

								m4v: "<?php echo base_url()?>uploads/20161110172632.REINDO_2015_YOGYAKARTA.mp4",

								poster: "<?php echo base_url()?>uploads/20161110173651.thumb.jpg"
							  });
							},
							swfPath: "js",
							supplied: "webmv, ogv, m4v",
							size: {
							  width: "100%",
							  height: "auto",
							  cssClass: "jp-video-respond"
							},
							smoothPlayBar: true,
							keyEnabled: true,
							remainingDuration: true,
							toggleDuration: true
						  });
						});
						//]]>
						</script>
						<div id="jp_container_1" class="jp-video">
						  <div class="jp-type-single">
							<div id="jquery_jplayer_1" class="jp-jplayer"></div>
							<div class="jp-gui">
							  <div class="jp-video-play jp-video-play-homepage">
								<a href="javascript:;" class="jp-video-play-icon" id="play-icon-home" tabindex="2">play</a>
							  </div>
							  <div class="jp-interface">
								<div class="jp-progress">
								  <div class="jp-seek-bar">
									<div class="jp-play-bar"></div>
								  </div>
								</div>
								<div class="jp-current-time"></div>
								<div class="jp-duration"></div>
								<div class="jp-controls-holder" style="padding-top:10px;">
								  <ul class="jp-controls" style="margin-left: 20px;">
									<li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
									<li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
									<li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
									<li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute" style="left: 115px;margin-top: 10px;">mute</a></li>
									<li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute" style="left: 115px;margin-top: 10px;">unmute</a></li>
									<li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume" style="left: 190px;margin-top: 10px;">max volume</a></li>
								  </ul>
								  <div class="jp-volume-bar" style="left: 135px;margin-top: 10px;">
									<div class="jp-volume-bar-value"></div>
								  </div>
								  <ul class="jp-toggles" style="width: auto;">
									<li><a href="javascript:;" class="jp-full-screen" tabindex="1" title="full screen">full screen</a></li>
									<li><a href="javascript:;" class="jp-restore-screen" tabindex="1" title="restore screen">restore screen</a></li>
									<li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
									<li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
								  </ul>
								</div>
								<div class="jp-details">
								  <ul>
									<li><span class="jp-title"></span></li>
								  </ul>
								</div>
							  </div>
							</div>
							<div class="jp-no-solution">
							  <span>Update Required</span>
							  To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
							</div>
						  </div>
						</div>
						<br/>
                      </div>                                           
                  </div>    
                </div><!--END HOME_SIDEBAR--> 
                <!--VIDEO PLAYER-->     

            </div><!-- END COL 4 -->
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="home_sidebar">
                    <div class="all_news_right">
                        <div class="follow_us_side">
                            <h2><img width="20" height="20" src="<?php echo base_url() ?>assets/theme/images/blue-dot.png"/>Latest Update</h2>
                          <span class="bar-blue"></span>
                        </div>    
                        <?php if($article->num_rows() > 0) { ?>
                        <?php foreach($article->result_array() as $arval) {?>
                        <?php if($arval['image']) {?>   
                        <!-- IMAGE LATEST --> 
                        <div class="">
                            <div class="single_fs_news_left_text">
                                <div class="fs_news_left_img">
                                    <a href="<?php echo site_url($arval['module_detail'].$arval['id'].'/'.url_title($arval['title']))?>" rel="bookmark">
                                        <img width="300" src="<?php echo base_url().'uploads/'.getThumb($arval['image'])?>" alt="<?php echo $arval['title']?>"/>
                                         <!--<img width="300" src="http://localhost/reindo_intranet/uploads/20161108135510.POSTER_final_thumb-two.jpg" alt="<?php echo $arval['title']?>"/>-->
                                    </a>
                                </div>
                            </div>
                        </div>
                                <div class="fs_news_right">
                                    <div class="single_fs_news_right_text"><!-- super-item -->
                                        <h4><a href="<?php echo site_url($arval['module_detail'].$arval['id'].'/'.url_title($arval['title']))?>" rel="bookmark"><?php echo $arval['title']?></a></h4>
                                        <div class="intro">
                                            <p><?php echo character_limiter($arval['headline'],190)?></p>
                                        </div>  
                                        <!-- MORE AND DATE -->     
                                            <div class="more-and-date">
                                                <p><a class="gad_color" href="<?php echo site_url($arval['module_detail'].$arval['id'].'/'.url_title($arval['title']))?>" rel="bookmark" title="Read more">Readmore </a><span><i class="fa fa-clock-o"></i>&nbsp;<?php echo date('M d, Y',strtotime($arval['create_date']))?></span></p>  
                                            </div>                                        
                                        <!-- MORE AND DATE -->                                         
                                    </div>                          
                                </div>                                 
                        <?php } ?>
                        <?php } ?>
                        <?php } ?>
                        <!-- IMAGE LATEST -->               
                    </div>                    
                </div>
            </div><!-- END COL 4 -->
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="home_sidebar">
                    <div class="all_news_right">
                        <div class="follow_us_side">
                            <h2><img width="20" height="20" src="<?php echo base_url() ?>assets/theme/images/green-dot.png"/>Pengumuman</h2>
              <span class="bar-green"><span>
                        </div>
                        <div class="blog-of-item">
                            <ul>
                                <?php if($announcement->num_rows() > 0) { ?>
                                <?php foreach($announcement->result_array() as $anval){ ?>
                                    <li>
                                    <h4><?php echo $anval['title']?></h4>
                                    <p><?php echo ((strlen($anval['headline']) == 0) || ($anval['headline'] == 'NULL')) ? $anval['title'] : $anval['headline']?>
                                    </p>
                                    <div class="more-and-date">
                                        <p><a href="<?php echo site_url('announcement/detail/'.$anval['id'].'/'.url_title($anval['title']))?>" title="Read more">read more</a><span><i class="fa fa-clock-o"></i>&nbsp;<?php echo date('M d, Y',strtotime($anval['date_announcement']))?></span></p>
                                    </div>                                    
                                </li>
                                <?php } ?>
                                <?php }else{ ?>
                                <li>
                                    <h4>Title dummmy here</h4>
                                    <p>Fusce aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique
                                    </p>
                                    <div class="more-and-date">
                                        <p><a class="" href="#">Readmore </a><span><i class="fa fa-clock-o"></i>&nbsp;Aug 02, 2016</span></p>  
                                    </div>                                    
                                </li>
                                <li>
                                    <h4>Title dummmy here</h4>
                                    <p>Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique ut, consectetur non erat. Donec dignissim tellus
                                    </p>
                                    <div class="more-and-date">
                                        <p><a class="" href="#">Readmore </a><span><i class="fa fa-clock-o"></i>&nbsp;Aug 02, 2016</span></p>  
                                    </div>                                    
                                </li>
                                <?php } ?>                   
                            </ul>                            
                        </div>                             
                    </div>                  
                </div>
                <!--NEXT EVENT-->
                <!--<div class="home_sidebar">
                    <div class="all_news_right">
                        <div class="follow_us_side">
                            <h2>Reindo Next Event</h2>
                        </div>
                    <div class="calender-box-list">
                        <ul>
                            <?php if($calendar->num_rows() > 0) {?>
                            <?php foreach($calendar->result_array() as $cval){?>
                            <li>
                                <div class="fs_news_right v-middle-calendar">                   
                                    <span class="date-box-blues"><?php echo strtoupper(date('M d',strtotime($cval['date_calendar'])))?></span>                        
                                    <p><a href="<?php echo site_url('calendar/detail/'.$cval['id'].'/'.url_title($cval['title']))?>"><?php echo character_limiter($cval['title'],50)?></a></p>                
                                </div>                                   
                            </li>
                            <?php } ?>
                            <?php }else{ ?>
                            <li>
                                <div class="fs_news_right v-middle-calendar">                   
                                    <span class="date-box-blues">DES 21</span>                        
                                    <p><a href="#">Title Here</a></p>                
                                </div>                            
                            </li>
                            <li>
                                <div class="fs_news_right v-middle-calendar">                   
                                    <span class="date-box-blues">DES 21</span>                        
                                    <p><a href="#">Title Here</a></p>                
                                </div>                            
                            </li>
                            <li>
                                <div class="fs_news_right v-middle-calendar">                   
                                    <span class="date-box-blues">DES 21</span>                        
                                    <p><a href="#">Title Here</a></p>                
                                </div>                            
                            </li>
                            <?php }?>
                        </ul>
                        <div class="more-for-all-page">
                            <a href="<?php echo site_url('calendar')?>" class="more-items"" class="more-items">More Info</a>
                        </div>                        
                    </div>                                                  
                    </div>                  
                </div>-->
                <!--NEXT EVENT-->
                <!--MEETING ROOM-->
                <div class="home_sidebar">
                    <div class="all_news_right">
                        <div class="follow_us_side">
                            <h2><img width="20" height="20" src="<?php echo base_url() ?>assets/theme/images/blue-dot.png"/>Ruang Meeting</h2>
              <span class="bar-blue"><span>
            </div>
                        <div class="meeting-box">
                            <ul>
                                <?php if($meeting->num_rows() > 0) {?>
                                    <?php foreach($meeting->result_array() as $mval) {?>
                                        <li>
                                            <div class="date-box">
                                                <h4><?php echo strtoupper(date('M d',strtotime($mval['date_meeting'])))?></h4>
                                            </div>
                                            <div class="meeting-info">
                                                <strong>Ruang <?php echo $mval['title']?> | <?php echo $mval['time_meeting']?></strong><br/>
                                                <span></span><?php echo $mval['headline']?>
                                            </div>
                                            <div style="clear: both"></div>
                                        </li>
                                    <?php } ?>
                                <?php }else{ ?>
                                <li>
                                <div class="fs_news_right v-middle-calendar">                   
                                    <span class="date-box-blues">MM DD</span>                        
                                    <p><a href="#"><strong>Belum ada jadwal meeting</strong> | 00:00</a>
                                    <br/><span>Dummy text for content</span>
                                    </p>
                                </div> 
                                    <div style="clear: both"></div>
                                </li>
                                <?php } ?>
                            </ul>                            
                        </div>
                    </div>
                </div>                    
                <!--MKEETING ROOM-->
                <!--ABSENSI-->
                <div class="home_sidebar">
                    <div class="all_news_right">
                        <div class="follow_us_side">
                            <h2><img width="20" height="20" src="<?php echo base_url() ?>assets/theme/images/re-dot.png"/>Absensi Karyawan</h2>
              <span class="bar-red"><span>
            </div>
                        <div class="absensi-list">
                            <ul>
                                <?php if($absence->num_rows() > 0) { ?>
                                    <?php foreach($absence->result_array() as $abval) {?>
                                        
                                        <li>
                                            <!-- <a href="<?php echo site_url('absence/detail/'.$abval['id'].'/'.url_title($abval['title']))?>"><?php echo $abval['title']?></a> -->
                                            <a href="<?php echo site_url('absence/download/'.$abval['id'].'/'.url_title($abval['title']))?>" title="Download <?php echo $abval['title']?>"><?php echo $abval['title']?></a>
                                        </li>
                                        
                                    <?php } ?>
                                <?php }else{ ?>
                                <li>
                                    <a href="#"><img src="<?php echo base_url()?>assets/theme/images/employe-icon.png" alt="employe absence">Title absence here</a>
                                </li>
                                <?php } ?>
                            </ul>
                            <div class="more-for-all-page">                                    
                                <a href="<?php echo site_url('absence')?>" class="more-items">More Info</a>
                            </div>                          
                        </div>                        
                    </div>                        
                </div>
                <!--ABSENSI-->
                <!--VOTE-->
                <div class="home_sidebar">
                    <div class="all_news_right">
                        <?php 
                            if($vote->num_rows() > 0) {
                                $voting = $vote->row_array();
                        ?>
                        <form id="vote_form" action="<?php echo site_url('home/vote')?>" method="POST">
                            <div class="follow_us_side">
                                <h2><img width="20" height="20" src="<?php echo base_url() ?>assets/theme/images/green-dot.png"/>Berikan Masukan Anda</h2>
                <span class="bar-green"><span>
              </div>       
                            <p><?php echo $voting['content']; ?></p>    
                            <div class="custome-vertical-rb">
                                <div class="rb-vote">
                                    <input type="radio" name="answerit" value='option1' id="test1">
                                    <label for="test1"><?php echo $voting['option1']?></label>        
                                </div>
                            </div> 

                            <div class="custome-vertical-rb">
                                <div class="rb-vote">
                                    <input type="radio" id="test2" name="answerit" value='option1' id="test2">
                                    <label for="test2"><?php echo $voting['option2']?></label>        
                                </div>
                            </div> 

                            <div class="custome-vertical-rb">
                                <div class="rb-vote">
                                    <input type="radio" name="answerit" value='option3' id="test3">
                                    <label for="test3"><?php echo $voting['option3']?></label>        
                                </div>
                            </div> 

                            <div class="custome-vertical-rb">
                                <div class="rb-vote">
                                    <input type="radio" id="test4" name="answerit" value='option4' id="test4">
                                    <label for="test4"><?php echo $voting['option4']?></label>        
                                </div>
                            </div> 

                            <div class="wrap-btn comments_form">
                                <button class="btn-default-vote" type="button">Batal</button>
                                <button class="btn-default-vote" type="submit">Submit</button>
                                <input type="hidden" name="id_polling" value="<?php echo $voting['id'];?>">
                                <input type="hidden" name="title" value="<?php echo $voting['title'];?>">             
                                <?php
                                $filterpollingmember = array("id_polling"=>"where/".$voting['id'],"id_member"=>"where/".$this->session->userdata('user_id_sess'));
                                $pollingmember = getAll('kg_polling_member',$filterpollingmember);
                                if($pollingmember->num_rows() > 0){
                                    echo "<a href='".site_url('vote/detail/'.$voting['id'].'/'.url_title($voting['title']))."' class='view_polling'>View voting</a>";
                                }
                                ?>
                            </div>                            
                                 
                        <!-- <div class="vote-item">
                            <div class="follow_us_side">
                                <h2>Berikan Masukan Anda</h2>
                            </div>
                            <p><?php echo $voting['content']; ?></p>
                            
                            <span class="optional">
                                <input type="radio" name="answerit" value='option1'><?php echo $voting['option1']?>
                            </span>
                            <span class="optional">
                                <input type="radio" name="answerit" value='option2'><?php echo $voting['option2']?>
                            </span>
                            <?php 
                                if($voting['option3']){
                                    echo "<span class='optional'>";
                                    echo "<input type='radio' name='answerit' value='option3'>".$voting['option3'];
                                    echo "</span>";
                                }
                            ?>
                            
                             <?php 
                                if($voting['option3']){
                                    echo "<span class='optional'>";
                                    echo "<input type='radio' name='answerit' value='option4'>".$voting['option4'];
                                    echo "</span>";
                                }
                            ?>
                            <div class="wrap-btn">
                                <?php
                                $filterpollingmember = array("id_polling"=>"where/".$voting['id'],"id_member"=>"where/".$this->session->userdata('user_id_sess'));
                                $pollingmember = getAll('kg_polling_member',$filterpollingmember);
                                if($pollingmember->num_rows() > 0){
                                    echo "<a href='".site_url('vote/detail/'.$voting['id'].'/'.url_title($voting['title']))."' class='btn-default view_polling'>View voting</a>";
                                }
                                ?>
                                <button class="btn-default" type="button">Batal</button>
                                <button class="btn-default" type="submit">Submit</button>
                                <input type="hidden" name="id_polling" value="<?php echo $voting['id'];?>">
                                <input type="hidden" name="title" value="<?php echo $voting['title'];?>">
                            </div>
                            <div style="clear:both"></div>
                        </div> -->
                        </form>
                        <?php } ?>                   
                    </div>
                </div> 
                <!--VOTE-->   
                <div class="home_sidebar">
                    <div class="all_news_right" style="padding-bottom:6px;">
                        <!-- EMPLOYEE SELF-SERVICE -->
                        <div class="self-service">
                            <h3><span class="h3-orange">EMPLOYEE</span> SELF-SERVICE</h3>
                            <ul>
                                <li><a data-toggle="modal" href="#vila-form-mdl" class="">Booking Vila</a></li>
                                <li><a class="opencar" data-toggle="modal" href="#car-form-mdl">Peminjaman Mobil Perusahaan</a></li>
                                <li><a href="<?php echo base_url();?>id/pengaduan">Pengaduan</a></li>
                            </ul>
                        </div>                        
                    </div>  
                </div>                                  
            </div><!-- END COL 4 -->                        
        </div>
    </div>  
</section>
<!-- ~~~=| GRID2 END |=~~~ --> 

<!--  BOOKING VILA -->
<div class="modal" id="vila-form-mdl">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Booking Villa</h4>
        </div>
        <div class="modal-body">
            <?php   
                if($this->session->userdata('user_id_sess')) {
                $id = $this->session->userdata('user_id_sess');
                $filter = array(
                        "id"=>"where/".$id,
                    );
                $member = GetAll('kg_member',$filter);
                if($member->num_rows() > 0){
                $v = $member->row_array();
            ?>
            <div class="comments_form">
                <div class="contact_cc_name">
                    <form class="rz-form2" method="post" action="<?php echo base_url();?>id/home/booking_villa">
                        <input type="hidden" name="member" value="<?php echo $v['id'];?>">
                        <input type="hidden" name="session_villa_id" value="<?php echo getLastSessionVilla();?>">
                <?php }} ?>
                        <div class="info-villa">
                            *Mohon maaf, pengajuan tanggal booking villa hanya dapat dilakukan 60 hari dari tanggal sekarang !
                        </div>
                        <div class="form-rent-car" style="margin-right:6px">
                            <label>Check In</label>
                            <input type="text" class="tgl-villa datebook" name="start_date" placeholder="YYYY - MM - DD 00:00" readonly="readonly" required>
                        </div>
                        </div>
                        <div class="form-rent-car">
                        <label>Check Out</label>
                        <input type="text" class="tgl-villa datebook" id="outpicker" name="end_date" placeholder="YYYY - MM - DD 00:00" readonly="readonly" required>
                        </div>
                        <div class="form-rent-car">
                            <h6>Tipe Villa</h6>
                            <label class="select">
                            <?php echo form_dropdown('tipe_villa', $opt_villa, set_value('tipe_villa'), 'required');?>
                            <i></i>
                            </label>                                                 
                        </div>
                        <div class="form-rent-car">
                            <label>Keperluan / Acara</label>
                            <input type="text" name="title" value="<?php echo set_value('title');?>" required>
                        </div>
                        <div class="form-rent-car">
                            <label>Jumlah Peserta</label>
                            <input type="text" name="jumlah" value="<?php echo set_value('jumlah');?>" required>
                        </div>
                        <div class="submit-rent-car">
            <br/>
                            <button class="btn-sbmt" type="submit">Pesan Sekarang</button>
                        </div>                        
                    </form>                    
                </div>                
            </div>
        </div>
      </div>
    </div>
</div>
<!--  BOOKING VILA -->

<!-- RENT CAR  -->
<div class="modal" id="car-form-mdl">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h4 class="modal-title">Peminjaman Mobil</h4>
        </div>
        <div class="modal-body">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <?php   
                        if($this->session->userdata('user_id_sess')) {
                        $id = $this->session->userdata('user_id_sess');
                        $filter = array(
                                "id"=>"where/".$id,
                            );
                        $member = GetAll('kg_member',$filter);
                        if($member->num_rows() > 0){
                        $v = $member->row_array();
                    ?>
                    <form method="post" action="<?php echo base_url();?>id/home/pengajuan_mobil">
                        <div class="form-rent-car">
                            <label>Nama</label>
                            <input type="text" name="name" value="<?php echo $v['title'];?>" disabled>
                            <input type="hidden" name="member" value="<?php echo $v['id'];?>">                        
                        </div>
                <?php }} ?> 
                        <div class="form-rent-car">
                            <h6>Keperluan</h6>
                            <label class="select">
                            <?php echo form_dropdown('keperluan', array('Dinas'=>'Dinas','Pribadi'=>'Pribadi'), set_value('keperluan'), 'required style="width:100%;"');?> 
                            <i></i>
                            </label>                                            
                        </div>
                        <div class="form-rent-car">
                            <label>Tanggal dan waktu peminjaman</label>
                            <input type="text" class="tgl-pengajuan" name="date_pinjam" value="<?php echo gmdate('Y-m-d H:i', time()+60*60*7);?>" required>
                        </div>
                        <div class="form-rent-car">
                            <label>Tujuan</label>
                            <input type="text" class="" name="tujuan" value="<?php echo set_value('tujuan');?>" required>
                        </div>
                        
                        <div class="form-rent-car">
                            <label>Catatan</label>
                            <textarea name="description" required><?php echo set_value('description');?></textarea>
                        </div>
                        <div class="submit-rent-car">
                            <button class="btn-sbmt" type="submit">AJUKAN</button>
                        </div>
                    </form>                    
                </div>
            </div>
        </div>
      </div>
    </div>
</div>
<!-- RENT CAR  -->

<script type="text/javascript">
    $(document).ready(function() {
        $('#openBtn').click(function(){
            $('#vila-form-mdl').modal({show:true})
        }); 

        $('#openBtn').click(function(){
            $('#car-form-mdl').modal({show:true})
        });       

        $('.rz-form').validate({
            rules: {
                date_pinjam: "required", //pengajuan mobil
                description: "required",
                keperluan: "required",
                tujuan: "required"
            },
            messages: { 
                date_pinjam: "*Tanggal dan waktu peminjaman is required",
                description: "*Catatan peminjaman is required",
                keperluan: "*Keperluan is required",
                tujuan: "*Tujuan is required"
                }
        });
        
        $('.rz-form2').validate({
            rules: {
                start_date: "required",
                end_date: "required",
                tipe_villa: "required",
                title: "required",
            },
            messages: { 
                start_date: "*Check In is required",
                end_date: "*Check Out is required",
                tipe_villa: "*Tipe Villa is required",
                title: "*Keperluan/Acara is required",
                jumlah: "*Jumlah Peserta is required"
                }
        });

    });

    $(":submit").click(function(e){

        if(!$(this).closest("form").valid()){
            e.preventDefault();
            var errText="";
            $(".error").each(function(i,j){
             errText+=$(j).text()+"<br/>";           
            });
            $("#errDiv").html(errText).css({color:'Red'});  
            return false;
            }
        $("#errDiv").empty();
        
    });

    //Datetimepicker
    var tgl_pinjam = new Date();

    tgl_pinjam.setDate(tgl_pinjam.getDate() + 60);
    
    var dd = (tgl_pinjam.getDate()<10)?"0"+tgl_pinjam.getDate():tgl_pinjam.getDate();
    //var mm = tgl_pinjam.getMonth() + 1;
    var mm = ((tgl_pinjam.getMonth() + 1)<10) ?"0"+(tgl_pinjam.getMonth() + 1):(tgl_pinjam.getMonth() + 1);
    var y = tgl_pinjam.getFullYear();
    
    var tglBolehpinjam = y + '-' + mm + '-' + dd;
    
    jQuery('.tgl-villa').datetimepicker( 
    {
        startDate: tglBolehpinjam,
        minDate: tglBolehpinjam
    });
    
    jQuery('.tgl-pengajuan').datetimepicker({
    });

</script>    

