<!-- DETAIL PAGE BOARD NEWS -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="cc_single_post">
                    <?php if($article->num_rows() > 0) {?>
                    <?php $val = $article->row_array();?>
                    <div class="" id="">
                        <!-- REINDO BREAD CRUMB -->
                        <div class="breadcum_c_left">
                            <a href="<?php echo site_url('home')?>">Home</a> / 
                            <a href="<?php echo site_url($controller_name)?>">Board News</a> /
                            <?php echo $val['title']?>
                        </div>
                        <?php if($val['upper_deck']) {?>
                        <div class=updeck>
                            <h3><?php echo $val['upper_deck']?></h3> 
                        </div>
                        <?php }?>
                        <h2>
                            <?php echo $val['title']?>
                        </h2>
                        <?php if($val['lower_deck']) {?>
                        <div class="quote-article">
                            <p><?php echo $val['lower_deck']?></p>
                        </div>
                        <?php } ?>
                         <?php if($val['thumbnail']) {?>
                         <?php if($val['is_thumbnail'] == 1) {?>
                        <div class="image-thumb">
                            <img src="<?php echo base_url()?>uploads/<?php echo $val['thumbnail']?>" style="max-width:300px;"/>
                        </div> 
                        <?php } ?>
                        <?php } ?>
                        <div class="post_text">
                            <?php echo $val['content']?>
                        </div>
                            <div class="post_meta">
                                <ul>
                                    <li>
                                        <a><i class="fa fa-clock-o"></i><?php echo date('M d, Y',strtotime($val['create_date']))?></a>
                                    </li>
                                    <li><a href="">by </a></li>             
                                    <li>
                                        <a href=""><i class="fa fa-user"></i><?php echo GetUserName('kg_admin','name',$val['create_user_id'])?></a>
                                    </li>                   
                                </ul>
                                <div class="social_tags">
                                    <div class="social_tags_left">
                                        <p>Tags :</p>
                                        <ul>
                                            <li><a href=""><?php echo explodetags($val['tags']) ?></a></li>
                                        </ul>
                                    </div>
                                    <div class="social_tags_right">
                                        <ul>
                                            <li class="facebook"><a class="fa fa-facebook" href=""></a></li>
                                            <li class="twitter"><a class="fa fa-twitter" href=""></a></li>
                                            <li class="google-plus"><a class="fa fa-google-plus" href=""></a></li>
                                            <li class="linkedin"><a class="fa fa-linkedin" href=""></a></li>
                                        </ul>
                                    </div>
                                </div>                                  
                            </div>

                    </div>
                    <?php }?>


                    <?php if($rel_link->num_rows() > 0) {?>
                    <!-- RELATED POSTS -->
                    <div class="box">
                        <div class="related-title">
                            <b>Related Posts
                                <span class="arrows">&raquo;</span>
                            </b>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div id="related-posts">
                        <ul>
                            <?php foreach($rel_link->result_array() as $rval) { ?>
                            <li>
								<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="title-related">
										<?php if($rval['thumbnail']) {?>
										<a href="<?php echo site_url($controller_name.'/detail/'.$rval['id'].'/'.url_title($rval['title']))?>" rel="bookmark">
											<img class="img-responsive" src="<?php echo base_url()?>uploads/<?php echo getThumb($rval['thumbnail'])?>" />
										</a>									
									</div>
								</div>
                                <?php } ?>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
									<div class="title-related">
									    <a href="<?php echo site_url($controller_name.'/detail/'.$rval['id'].'/'.url_title($rval['title']))?>" rel="bookmark"><?php echo $rval['title']?></a>
									</div>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php } ?>                                    
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="">
                    <div class="home_sidebar">
                        <div class="follow_us_side">
                            <?php echo $this->load->view('detail_sidebar')?>     
                        </div>
                    </div>                      
                </div>
            </div>
        </div>
    </div>

</section>
<!-- DETAIL PAGE BOARD NEWS-->
