<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">

<head profile="http://gmpg.org/xfn/11">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Intranet PT. Reasuransi Internasional Indonesia</title>
    <link rel="shortcut icon" href="#" />
    <!-- <link rel="stylesheet" href="<?php echo base_url()?>assets/theme/css/jquery-ui.css" type="text/css" /> -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/theme/style.css" type="text/css" />
    <style type='text/css'>
        
    </style>
   
</head>
<body class="home blog">

    <div class="outer_wrap">
      <table style="padding: 125px 30px;">
        <tr style="">
            <td style="padding-bottom: 40px;font-size:18px;" colspan="2">
              <table>
                <tr>
                  <td style="width: 350px"><?php echo 'No. : '.$no_surat;?></td>
                  <td style="width: 320px;text-align:right"><?php echo 'Jakarta, '.$create_date?></td>
                </tr>
              </table>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding-bottom: 40px;font-size:16px;">
              Kepada Yth.<br/>
              <?php echo $nama_peminjam?><br/>
              Di Jakarta
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding-bottom: 40px;font-size:16px;">
              Dengan hormat,
            </td>
        </tr>
        <tr>
            <td colspan="2" style="width: 680px;padding-bottom:40px;">
              <h2 style="text-align:center;color:#000;"><u><b>Perihal : <?php echo $title?></b></u></h2>
              <p style="font-size:16px;padding-bottom:0px;">Sehubungan dengan permohonan untuk menggunakan wisma di BP. Arga Sonya, dengan ini kami beritahukan bahwa 
                PT Reasuransi Internasional Indonesia berkenan memberikan izin penggunaan wisma sebagai berikut :</p>
              <table style="font-size:16px;">
                <tr>
                  <td>Wisma</td>
                  <td colspan="3"> : <?php echo $villa_title?></td>
                </tr>
                <tr>
                  <td>Tanggal</td>
                  <td> : Check In </td>
                  <td><?php echo date('d M Y',strtotime($start_date))?></td>
                  <td> Pukul <?php echo date('H:i',strtotime($start_date))?> WIB</td>
                </tr>
                <tr>
                  <td></td>
                  <td> : Check Out </td>
                  <td><?php echo date('d M Y',strtotime($end_date))?></td>
                  <td> Pukul <?php echo date('H:i',strtotime($end_date))?> WIB</td>
                </tr>
                <tr>
                  <td colspan="4"><b><?php echo $remark_approve?></b></td>
                </tr>
              </table>
              <p style="font-size:16px;">Demikian kami sampaikan, dan selamat menikmati fasilitas yang tersedia.</p>
            </td>
        </tr>
        <tr>
          <td style="width: 350px"></td>
          <td style="text-align:center;font-size:16px;">
            Hormat Kami,<br/>
            <b>PT Reasuransi Internasional Indonesia</b><br/>
            General Affair Group<br/><br/><br/><br/><br/>
            <b><u>Endah Triwulandari</u></b><br/>
            Group Head
          </td>
        </tr>
      </table>
        
    </div>
    
</body>
<meta http-equiv="content-type" content="text/html;charset=UTF-8">
</html>