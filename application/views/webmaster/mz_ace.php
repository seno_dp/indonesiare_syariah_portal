<html>
<body>
<script src="<?php echo base_url();?>assets/mz_js/jquery.min.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo base_url();?>assets/mz_js/ace/ace.js" type="text/javascript" charset="utf-8"></script>
<script src="<?php echo base_url();?>assets/mz_js/ace/mode-php.js" type="text/javascript" charset="utf-8"></script>
<p class='alert-save' style="margin:0px 5px 0px 0px;float:left;color:green;font-weight:bold;display:none;">Saving </p>
<p style="margin:5px;">File : <?php echo $nmfile;?></p>
<div id="editor" style="height: 300px; width: 99%;margin-bottom:15px;"><?php echo $content;?></div>

<script>
window.onload = function() {
    var editor = ace.edit("editor");
    var PHPMode = require("ace/mode/php").Mode;
    editor.getSession().setMode(new PHPMode());
    
    editor.commands.addCommand({
		    name: 'myCommand',
		    bindKey: {
		        win: 'Ctrl-M',
		        mac: 'Command-M',
		        sender: 'editor'
		    },
		    exec: function(env, args, request) {
		        saveFile();
		    }
		})
		
		saveFile = function() {
		    var contents = editor.getSession().getValue();
				$.post("<?php echo base_url();?>index.php/webmaster/mz_load/save_ace/<?php echo $nmfile;?>", 
		            {contents: contents },
		            function() {
		                   $('.alert-save').fadeIn(200);
		            }
		    );
		};
};
</script>
</body>
</html>