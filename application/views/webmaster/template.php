<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $cms_name;?></title>
    <meta name="description" content="webmazhters http://www.mazhters.com/">
    <meta name="author" content="Mazhters Irwan">

    <!-- script -->
		<script src="<?php echo base_url();?>assets/mz_js/jquery.min.js"></script>
		<script src="<?php echo base_url();?>assets/mz_js/mz_custom.js"></script>
		<script src="<?php echo base_url();?>assets/mz_js/jquery-ui-1.7.1.custom.min.js"></script>
		<script src="<?php echo base_url();?>assets/mz_js/jquery.effects.core.js"></script>
		<script src="<?php echo base_url();?>assets/mz_js/validate_new/validate.js"></script>
		<script src="<?php echo base_url();?>assets/mz_js/bootstrap/bootstrap-alerts.js"></script>
		<script src="<?php echo base_url();?>assets/mz_js/bootstrap/bootstrap-modal.js"></script>
		<script src="<?php echo base_url();?>assets/mz_js/datepicker.js"></script>
		
		<!-- Menu -->
		<script src="<?php echo base_url();?>assets/mz_js/menu.js" ></script>
		
		<!-- styles -->
    <link href="<?php echo base_url();?>assets/mz_style/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/mz_style/custom.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/mz_style/datepicker.css" rel="stylesheet">
    
    <!-- Icons -->
    <!--<link rel="shortcut icon" href="<?php echo base_url();?>assets/mz_images/favicon.ico">-->
    
    <!-- Custom Script JS -->
    <script>
    $(document).ready(function() {
    	var temp_id = document.getElementById('temp_id');
			$('input.delete').click(function(e) 
			{
				var parent = $(this).parent('td').parent('tr');
				var val = $(this).attr('value');
				var frm = $("#"+parent.attr('id')).children('td');
				if($(this).attr('checked'))
				{
					frm .css({backgroundColor:'#F90'});					
					temp_id.value += val+"-";
				}
				else
				{
					frm .css({backgroundColor:''});
					temp_id.value = temp_id.value.replace(val+'-','');
				}
				
				if(temp_id.value) $('input.delete_button').attr('disabled', false);
				else $('input.delete_button').attr('disabled', true);
			});
			
    	$('input.delete_button').click(function(e) 
			{
				var answer = confirm("<?php echo lang('delete_confirm');?> ??");
				if(answer)
				{
					//var uri 	= '<?php echo site_url("webmaster/'.$filename.'/delete");?>';
					var uri 	= '<?php echo base_url();?>index.php/webmaster/<?php echo $filename;?>/delete';
					var data  = { del_id : temp_id.value };
					$.ajax({ type: "POST", url: uri,data: data,  dataType: "html", success : function(data) {
						window.location='<?php echo current_url(); ?>';
					}});
				}
			});
			
			$('a.del_img').click(function(e) 
			{
				var id_del_img = document.getElementById('id');
				var answer = confirm("<?php echo lang('delete_confirm');?> ??");
				if(answer)
				{
					var tabel = '<?php echo $table;?>';
					var field = $(this).attr('alt');
					var uri 	= '<?php echo site_url("webmaster/mz_load/delete_image");?>';
					var data  = { del_id_img : id_del_img.value, del_field : field, del_table: tabel};
					$.ajax({ type: "POST", url: uri,data: data,  dataType: "html", success : function(data) {
						$("#"+field+"img").fadeOut(1000);
						$("#"+field+"_file").attr("value","-");
					}});
				}
			});
			
			$('.a_lang').click(function(e) 
			{
				var id_del_img = document.getElementById('id');
				var id_lang = $(this).attr('alt');
				var uri 	= '<?php echo site_url("webmaster/mz_load/change_lang");?>';
				var data  = { id_lang : id_lang};
				$.ajax({ type: "POST", url: uri,data: data,  dataType: "html", success : function(data) {
					window.location.reload();
				}});
			});
		});
		
		function per_page(val)
		{
			window.location="<?php echo $path_per_paging;?>/"+val;
		}
		</script>
  </head>

  <body class="admin_body">
  	<header>
			<?php $this->load->view($header); ?>
  	</header>

    <div class="container-fluid">
      <div class="sidebar">
        <?php $this->load->view($sidebar); ?>
        <footer>
		      <?php $this->load->view($footer); ?>
		    </footer>
      </div>
      
      <div class="content">
      	<ul class="breadcrumb">
					<?php echo $breadcrumb;?>
				</ul>

        <?php $this->load->view($main_content); ?>
        
      </div>


    </div>		
  </body>
</html>

<!-- Modal -->
<div id="modal-setting" class="modal hide fade" style="width:770px;left:42%;">
  <div class="modal-header">
    <a href="#" class="close">&times;</a>
    <h3><?php echo lang('setting')." ".$title;?></h3>
  </div>
  <div class="modal-body">
    <iframe frameborder="0" scrolling="auto" width="750" height="450" 
    	src="<?php echo site_url('webmaster/mz_config/main/'.$table.'/'.$filename);?>"></iframe>
  </div>
</div>