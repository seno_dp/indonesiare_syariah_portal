<?php
$menu=$current_menu="";
$webmaster_grup = $this->session->userdata("webmaster_grup");
if($multi_lang) $this->db->where("id_lang",$this->session->userdata("ses_id_lang"));
$this->db->where("id_parents",0);
$this->db->order_by("id_parents", "asc");
$this->db->order_by("sort", "asc"); 
$query=$this->db->get("menu_admin");
foreach($query->result() as $row)
{
	if($row->filez == $this->uri->segment(2)) $current_menu=$row->title;
	if($multi_lang) $this->db->where("id_lang",$this->session->userdata("ses_id_lang"));
	$this->db->where("id_parents",$row->id);
	$this->db->order_by("sort", "asc"); 
	$query2=$this->db->get("menu_admin");
	$sub_menu="";
	if($query2->num_rows() > 0)
	{
		$sub_menu.="<ul class='sub'>";
		foreach($query2->result() as $row2)
		{
			if($row2->filez == $this->uri->segment(2)) $current_menu=$row2->title;
				
				if($multi_lang) $this->db->where("id_lang",$this->session->userdata("ses_id_lang"));
				$this->db->where("id_parents",$row2->id);
				$this->db->order_by("sort", "asc"); 
				$query3=$this->db->get("menu_admin");
				$sub_menu1="";
				if($query3->num_rows() > 0)
				{
					$sub_menu1.="<ul class='submenu'>";
					foreach($query3->result() as $row3)
					{
						if($row3->filez == $this->uri->segment(2)) $current_menu=$row3->title;
						
						if(cek_akses($this->db, $row3->id, $webmaster_grup) || $webmaster_grup == "8910")
						$sub_menu1.="<li><a href='".site_url('webmaster/'.$row3->filez)."' title='".$row3->title."'><span class='text'>".$row3->title."</span></a></li>";
					}
					$sub_menu1.="</ul>";
				}
			
			if(cek_akses($this->db, $row2->id, $webmaster_grup) || $webmaster_grup == "8910")
			{
				if($row2->filez != "#")
				{
					$sub_menu.="<li><a href='".site_url('webmaster/'.$row2->filez)."' title='".$row2->title."'><span class='text'>".$row2->title."</span></a></li>";
				}
				else $sub_menu.= "<li><a><span class='text'>".$row2->title."</span></a>".$sub_menu1."</li>";
			}
			
		}
		$sub_menu.="</ul>";
	}
	
	if(cek_akses($this->db, $row->id, $webmaster_grup) || $webmaster_grup == "8910")
	{
		if($row->filez != "#")
		{
			$menu.= "<li><a class='submenu' title='".$row->title."' href='".site_url('webmaster/'.$row->filez)."'><span class='openheader menuheader'>".$row->title."</span></a></li>";
		}
		else $menu.= "<li><a><span class='openheader menuheader'>".$row->title."</span></a>".$sub_menu."</li>";
	}
}

function cek_akses($db, $id_menu, $webmaster_grup)
{
	$db->where("id_admin_grup", $webmaster_grup);
	$db->where("id_menu_admin", $id_menu);
	$q = $db->get("admin_auth");
	if($q->num_rows() > 0) return true;
	else return false;
}
?>

<input type="hidden" id="contain_menu" value="<?php echo $current_menu;?>"/>

<?php
if($menu)
{
?>
<div class="well">
	<div class="arrowlistmenu">
		<div id="side-menu-container">
			<ul id="side-menu" class="menu expandfirst">
				<?php echo $menu;?>
			</ul>
		</div>
	</div>
</div>
<?php
}

if($this->session->userdata("webmaster_grup") == "8910")
{
	?>
	<div class="well-super">
		<div class="arrowlistmenu">
			<div id="side-menu-container">
				<h4>Super Admin Menu</h4>
				<ul id="side-menu" class="menu expandfirst">
					<li>
						<!--a href="<?php echo base_url();?>webmaster/sa_config" title="Manage Global Config" class="submenu"-->
						<a href="<?php echo site_url('webmaster/sa_config');?>" title="Manage Global Config" class="submenu">
							<span class="openheader menuheader">Manage Config</span>
						</a>
					</li>
					<li>
					<a href="<?php echo site_url('webmaster/sa_menu');?>" title="Manage Menu" class="submenu">
							<span class="openheader menuheader">Manage Menu</span>
						</a>
					</li>
					<li>
					<a href="<?php echo site_url('webmaster/sa_contents');?>" title="Manage Contents" class="submenu">
							<span class="openheader menuheader">Manage Contents</span>
						</a>
					</li>
          			<li>
          				<a href="<?php echo site_url('webmaster/sa_lang');?>" title="Manage Language" class="submenu">
							<span class="openheader menuheader">Manage Language</span>
						</a>
					</li>
					<li>
						<a>
							<span class="openheader menuheader">Manage File</span>
						</a>
						<ul class="sub" style="display: block;">
							<li>
								<a title="File Config" href="<?php echo site_url('webmaster/sa_file_manager/main/5');?>">
									<span class="text">File Config</span>
								</a>
							</li>
							<li>
								<a title="File Controller" href="<?php echo site_url('webmaster/sa_file_manager/main/1');?>">
									<span class="text">File Controllers</span>
								</a>
							</li>
							<li>
								<a title="File Views" href="<?php echo site_url('webmaster/sa_file_manager/main/2');?>">
									<span class="text">File Views</span>
								</a>
							</li>
							<li>
								<a title="File Models" href="<?php echo site_url('webmaster/sa_file_manager/main/3');?>">
									<span class="text">File Models</span>
								</a>
							</li>
							<li>
								<a title="File Language" href="<?php echo site_url('webmaster/sa_file_manager/main/4');?>">
									<span class="text">File Language</span>
								</a>
							</li>
						</ul>
					</li>
					<li>
						<a>
							<span class="openheader menuheader">Manage Database</span>
						</a>
						<ul class="sub" style="display: block;">
							<li>
								<a href="<?php echo site_url('webmaster/sa_myadmin');?>" title="Myadmin" class="submenu">
									<span  class="text">Backup Tables</span>
								</a>
							</li>
							<!--li>
								<a href="<?php echo site_url('webmaster/sa_myadmin/dosql');?>" title="Myadmin" class="submenu">
									<span  class="text">SQL</span>
								</a>
							</li-->
						</ul>
					</li>
				</ul>
			</div>
		</div>
	</div>
	<?php
}
?>