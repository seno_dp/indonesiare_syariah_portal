<form action="<?php echo site_url('webmaster/'.$filename.'/cek_login');?>" method="post" class="form-signin">
  <h2 class="form-signin-heading"><?php echo $title;?></h2>
		<div class="alert alert-danger" style="<?php echo $dis_error;?>">
      <p>Login Failed</p>
    </div>
      <input type="text" class="form-control" placeholder="Username" name="username" autofocus>
      <input type="password" class="form-control" placeholder="Password" name="password">
      <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
  <footer>
    <?php $this->load->view($footer); ?>
  </footer>
</form>