<script language="JavaScript" src="<?php echo base_url();?>assets/mz_ckeditor/ckeditor.js" type="text/javascript"></script>
<div id="block_add_edit">
	<h3><?php echo $title;?></h3>
	<?php
	$flashmessage = $this->session->flashdata('message');
	if($flashmessage)
	{
		?>
		<div class="alert-message success fade in">
      <a href="#" class="close">&times;</a>
      <p><?php echo $flashmessage;?></p>
    </div>
    <?php
	}
	?>
	<!--form id="form_edit" action="<?php echo base_url();?>webmaster/<?php echo $filename;?>/update" method="post" enctype="multipart/form-data"-->
	<form id="form_edit" action="<?php echo site_url('webmaster/'.$filename.'/update');?>" method="post" enctype="multipart/form-data">
		<fieldset>
			<?php echo $list_input;?>
	    <div class="clearfix_button span5">
	    	<input type="submit" name="stay" value="<?php echo $val_button;?>" class="btn">
	    </div>
		</fieldset>
	</form>
</div>