<?php
if($flag == 1)
{
	$folder = "controllers";	
}
else if($flag == 2)
{
	$folder = "views";
}
else if($flag == 3)
{
	$folder = "models";
}
else if($flag == 4)
{
	$folder = "language";
}
else if($flag == 5)
{
	$folder = "config";
}

$dir = "./application/".$folder."/";
if($sub_folder) $dir .= $sub_folder."/";
$this->session->set_userdata("path_filemanager", $dir);
?>
<style>
.icon_file{
	float: left;
  height: 20px;
  margin-right: 5px;
  width: 16px;
  background:url('<?php echo base_url();?>assets/mz_images//admin/icon-red.png') no-repeat -31px -95px;
}
.icon_folder{
	float: left;
  height: 20px;
  margin-right: 5px;
  width: 16px;
  background:url('<?php echo base_url();?>assets/mz_images//admin/icon-red.png') no-repeat -15px -95px;
}
.del_fm, .del_item
{
	float: left;
  height: 20px;
  margin-right: 5px;
  width: 16px;
  background:url('<?php echo base_url();?>assets/mz_images//admin/icon-red.png') no-repeat -177px -95px;
}
.rename_fm, .rename_item
{
	float: left;
  height: 20px;
  margin-right: 15px;
  width: 16px;
  background:url('<?php echo base_url();?>assets/mz_images//admin/icon-blue.png') no-repeat -64px -78px;
}
.copy_fm, .copy_item
{
	float: left;
  height: 20px;
  margin-right: 15px;
  width: 16px;
  background:url('<?php echo base_url();?>assets/mz_images//admin/icon-black.png') no-repeat -48px -78px;
}
.zebra-striped tbody tr:hover td{background-color:none;}
</style>
<div id="block_list">
		<div style="float:left;">
    	<h3>File Manager - <?php echo ucfirst($folder);?></h3>
    </div>
    <div style="float:right;">
    	<a class="copy_item" style="margin-top:8px;margin-right:2px;"></a>
    	<span style="float:left;line-height:36px;margin-right:10px;">Copy</span>
    	<a class="rename_item" style="margin-top:8px;margin-right:2px;"></a>
    	<span style="float:left;line-height:36px;margin-right:10px;">Rename</span>
    	<a class="del_item" style="margin-top:8px;margin-right:2px;"></a>
    	<span style="float:left;line-height:36px;">Delete</span>
    </div>
    <div class="clear"></div>
    <table class="bordered-table zebra-striped">
      <tr>
      	<th>File</th><th width="200">Modify Date</th><th width="100">Action</th>
      </tr>
      <?php
      $list_file=array();
      if ($open = opendir($dir)) 
      {
        while (($file = readdir($open)) !== false) 
        {
        	if($file !== '.' && $file !== '..')
        	{
        		$list_file[] = array("nm_file"=> $file, "type_file"=> filetype($dir.$file), "modify_file"=> date("d F Y H:i:s", filemtime($dir.$file)));
	        }
        }
        closedir($open);
	    }
	    
	    $no=0;
	    asort($list_file);
	    foreach($list_file as $r)
	    {
	    	$no++;
	    	$act = "<a class='copy_fm' alt='".$no."' title='".$r['nm_file']."'></a>";
	    	$act .= "<a class='rename_fm' alt='".$no."' title='".$r['nm_file']."'></a>";
	    	if($r['type_file'] == "dir")
	    	{
	    		$cls="icon_folder";
	    		$nm_file = "<a href='".site_url('webmaster/sa_file_manager/main/'.$flag.'/'.$r['nm_file'])."'>".$r['nm_file']."</a>";
	    	}
	    	else 
	    	{
	    		$act .= "<a class='del_fm' alt='".$no."' title='".$r['nm_file']."'></a>";
	    		$cls="icon_file";
	    		$nm_file = "<a class='nm_file' alt='".$no."' title='".$r['nm_file']."'>".$r['nm_file']."</a>";
	    	}
		    echo "<tr class='line".$no."'>";
	      echo "<td><div class='".$cls."'></div><div class='mz_filez".$no."'>".$nm_file."</div></td><td>".$r['modify_file']."</td><td>".$act."</td>";
	      echo "</tr>";
	      echo "<tr class='line".$no."'>";
	      echo "<td colspan='3' style='padding:0px;'><div class='editor_hide' id='file".$no."'></div></td>";
	      echo "</tr>";
	    }
      ?>
		</table>     	
</div>
<br><br>