<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $cms_name;?></title>
    <meta name="description" content="webmazhters http://www.mazhters.com/">
    <meta name="author" content="Mazhters Irwan">

    <!-- script -->
		<script src="<?php echo base_url();?>assets/mz_js/jquery.min.js"></script>
		<script src="<?php echo base_url();?>assets/mz_js/mz_custom.js"></script>
		<script src="<?php echo base_url();?>assets/mz_js/jquery-ui-1.7.1.custom.min.js"></script>
		<script src="<?php echo base_url();?>assets/mz_js/jquery.effects.core.js"></script>
		<script src="<?php echo base_url();?>assets/mz_js/validate_new/validate.js"></script>
		<script src="<?php echo base_url();?>assets/mz_js/bootstrap/bootstrap-alerts.js"></script>
		<script src="<?php echo base_url();?>assets/mz_js/bootstrap/bootstrap-modal.js"></script>
		
		<!-- Menu -->
		<script src="<?php echo base_url();?>assets/mz_js/menu.js" ></script>
		
		<!-- styles -->
    <link href="<?php echo base_url();?>assets/mz_style/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/mz_style/custom.css" rel="stylesheet">
    
    <!-- Icons -->
    <!--<link rel="shortcut icon" href="<?php echo base_url();?>assets/mz_images/favicon.ico">-->
    
    <!-- Custom Script JS -->
    <script>
    $(document).ready(function() {
    	$('.a_lang').click(function(e) 
			{
				var id_del_img = document.getElementById('id');
				var id_lang = $(this).attr('alt');
				var uri 	= '<?php echo site_url("webmaster/mz_load/change_lang");?>';
				var data  = { id_lang : id_lang};
				$.ajax({ type: "POST", url: uri,data: data,  dataType: "html", success : function(data) {
					window.location.reload();
				}});
			});
			
			$('a.nm_file').click(function(e) 
			{
				var urut = $(this).attr("alt");
				var nmfile = $(this).attr("title");
				$('.editor_hide').slideUp(500);
				$("#file"+urut).slideDown(500);
				$("#file"+urut).load('<?php echo base_url();?>index.php/webmaster/mz_load/load_ace_frame/'+nmfile);
			});
			
			$('a.del_fm').click(function(e) 
			{
				var answer = confirm("<?php echo lang('delete_confirm');?> ??");
				if(answer)
				{
					var urut = $(this).attr('alt');
					var nmfile = $(this).attr('title');
					var uri 	= '<?php echo base_url();?>index.php/webmaster/mz_load/delete_file';
					var data  = {nm_file : nmfile};
					$.ajax({ type: "POST", url: uri,data: data,  dataType: "html", success : function(data) {
						$('.line'+urut).fadeOut(500);
					}});
				}
			});
			
			$('a.rename_fm').click(function(e) 
			{
				var urut = $(this).attr('alt');
				var nmfile = $(this).attr('title');
				if($('.rename'+urut).attr("alt") != 2)
				{
					if($('.rename'+urut).attr("alt") != 1)
					$('.mz_filez'+urut).append("<input alt='2' class='rename"+urut+"' value='"+nmfile+"' onChange=javascript:rename_file(this.value,'"+nmfile+"')>");
					else
					{
						$('.rename'+urut).show();
						$('.rename'+urut).attr("alt",2);
					}
					$('.mz_filez'+urut+' a').hide();
				}
				else
				{
					$('.rename'+urut).hide();
					$('.rename'+urut).attr("alt",1);
					$('.mz_filez'+urut+' a').show();
				}
			});
			
			$('a.copy_fm').click(function(e) 
			{
				var urut = $(this).attr('alt');
				var nmfile = $(this).attr('title');
				if($('.copy'+urut).attr("alt") != 2)
				{
					if($('.copy'+urut).attr("alt") != 1)
					$('.mz_filez'+urut).append("<div alt='2' class='copy"+urut+"'> &raquo; <input value='Copy-"+nmfile+"' onChange=javascript:copy_file(this.value,'"+nmfile+"')></div>");
					else
					{
						$('.copy'+urut).show();
						$('.copy'+urut).attr("alt",2);
					}
				}
				else
				{
					$('.copy'+urut).hide();
					$('.copy'+urut).attr("alt",1);
				}
			});
		});
		
		function rename_file(nm_file,nm_file_old)
		{
			var uri 	= '<?php echo site_url("webmaster/mz_load/rename_file");?>';
			var data  = {nm_file : nm_file, nm_file_old : nm_file_old};
			$.ajax({ type: "POST", url: uri,data: data,  dataType: "html", success : function(data) {
				window.location.reload();
			}});
		}
		
		function copy_file(nm_file,nm_file_old)
		{
			var uri 	= '<?php echo site_url("webmaster/mz_load/copy_file");?>';
			var data  = {nm_file : nm_file, nm_file_old : nm_file_old};
			$.ajax({ type: "POST", url: uri,data: data,  dataType: "html", success : function(data) {
				window.location.reload();
			}});
		}
		</script>
  </head>

  <body>
  	<header>
			<?php $this->load->view($header); ?>
  	</header>

    <div class="container-fluid">
      <div class="sidebar">
        <?php $this->load->view($sidebar); ?>
        <footer>
		      <?php $this->load->view($footer); ?>
		    </footer>
      </div>
      
      <div class="content">
      	<ul class="breadcrumb">
					<?php echo $breadcrumb;?>
				</ul>
        <?php $this->load->view($main_content); ?>
      </div>
    </div>		
  </body>
</html>