<?php
$data_checked = array('type'=> 'checkbox', 'id'=> 'primary_check', 'onClick'=> "checkedAll('".$filename."', true)");
$data_temp_id = array('type'=> 'hidden', 'id'=> 'temp_id');

if($search) echo "<div id='block_search'>";
else echo "<div id='block_search' style='display:none;'>";
?>
	<h3><?php echo lang('search')." ".$title;?> <img class='show_search' src='<?php echo base_url();?>assets/mz_images/admin/panah_bawah.gif'></h3>
	<form action="<?php echo site_url('webmaster/'.$filename.'/search');?>" method="post">
		<div class="fieldset_search">
			<?php echo $search;?>
	    <div class="clearfix_button">
	    	<input type="submit" value="<?php echo lang('search');?>" class="btn">
	    </div>
	  </div>
	</form>
</div>

<div id="block_list">
		<?php
		$flashmessage = $this->session->flashdata('message');
		if($flashmessage)
		{
			?>
			<div class="alert-message success fade in">
        <a href="#" class="close">&times;</a>
        <p><?php echo $flashmessage;?></p>
      </div>
      <?php
		}
		?>
    <!--div class="tombol">
      <input type="button" value="<?php echo lang("delete");?>" alt="<?php echo lang("delete");?>" title="<?php echo lang("delete");?>" class="delete_button btn" disabled/>
      <input type="button" value="<?php echo lang("add");?>" alt="<?php echo lang("add");?>" title="<?php echo lang("add");?>" class="btn" onClick="javascript:window.location='<?php echo site_url("webmaster/".$filename."/detail/0");?>';"/>
    </div-->
		<form id="<?php echo $filename;?>">
    <h3><?php echo lang('list')." ".$title;?></h3>
    <table class="table gridz">
      <tr>
      	<?php
      	echo form_input($data_temp_id);
      	foreach($grid as $r)
      	{
      		if($r == "#") echo "<th class='box_delete'>".form_input($data_checked)."</th>";
      		else echo "<th>".$r."</th>";
        }
    		?>
      </tr>
		</table>
		
		<ul class='is_sort' rel='<?php echo site_url("webmaster/mz_load/sortz/".$table);?>' title='<?php echo $pg;?>'  id='<?php echo $sortable;?>'>
			<?php
			echo $list;
		  ?>
  	</ul>
  	<br><br>
    <div class="clear"></div>
    <div class="pagination">
      <?php 
        if(isset($num_rows))
        {
      ?>
          <ul id="perpage">
            <li class="prev disabled"><a>Total rows :</a><input class="span1" value="<?php echo $num_rows;?>" readonly></li>
          </ul>
      <?php }?>
    	<ul id="perpage">
    		<li class="prev disabled"><a>Per <?php echo lang('page');?></a><input class="span1" value="<?php echo $per_page;?>" onchange="javascript:per_page(this.value);"></li>
    	</ul>
    	<ul>
      	<li class="prev disabled"><a><?php echo lang('page');?></a></li>
        <?php echo $pagination;?>
      </ul>
    </div>
  	<!--div class="tombol">
  		<input type="button" value="<?php echo lang("delete");?>" alt="<?php echo lang("delete");?>" title="<?php echo lang("delete");?>" class="delete_button btn" disabled/>
  		<input type="button" value="<?php echo lang("add");?>" alt="<?php echo lang("add");?>" title="<?php echo lang("add");?>" class="btn" onClick="javascript:window.location='<?php echo site_url("webmaster/".$filename."/detail/0");?>';"/>
  	</div-->
  	</form>
</div>