<link rel="stylesheet" href="<?php echo base_url();?>assets/mz_style//multiselect/common.css" type="text/css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>assets/mz_style//multiselect/multiple-jquery-ui.css" />
<link type="text/css" href="<?php echo base_url();?>assets/mz_style//multiselect/ui.multiselect.css" rel="stylesheet" />
<script type="text/javascript" src="<?php echo base_url();?>assets/mz_js/multiselect/multiple-jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/mz_js/multiselect/multiple-jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/mz_js/multiselect/plugins/localisation/jquery.localisation-min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/mz_js/multiselect/plugins/scrollTo/jquery.scrollTo-min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/mz_js/multiselect/ui.multiselect.js"></script>
<script>
$(document).ready(function(){
	$(".multiselect").multiselect();
});
</script>
<form method="post" action="<?php echo site_url('webmaster/'.$filename.'/setting');?>" target="_parent">
	<select class="multiselect" multiple="multiple" name="setting_id[]">
		<?php echo $opt_setting;?>
	</select>
	<br>
	<input type="submit" value="Submit" class="btn">
</form>