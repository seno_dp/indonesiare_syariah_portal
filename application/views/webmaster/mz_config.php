<script src="<?php echo base_url();?>assets/mz_js/jquery.min.js"></script>
<link href="<?php echo base_url();?>assets/mz_style/bootstrap.css" rel="stylesheet">
<link href="<?php echo base_url();?>assets/mz_style/custom.css" rel="stylesheet">
<style>
body{padding-top:0px;}
</style>
<script>
function list_kolom_current(val, idz)
{
	//var uri 	= '<?php echo base_url();?>webmaster/mz_load/list_column';
	var uri 	= '<?php echo site_url("webmaster/mz_load/list_column");?>';
	var data  = { tbl : val, field : idz };
	$.ajax({ type: "POST", url: uri,data: data,  dataType: "html", success : function(data) {
		$("#"+idz).html(data);
	}});
}

function list_kolom(val, idz)
{
	//var uri 	= '<?php echo base_url();?>webmaster/mz_load/list_column';
	var uri 	= '<?php echo site_url("webmaster/mz_load/list_column");?>';
	var data  = { tbl : val, field : idz };
	$.ajax({ type: "POST", url: uri,data: data,  dataType: "html", success : function(data) {
		$("#"+idz).html(data);
	}});
}
</script>
<?php
$data_filename = array('name'=> 'filename', 'value'=> $filename, 'type'=> 'hidden');
$data_tabel = array('name'=> 'tabel', 'value'=> $tabel, 'type'=> 'hidden');
$opt_is_active = array("0"=> "Hide", "1"=> "Show");
$opt_typeinput = array(""=> "-- Type --", "Text"=> "Text", "Dropdown"=> "Dropdown", "Checkbox"=> "Checkbox", "Textarea"=> "Textarea", "Textarea-nojs"=> "Textarea-nojs", "File"=> "File", "Label"=> "Label","Date"=>"Date");
?>
<form id="modal" method="post" action="<?php echo site_url('webmaster/mz_config');?>" target="_parent">
<!--form id="modal" method="post" action="<?php echo base_url();?>webmaster/mz_config" target="_parent"-->
	<?php echo form_input($data_tabel);?>
	<?php echo form_input($data_filename);?>
	<table class="bordered-table zebra-striped">
		<tr>
      <th>Field<br>Show(Alias)</th>
      <th>Related Table</th>
      <th>Search</th>
      <th>Sort</th>
      <th>Type</th>
    </tr>
	<?php 
	$flag_id_lang=$flag_sort=0;
	foreach($columns as $r)
	{
		if($r['Field'] == "id_lang") $flag_id_lang=1;
		if($r['Field'] == "sort") $flag_sort=1;
		
		if(isset($config[$r['Field']]['show']))
		{
			if($config[$r['Field']]['show']) $show = true;
			else $show = false;
		}
		else $show = false;
		
		if(isset($config[$r['Field']]['search']))
		{
			if($config[$r['Field']]['search']) $search = true;
			else $search = false;
		}
		else $search = false;
		
		if(isset($config[$r['Field']]['addedit']))
		{
			if($config[$r['Field']]['addedit']) $addedit = true;
			else $addedit = false;
		}
		else $addedit = false;
		?>
		<tr>
      <td class="input-prepend">
      	<b><?php echo $r['Field'];?></b><br>
      	<label class="add-on"><?php echo form_checkbox('show_'.$r['Field'], 1, $show);?></label>
      	<?php echo form_input('alias_'.$r['Field'], isset($config[$r['Field']]['alias']) ? $config[$r['Field']]['alias'] : '', 'class="mini" style="height:28px;width:100px;"');?>
      </td>
      <td>
      	<?php 
      		echo form_dropdown('related_'.$r['Field'], $opt_table, isset($config[$r['Field']]['related']) ? $config[$r['Field']]['related'] : '', "class='span4' onChange=list_kolom(this.value,'".$r['Field']."')"); 
      		echo "<div id='".$r['Field']."'>";
      		$relasi = isset($config[$r['Field']]['related']) ? $config[$r['Field']]['related'] : '';
      		if($relasi)
      		{
						$query = $this->model_admin_all->GetColumns($relasi);
						$list_column = "<select name='title_related_".$r['Field']."' class='span4'>";
						foreach($query as $s)
						{
							$val = $s['Field'];
							if($val == $config[$r['Field']]['title_related']) $list_column .= "<option value='".$val."' selected>".$val."</option>";
							else $list_column .= "<option value='".$val."'>".$val."</option>";
						}
						$list_column .= "</select>";
						echo $list_column;
      		}
      		echo "</div>";
      	?>
      </td>
      <td><?php echo form_checkbox('search_'.$r['Field'], 1, $search);?></td>
      <td><?php echo form_dropdown('sort_'.$r['Field'], $opt_sort, isset($config[$r['Field']]['sort']) ? $config[$r['Field']]['sort'] : '', "class='span2'"); ?></td>
      <td><?php echo form_dropdown('typeinput_'.$r['Field'], $opt_typeinput, isset($config[$r['Field']]['typeinput']) ? $config[$r['Field']]['typeinput'] : '', "class='span3'"); ?></td>
    </tr>
    <?php
  }
  ?>
	</table>
	<br>
	<div>
		<label class='search'>Field to detail</label>
		<select name="to_detail" class="span3">
			<option value="">-Select Field-</option>
		<?php
		foreach($columns as $r)
		{
			if($r['Field'] == $to_detail) echo "<option value='".$r['Field']."' selected>".$r['Field']."</option>";
			else echo "<option value='".$r['Field']."'>".$r['Field']."</option>";
    }
    ?>
  	</select>
	</div>
	<?php
	if($flag_id_lang)
	{
	?>
	<div>
		<label class='search'>Multi Language</label>
		<?php echo form_checkbox('is_multi_lang', 1, $is_multi_lang);?>
	</div>
	<br>
	<?php
	}
	
	if($flag_sort)
	{
	?>
	<div>
		<label class='search'>Sort</label>
		<?php echo form_checkbox('is_sort', 1, $is_sort);?>
	</div>
	<?php
	}
	?>
	<div>
		<label class='search'>Show item per-page (frontend)</label>
		<?php echo form_input('per_page', $per_page, 'class="mini" style="height:28px;width:100px;"');?>
	</div>
	<div>
		<label class='search'>Show item at homepage (frontend)</label>
		<?php echo form_input('item_homepage', $item_homepage, 'class="mini" style="height:28px;width:100px;"');?>
	</div>
	<input type="submit" value="Submit" class="btn">
</form>