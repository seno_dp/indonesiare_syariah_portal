<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $cms_name;?></title>
    <meta name="description" content="webmazhters">
    <meta name="author" content="mazhters irwan">

    <!-- script -->
		<script src="<?php echo base_url();?>assets/mz_js/jquery.min.js"></script>
		<script src="<?php echo base_url();?>assets/mz_js/jquery.effects.core.js"></script>
		<script src="<?php echo base_url();?>assets/mz_js/bootstrap/bootstrap-alerts.js"></script>
		<script src="<?php echo base_url();?>assets/mz_js/bootstrap/bootstrap-modal.js"></script>
		<script src="<?php echo base_url();?>assets/mz_js/datepicker.js"></script>
		<script>
		$(document).ready(function(){
			$('img.show_search').click(function(){
				$( ".fieldset_search" ).toggleClass("fieldset_search_new", 500);				
			});
			
			$('#my-modal').modal(true);
			
			$('.a_lang').click(function(e) 
			{
				var id_del_img = document.getElementById('id');
				var id_lang = $(this).attr('alt');
				var uri 	= '<?php echo site_url("webmaster/mz_load/change_lang");?>';
				var data  = { id_lang : id_lang};
				$.ajax({ type: "POST", url: uri,data: data,  dataType: "html", success : function(data) {
					window.location.reload();
				}});
			});
		});
		
		</script>
		<!-- Menu -->
		<script src="<?php echo base_url();?>assets/mz_js/menu.js" ></script>
		
		<!-- styles -->
    <link href="<?php echo base_url();?>assets/mz_style/bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/mz_style/custom.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/mz_style/datepicker.css" rel="stylesheet">    
    <!-- Icons -->
    <!--<link rel="shortcut icon" href="<?php echo base_url();?>assets/mz_images/favicon.ico">-->
  </head>

  <body>
  	<header>
			<?php $this->load->view($header); ?>
  	</header>

    <div class="container-fluid">
      <div class="sidebar">
        <?php $this->load->view($sidebar); ?>
        <footer>
		      <?php $this->load->view($footer); ?>
		    </footer>
      </div>
      
      <div class="content">
      	<div class="breadcrumb">
      	<?php 
      	 if($this->session->userdata("webmaster_grup") == "8910")
      	 {
      	 	?>
				  <a href="#" data-controls-modal="modal-from-dom" class="setting"><img src="<?php echo base_url();?>assets/mz_images/admin/setting.png" width="18"></a>
				  <?php
				 }
				?>
				  <a href="<?php echo site_url('webmaster/'.$filename);?>"><?php echo lang('home');?></a>
				</div>
        <?php $this->load->view($main_content); ?>
      </div>
    </div>		
  </body>
</html>