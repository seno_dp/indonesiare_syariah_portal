<script language="JavaScript" src="<?php echo base_url();?>assets/mz_ckeditor/ckeditor.js" type="text/javascript"></script>
<script language="JavaScript" src="<?php echo base_url();?>assets/mz_js/recopy.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){
  var removeLink = ' <a class="remove" href="#" onclick="$(this).parent().slideUp(function(){ $(this).remove() }); return false">remove</a>';
$('a.add').relCopy({ append: removeLink});	
});
</script>
<div id="block_add_edit">
	<h3>Silakan masukkan 5 foto : </h3>
	<?php
	$flashmessage = $this->session->flashdata('message');
	if($flashmessage)
	{
		?>
		<div class="alert-message success fade in">
      <a href="#" class="close">&times;</a>
      <p><?php echo $flashmessage;?></p>
    </div>
    <?php
	}
	?>
	<!--form id="form_edit" action="<?php echo base_url();?>webmaster/<?php echo $filename;?>/update" method="post" enctype="multipart/form-data"-->
	<form id="form_edit" action="<?php echo site_url('webmaster/multiupload_foto/do_upload');?>" method="post" enctype="multipart/form-data">
		<fieldset>
			<p class="clone">
				<input type="file" name="files[]" /><br/>
				<input type="text" name="caption[]" placeholder="caption">
			</p>
			<p><a href="#" class="add" rel=".clone">Add More</a></p>	    
	    <div class="clearfix_button span5">
	    	<input type="hidden" name="id_album" value="<?php echo $this->uri->segment('5')?>">
	    	<input type="submit" name="stay" value="<?php echo $val_button;?>" class="btn">
	    	<input type="submit" name="back" value="<?php echo lang("save");?>" class="btn">
	    </div>
		</fieldset>
	</form>
</div>