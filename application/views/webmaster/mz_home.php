<?php 
//ga_count = 0 not using google analytics
//ga_count = 1 using google analytics
$ga_count = 0;
if($ga_count > 0) {
	$key = 'start_date';
	$data_input = array('name'=> $key, 'id'=> $key, 'value'=> $vstart_date, 'type'=> 'text', 'class'=> 'span5');
	$start_date = form_input($data_input);
	$start_date .= "<script>
	            		$(document).ready(function() {
							$('#".$key."').DatePicker({
								format:'Y-m-d',
								date: $('#".$key."').val(),
								current: $('#".$key."').val(),
								starts: 1,
								position: 'right',
								onBeforeShow: function(){
									$('#".$key."').DatePickerSetDate($('#".$key."').val(), true);
								},
								onChange: function(formated, dates){
									$('#".$key."').val(formated);
								}
							});
						}); 
	            	</script>";

	$keyend = 'end_date';
	$data_end = array('name'=> $keyend, 'id'=> $keyend, 'value'=> $vend_date, 'type'=> 'text', 'class'=> 'span5');
	$end_date = form_input($data_end);
	$end_date .= "<script>
	            		$(document).ready(function() {
							$('#".$keyend."').DatePicker({
								format:'Y-m-d',
								date: $('#".$keyend."').val(),
								current: $('#".$keyend."').val(),
								starts: 1,
								position: 'right',
								onBeforeShow: function(){
									$('#".$keyend."').DatePickerSetDate($('#".$keyend."').val(), true);
								},
								onChange: function(formated, dates){
									$('#".$keyend."').val(formated);
								}
							});
						}); 
	            	</script>";
	echo form_open('webmaster/mz_home/main');
	echo 'Start Date: '.$start_date.'&nbsp;&nbsp;End Date: '.$end_date.'&nbsp;<input type="submit" value="SUBMIT"/>';
	echo form_close();

?>

<div id="chart"></div>

<!-- Include the Google Charts API -->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<!-- Create a new chart and plot the pageviews for each day -->
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = new google.visualization.DataTable();

   
    data.addColumn('string', 'Day');
    data.addColumn('number', 'Visits');

    data.addRows([
      <?php
      foreach($results as $result) {
          echo '["'.date('M j',strtotime($result->getDate())).'", '.$result->getvisits().'],';
      }
      ?>
    ]);

    var chart = new google.visualization.AreaChart(document.getElementById('chart'));
    chart.draw(data, {width: 950, height: 300, title: 'Visits: <?php echo $vstart_date.' until '.$vend_date; ?>',
                      colors:['#058dc7','#e6f4fa'],
                      areaOpacity: 0.1,
                      hAxis: {textPosition: 'in', showTextEvery: 5, slantedText: false, textStyle: { color: '#058dc7', fontSize: 10 } },
                      pointSize: 5,
                      legend: 'none',
                      chartArea:{left:10,top:30,width:"100%",height:"100%",bottom:30}
    });
  }
</script>
<?php
echo '<div class="row">';

	echo '<div class="span4">';
      echo '<h3>Total Visits</h3>';
      echo '<blockquote>';
      echo '<p>'.number_format($Visits,0,',','.').'</p>';
      echo '</blockquote>';
    echo '</div>';
    
    echo '<div class="span4">';
      echo '<h3>Total Pageviews</h3>';
      echo '<blockquote>';
      echo '<p>'.number_format($Pageviews,0,',','.').'</p>';
      echo '</blockquote>';
    echo '</div>';
    
    echo '<div class="span4">';
      echo '<h3>Total newVisits</h3>';
      echo '<blockquote>';
      echo '<p>'.number_format($newVisits,0,',','.').'</p>';
      echo '</blockquote>';
    echo '</div>';

    echo '<div class="span4">';
      echo '<h3>Total uniquePageviews</h3>';
      echo '<blockquote>';
      echo '<p>'.number_format($uniquePageviews,0,',','.').'</p>';
      echo '</blockquote>';
    echo '</div>';

    echo '<div class="span4">';
      echo '<h3>Pages / Visit</h3>';
      echo '<blockquote>';
      echo '<p>'.number_format($pageviewsPerVisit,2,',','.').'</p>';
      echo '</blockquote>';
    echo '</div>';

    echo '<div class="span4">';
      echo '<h3>% newVisits</h3>';
      echo '<blockquote>';
      echo '<p>'.number_format($percentnewVisits,2,',',' ').' %</p>';
      echo '</blockquote>';
    echo '</div>';
    
echo '</div>';
?>

<?php } ?>



<?php
foreach($list_grid as $list=> $grid)
{
	?>
	<div class="row">
	<?php
	foreach($grid as $r)
	{
		?>
	  <div class="span4 dashboard">
	    <h2><?php echo $r['title'];?></h2>
	    <blockquote>
		    <p>Total Record : <?php echo $r['total'];?></p>
		    <p>Last Update : <?php echo date("d/m/Y H:i:s", strtotime($r['modify_date']));?></p>
		  </blockquote>
		  <p><a href="<?php echo site_url('webmaster/'.$r['uri']);?>" class="btn">View details &raquo;</a></p>
		  
	  </div>
	  <?php
	}
	?>
  </div>
  <hr>
  <?php
}
?>





<!-- Wall Activities -->
<!--<div class="breadcrumb">
  <?php echo lang("log_activities");?>
</div>
<table class="bordered-table zebra-striped">
<tr><th>Menu</th><th>Activites</th><th>Create Date</th></tr>
<?php
foreach($activities as $s)
{
	$msg = str_replace("url/",base_url(),$s['message']); 
	$msg = str_replace("id_admin",$s['name'],$msg); 
	echo $msg;
}
?>
</table>
<br><br>-->

<!-- Modal -->
<div id="modal-from-dom" class="modal hide fade">
  <div class="modal-header">
    <a href="#" class="close">x</a>
    <h3>Dashboard Setting</h3>
  </div>
  <div class="modal-body">
    <iframe frameborder="0" scrolling="0" width="500" height="300" src="<?php echo base_url();?>webmaster/mz_home/setting"></iframe>
  </div>
</div>