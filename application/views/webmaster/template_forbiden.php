<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $cms_name;?></title>
    <meta name="description" content="webmazhters">
    <meta name="author" content="mazhters irwan">

    <!-- Menu -->
    <script src="<?php echo base_url();?>assets/mz_js/jquery.min.js"></script>
		<script src="<?php echo base_url();?>assets/mz_js/menu.js" ></script>
		
		<!-- styles -->
    <link href="<?php echo base_url();?>assets/mz_style//bootstrap.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/mz_style//custom.css" rel="stylesheet">

    <!-- Icons -->
    <!--<link rel="shortcut icon" href="<?php echo base_url();?>assets/mz_images//favicon.ico">-->
  </head>

  <body>
  	<header>
			<?php $this->load->view($header); ?>
		</header>

    <div class="container-fluid">
      <div class="content" style="margin-left:0px;">
        <div class="alert-message alert">
        	<p><strong>you don't have permission</strong>.</p>
      	</div>
      </div>
    </div>
  </body>
</html>