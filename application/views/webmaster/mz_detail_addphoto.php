<?php 
$CI =& get_instance();
$CI->load->library('phpflickr');
$q = GetAll('kg_flickr_api',array("id"=>"where/1"));
$val = $q->row_array();

$apiKey = $val['api_key'];
$apiSecret = $val['secret_key'];
$permissions  = $val['permission'];
$token        = $val['token'];

$CI->phpflickr->phpflickr($apiKey, $apiSecret, true);
      
?>
<script language="JavaScript" src="<?php echo base_url();?>assets/mz_ckeditor/ckeditor.js" type="text/javascript"></script>
<div id="block_add_edit">
	<h3><?php echo $title;?></h3>
	<?php
	$flashmessage = $this->session->flashdata('message');
	if($flashmessage)
	{
		?>
		<div class="alert-message success fade in">
      <a href="#" class="close">&times;</a>
      <p><?php echo $flashmessage;?></p>
    </div>
    <?php
	}
	?>
	<!--form id="form_edit" action="<?php echo base_url();?>webmaster/<?php echo $filename;?>/update" method="post" enctype="multipart/form-data"-->
	<form id="form_edit" action="<?php echo site_url('webmaster/'.$filename.'/update');?>" method="post" enctype="multipart/form-data">
		<fieldset>
			<?php echo $list_input;?>

			<?php
			if($photos){
				//die(print_r($photos));
		    $i = 0;

		    foreach ((array)$photos['photos']['photo'] as $photo) {
		     //print_r($photo);
		      $url = "http://farm" . $photo['farm'] . ".static.flickr.com/" . $photo['server'] . "/" . $photo['id'] . "_" . $photo['secret'] . "_s.jpg";
		      
		      echo "<a href='#' id='$photo[id]' class='photo_primary'>";
		      echo "<img style='margin-left:5px;' border='0' alt='$photo[title]' src=" . $url . ">";
		      echo "</a>";
		      $i++;
		      // If it reaches the sixth photo, insert a line break
		      if ($i % 10 == 0) {
		          echo "<br>\n";
		    }
		  }
		}
		?>
	    <div class="clearfix_button span5">
	    	<input type="submit" name="back" value="<?php echo lang("save");?>" class="btn">
	    </div>
		</fieldset>
	</form>
</div>
<script>
	$( ".photo_primary" ).click(function(e) {
		e.preventDefault();
		$("input[name='photo_id']" ).val($(this).attr('id'));
		});
</script>