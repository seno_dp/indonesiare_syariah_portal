<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title><?php echo $cms_name;?></title>
    <meta name="description" content="webmazhters">
    <meta name="author" content="mazhters irwan">

    <!-- script -->
		<script src="<?php echo base_url();?>assets/mz_js/jquery.min.js"></script>
		<script src="<?php echo base_url();?>assets/mz_js/bootstrap/bootstrap.min.js"></script>
		<script>
		$(document).ready(function(){
			$('.close').click(function(){
				$(".alert-message").alert('close');
			});
		});
		</script>
		
    <!-- styles -->
    <link href="<?php echo base_url();?>assets/mz_style//bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/mz_style/signin.css" rel="stylesheet">
    
    <!-- Icons -->
    <!--<link rel="shortcut icon" href="<?php echo base_url();?>assets/mz_images//favicon.ico">-->
  </head>

  <body>
  	<!--header>
			<?php $this->load->view($header); ?>
  	</header-->

    <div class="container">
      <!--div class="content" style="margin-left:0px;"-->
        <?php $this->load->view($main_content); ?>
        
      <!--/div-->
    </div>

  </body>
</html>