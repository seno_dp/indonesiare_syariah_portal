<?php 
    $image_field = ($controller_name == "article") ? 'thumbnail' : 'image';
?>
<!-- KRITERION ARCHIVE -->
<div class="flex_66">
    <div class="box">
        <div class="listarticle-title">
            <b><?php echo ($menu_title) ? ucfirst($menu_title) : "Article"?>
                <span class="arrows">&raquo;</span>
            </b>
        </div>
        <?php if($controller_name == 'klipingdd') { ?>
        <?php 
            $options = array();
            $options[] = "pilih koran";
            $filterkoran = array("title"=>"order/asc");
            $qkoran = getAll('kg_koran',$filterkoran);
            if($qkoran->num_rows() > 0 ){
                foreach ($qkoran->result_array() as $vkor) {
                    $options[$vkor['id']] = $vkor['title'];
                }
            }
        ?>
       <!--  <div class="search-kliping">
            <h4>Cari kliping koran</h4>
            <form action="<?php //echo site_url('kliping/search')?>" method="post">
                <ul>
                    <li><span>koran</span> : <?php //echo form_dropdown('id_koran', $options,'','class="chzn-select" id="id_koran"');?></li>
                    <li><span>tanggal</span> : <input type="text" id="date_kliping" name="date_kliping"></li>
                    <li><span>Kata kunci</span> : <input type="text" id="text_kliping" name="text_kliping"></li>
                    <li><button type="button" id="submit_cari_kliping" Value="Cari">Cari</button></li>
                </ul>
            </form>
        </div> -->

        <div class="search-klipbox">
            <form action="<?php echo site_url('kliping/search')?>" method="post">
            <p class="field-search-kliping">
                <div class="double-field">
                    <label class="kliping">Jenis Koran 
                        <span class="required">:</span>
                    </label>
                    <?php echo form_dropdown('id_koran', $options,'','class="chzn-select search-klip" id="id_koran"');?>
                    <!-- <select class="search-klip">
                        <option>Mustard</option>
                        <option>Ketchup</option>
                        <option>Relish</option>
                    </select> -->
                </div>
                <div class="double-field">
                <label class="kliping"> Tanggal
                    <span class="required">:</span>
                </label>
                <input id="datepicker" class="search-klip" name="date_kliping" type="text" value="" size="20" aria-required='true' />
                </div>
            </p> 
            
                     
            <p class="field-search-kliping">                            
                <div class="klip-searchbox">
                    <label class="kliping"> Keyword
                        <span class="required">:</span>
                    </label>
                    <input class="search-title-klip" type="text" name="text_kliping">
                    <input class="search-button" value="" type="submit">
                </div>                          
            </p> 
            </form>
        </div>
       
        <?php } ?>
        <div class="clear"></div>
    </div>
    <br/>
    <br/>
    <div id="search_kliping_result">
    <?php if($qp->num_rows() > 0) {?>
    
    <?php foreach($qp->result_array() as $qval) {?>
        <div class="post-<?php echo $qval['id']?> post type-post">
            <h1 id="post-<?php echo $qval['id']?>">
                <a href="<?php echo site_url($controller_name.'/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" title="<?php echo $qval['title']?>">
                    <?php echo $qval['title']?>
                </a>
            </h1>
            <div class="entry">
                <?php if($qval[$image_field]) {?>
                <div class="thumb-arhive-book">
                    <!-- <a href="<?php //echo site_url($controller_name.'/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" rel="bookmark"> -->
                    <a href="<?php echo site_url($controller_name.'/download/'.$qval['id'].'/'.url_title($qval['title']))?>" rel="bookmark">
                        <img width="180" height="235" src="<?php echo base_url()?>uploads/<?php echo GetThumb($qval[$image_field])?>" class="attachment-blog-image wp-post-image" alt="<?php echo $qval['title']?>"/>
                    </a>
                </div>
                
                <?php } ?>
                <div class="excerptarhive-book"><?php echo $qval['headline']?>
                    ... <a href="<?php echo site_url($controller_name.'/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" rel="tag">More</a>
                </div>
                <div class="postmetabook">
                    <!-- KRITERION META -->
                    <div class="meta">
                        <?php if($controller_name == 'kliping') { ?>
                            <span class="time">Tanggal koran <?php echo date('M d, Y',strtotime($qval['date_koran'])) ?></span><br/>
                            <span class="categories">Koran <?php echo $qval['title_koran'] ?></span><br/>
                        <?php } ?>
                        <span class="time"><?php echo date('M d, Y',strtotime($qval['create_date']))?></span>
                        <em>by&nbsp;</em><?php echo GetUserName('kg_admin','name',$qval['create_user_id'])?> | Type: <?php echo ExplodeNameFile($qval['uploaded_file'])['ext']?>
                    </div>
                    <?php if(($qval['uploaded_file']) || ($qval['uploaded_file'] != 0)){?>
                        <span class="categories">Download File
                            <a href="<?php echo site_url($controller_name.'/download/'.$qval['id'].'/'.url_title($qval['title']))?>" title="Download <?php echo $qval['title']?>" rel="category tag"><?php echo $qval['title']?></a>
                        </span>
                        <br/>
                    <?php } ?>
                    Tags:
                    <?php echo explodetags($qval['tags']) ?>
                </div>
                <div class="clear"></div>
            </div>
            <br/>
            
        </div>
        <?php } ?>
    
    <?php }else{ ?>
    <!-- <div class="post-1 post type-post">
        <h1 id="post-1">
            <a href="#" title="Lipsum">
                Belum Ada Content</a>
        </h1>
        <div class="entry">
            <div class="thumb-arhive">
                <a href="#" rel="bookmark">
                    <img width="263" height="145" src="<?php //echo base_url()?>assets/theme/images/no_image.jpg" class="attachment-blog-image wp-post-image" alt="Fashion photography by Jenya Kushnir" width="150px" />
                </a>
            </div>
            <div class="excerptarhive">Belum ada content
            </div>
            <div class="clear"></div>
        </div>
        <br/>
        <div class="postmetadata">
            <div class="meta">
                <span class="time">June 7th, 2014</span>
                <em>by</em>webmin reindo</div>
            Tags:
        </div>
    </div> -->
    <?php } ?>
    </div>
    <!-- KRITERION PAGINATION -->
    <!-- KRITERION PAGINATION -->
    <div class="navigation flex_66">
        <div class="pagination">
            <?php echo $pagination?>
            <!--span class='page-numbers current'>1</span>
            <a class='page-numbers' href='page/2/index.html'>2</a>
            <a class="next page-numbers" href="page/2/index.html">Next &raquo;</a-->
        </div>
    </div>
</div>
<div class="flex_33">
    <div id="sidebar">
        <?php echo $this->load->view('detail_sidebar')?>
    </div>
</div>
<div style="clear: both"></div>
<?php if($controller_name == 'kliping') { ?>
<script>
 jQuery(document).ready(function() {
    
            var config = {
              '.chzn-select'           : {},
              '.chzn-select-deselect'  : {allow_single_deselect:true},
              '.chzn-select-no-single' : {disable_search_threshold:10},
              '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
              '.chzn-select-width'     : {width:"100%"}
            }

            for (var selector in config) {
              jQuery(selector).chosen(config[selector]);
            }

            //jQuery("#datepicker").datepicker();

            jQuery("#submit_cari_kliping").click(function(){
                alert(jQuery("#date_kliping").val());
                    //alert($('input[name="totalsemua"]').val());
                        var data = {
                            id_koran:jQuery("#id_koran").val(), 
                            date_kliping:jQuery("#date_kliping").val(), 
                            text_kliping:jQuery("#text_kliping").val() 
                        };
                        //$("#shipping").hide();
                        jQuery.ajax({
                                type: "POST",
                                url : "<?php echo site_url('kliping/search')?>",
                                data: data,
                                success: function(msg){

                                    //var variable_returned_from_php = msg.biaya_kirim;
                                    //$("#shipping").show();
                                    //jQuery("#comment").val(''); 
                                    jQuery("#search_kliping_result").html(msg);
                                },
                                error: function(){
                                    alert('failure');
                                }
                            });                         
                }); 

        }); 
</script>
<?php } ?>