<!-- START USER BLOG PROFILE PAGE -->
<section>
    <?php 
            if($member->num_rows() > 0){
                $v = $member->row_array();
        ?>
        <div class="row">
            <div class="cover-area-profile">
                <?php if($v['cover']) { ?>
                     <img class="cover_photo_admin" src="<?php echo base_url().'uploads/'.$v['cover']?>"/>
                <?php }
				else{ ?>
                    <!-- <img class="cover_photo_admin" src="<?php echo base_url()?>uploads/2013/06/cover-photo-admin.jpg" /> -->
                <?php } ?>
            </div>            
        </div>     

        <div class="container">
            <div class="row">
                <div class="nametag-admin-blog">
                    <div class="admin-photo-profile">
                        <?php if($v['image']) { ?>
                            <img src="<?php echo base_url().'uploads/'.getThumb($v['image'])?>" />
                        <?php }else{ ?>
                            <img src="<?php echo base_url().'assets/theme/images/180x180.jpg'?>" />
                        <?php } ?>
                    </div>
                    <div class="fullname-admin-blog">
                        <h2><?php echo $v['title']?></h2>
                        <div class="admin-edit">
                            <?php if($this->uri->segment(4) == $this->session->userdata('user_id_sess')) { ?>
                                <a href="<?php echo site_url('member/profile')?>"><img width="24" height="24" src="<?php echo base_url()?>assets/theme/images/edit_icon.png" />Edit Profile</a>
                            <?php } ?>
                        </div>
                    </div>
                </div>              
                    <div class="" id="">
                        <div class="wrapper-administrator">
                            <div id="" class="">  
                                <div style="clear:both"></div>    
                                    <div class="row">
                                        <div class="col-lg-9 col-md-9">
                                            <div class="wrapper-left-profile">
                                                <!--<div class="top-about-administrator"></div> -->
                                                
                                                <div class="about-administrator">
                                                    <?php if($v['content']) { ?>
                                                        <p><strong>About Me : </strong><?php echo $v['content']?></p>
                                                    <?php } ?>
                                                </div>
                                                
                                                <div class="latest-activity">
                                                    <h4>Latest Activity</h4>
                                                    <?php if($ma->num_rows() > 0){?>
                                                        <ul>
                                                            <?php foreach($ma->result_array() as $mval) {?>
                                                                <?php 
                                                                    if($mval['activity'] == 'login' || $mval['activity'] == 'logout' || $mval['activity'] == 'update_cover' || $mval['activity'] == 'update_avatar' || $mval['activity'] == 'update_profile' || $mval['activity'] == 'delete_blog'){
                                                                        $url = current_url();
                                                                    }else{
                                                                        $url = site_url($mval['url']);
                                                                    }
                                                                ?>
                                                                <li>
                                                                    <div class="list-of-article">
                                                                        <div class="excerptarhive">
                                                                            <a href="<?php echo $url ?>"><?php echo $mval['title']?></a><span class="activity-date"><i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?php echo date('d M Y H:i:s', strtotime($mval['create_date'])).' WIB'?></span>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            <?php } ?>
                                                        </ul>
                                                    <?php } ?>
                                                </div>                              
                                            </div>                                          
                                        </div>
                                        <div class="col-lg-3 col-md-3">
                                            <div class="home_sidebar">
                                                <div class="follow_us_side">
                                                    <h2>Blog Post</h2>
                                                    <?php if($this->uri->segment(4) == $this->session->userdata('user_id_sess')) { ?>
                                                        <div class="button-post-first">
                                                            <a href="<?php echo site_url('blog/post')?>"><img width="24" height="24" src="<?php echo base_url()?>assets/theme/images/post-icn.png" />Posting Blog</a>
                                                        </div>
                                                    <?php } ?>                                                
                                                </div>
                                                <div id="tab-container" class="tab-container">
                                                    <div class="panel-container">
                                                        <div id="tabs-recent">
                                                            <?php if($qp->num_rows() > 0){?>
                                                                <ul>
                                                                    <?php foreach($qp->result_array() as $qval) {?>
                                                                        <li>
                                                                        <a href="<?php echo site_url('blog/detail/'.$qval['id'].'/'.url_title($qval['title']))?>"><?php echo $qval['title']?>
                                                                            <div class="mr-grey">
                                                                                <i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?php echo date('d M Y', strtotime($qval['create_date']))?>
                                                                            </div>                            
                                                                        </a>
                                                                        </li>
                                                                    <?php } ?>
                                                                </ul>
                                                            <?php } ?>                                                                
                                                        </div>                                        
                                                    </div>                                               
                                                </div>
                                            </div>                                       
                                        </div>                                         
                                    </div>                    
                            </div>
                            <div style="clear:both"></div>
                        </div>
                    </div>                
            </div>
        </div>
    <?php } ?>  
</section>
<!-- END USER BLOG PROFILE PAGE -->