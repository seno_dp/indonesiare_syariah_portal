<?php 
    $image_field = ($controller_name == "article" || $controller_name == "jadwal_closing" || $controller_name == "it_support" || $controller_name == "board_news" || $controller_name == "baporsi" || $controller_name == "csr" || $controller_name == "sp_reindo") ? 'thumbnail' : 'image';
?>
<!-- KRITERION ARCHIVE -->
<div class="flex_66">
    <div class="box">
        <div class="listarticle-title">
            <b><?php echo ($menu_title) ? ucfirst($menu_title) : "Article"?>
                <span class="arrows">&raquo;</span>
            </b>
        </div>
    </div>
    <br/>
    <br/>
    <?php if($qp->num_rows() > 0) {?>
    <?php foreach($qp->result_array() as $qval) {?>
        <div class="post-<?php echo $qval['id']?> post type-post">
            <h1 id="post-<?php echo $qval['id']?>">
                <a href="<?php echo site_url($controller_name.'/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" title="<?php echo $qval['title']?>">
                    <?php echo $qval['title']?>
                </a>
            </h1>
            <div class="entry">
                <?php if($qval[$image_field]) {?>
                <div class="thumb-arhive">
                    <a href="<?php echo site_url($controller_name.'/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" rel="bookmark">
                        <img width="263" src="<?php echo base_url()?>uploads/<?php echo GetThumb($qval[$image_field])?>" class="attachment-blog-image wp-post-image" alt="<?php echo $qval['title']?>"/>
                    </a>
                </div>
                <?php }else{ ?>
                 <div class="thumb-arhive">
                    <a href="<?php echo site_url($controller_name.'/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" rel="bookmark">
                        <img width="263" src="<?php echo base_url()?>assets/theme/images/no_image.jpg" class="attachment-blog-image" alt="<?php echo $qval['title']?>"/>
                    </a>
                </div>
                <?php } ?>
                <div class="excerptarhive"><?php echo $qval['headline']?></div>
                <div class="clear"></div>
            </div>
            <br/>
            <div class="postmetadata">
                <!-- KRITERION META -->
                <div class="meta">
                    <span class="time"><?php echo date('M d, Y',strtotime($qval['create_date']))?></span>
                    <em>by&nbsp;</em><?php echo GetUserName('kg_admin','name',$qval['create_user_id'])?></div>
                Tags:
                <?php echo explodetags($qval['tags']) ?>
            </div>
        </div>
        <?php } ?>
    <?php }else{ ?>
    <!-- <div class="post-1 post type-post">
        <h1 id="post-1">
            <a href="#" title="Lipsum">
                Belum Ada Content
            </a>
        </h1>
        <div class="entry">
            <div class="thumb-arhive">
                <a href="#" rel="bookmark">
                    <img width="263" height="145" src="<?php echo base_url()?>assets/theme/images/no_image.jpg" class="attachment-blog-image wp-post-image" alt="Fashion photography by Jenya Kushnir" width="150px" />
                </a>
            </div>
            <div class="excerptarhive">Belum ada content
            </div>
            <div class="clear"></div>
        </div>
        <br/>
        <div class="postmetadata">
            <div class="meta">
                <span class="time">June 7th, 2014</span>
                <em>by&nbsp;</em>Webmin Reindo</div>
            Tags:
            <a href="#" rel="tag">tags</a>
        </div>
    </div> -->
    <?php } ?>
    <!-- KRITERION PAGINATION -->
    <!-- KRITERION PAGINATION -->
    <div class="navigation flex_66">
        <div class="pagination">
            <?php echo $pagination?>
            <!--span class='page-numbers current'>1</span>
            <a class='page-numbers' href='page/2/index.html'>2</a>
            <a class="next page-numbers" href="page/2/index.html">Next &raquo;</a-->
        </div>
    </div>
</div>
<div class="flex_33">
    <div id="sidebar">
        <?php echo $this->load->view('detail_sidebar')?>
    </div>
</div>
<div style="clear: both"></div>