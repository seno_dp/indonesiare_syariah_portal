<!-- START ABSENCE PAGE -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                    <div class="cc_single_post">
                        <?php 
                            $image_field = ($controller_name == "article") ? 'thumbnail' : 'image';
                        ?>      
                        <div class="title-list-page">
                            <h3><?php echo ($menu_title) ? ucfirst($menu_title) : "Article"?></h3>
                        </div>   
                        <?php if($qp->num_rows() > 0) {?>
                        <?php foreach($qp->result_array() as $qval) {?>
                            <div class="post-<?php echo $qval['id']?>">
                                <!-- <h1 id="post-<?php echo $qval['id']?>">
                                    <a href="<?php echo site_url($controller_name.'/download/'.$qval['id'].'/'.url_title($qval['title']))?>" title="Download <?php echo $qval['title']?>">
                                        <?php echo $qval['title']?>
                                    </a>
                                </h1> -->
                                <div class="">
                                    <div class="list-of-article">
                                        <div class="calender-thumb-blue">
                                                <h4><?php echo date('M',strtotime($qval['date_absence']))?></h4>
                                                <span><?php echo date('d',strtotime($qval['date_absence']))?></span>
                                        </div>
                                        <div class="right-absen">
                                            <h4 class=""><?php echo $qval['title']?></h4>
                                            <div class="post_meta">            
                                                <ul>
                                                    <li>Post on:</li>
                                                    <li><i class="fa fa-clock-o"></i>&nbsp;<?php echo date('M d, Y',strtotime($qval['create_date']))?></li>
                                                </ul>
                                            </div>  
                                            <a class="direct-link-download" href="<?php echo site_url($controller_name.'/download/'.$qval['id'].'/'.url_title($qval['title']))?>" title="Download <?php echo $qval['title']?>">Download</a>
                                        </div>
                                    </div>
                                    <!-- <div class="postmetaabsence">
                                        <div class="meta">
                                            <span class="time"><?php echo date('M d, Y',strtotime($qval['create_date']))?></span>
                                            <em>by&nbsp;</em><?php echo GetUserName('kg_admin','name',$qval['create_user_id'])?> | Type: <?php echo ExplodeNameFile($qval['uploaded_file'])['ext']?>
                                        </div>
                                        <?php if(($qval['uploaded_file']) || ($qval['uploaded_file'] != 0)){?>
                                            <span class="categories">Download File
                                                <a href="<?php echo site_url($controller_name.'/download/'.$qval['id'].'/'.url_title($qval['title']))?>" title="Download <?php echo $qval['title']?>" rel="category tag"><?php echo $qval['title']?></a>
                                            </span>
                                            <br/>
                                        <?php } ?>
                                        Tags:
                                        <?php echo explodetags($qval['tags']) ?>
                                    </div> -->
                                    <div class="clear"></div>
                                </div>
                            </div>
                            <?php } ?>
                        <?php }else{ ?>
                        <div class="post-1 post type-post">
                            <h1 id="post-1">
                                <a href="#" title="Lipsum">
                                    Title Here</a>
                            </h1>
                            <div class="entry">
                                <div class="thumb-arhive">
                                    <a href="#" rel="bookmark">
                                        <img width="263" height="145" src="<?php echo base_url()?>assets/theme/images/no_image.jpg" class="attachment-blog-image wp-post-image" alt="Fashion photography by Jenya Kushnir" width="150px" />
                                    </a>
                                </div>
                                <div class="excerptarhive">Lorem ipsum dolor sit amet, consec tetur adipiscing elit. Quisque eu enim imperdiet, malesuada sapien ac, tempor magna. Cras bibendum adipiscing arcu, id bibendum lorem mattis et. Nulla sed tempus enim. Proin egestas nisi ultricies auctor viverra. Nunc a diam sit amet elit venenatis lacinia sed vel diam. Phasellus vestibulum magna lectus, sit amet accumsan risus ornare eu. Integer in urna sed velit sollicitudin sagittis ut ut arcu. Nam vitae fermentum mauris. Morbi arcu felis, congue vel odio vel, congue laoreet sapien. Maecenas vel pretium magna. Ut...
                                </div>
                                <div class="clear"></div>
                            </div>
                            <br/>
                            <div class="postmetadata">
                                <!-- KRITERION META -->
                                <div class="meta">
                                    <span class="time">June 7th, 2013</span>
                                    <em>by</em>admin | Type: Audio</div>
                                Tags:
                                <a href="#" rel="tag">indonesiare</a>,
                                <a href="#" rel="tag">reinsurance</a>,
                                <a href="#" rel="tag">indonesia</a>,
                            </div>
                        </div>
                        <?php } ?>                                   
                        <!-- START PAGINATION AREA --> 
                        <div class="news_pagination">
                            <ul class="news_pagi">
                                <?php echo $pagination?>
                            </ul>
                        </div>     
                        <!-- END PAGINATION AREA -->                           
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="home_sidebar">
                        <div class="follow_us_side">
                            <?php echo $this->load->view('detail_sidebar')?>  
                        </div>
                    </div>                      
                </div>
            </div>
        </div>
    </section>
<!-- END ABSENCE PAGE -->
