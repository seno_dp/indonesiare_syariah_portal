<div class="flex_66_1">
    <div class="post" id="post-35">
        <div class="entry">				
		<h1><?php echo $menu_title?></h1>
            <div itemscope itemtype="http://schema.org/Product" id="product-<?php echo $id_album?>" class="post-<?php echo $id_album?>">
				<!-- Gallery Images -->
                <div class="images">
                    <?php if($qp->num_rows() > 0) {?>
                    <div class="thumbnails">
                        <?php foreach($qp->result_array() as $qval) {?>
                        <a href="<?php echo base_url()?>uploads/<?php echo $qval['image'] ?>" class="zoom first" data-rel="prettyPhoto[product-gallery]" title="<?php echo $val['title']?>">
                            <img style="max-width: 143px" src="<?php echo base_url()?>uploads/<?php echo $qval['image'] ?>" class="attachment-shop_thumbnail" alt="<?php echo $val['title']?>"/>
                        </a>
                        <?php } ?>
                        
                    </div>
                    <?php } ?>
                </div>
				<!-- Gallery Images -->
                <meta itemprop="url" content="index.html" />

            </div>
            <!-- #product-523 -->

            <div style="clear:both"></div>
        </div>
    </div>
</div>
<div class="flex_33">
    <div id="sidebar">
        <?php echo $this->load->view('detail_sidebar')?>
    </div>
</div>
<div style="clear: both"></div>