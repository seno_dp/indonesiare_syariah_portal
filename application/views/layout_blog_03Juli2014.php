<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">

<head profile="http://gmpg.org/xfn/11">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Reindo Reinsurance</title>
    <link rel="shortcut icon" href="#" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/theme/style.css" type="text/css" />
     <?php 
        $filter_background = array("is_publish"=>"where/publish","limit"=>"0/1","id"=>"order/desc");
        $query_background = GetAll("kg_background",$filter_background);
        if($query_background->num_rows() > 0){
                $val_background = $query_background->row_array(); 
                $img_background = $val_background['image'];
                $title_background = $val_background['title'];
                $url_background = base_url().'uploads/'.$img_background;
        }else{
                $url_background = base_url().'assets/theme/images/background.jpg';
        }
    ?>
  
<link rel='stylesheet' href="<?php echo base_url()?>assets/theme/css/tabber.css" type="text/css"/>
    <style type='text/css'>
        body {
            background: #980613 url("<?php echo $url_background?>") ;
            background-position: center top;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }
        .logo {
            float: left;
            background: none;
        }
        #top-featured .border {
            color: #2a2a2a;
            border-bottom: 4px solid #D51F2B;
            width: 922px;
            margin: -10px 5px 5px 6px;
            font: normal 20px'Fjalla One', Helvetica, Arial, Sans-Serif;
            color: #000;
        }
        #wrapper-menu {
            width: 960px;
            height: 45px;
            background: #454545;
        }
        #navigation li ul {
            width: auto !important;
            background: 0 0 repeat #454545;
        }
        #navigation {
            list-style: none;
            margin: 0 auto;
            padding: 0;
            border-bottom: 4px solid #D51F2B;
        }
        #top-featured .featured_posts .plus {
            background: #D51F2B url(wp-content/themes/REINDO/images/sign-next.png) no-repeat;
            width: 38px;
            height: 24px;
            position: absolute;
            right: -4px;
            top: 10px;
        }
        #top-featured .featured_posts .depth {
background: #D51F2B url(wp-content/themes/REINDO/images/depth.png) no-repeat 0 0;
            width: 4px;
            height: 4px;
            position: absolute;
            top: 34px;
            right: -4px;
        }
        .top-nav {
            background: #fff;
            
            border-bottom: 1px solid #ececec;
            
            height: 30px;
        }
        .top-nav ul li.current-menu-item:after {
            content: " ";
            display: block;
            width: 0;
            height: 0;
            position: absolute;
            top: 0px;
            z-index: 2;
            right: 45%;
            border: 4px solid transparent;
            border-top-color: #D51F2B;
        }
        .search-block .search-button {
            background: #D51F2B url(wp-content/themes/REINDO/images/search.png) no-repeat center;
            cursor: pointer;
            float: right;
            height: 30px;
            width: 50px;
            text-indent: -999999px;
            display: block;
            border: 0 none;
        }
        .flex-control-paging li a:hover {
            background: #D51F2B;
        }
        .flex-control-paging li a.flex-active {
            background: #D51F2B;
        }
        .arrows {
            font-family: Arial;
            font-size: 27px;
            color: #D51F2B;
            text-align: left;
        }
        .arrows-tabs {
            font-family: Arial;
            font-size: 18px;
            color: #D51F2B;
            text-align: left;
        }
        #footer-border {
            height: 29px;
            width: 961px;
            position: absolute;
            left: 0px;
            top: -21px;
            background: #D51F2B;
            opacity: 0.9;
        }
        #comment-block {
            border-top: 4px solid #D51F2B;
        }
        .scrollup:hover {
            background: url(wp-content/themes/REINDO/images/icon_top.png) center center no-repeat #D51F2B;
        }
        a {
            color: #0d0c0c;
            -moz-transition: .6s linear;
            -webkit-transition: .6s ease-out;
            transition: .6s linear;
        }
        a:focus,
        a:active,
        a:hover {
            color: #D71A1A;
            text-decoration: none;
        }
        .box .text h5 a:hover {
            color: #D71A1A;
        }
        .tags a:hover {
            background-color: #D51F2B;
            color: #FFF;
            -moz-transition: .8s linear;
            -webkit-transition: .8s ease-out;
            transition: .8s linear;
        }
        .arrows-tweet {
            font-family: Arial;
            font-size: 15px;
            color: #D51F2B;
            text-align: left;
        }
        .postdate {
            position: absolute;
            margin-left: -61px;
            padding: 5px;
            background: #D51F2B;
            width: 30px;
        }
        div.pagination a {
            background-color: #D51F2B;
        }
        #kr-carousel .next {
            background-color: #D51F2B;
        }
        #kr-carousel .prev {
            background-color: #D51F2B;
        }
    </style>
    <link rel="pingback" href="xmlrpc.php" />
    <!-- <link rel="alternate" type="application/rss+xml" title="Reindo Reinsurance &raquo; Feed" href="../../feed/index.html" />
    <link rel="alternate" type="application/rss+xml" title="Reindo Reinsurance &raquo; Comments Feed" href="../../comments/feed/index.html" />
    <link rel="alternate" type="application/rss+xml" title="Reindo Reinsurance &raquo; Fashion Category Feed" href="feed/index.html" />
    <link rel='stylesheet' id='awesome-weather-css' href='../../wp-content/plugins/awesome-weather/awesome-weathercd70.css?ver=3.8.3' type='text/css' media='all' /> -->
    <link rel='stylesheet' id='opensans-googlefont-css' href='<?php echo base_url()?>assets/theme/fonts/opensans.css' type='text/css' media='all' />
    <!-- <link rel='stylesheet' id='tp_twitter_plugin_css-css' href='../../wp-content/plugins/recent-tweets-widget/tp_twitter_plugin5152.css?ver=1.0' type='text/css' media='screen' />
    <link rel='stylesheet' id='woocommerce-layout-css' href='../../wp-content/plugins/woocommerce/assets/css/woocommerce-layout3c94.css?ver=2.1.0' type='text/css' media='all' />
    <link rel='stylesheet' id='woocommerce-smallscreen-css' href='../../wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen3c94.css?ver=2.1.0' type='text/css' media='only screen and (max-width: 768px)' />
    <link rel='stylesheet' id='woocommerce-general-css' href='../../wp-content/plugins/woocommerce/assets/css/woocommerce3c94.css?ver=2.1.0' type='text/css' media='all' /> -->
	<!--[if IE]><link rel="stylesheet" href="theme/css/forIE.css" type="text/css" /></link> <![endif]--> 
    <script src="<?php echo base_url()?>assets/theme/js/jquery-1.7.1.min.js" type="text/javascript"></script>	
	<!-- <script type='text/javascript' src='theme/js/jquery3e5a.js?ver=1.10.2'></script> -->
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery-migrate.min1576.js?ver=1.2.1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery.carousel68b3.js?ver=1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery.ticker68b3.js?ver=1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/ajaxtabs68b3.js?ver=1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery-ui.min68b3.js?ver=1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/superfish68b3.js?ver=1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/supersubs68b3.js?ver=1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/custom68b3.js?ver=1'></script>
    <script src='<?php echo base_url()?>assets/theme/js/jquery.easytabs.js' type="text/javascript"></script>
    <script src='<?php echo base_url()?>assets/theme/js/jquery.hashchange.min.js' type="text/javascript"></script>	
	
	
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="#" />

    <!-- Ck editor >
    <script type="text/javascript" src="<?php echo base_url()?>assets/theme/js/ckeditor/ckeditor.js"></script> 
    <script src="<?php echo base_url()?>assets/theme/js/ckeditor/config.js"></script>
    <link href="<?php echo base_url()?>assets/theme/js/ckeditor/sample.css" rel="stylesheet">
    <Ck editor -->

</head>
<?php /*if($this->session->userdata('user_id_sess')) {*/?>
<body class="archive category category-fashion category-3">
    <div class="outer_wrap">
        <div class="inner_wrap_blog">
        <!-- KRITERION TOP MENU -->
        <?php echo $this->load->view('main_menu_blog');?>			
        
        <?php 
            $id = $this->session->userdata('user_id_sess');
            $filter = array(
                    "id"=>"where/".$id,
                );

            $member = GetAll('kg_member',$filter);
            if($member->num_rows() > 0){
            $v = $member->row_array();
        ?>
		<div class="cover-area-blog">
			<img src="<?php echo base_url()?>uploads/2013/06/cover-photo-admin.jpg" />
		</div>
        
		<div style="clear:both"></div>
		<div class="nametag-admin">
			<div class="admin-photo-profile">
                <img src="<?php echo base_url().'uploads/'.getThumb($v['image'])?>" />
                <!-- <img src="<?php echo base_url()?>uploads/2013/06/Rianti_Arthawidya.jpg" alt="photo administrator" /> --></div>								
			<div class="fullname-admin-blog">
				<h2><?php echo $v['title']?></h2>
                <input class="button-blog" type="button" value="My blog" name="my_blog" id="my_blog">
                <input class="button-blog" type="button" value="My profile" name="my_profile" id="my_profile">
                <input class="button-blog" type="button" value="Posting blog" name="posting_blog" id="posting_blog">
			</div>
		</div>								            		
        <div class="shaddow"></div>
        <?php }?>
       
        <div id="main-content-blog">
            
            <?php echo $this->load->view($main_content);?>

        </div>
        <div id="footer">
            <div id="footer-border"></div>
            <div class="flex_33_1">
                <div class="widget-footer">Kontak
                    <span class="arrows">&raquo;</span>
                </div>


                <div class="social-profile-icons">
                    <div class="textwidget">Jl. Salemba Raya No 30 Jakarta Pusat 10430 INDONESIA
                        <br>
                        <br>Kontak
                        <br/>Telp: 62-21 3920101,334208 Fax: 62-21 3143828
                        <br/>Email : cosecretary@reindo.co.id
                        <br/>
                    </div>
                </div>
            </div>

            <div class="flex_33_1">
                <div class="widget-footer">
                </div>
            </div>
            <div class="copyrigt">
                <span class="right-copy">Copyright&copy; 2013 - PT Reasuransi Internasional Indonesia - Alright reserved</span>
            </div>
        </div>		
    </div>
    <a href="#" class="scrollup">Scroll</a>
    <script type="text/javascript">
        jQuery(document).ready(function(){
                jQuery("#submit-comment-blog").click(function(){
                    //alert($('input[name="totalsemua"]').val());
                        var data = {
                            blog_id:jQuery("#blog_id").val(), 
                            member_id:jQuery("#member_id").val(), 
                            comment:jQuery("#comment").val() 
                        };
                        //$("#shipping").hide();
                        jQuery.ajax({
                                type: "POST",
                                url : "<?php echo site_url('blog/comment')?>",
                                data: data,
                                success: function(msg){
                                    //var variable_returned_from_php = msg.biaya_kirim;
                                    //$("#shipping").show();
                                    jQuery("#comment").val(''); 
                                    jQuery("#area-commenters").html(msg);
                                },
                                error: function(){
                                    alert('failure');
                                }
                            });                         
                }); 

                jQuery("#posting_blog").click(function(){
                    window.location.href = "<?php echo site_url('blog/post')?>";
                });

                jQuery("#my_profile").click(function(){
                    window.location.href = "<?php echo site_url('member/profile')?>";
                });

                jQuery("#my_blog").click(function(){
                    window.location.href = "<?php echo site_url('blog/my_profile')?>";
                });
            
        })
    </script>
    
</body>
<?php /*}*/ ?>

</html>