<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!--[if IE]><![endif]-->
<!--[if lt IE 7 ]> <html lang="en" class="ie6">    <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7">    <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8">    <![endif]-->
<!--[if IE 9 ]>    <html lang="en" class="ie9">    <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">

<head profile="http://gmpg.org/xfn/11">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Intranet PT. Reasuransi Internasional Indonesia</title>
    <link rel="shortcut icon" href="#" />
    <!-- <link rel="stylesheet" href="<?php echo base_url()?>assets/theme/css/jquery-ui.css" type="text/css" /> -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/theme/style.css" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/theme/css/chosen.css" type="text/css" />
    <link rel='stylesheet' href="<?php echo base_url()?>assets/theme/css/tabber.css" type="text/css"/>
	<!-- ~~~=| STYLE 2017|==-->
    <!-- ~~~=| Fonts files |==-->
	<link rel='stylesheet' href="<?php echo base_url()?>assets/theme/css/font-awesome.min.css" type="text/css"/>
	<link rel='stylesheet' href="<?php echo base_url()?>assets/theme/css/lato-fonts.css" type="text/css"/>
	<link rel='stylesheet' href="<?php echo base_url()?>assets/theme/css/roboto-fonts.css" type="text/css"/>
    <!-- ~~~=| Fonts files |==-->	
	<link rel='stylesheet' href="<?php echo base_url()?>assets/theme/css/bootstrap.css" type="text/css"/>
	<link rel='stylesheet' href="<?php echo base_url()?>assets/theme/css/bootstrap.css" type="text/css"/>
	<link rel='stylesheet' href="<?php echo base_url()?>assets/theme/css/style-2017.css" type="text/css"/>
	<link rel='stylesheet' href="<?php echo base_url()?>assets/theme/css/jquery-ui.css" type="text/css"/>

    <!-- ~~~=| STYLE 2017|==-->    
    <?php 
        $filter_background = array("is_publish"=>"where/publish","limit"=>"0/1","id"=>"order/desc");
        $query_background = GetAll("kg_background",$filter_background);
        if($query_background->num_rows() > 0){
                $val_background = $query_background->row_array(); 
                $img_background = $val_background['image'];
                $title_background = $val_background['title'];
                $url_background = base_url().'uploads/'.$img_background;
        }else{
                $url_background = base_url().'assets/theme/images/background.jpg';
        }
    ?>
    <!--<style type='text/css'>
        body {
            background: #980613 url("<?php echo $url_background?>") ;
            background-position: center top;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }
        .logo {
            float: left;
            background: none;
            margin-top:-16px
        }
        #top-featured .border {
            color: #2a2a2a;
            border-bottom: 4px solid #D51F2B;
            width: 922px;
            margin: -10px 5px 5px 6px;
            font: normal 20px'Fjalla One', Helvetica, Arial, Sans-Serif;
            color: #000;
        }
        #wrapper-menu {
            width: 960px;
            height: 45px;
            background: #454545;
        }
        #navigation li ul {
            width: auto !important;
            background: 0 0 repeat #454545;
        }
        #navigation {
            list-style: none;
            margin: 0 auto;
            padding: 0;
            border-bottom: 4px solid #D51F2B;
        }
        #top-featured .featured_posts .plus {
            background: #D51F2B url(wp-content/themes/REINDO/images/sign-next.png) no-repeat;
            width: 38px;
            height: 24px;
            position: absolute;
            right: -4px;
            top: 10px;
        }
        #top-featured .featured_posts .depth {
            background: #D51F2B url(wp-content/themes/REINDO/images/depth.png) no-repeat 0 0;
            width: 4px;
            height: 4px;
            position: absolute;
            top: 34px;
            right: -4px;
        }
        .top-nav {
            background: none;
            border-top: 3px solid #D51F2B;
            height: 42px;
        }
        .top-nav ul li.current-menu-item:after {
            content: " ";
            display: block;
            width: 0;
            height: 0;
            position: absolute;
            top: 0px;
            z-index: 2;
            right: 45%;
            border: 4px solid transparent;
            border-top-color: #D51F2B;
        }
        .search-block .search-button {
            background: #D51F2B url(wp-content/themes/REINDO/images/search.png) no-repeat center;
            cursor: pointer;
            float: right;
            height: 30px;
            width: 50px;
            text-indent: -999999px;
            display: block;
            border: 0 none;
        }
        .flex-control-paging li a:hover {
            background: #D51F2B;
        }
        .flex-control-paging li a.flex-active {
            background: #D51F2B;
        }
        .arrows {
            font-family: Arial;
            font-size: 27px;
            color: #D51F2B;
            text-align: left;
        }
        .arrows-tabs {
            font-family: Arial;
            font-size: 18px;
            color: #D51F2B;
            text-align: left;
        }
        #footer-border {
            height: 29px;
            width: 961px;
            position: absolute;
            left: 0px;
            top: -21px;
            background: #D51F2B;
            opacity: 0.9;
        }
        #comment-block {
            border-top: 4px solid #D51F2B;
        }
        .scrollup:hover {
            background: url(wp-content/themes/REINDO/images/icon_top.png) center center no-repeat #D51F2B;
        }
        a {
            color: #0d0c0c;
            -moz-transition: .6s linear;
            -webkit-transition: .6s ease-out;
            transition: .6s linear;
        }
        a:focus,
        a:active,
        a:hover {
            color: #D71A1A;
            text-decoration: none;
        }
        .box .text h5 a:hover {
            color: #D71A1A;
        }
        .tags a:hover {
            background-color: #D51F2B;
            color: #FFF;
            -moz-transition: .8s linear;
            -webkit-transition: .8s ease-out;
            transition: .8s linear;
        }
        .arrows-tweet {
            font-family: Arial;
            font-size: 15px;
            color: #D51F2B;
            text-align: left;
        }
        .postdate {
            position: absolute;
            margin-left: -61px;
            padding: 5px;
            background: #D51F2B;
            width: 30px;
        }
        div.pagination a {
            background-color: #D51F2B;
        }
        #kr-carousel .next {
            background-color: #D51F2B;
        }
        #kr-carousel .prev {
            background-color: #D51F2B;
        }
    </style>-->
    <link rel='stylesheet' id='opensans-googlefont-css' href='<?php echo base_url()?>assets/theme/fonts/opensans.css' type='text/css' media='all' />
    <link rel='stylesheet' id='woocommerce_prettyPhoto_css-css' href='<?php echo base_url()?>assets/theme/css/prettyPhotocd70.css?ver=3.8.3' type='text/css' media='all' />
    <?php if($this->uri->segment(2) != 'foto_jalbum'){ ?>
        <link rel='stylesheet' href='<?php echo base_url()?>assets/theme/css/slider.css' type='text/css' media='all' />
    <?php }else{ ?>
        <link rel='stylesheet' href='<?php echo base_url()?>assets/theme/css/slider_j.css' type='text/css' media='all' />
    <?php } ?>
    <!--[if IE]><link rel="stylesheet" href="<?php echo base_url()?>assets/theme/css/forIE.css" type="text/css" /></link> <![endif]-->     

    <link rel="stylesheet" href="<?php echo base_url()?>assets/theme/css/themes/base/jquery.ui.all.css">
       
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery3e5a.js?ver=1.10.2'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery-migrate.min1576.js?ver=1.2.1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery.carousel68b3.js?ver=1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery.ticker68b3.js?ver=1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/ajaxtabs68b3.js?ver=1'></script>
    <!--<script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery-ui.min68b3.js?ver=1'></script>-->
	<script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery-ui.js'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/superfish68b3.js?ver=1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/supersubs68b3.js?ver=1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/custom68b3.js?ver=1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery.prettyPhoto.minc6bd.js?ver=3.1.5'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery.prettyPhoto.init.min3c94.js?ver=2.1.0'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/chosen.jquery.js'></script>
    <script src='<?php echo base_url()?>assets/theme/js/jquery.easytabs.js' type="text/javascript"></script>
    <script src='<?php echo base_url()?>assets/theme/js/jquery.hashchange.min.js' type="text/javascript"></script>
	<script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery.fancybox.js'></script>
	<script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery.datetimepicker.js'></script>
	<script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery.validate.js'></script>
	<!-- JS & PLUGIN 2017 -->
	<script type='text/javascript' src='<?php echo base_url()?>https://code.jquery.com/jquery.min.js'></script>
	<script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/bootstrap.min.js'></script>
	<!-- ~~~=| Opacity & Other IE fix for older browser |=~~~ --> 
	<!--[if lte IE 8]>
		<script type='text/javascript' src=<?php echo base_url()?>assets/theme/js/ie-opacity-polyfill.js'></script>
	<![endif]--> 	
	<script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/main.js'></script>	
	<!-- JS & PLUGIN 2017 -->
</head>
<body>

    <div class="container-fluid">
<!--
        <?php 
        $filter_logo = array("is_publish"=>"where/publish","limit"=>"0/1","id"=>"order/desc");
        $query_logo = GetAll("kg_header",$filter_logo);
        if($query_logo->num_rows() > 0){
            
                $val_logo = $query_logo->row_array();
                $img_logo = $val_logo['image'];
                $title_logo = $val_logo['title'];

                $bg_header_style = "background: url(".base_url()."uploads/".$img_logo.") no-repeat top #454545; padding: 0px 0px 59px 0px;";

                /*echo '<div class="logo">';
                    echo '<a href="'.site_url('home').'">';
                        echo '<img src="'.base_url().'uploads/'.$img_logo.'" alt="'.$title_logo.'" border="0" />';
                    echo '</a>';
                echo '</div>';*/
        }else{
                $bg_header_style = "background: url(".base_url()."uploads/2013/06/bg_header.jpg) no-repeat top #454545; padding: 0px 0px 59px 0px;";
                /*echo '<div class="logo">';
                    echo '<a href="'.site_url('home').'">';
                        echo '<img src="'.base_url().'assets/theme/images/logo.png" alt="Reindo Reinsurance" border="0" />';
                    echo '</a>';
                echo '</div>';*/
        }
        ?>
-->

        <div class="row">
            <!-- REINDO HEADER -->
            <div class="">
                <?php echo $this->load->view($header)?>
            </div>
            <!-- REINDO MENU -->
            <?php echo $this->load->view($main_menu)?>
        </div>
        <!-- REINDO LATEST NEWS TIKER -->
        <?php //echo $this->load->view('news_ticker')?>
        <?php //die('error');?>
        <!-- REINDO CONTENT -->
        <div id="">
            <?php echo $this->load->view($main_content)?>
        </div>
        <div style="clear: both"></div>
        <!-- REINDO FOOTER -->
        <div class="row">
            <?php echo $this->load->view($footer);?>
        </div>
    </div>
    <a href="#" class="scrollup">Scroll</a>
    <script type="text/javascript">
        jQuery(document).ready(function(){
                jQuery("#submit-comment-blog").click(function(){
                    var data = {
                            blog_id:jQuery("#blog_id").val(), 
                            member_id:jQuery("#member_id").val(), 
                            comment:jQuery("#comment").val() 
                        };
                        jQuery.ajax({
                                type: "POST",
                                url : "<?php echo site_url('blog/comment')?>",
                                data: data,
                                success: function(msg){
                                    jQuery("#comment").val(''); 
                                    jQuery("#area-commenters").html(msg);
                                },
                                error: function(){
                                    alert('failure');
                                }
                            });                         
                }); 

                jQuery("#posting_blog").click(function(){
                    window.location.href = "<?php echo site_url('blog/post')?>";
                });

                 jQuery("#my_profile").click(function(){
                    window.location.href = "<?php echo site_url('member/profile')?>";
                });

                 jQuery(".delete_avatar").click(function(){
                    var data = {
                            id:jQuery('input[name="id"]').val(),
                            file_old:jQuery('input[name="file_old"]').val()
                        };
                        jQuery.ajax({
                                type: "POST",
                                url : "<?php echo site_url('member/del_image')?>",
                                data: data,
                                success: function(){
                                    jQuery(".thumb_image_avatar").fadeOut(1000);
                                },
                                error: function(){
                                    alert('failure');
                                }
                            });                
                });

                 jQuery(".delete_cover").click(function(){
                    var data = {
                            id:jQuery('input[name="id"]').val(),
                            file_old_cover:jQuery('input[name="file_old_cover"]').val()
                        };
                        jQuery.ajax({
                                type: "POST",
                                url : "<?php echo site_url('member/del_cover')?>",
                                data: data,
                                success: function(){
                                    jQuery(".thumb_image_cover").fadeOut(1000);
                                },
                                error: function(){
                                    alert('failure');
                                }
                            });                
                });
            
        })
    </script>
</body>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
</html>