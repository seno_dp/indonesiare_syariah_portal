<!-- KNOWLEEDGE DETAIL / REINFOKUS DETAIL -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="cc_single_post">
                    <?php if($knowledge->num_rows() > 0) {?>
                    <?php $val = $knowledge->row_array();?>
                    <div class="list-of-article" id="">
                        <!-- REINDO BREAD CRUMB -->
                        <div class="breadcum_c_left">
                            <a href="<?php echo site_url('home')?>">Home</a> / 
                            <a href="<?php echo site_url($controller_name)?>"><?php echo ucfirst($controller_name) ?></a> /
                            <?php echo $val['title']?>
                        </div>
                        <h2>
                            <?php echo $val['title']?>
                        </h2>
                         <?php if($val['image']) {?>
                         <?php if($val['is_thumbnail'] == 1) {?>
                        <div class="image-thumb">
                            <img src="<?php echo base_url()?>uploads/<?php echo $val['image']?>" style="max-width:300px;"/>
                        </div>
                        <?php } ?>
                        <?php } ?>
                        <div class="post_text">
                            <?php echo $val['content']?>
                        </div>
                        <div class="post_meta">
                            <div class="date-by-author">
                                <?php if($controller_name == 'kliping') { ?>
                                            <i class="fa fa-clock-o"></i>&nbsp;Tanggal koran <?php echo date('M d, Y',strtotime($val['date_koran'])) ?><br/>
                                            <i class="fa fa-filter"></i>&nbsp;Koran <?php echo $val['title_koran'] ?><br/>
                                        <?php } ?>
                                <i class="fa fa-clock-o"></i>&nbsp;<?php echo date('M d, Y',strtotime($val['create_date']))?>
                                <em>by </em>
                                <?php echo GetUserName('kg_admin','name',$val['create_user_id'])?>
                            </div>
                            <?php if($val['uploaded_file'] || ($val['uploaded_file'] != 0)) {?>
                            <i class="fa fa-download"></i>&nbsp;Download File
                                <a href="<?php echo site_url($controller_name.'/download/'.$val['id'].'/'.url_title($val['title']))?>" title="Download <?php echo $val['title']?>" target="_BLANK"><?php echo $val['title']?></a>
                            <?php } ?>
                            <br/>
                            Tags:
                            <?php echo explodetags($val['tags']) ?>
                        </div>
                    </div>
                    <?php }?>

                    <?php if($rel_link->num_rows() > 0) {?>
                    <!-- RELATED POSTS -->
                    <div class="">
                        <div class="follow_us_side">
                            <h2>Related Posts</h2>
                        </div>
                    </div>
                    <div style="clear:both"></div>
					<div class="row">
						<div id="related-posts">
							<ul>
								<?php foreach($rel_link->result_array() as $rval) { ?>
								<li>
									<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
										<?php if($rval['image']) {?>
										<a href="<?php echo site_url($controller_name.'/detail/'.$rval['id'].'/'.url_title($rval['title']))?>" rel="bookmark">
											<img class="img-responsive" src="<?php echo base_url()?>uploads/<?php echo getThumb($rval['image'])?>" />
										</a>
										<?php } ?>
										<div class="title-related">
											<a href="<?php echo site_url($controller_name.'/detail/'.$rval['id'].'/'.url_title($rval['title']))?>" rel="bookmark"><?php echo $rval['title']?></a>
										</div>                                    
									</div>
								</li>
								<?php } ?>
							</ul>
						</div>					
					</div>
                    <?php } ?>                    
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="home_sidebar">
                    <div class="follow_us_side">
                        <?php echo $this->load->view('detail_sidebar')?> 
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- KNOWLEEDGE DETAIL / REINFOKUS DETAIL -->
