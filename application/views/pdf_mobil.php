<?php
    $date1 = date_create($villa['start_date']);
    $date2 = date_create($villa['end_date']);
    $interval = date_diff($date1, $date2);
    //echo 'Tanggal : '.date('d M Y', strtotime($mval['start_date'])).' WIB - '.date('d M Y', strtotime($mval['end_date'])).' WIB'
    //echo 'Lama Istirahat : '.$interval->format('%a hari')?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">

<head profile="http://gmpg.org/xfn/11">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Intranet PT. Reasuransi Internasional Indonesia</title>
    <link rel="shortcut icon" href="#" />
    <!-- <link rel="stylesheet" href="<?php echo base_url()?>assets/theme/css/jquery-ui.css" type="text/css" /> -->
    
    <style type='text/css'>
        #header {
			margin-bottom:20px;
			clear:both;
		}
		#header td.left {
			width:300px;
		}
		#header td.right {
			width:430px;
		}
		#header td.right h2{
			font-size:18px;
			text-align: center;
		}
		
		#info-header {
			width:100%;
			border:1px solid #000;
			font-size:11px;
			padding:5px 12px;
			margin-top:10px;
			clear:both;
		}
		#info-header h4{
			margin:0px;
			padding:0px;
			font-size:12px;
		}
		#info-header ul {
			margin-left:-20px;
			padding:0px;
		}
		
		#content {
			margin-top:20px;
			font-size:14px;
		}
		#content td{
			padding:5px 0px;
		}
		#content .left {
			width:200px;
		}
		
		#ttd {
			width:100%;
			text-align:center;
			margin-bottom:10px;
		}
		#ttd th {
			width:33%;
			font-weight:bold;
			font-size:15px;
			height:75px;
		}
		
		#uraian {
			width:97%;
			border:1px solid #000;
			font-size:14px;
			padding:10px;
			margin-top:20px;
		}
		#uraian .left {
			width: 40%;
		}
		#uraian .right {
			width: 60%;
		}
		#uraian .center {
			text-align:center;
		}
		#uraian .box {;
			border:1px solid #000;
			height:80px;
		}
    </style>
   
</head>
<body class="home blog">

    <div class="outer_wrap"> 
       <!--  <div class="flex_100" style="padding: 0.5%;width: 980px;border: 1px solid #000;"> -->
        	<table id="header">
        		<tr>
					<td class="left">
						<img src="<?php echo base_url();?>assets/theme/images/logo.png">
					</td>
					<td class="right">
						<H2>FORMULIR IZIN MENINGGALKAN KANTOR<br/>
						UNTUK KEPERLUAN DINAS / PRIBADI</h2>
					</td>
				</tr>
        	</table>
			<table id="info-header">
        		<tr>
					<td>
						<h4>Keterangan :</h4>
						<ul>
							<li>Keperluan dinas, formulir diisi rangkap 2 (dua) sedangkan untuk pribadi 1 (satu) lembar</li>
							<li>Khusus untuk keperluan pribadi, formulir hanya digunakan bagi pegawai yang izin meninggalkan kantor dimana telah melaksanakan aktivitas pekerjaan untuk hari yang sama dengan tanggal pengajuan surat ini</li>
							<li>Khusus untuk izin keperluan pribadi, penggunaan waktu lebih dari 3 (tiga) jam akan dipotong 1/2 hari kerja sedangkan untuk kelipatannya akan dipotong 1 (satu) hari kerja</li>
						</ul>
					</td>
				</tr>
        	</table>
			<table id="content">
        		<tr>
					<td class="left">
						Nama
					</td>
					<td class="right">
					: &nbsp;<?php echo GetValue('title','kg_member', array('id'=>'where/'.$mobil['member_id']));?>	
					</td>
				</tr>
				<tr>
					<td class="left">
						Bagian / Divisi
					</td>
					<td class="right">
					: &nbsp;<?php echo GetValue('title','kg_pangkat', array('id'=>'where/'.$mobil['member_id']));?>	
					</td>
				</tr>
				<tr>
					<td class="left">
						Keperluan *
					</td>
					<td class="right">
					: &nbsp;<?php echo $mobil['keperluan'];?>	
					</td>
				</tr>
				<tr>
					<td class="left">
						Rencana Berangkat Jam
					</td>
					<td class="right">
					: &nbsp;<?php echo $mobil['date_pinjam'];?>&nbsp; WIB	
					</td>
				</tr>
				<tr>
					<td class="left">
						Tujuan
					</td>
					<td class="right">
					: &nbsp;<?php echo $mobil['tujuan'];?>	
					</td>
				</tr>
				<tr>
					<td class="left">
						Keterangan
					</td>
					<td class="right">
					: &nbsp;<?php echo $mobil['description'];?>	
					</td>
				</tr>
				<tr>
					<td class="left">
						Kendaraan **
					</td>
					<td class="right">
					: &nbsp;<?php echo GetValue('title','kg_mobil', array('id'=>'where/'.$mobil['mobil_id'])).", ".GetValue('plat_no','kg_mobil', array('id'=>'where/'.$mobil['mobil_id']));?>	
					</td>
				</tr>
				<tr>
					<td class="left">
						Pengemudi **
					</td>
					<td class="right">
					: &nbsp;<?php echo GetValue('title','kg_driver', array('id'=>'where/'.$mobil['driver_id']));?>	
					</td>
				</tr>
				<tr>
					<td class="left">
						Berangkat Jam ***
					</td>
					<td class="right">
				<?php if(GetValue('km_terakhir','kg_mobil', array('id'=>'where/'.$mobil['mobil_id'])) == 0) { ?>
					: .................................................................................  KM ............................................
				<?php } else {?>
					: .................................................................................  KM <?php echo GetValue('km_terakhir','kg_mobil', array('id'=>'where/'.$mobil['mobil_id']));?>	
				<?php }?>
					</td>
				</tr>
				<tr>
					<td class="left">
						Kembali Jam ***
					</td>
					<td class="right">
					: .................................................................................  KM ............................................	
					</td>
				</tr>
				<tr>
					<td colspan="2" style="text-align:right">
						Jakarta, <?php echo date('d-m-Y');?>
					</td>
				</tr>
        	</table>
			
			<table id="ttd">
				<tr>
					<th>Pelayanan Korporasi</th>
					<th>Pimpinan Ybs.</th>
					<th>Pemohon</th>
				</tr>
				<tr>
					<td>.............................................</td>
					<td>.............................................</td>
					<td>.............................................</td>
				</tr>
			</table>
			
			<table id="uraian">
				<tr>
					<td colspan="2"><b>Uraian dinas (diisi oleh pengemudi / pemakai kendaraan)</b></td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td class="left center">
						Jam
					</td>
					<td class="right center">
						Tujuan dinas
					</td>
				</tr>
				<tr>
					<td class="left center">................................ s/d ................................ 
					</td>
					<td class="right center">.....................................................................................................
					</td>
				</tr>
				<tr>
					<td class="left center">................................ s/d ................................ 
					</td>
					<td class="right center">.....................................................................................................
					</td>
				</tr>
				<tr>
					<td class="left center">................................ s/d ................................ 
					</td>
					<td class="right center">.....................................................................................................
					</td>
				</tr>
				<tr>
					<td class="left center">................................ s/d ................................ 
					</td>
					<td class="right center">.....................................................................................................
					</td>
				</tr>
				<tr>
					<td class="left center">................................ s/d ................................ 
					</td>
					<td class="right center">.....................................................................................................
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td>Catatan PK</td>
					<td class="center">Tanda Tangan</td>
				</tr>
				<tr>
					<td class="box" rowspan="2">&nbsp;</td>
					<td class="center">Pengemudi / Pemakai Kendaraan</td>
				</tr>
				<tr>
					
					<td class="center" style="vertical-align:bottom">.....................................................................................................</td>
				</tr>
			</table>
			<br/>
			<table>
				<tr>
					<td>**</td><td>: Coret yang tidak perlu</td>
				</tr>
				<tr>
					<td>**</td><td>: Diisi oleh PK</td>
				</tr>
				<tr>
					<td>***&nbsp;&nbsp;</td><td>: Diisi oleh satpam</td>
				</tr>
			</table>
            <!-- <div class="flex_50" style="float: left;padding: 0.5%;width: 49%;"><?php echo $title;?></div>
            <div class="flex_50" style="float: left;padding: 0.5%;width: 49%;"><?php echo $message;?></div> -->
            <!-- <p><?php echo $message;?></p> -->
        <!-- </div> -->
        
    </div>
    
</body>
<meta http-equiv="content-type" content="text/html;charset=UTF-8">
</html>