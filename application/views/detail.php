<!-- START DETAIL PAGE -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="cc_single_post">
                    <?php if($article->num_rows() > 0) {?>
                    <?php $val = $article->row_array();?>
                    <div class="post type-post" id="">
                        <!-- REINDO BREAD CRUMB -->
                        <div class="bread-crumb">
                            <a href="<?php echo site_url('home')?>">Home</a> / 
                            <a href="<?php echo site_url('article')?>">artikel</a> /
                            <?php echo $val['title']?>
                        </div>
                        <?php if($val['upper_deck']) {?>
                        <div class=updeck>
                            <h3><?php echo $val['upper_deck']?></h3> 
                        </div>
                        <?php }?>
                        <h1>
                            <?php echo $val['title']?>
                        </h1>
                        <?php if($val['lower_deck']) {?>
                        <div class="quote-article">
                            <p><?php echo $val['lower_deck']?></p>
                        </div>
                        <?php } ?>
                        <?php if($val['image']) {?>
                        <div class="image-thumb">
                            <img src="<?php echo base_url()?>uploads/<?php echo $val['image']?>" />
                        </div>
                        <?php } ?>
                        <div class="entry">
                            <?php echo $val['content']?>
                        </div>
                        <div class="postmetadata">
                            <div class="meta">
                                <span class="time"><?php echo date('M d, Y',strtotime($val['create_date']))?></span>
                                <em>by </em>
                                <?php echo GetUserName('kg_admin','name',$val['create_user_id'])?>
                            </div>
                            Tags:
                            <?php echo explodetags($val['tags']) ?>
                        </div>
                    </div>
                    <?php }?>


                    <?php if($rel_link->num_rows() > 0) {?>
                    <!-- RELATED POSTS -->
                    <div class="box">
                        <div class="related-title">
                            <b>Related Posts
                                <span class="arrows">&raquo;</span>
                            </b>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                    <div id="related-posts">
                        <ul>
                            <?php foreach($rel_link->result_array() as $rval) { ?>
                            <li>
                                <?php if($rval['thumbnail']) {?>
                                <a href="<?php echo site_url('article/detail/'.$rval['id'].'/'.url_title($rval['title']))?>" rel="bookmark">
                                    <img width="147" height="100" src="<?php echo base_url()?>uploads/<?php echo getThumb($rval['thumbnail'])?>" />
                                </a>
                                <?php } ?>
                                <div class="title-car">
                                    <a href="<?php echo site_url('article/detail/'.$rval['id'].'/'.url_title($rval['title']))?>" rel="bookmark"><?php echo $rval['title']?></a>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <?php } ?>                    
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="home_sidebar">
                    <div class="follow_us_side">
                        <?php echo $this->load->view('detail_sidebar')?>   
                    </div>
                </div>                      
            </div>
        </div>
    </div>
</section>
<!-- END DETAIL PAGE -->