<!-- Accordion Menu Knowledge -->




<!-- begin widget sidebar -->
<?php if($menu_sidebar != null) {?>
<div id="katknowledge" class="sidebar_100">
    <div class="calender-title">
        <b><?php echo $menu_sidebar;?>
            <span class="arrows">&raquo;</span>
        </b>
    </div>
	<?php $filter = array("id_lang"=>"where/".GetIdLang(), "is_publish"=>"where/Publish", "title"=>"order/desc");
	      $cat = getAll($tb_kategori, $filter);
		  echo "<ul>";
			  foreach ($cat->result_array() as $menu) {
				  
				  $filter  = array("id_lang"=>"where/".GetIdLang(), "id_category"=>"where/".$menu['id'], "is_publish"=>"where/Publish", "title"=>"order/desc");
				  $subcat  = getAll($tb_sub_kategori, $filter);
				  
				  if ($subcat->num_rows() > 0) {
					  echo "<li><h5>".$menu['title']."</h5>"
								 ."<ul>";
					  foreach ($subcat->result_array() as $submenu) {
						//echo "<ul><li>".$menu['title'];
						echo "<li>
								<a href='".base_url()."id/ebook/cat/".$menu['id']."/".$submenu['id']."'>&rsaquo;&nbsp;".$submenu['title']."</a>
							  </li>";
					  }
					  echo "</ul></li>";
				  }
				  else {
					  echo "<li><h5><a href='".base_url()."id/ebook/cat/".$menu['id']."/0'>".$menu['title']."</a></h5></li>";
				  }
			  }
		  echo "</ul>";
		  
	?>
        <!-- /.awesome-weather-wrap -->
</div>
<br/>
<div class="clear"></div>
<?php }?>

<div class="sidebar_100">
    <div class="calender-title">
        <b>Reindo Next Event
            <span class="arrows">&raquo;</span>
        </b>
    </div>
	<div class="box">
        <div class="calender-box">
            <ul>
                <?php
                $calendar_config = tableconfig("kg_calendar");
                $calendar_limit = $calendar_config['item_homepage'];
                
                //$filtercalendar = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/publish","limit"=>"0/4","date_calendar"=>"order/desc");
                $filtercalendar = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/publish","date_calendar >="=>"where/".date('Y-m-d',now()),"limit"=>"0/".$calendar_limit,"date_calendar"=>"order/asc");
                $calendar = GetAll("kg_calendar",$filtercalendar);
                if($calendar->num_rows() > 0) {
                    foreach($calendar->result_array() as $cval){ ?>
                <li>
                    <div class="date-box">
                        <h4><?php echo strtoupper(date('M d',strtotime($cval['date_calendar'])))?></h4>
                    </div>
                    <div class="date-info">
                        <a href="<?php echo site_url('calendar/detail/'.$cval['id'].'/'.url_title($cval['title']))?>"><?php echo character_limiter($cval['title'],50)?></a>
                    </div>
                </li>
                <?php } ?>
                <?php }else{ ?>
                <li>
                    <div class="date-box">
                        <h4>DES 21</h4>
                    </div>
                    <div class="date-info">
                        <a href="#">Title Here</a>
                    </div>
                </li>
                <li>
                    <div class="date-box">
                        <h4>DES 21</h4>
                    </div>
                    <div class="date-info">
                        <a href="#">Title Here</a>
                    </div>
                </li>
                <li>
                    <div class="date-box">
                        <h4>DES 21</h4>
                    </div>
                    <div class="date-info">
                        <a href="#">Title Here</a>
                    </div>
                </li>
                <?php } ?>
            </ul>
            <a href="#" class="more-items">More Info »</a>
        </div>
	</div>
        <!-- /.awesome-weather-wrap -->
</div>
<br/>
<div class="clear"></div>


<div class="sidebar_100">
    <div class="pengumuman-item">
        <div class="blog-title">
            <b>Latest Update
                <span class="arrows">&raquo;</span>
            </b>
        </div>

        <div class="pengumuman-item">
            <?php 
                $article_config = tableconfig("kg_article");

                $article_limit = $article_config['item_homepage'];
                $filterarticle = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/publish","limit"=>"0/".$article_limit,"create_date"=>"order/desc");
                $annoucement = GetAll("kg_view_latest",$filterarticle);

            ?>
            <ul>
                <?php if($annoucement->num_rows() > 0) { ?>
                <?php foreach($annoucement->result_array() as $anval){ ?>
                    <li>
                    <p><?php echo '<strong>'.$anval['title'].'</strong>'?> : 
                        <?php echo ((strlen($anval['headline']) == 0) || ($anval['headline'] == 'NULL')) ? word_limiter($anval['headline'], 10) : '' ?>
                        <a href="<?php echo site_url($anval['module_detail'].$anval['id'].'/'.url_title($anval['title']))?>" title="Read more">...read more</a>
                    </p>
                </li>
                <?php } ?>
                <?php }else{ ?>
                <li>
                    <p>Fusce aliquet dolor sed dolor feugiat fermentum. Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique
                        <a href="#" title="Read more">...read more</a>
                    </p>
                </li>
                <li>
                    <p>Aliquam erat volutpat. Aenean leo nulla, aliquet sit amet tristique ut, consectetur non erat. Donec dignissim tellus
                        <a href="#">...readmore</a>
                    </p>
                </li>
                <?php } ?>                   
            </ul>
            <a href="#" class="more-items">More Info »</a>
            <div style="clear:both"></div>
        </div>        
    </div>
</div> 

<!-- Accordion Menu Knowledge -->
<script>
	$(document).ready(function(){
	$("#katknowledge h5").click(function(){
		//slide up all the link lists
		$("#katknowledge ul ul").slideUp();
		//slide down the link list below the h3 clicked - only if its closed
		if(!$(this).next().is(":visible"))
		{
			$(this).next().slideDown();
		}
	});
	})
</script>