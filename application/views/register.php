<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Reindo Reinsurance - Register</title>
    <link href="<?php echo base_url()?>assets/theme/login_style.css" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript" src="<?php echo base_url()?>assets/theme/js/jquery.min.js"></script>
    <meta http-equiv="X-UA-Compatible" content="IE=8" />
    <!--[if IE]><link rel="stylesheet" href="theme/css/forIE.css" type="text/css" /></link> <![endif]-->    
    <!--[if IE]><link rel="stylesheet" href="theme/css/logIE.css" type="text/css" /></link> <![endif]-->    
</head>

<body>
    <div id="message-wrapper" class="login-form-center" style="top:auto;margin-top:50px;height:auto;padding-bottom:80px;">
        <header>
            <hgroup>
                <h1 class="logo">
                    <a href="#">
                        <img src="<?php echo base_url()?>assets/theme/images/logo.png" alt="Reindo – Login">
                    </a>
                </h1>
                <h2>Sign in to your Reindo account</h2>
            </hgroup>
        </header>
        <form action="<?php echo site_url('member/register')?>" onsubmit="" method="post">
           
            <fieldset class="fieldset">

                <div class="wrap-for-login">
                    <div class="form-for-login">
                        <label for="">Name</label>
                            <?php echo form_input(array('name'=>'name','class'=>'','value'=>set_value('name')));?>
                            <?php echo form_error('name'); ?>
                            <!--input class="xlarge" id="username" name="username" type="text" required value="" placeholder=""-->
                    </div>  
                    <div class="form-for-login">
                        <label for="form-password">Email</label>
                            <?php echo form_input(array('name'=>'email','class'=>'','value'=>set_value('email')));?>
                            <?php echo form_error('email'); ?>
                            <!--input class="xlarge" id="email" type="text" required name="email" placeholder="" autocomplete="off"-->
                    </div> 
                    <div class="form-for-login">
                        <label for="form-password">Password</label>
                            <?php echo form_password(array('name'=>'password','class'=>'','value'=>set_value('password')));?>
                            <?php echo form_error('password'); ?>
                            <!--input class="xlarge" id="password" type="password" required name="password" placeholder="" autocomplete="off"-->
                    </div>     
                    <div class="form-for-login">
                        <label for="form-password">Retype Password</label>
                            <?php echo form_password(array('name'=>'re_password','class'=>'','value'=>set_value('re_password')));?>
                            <?php echo form_error('re_password'); ?>
                            <!--input class="xlarge" id="re_password" type="password" required name="re_password" placeholder="" autocomplete="off"-->
                    </div>    
                    <div class="clearfix">
                        <div class="submit-register">
                            <input type="submit" class="button-reg small blue" value="Register">
                        </div>
                    </div>                                                                                         
                </div>
            

                <?php // echo validation_errors('<div class="input">', '</div>'); ?>
                <?php 
                    $flashmessage = $this->session->flashdata('message');
                    echo ! empty($flashmessage) ? '<div class="success-log">' . $flashmessage . '</div>': '';
                ?>
            </fieldset>
        </form>

    </div>
</body>
</html>