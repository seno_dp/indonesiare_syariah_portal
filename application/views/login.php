<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Reindo Reinsurance - Forum</title>
    <link href="<?php echo base_url()?>assets/theme/login_style.css" rel="stylesheet" type="text/css" media="all" />
    <script type="text/javascript" src="<?php echo base_url()?>assets/theme/js/jquery.min.js"></script>
	<!--[if IE]><link rel="stylesheet" href="theme/css/forIE.css" type="text/css" /></link> <![endif]-->    
	<!--[if IE]><link rel="stylesheet" href="theme/css/logIE.css" type="text/css" /></link> <![endif]-->    
</head>

<body>
    <div id="message-wrapper" class="login-form-center" style="top:auto;margin-top:50px;height:auto;padding-bottom:80px;">
        <header>
            <hgroup>
                <h1 class="logo">
                    <a href="#">
                        <img src="<?php echo base_url()?>assets/theme/images/logo.png" alt="Kaskus – Login">
                    </a>
                </h1>
                <h2>Sign in to your Reindo account</h2>
            </hgroup>
        </header>
        <form action="<?php echo site_url('member/login')?>" onsubmit="" method="post">
        <!-- <form action="<?php //echo 'http://192.169.1.51:8000/api/2.0/auth/signin' ?>" onsubmit="" method="post"> -->
        
            <input type="hidden" name="" />
            <input type="hidden" name="" />
            <fieldset class="fieldset">
                <div class="clearfix">
                    <?php 
                        $flashmessage = $this->session->flashdata('message');
                        echo ! empty($flashmessage) ? '<p>' . $flashmessage . '</p>': '';
                    ?>
                </div>
				<div class="wrap-for-login">
					<div class="form-for-login">
						<label for="form-username">Email</label>
							<?php echo form_input(array('name'=>'email','class'=>'','value'=>set_value('email')));?>
							<?php echo form_error('email'); ?>
					</div>
					<div class="form-for-login">
						<label for="form-password">Password</label>
							<?php echo form_password(array('name'=>'password','class'=>'','value'=>set_value('password')));?>
							<?php echo form_error('password'); ?>
					</div>
					<div class="clearfix">
						<div class="submit-login">
							<label>
								<input type="checkbox" name="rememberme" value="rememberme">
								<span>Remember me</span>
							</label>
							<input type="submit" class="login-sbmt" value="Login">
						</div>
					</div>				
				</div>
            </fieldset>
        </form>
        <footer>
            <div class="opt">
                <!--<a href="<?php echo site_url('member/forgot')?>">Forgot Password?
                    <span>Click here!</span>
                </a>-->
                <a href="<?php echo site_url('member/register')?>">Don't have account?
                    <span>Sign up here!</span>
                </a>
            </div>
        </footer>
    </div>
</body>
</html>