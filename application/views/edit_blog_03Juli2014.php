<script language="JavaScript" src="<?php echo base_url();?>assets/theme/js/ckeditor/ckeditor.js" type="text/javascript"></script>
<!-- KRITERION PAGE -->
            <div class="flex_66_11">
                <div class="post" id="post-35">
                    <div class="entry-thread">
                        <div id="product-523" class="post-523 product"> 
							<!-- REINDO BREAD CRUMB -->
							<div class="bread-crumb-thread">
								Posting blog
							</div>
							<div style="clear:both"></div>							
							<div class="forum-detail">			
								<div id="respond" class="comment-respond">
									<form action="<?php echo site_url('blog/update')?>" method="post" id="commentform" class="comment-form" enctype="multipart/form-data">
										<?php
										$flashmessage = $this->session->flashdata('message');
										if($flashmessage)
										{
										?>
											<p>
												<?php echo $flashmessage;?>
									    	</p>
									    <?php
										}
										?>
										<?php
											if($qp->num_rows() > 0){
											$val = $qp->row_array();
										?>
										<p>
											<label for="email">Upload Image
												<span class="required">:</span>
											</label>
											<input type="file" maxlength="50" name="attachedfile"></input>
											<input id="file_old" name="file_old" type="hidden" value="<?php echo $val['image']?>"/><br/>
											<?php
												if($val['image']){
													echo '<img width="150px;" src="'.base_url().'uploads/'.getThumb($val['image']).'" style="margin-left:8px;">';
												}
											?>
										</p>	

										<p>
											<label for="author">Title

												<span class="required">:</span>
											</label>
											
											<input id="post-title" name="title" type="text" value="<?php echo $val['title']?>" size="30" aria-required='true' />
											<?php echo form_error('title', '<span class="error" style="width: 50% !important; margin-left: 136px">', '</span>'); ?>
											
										</p> 

										<p>
											<label for="author">Headline
												<span class="required">:</span>
											</label>
											<textarea class="" id="headline" name="headline" style="height: 135px;width: 50% !important;" ><?php echo $val['headline']?></textarea>
										</p> 

                                        <div class="label-editor">
                                        	<p>Content :</p>
                                        </div>
                                        <div class="editor-area">
                                        	<textarea class="editor1" id="editor1" name="editor1" ><?php echo $val['content']?></textarea>
                                        	<script>
										        CKEDITOR.replace( 'editor1',
										                {
										            skin : 'office2003',
										            filebrowserBrowseUrl : '<?php echo base_url()?>assets/theme/js/ckfinder/ckfinder.html',
					                                 filebrowserImageBrowseUrl : '<?php echo base_url()?>assets/theme/js/ckfinder/ckfinder.html?Type=Images',
					                                 filebrowserFlashBrowseUrl : '<?php echo base_url()?>assets/theme/js/ckfinder/ckfinder.html?Type=Flash',
					                                 filebrowserUploadUrl : '<?php echo base_url()?>assets/theme/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
					                                 filebrowserImageUploadUrl : '<?php echo base_url()?>assets/theme/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
					                                 filebrowserFlashUploadUrl : '<?php echo base_url()?>assets/theme/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
										            toolbar :
										            [
										                ['Source','-','Preview','Templates','Cut','Copy','Paste'],['Bold','Italic','Underline','Strike','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','NumberedList','BulletedList','Subscript','Superscript','-'],
										                '/',
										                ['Link','Unlink','-','Image','Flash','Table','HorizontalRule','Smiley','SpecialChar'],['TextColor','BGColor','-','Font','FontSize','PasteFromWord']
										            ]
										        });
										    </script>
                                        </div>  

                                        <p>
											<label for="author">Tags (delimiter ",")
												<span class="required">:</span>
											</label>
											<input id="post-title" name="tags" type="text" value="<?php echo $val['tags']?>" size="30" aria-required='true' />
											<?php echo form_error('tags', '<span class="error" style="width: 50% !important; margin-left: 136px">', '</span>'); ?>
										</p>   
										
                                        <p class="form-submit">
											<input name="submit" type="submit" id="submit" value="Submit Post" />
											<input id="id_blog" name="id_blog" type="hidden" value="<?php echo $val['id']?>"/>
										</p>
										<?php } ?>
									</form>
								</div>
								<!-- #respond -->
							</div>	  
							<div style="clear:both"></div>	
 
							<div style="clear:both"></div>	
									
							<div style="clear:both"></div>	
							   							
                        </div>
                        <!-- #product-523 -->
                        <div style="clear:both"></div>
                    </div>
                </div>
            </div>
			
            <div style="clear: both"></div>