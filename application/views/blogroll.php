<!-- START BLOG ROLL LIST -->
<section class="main_news_wrapper cc_single_post_wrapper"> 
    <!-- START CONTAINER -->
    <div class="container">
        <div class="row">
            <!-- START COL-LG-9 -->
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="cc_single_post">
                    <div class="title-list-page">                        
                        <h3>Latest blog post</h3>                    
                    </div>     
                    <?php if($feat->num_rows() > 0) { 
                        $vfeat = $feat->row_array();
                    ?>
                    <div class="list-of-article">
                        <div class="">
                            <?php if($vfeat['image']) { ?>
                            <div class="">
                                    <a href="<?php echo site_url('blog/detail/'.$vfeat['id'].'/'.url_title($vfeat['title']))?>" rel="bookmark">
                                        <img class="img-responsive" style="max-width: 100%;"src="<?php echo base_url()?>uploads/<?php echo $vfeat['image']?>" class="attachment-blog-image wp-post-image" alt="<?php echo $vfeat['title']?>"/>
                                    </a>
                            </div>  
                            <?php } ?>
                            <div class="clear"></div>   
                            <h4 id="post-405">
                                <a href="<?php echo site_url('blog/detail/'.$vfeat['id'].'/'.url_title($vfeat['title']))?>" title="<?php echo $vfeat['title']?>">
                                    <?php echo $vfeat['title']?>
                                </a>
                            </h4>                       
                            <div class="post_inner">
                                <?php echo $vfeat['headline']?>
                            </div>
                            <div class="clear"></div>
                            <br/>
                            <?php 
                            if($vfeat['create_user_id']) {
                                $id = $vfeat['create_user_id'];
                                $filter = array(
                                        "id"=>"where/".$id
                                    );

                                $member = GetAll('kg_member',$filter);
                                if($member->num_rows() > 0){
                                $v = $member->row_array();
                            ?>

                            <?php if($v['image']) { ?>
                            <div class="writer-cover-thumb">
                                <img class="img-responsive" width="60" src="<?php echo base_url().'uploads/'.getThumb($v['image'])?>" alt="<?php echo $v['title']?>"></img>
                            </div>
                            <?php }else{ ?>
                            <div class="writer-cover-thumb">
                                <img class="img-responsive" width="60" src="<?php echo base_url().'assets/theme/images/180x180.jpg'?>" alt="<?php echo $v['title']?>"></img>
                            </div>
                            <?php } ?>
                            <div class="post_meta post-meta-list mr-grey">
                                <i class="fa fa-user"></i> Writer : <?php echo '<a href="'.site_url('member/userprofile/'.$v['id'].'/'.url_title($v['title'])).'">'.$v['title'].'</a>'?><br/>    
                                <time datetime="" itemprop="datePublished">
                                    <i class="fa fa-clock-o"></i> Tanggal publish : <?php echo date('d M Y',strtotime($vfeat['create_date']));?>
                                </time>                                                     
                            </div>
                        </div>  
                    </div>         
                    <?php } ?>
                    <?php } ?>
                    <?php } ?>
                    <?php if($qp->num_rows() > 0){?>
                        <?php foreach($qp->result_array() as $qval) {?>
                    <div class="post-<?php echo $qval['id']?> list-of-article">
                        <h4 id="post-<?php echo $qval['id']?>">
                            <a href="<?php echo site_url('blog/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" title="<?php echo $qval['title']?>">
                                <?php echo $qval['title']?>
                            </a>
                        </h4>
                        <div class="entry">
                            <?php if($qval['image']) { ?>
                                <div class="thumb-arhive">
                                    
                                    <a href="<?php echo site_url('blog/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" rel="bookmark">
                                        <img width="263" src="<?php echo base_url()?>uploads/<?php echo getThumb($qval['image'])?>" class="attachment-blog-image wp-post-image" alt="<?php echo $qval['title']?>"/>
                                    </a>
                                    
                                </div>
                            <?php } ?>
                            <div class="excerptarhive"><?php echo $qval['headline']?></div>
                            <div class="clear"></div>
                        </div>
                        <br/>
                        <?php 
                            if($qval['create_user_id']) {
                                $id = $qval['create_user_id'];
                                $filter = array(
                                        "id"=>"where/".$id
                                    );

                                $memberm = GetAll('kg_member',$filter);
                                if($memberm->num_rows() > 0){
                                $m = $memberm->row_array();
                            ?>
                        <?php if($m['image']) { ?>
                            <div class="writer-cover-thumb">
                                <img class="avatar avatar-60 photo" width="60" src="<?php echo base_url().'uploads/'.getThumb($m['image'])?>" alt="<?php echo $m['title']?>"></img>
                            </div>
                            <?php }else{ ?>
                            <div class="writer-cover-thumb">
                                <img class="avatar avatar-60 photo" width="60" src="<?php echo base_url().'assets/theme/images/180x180.jpg'?>" alt="<?php echo $m['title']?>"></img>
                            </div>
                            <?php } ?>
                        <div class="postmetadatablogroll">
                            <div class="post-meta-blog">
                                <ul>
                                    <li><a href=""><i class="fa fa-clock-o"></i><?php echo date('M d, Y',strtotime($qval['create_date']))?></a></li>
                                    <li><a href="">by</a></li>
                                    <li><?php echo '<a href="'.site_url('member/userprofile/'.$m['id'].'/'.url_title($m['title'])).'"><i class="fa fa-user"></i>'.$m['title'].'</a>'?></li>
                                </ul>
                            </div>
                            <div class="post-meta-blog">
                                <ul>
                                    <li><i class="fa fa-folder"></i>&nbsp;&nbsp;Filed Under:
                                        <a href="#" title="" rel="category tag">&nbsp;&nbsp;<?php echo $qval['category']?></a>
                                    </li>
                                    <li>
                                      <a href="">
                                        <i class="fa fa-tag"></i>Tags:<?php echo explodetags($qval['tags']) ?>
                                      </a>  
                                    </li>
                                    <li>
                                        <?php 
                                        $filtercomment = array("is_publish"=>"where/publish","id_blog"=>"where/".$qval['id'],"id"=>"order/desc");
                                        $comment = GetAll('kg_view_comment',$filtercomment);
                                        if($comment->num_rows() > 0) { 
                                            $num_comment = $comment->num_rows()." comments";
                                        }else{
                                            $num_comment = "No comment yet";
                                        }
                                        ?>
                                        <i class="fa fa-comment"></i>&nbsp;<a href="<?php echo site_url('blog/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" class="" title="Comment on <?php echo $qval['title']?>"><?php echo $num_comment;?></a>                                    
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="clear"></div>
                        <?php } ?>
                        <?php } ?>
                    </div>
                    <?php } ?>
                    <?php }else{ ?>
                    <div class="post-328 post type-post status-publish format-gallery hentry category-fashion tag-display tag-shelf tag-shoes">
                        <h4 id="post-328">
                            <a href="#" title="tidak ada posting yang tersedia">
                                Tidak ada posting yang tersedia</a>
                        </h4>
                        <div class="entry">
                            <div class="thumb-arhive">
                                <a href="#" rel="bookmark">
                                    <img src="<?php echo base_url()?>uploads/2013/06/listimage2.jpg" class="img-responsive" alt="jo" />
                                </a>
                            </div>
                            <div class="excerptarhive">Tidak ada posting artikel atau blog yang tersedia...
                            </div>
                            <div class="clear"></div>
                        </div>
                        <br/>
                        <div class="postmetadata">
                            <!-- KRITERION META -->
                            <div class="meta">
                                <em>by </em>admin</div>
                            <span class="categories">Filed Under:
                                <a href="index.html" title="View all posts in Fashion" rel="category tag">Reindo</a>
                            </span>
                            Tags:
                            <a href="index.html" rel="tag">Reindo</a>
                            <a href="index.html#respond" class="post-comments" title="Comment on Shoes">No comments yet</a>
                        </div>
                    </div>
                    <?php } ?>

                    <!-- START PAGINATION -->
                    
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="news_pagination">
							<ul class="news_pagi">
								<?php echo $pagination?>
							</ul>
                        </div>
                    </div> 
                    <!-- END PAGINATION -->                      
                </div>                                                               
            </div>
            <!-- END COL-LG-9 -->
            <!-- START COL-LG-3 -->
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <!-- begin widget sidebar -->
                    <div class="cc_single_post">
                      <script type="text/javascript">
                        jQuery(document).ready( function() {
                          jQuery('#tab-container').easytabs();
                        });
                      </script>
                        <div id="tab-container" class='tab-container'>
                            <ul class='etabs'>
                                <?php if($recent->num_rows() > 0 ) {?>
                                <li class='tab'>
                                    <a href="#tabs-recent">Recent</a>
                                </li>
                                <?php } ?>
                                <?php if($popular->num_rows() > 0 ) {?>
                                <li class='tab'>
                                    <a href="#tabs-popular">Popular</a>
                                </li>
                                <?php } ?>
                                <?php if($mostcomm->num_rows() > 0 ) {?>
                                <li class='tab'>
                                    <a href="#tabs-comment">Comments</a>
                                </li>
                                <?php } ?>
                                <!-- <li class='tab'>
                                    <a href="#tabs-comment">Comments</a>
                                </li> -->
                            </ul>
                            <div class='panel-container'>
                                <?php if($recent->num_rows() > 0 ) { ?>
                                <div id="tabs-recent">
                                    <ul>
                                        <?php foreach ($recent->result_array() as $valrec) {?>
                                            <li><a href="<?php echo site_url('blog/detail/'.$valrec['id'].'/'.url_title($valrec['title']))?>"><?php echo $valrec['title']?></a></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <?php } ?>
                                <?php if($popular->num_rows() > 0 ) { ?>
                                <div id="tabs-popular">
                                    <ul>
                                        <?php foreach ($popular->result_array() as $valpop) {?>
                                            <li><a href="<?php echo site_url('blog/detail/'.$valpop['id'].'/'.url_title($valpop['title']))?>"><?php echo $valpop['title']?></a></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <?php } ?>
                                <?php if($mostcomm->num_rows() > 0 ) { ?>
                                <div id="tabs-comment">
                                    <ul>
                                        <?php foreach ($mostcomm->result_array() as $valcomm) {?>
                                            <li>
                                                <div class="commenters">
                                                    <img src="<?php echo base_url()?>uploads/<?php echo getThumb($valcomm['image_member'])?>" style="width: 41px; "/>
                                                </div>
                                                <a href="<?php echo site_url('blog/detail/'.$valcomm['id_blog'].'/'.url_title($valcomm['title_blog']))?>"><?php echo $valcomm['title_blog']?></a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <div class="clear"></div>
                                
            </div>
            <!-- END COL-LG-3 -->            
        </div>
    </div>
    <!-- START CONTAINER -->
</section>
<!-- START BLOG ROLL LIST -->            