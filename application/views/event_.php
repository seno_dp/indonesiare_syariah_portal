<!-- REINDO SINGLE PAGE -->
<div class="flex_66_1">
    <?php if($event->num_rows() > 0) {?>
    <?php $val = $event->row_array();?>
    <div class="post type-post" id="">
        <!-- REINDO BREAD CRUMB -->
        <div class="bread-crumb">
            <a href="<?php echo site_url('home')?>">Home</a> / 
            <a href="<?php echo site_url('event')?>">Event</a> /
            <?php echo $val['title']?>
        </div>
        <h1>
            <?php echo $val['title']?>
        </h1>
        <?php if($val['image']) {?>
        <?php if($val['is_thumbnail'] == 1) {?>
        <div class="image-thumb">
            <img src="<?php echo base_url()?>uploads/<?php echo $val['image']?>"/>
        </div>
        <?php } ?>
        <?php } ?>
        <div class="entry">
            <?php echo $val['content']?></div>
        <div class="postmetadata">
            <div class="meta">
                <span class="time"><?php echo date('M d, Y',strtotime($val['create_date']))?></span>
                <em>by </em>
                <?php echo GetUserName('kg_admin','name',$val['create_user_id'])?>
            </div>
            Tags:
            <?php echo explodetags($val['tags']) ?>
        </div>
    </div>
    <?php }?>


    <?php if($rel_link->num_rows() > 0) {?>
    <!-- RELATED POSTS -->
    <div class="box">
        <div class="related-title">
            <b>Related Posts
                <span class="arrows">&raquo;</span>
            </b>
        </div>
    </div>
    <div style="clear:both"></div>
    <div id="related-posts">
        <ul>
            <?php foreach($rel_link->result_array() as $rval) { ?>
            <li>
                <?php if($rval['image']) {?>
                <a href="<?php echo site_url('event/detail/'.$rval['id'].'/'.url_title($rval['title']))?>" rel="bookmark">
                    <img width="147" height="100" src="<?php echo base_url()?>uploads/<?php echo getThumb($rval['image'])?>" />
                </a>
                <?php } ?>
                <div class="title-car">
                    <a href="<?php echo site_url('event/detail/'.$rval['id'].'/'.url_title($rval['title']))?>" rel="bookmark"><?php echo $rval['title']?></a>
                </div>
            </li>
            <?php } ?>
        </ul>
    </div>
    <?php } ?>
</div>
<div class="flex_33">
    <div id="sidebar">
        <?php echo $this->load->view('detail_sidebar')?>                   
    </div>
</div>
<div style="clear:both"></div>