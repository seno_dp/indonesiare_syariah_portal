<!-- REINDO SINGLE PAGE -->
<section class="main_news_wrapper cc_single_post_wrapper">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-sm-9 col-xs-12">
				<div class="cc_single_post">
					<div class="sp_details">
					    <?php if($content->num_rows() > 0) {?>
					    <?php $val = $content->row_array();?>
					    <div class="" id="">
					        <!-- REINDO BREAD CRUMB -->
					        <div class="breadcum_c_left">
					            <a href="<?php echo site_url('home')?>">Home</a> / 
					            <a href="<?php echo site_url('page')?>">About</a> /
					            <?php echo $val['title']?>
					        </div>
					        <h2>
					            <?php echo $val['title']?>
					        </h2>
					        <div class="post_text">
					            <?php echo $val['content']?>
					        </div>
					        <div class="post_meta">
					            <ul>
									<li>
										<a href=""><i class="fa fa-clock-o"></i><?php echo date('M d, Y',strtotime($val['create_date']))?></a>
									</li>
									<li><a href="">by </a></li>
									<li><a href=""><i class="fa fa-user"></i><?php echo GetUserName('kg_admin','name',$val['create_user_id'])?></a></li>
								</ul>
							</div>	
							<div class="social_tags">
								<div class="social_tags_left">
									<p>Tags :</p>
									<ul>
										<li><a href=""><?php echo explodetags($val['tags']) ?></a></li>
									</ul>
								</div>
								<div class="social_tags_right">
									<ul>
										<li class="facebook"><a class="fa fa-facebook" href=""></a></li>
										<li class="twitter"><a class="fa fa-twitter" href=""></a></li>
										<li class="google-plus"><a class="fa fa-google-plus" href=""></a></li>
										<li class="linkedin"><a class="fa fa-linkedin" href=""></a></li>
									</ul>
								</div>
							</div>					            
					    </div>
					    <?php }?>						
					</div>					
				</div>
			</div>
			<div class="col-md-3 col-sm-3 col-xs-12">
				<div class="home_sidebar">
					<div class="follow_us_side">
						<?php echo $this->load->view('detail_sidebar')?>   
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- END REINDO SINGLE PAGE -->

