<?php if($comment->num_rows() > 0) {?>
<?php foreach ($comment->result_array() as $comm) { ?>
<div class="sp-comments-box">
  <h2>Comments</h2>
  <div class="single_comment">
    <div class="single_comment_pic">
      <img src="images/comment-pic1.png" alt="">
      <?php if($comm['image_member']){ ?>
        <img class="img-responsive" src="<?php echo base_url()?>uploads/<?php echo getThumb($comm['image_member'])?>" /> 
      <?php }else{ ?>
        <img class="img-responsive" src="<?php echo base_url()?>uploads/2013/06/commenters.png" />
      <?php } ?>            
    </div> 
    <div class="single_comment_text">
      <div class="sp_title">
        <a href=""><h4><?php echo $comm['name_member']?></h4></a>
        <p class="name-commenters"><?php echo date('M d, Y',strtotime($comm['create_date']))?></p>
      </div>
      <p><?php echo $comm['content']?></p>
    </div>        
  </div>
</div>

<?php } ?>
<?php }else{ ?>
<div class="single_comment">
    <div class="single_comment_pic">
        <img class="img-responsive" src="<?php echo base_url()?>uploads/2013/06/commenters.png" />
    </div>
    <div class="single_comment_text">
        <div class="sp_title">
            <a href=""><h4>Anonymous</h4></a>
            <p class="name-commenters">dd mm, yyyy</p>
        </div>
        <p>No comment</p>
    </div>            
</div>
<?php } ?>