<div id="wrapper-menu">
    <div id="navigation">
        <div class="menu-main-menu-container">
            <ul id="menu-main-menu" class="sf-menu">
                <?php
                  //echo "id_lang ".GetIdLang();
                    if($this->session->userdata('user_id_sess')) {
                      $filter = array("is_publish"=>"where/publish","id_parents"=>"where/0","id_lang"=>"where/".GetIdLang(),"sort"=>"order/asc");
                    }else{
                      $filter = array("is_publish"=>"where/publish","is_login"=>"where/NotLogin","id_parents"=>"where/0","id_lang"=>"where/".GetIdLang(),"sort"=>"order/asc");
                    }
                    //$filter = array("is_publish"=>"where/publish","id_parents"=>"where/0","id_lang"=>"where/".GetIdLang(),"sort"=>"order/asc");
                    $mainmenu = GetAll("kg_menu",$filter);
                    $active = ($this->uri->segment(2) == "home") ? 'current' : '';
                    if($mainmenu->num_rows() > 0){
                      foreach($mainmenu->result_array() as $mm){
                        $active_mm = (ucfirst($this->uri->segment(2)) == ucfirst($mm['title'])) ? 'active' : '';
                        if($this->session->userdata('user_id_sess')) {
                          $filterp = array("is_publish"=>"where/publish","id_parents"=>"where/".$mm['id'],"id_lang"=>"where/".GetIdLang(),"sort"=>"order/asc");
                        }else{
                          $filterp = array("is_publish"=>"where/publish","is_login"=>"where/NotLogin","id_parents"=>"where/".$mm['id'],"id_lang"=>"where/".GetIdLang(),"sort"=>"order/asc");
                        }
                        //$filterp = array("is_publish"=>"where/publish","id_parents"=>"where/".$mm['id'],"id_lang"=>"where/".GetIdLang(),"sort"=>"order/asc");
                        $pmenu = GetAll("kg_menu",$filterp);
                        if($pmenu->num_rows() > 0){

                          echo '<li class="'.$active.'">';
                          echo '<a href="'.site_url($mm['file']).'">'.ucfirst($mm['title']).'</a>';
                          echo '<ul class="sub-menu">';
                          foreach($pmenu->result_array() as $pm){
                           // if($pm['id'] != 17 || $pm['id'] != 18 || $pm['id'] != 19 || $pm['id'] != 20 || $pm['id'] != 21 || $pm['id'] != 22) {
                              echo '<li>';
                              //echo '<a href="'.site_url($pm['file']).'">'.ucfirst($pm['title']).'</a>';
                              if(strpos($pm['file'],'http://') !== FALSE){
                                echo '<a href="'.$pm['file'].'" target="_BLANK">'.ucfirst($pm['title']).'</a>';
                              }else{
                                echo '<a href="'.site_url($pm['file']).'">'.ucfirst($pm['title']).'</a>';
                              }
                              echo '</li>';
                            //}
                            /*else{
                              die('here');
                            }*/
                          }

                          echo '</ul>';
                        }else{
                          echo '<li class="'.$active_mm.'">';
                          if(strpos($mm['file'],'http://') !== FALSE){
                            echo '<a href="'.$mm['file'].'" target="_BLANK">'.ucfirst($mm['title']).'</a>';
                          }else{
                            echo '<a href="'.site_url($mm['file']).'">'.ucfirst($mm['title']).'</a>';
                          }
                        }
                        echo '</li>';
                      }
                    echo '</ul>';
                  }
                  ?>
            </ul>
        </div>
    </div>
</div>