<!-- VOTE DETAIL -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="cc_single_post">
                    <?php if($voting->num_rows() > 0) {?>
                    <?php $val = $voting->row_array();?>
                    <?php
                    $this->db->select_sum('result');
                    $this->db->where('id_polling', $val['id']);
                    $total = $this->db->get('kg_polling_result');
                    $nilai = $total->row_array(); 
                    $nilai_total = $nilai['result'];
                    ?>
                    <div class="" id="">
                        <!-- REINDO BREAD CRUMB -->
                        <div class="breadcum_c_left">
                            <a href="<?php echo site_url('home')?>">Home</a> / 
                            <a href="<?php echo site_url($controller_name)?>"><?php echo $controller_name ?></a> /
                            <?php echo $val['title']?>
                        </div>
                        <h2>
                            <?php echo ucfirst($val['title'])?>
                        </h2>
                       
                        <div class="post_text">
                            <?php echo $val['content']?>
                        </div>
                        <div class="postmeta-vote">
                            <div class="meta-vote">
                                <p>Hasil polling sementara : </p>
                                <?php
                                //$filterresult = array("id_polling"=>"where/".$val['id']);
                                //$result = getAll('kg_polling_result',$filterresult);
                                //$this->db->select_sum('result');
                                $this->db->select('id,id_polling,option_answer,result');
                                $this->db->order_by('result', 'desc');
                                $this->db->where('id_polling', $val['id']);
                                $result = $this->db->get('kg_polling_result');
                                if($result->num_rows() > 0){
                                    echo "<ol class='hasil_polling'>";
                                    foreach ($result->result_array() as $val_result) {
                                        $persentase = ($val_result['result'] / $nilai_total);
                                        $percent_friendly = number_format( $persentase * 100, 2 ) . '%';
                                        echo "<li>";
                                        echo $val[$val_result['option_answer']]." <span><i class='fa fa-pie-chart'></i>&nbsp;&nbsp;".$percent_friendly." suara.</span>";
                                        echo "</li>";
                                        //echo "<div class='clear'></div>";
                                    }
                                    echo "</ol>";
                                }
                                ?>
                            </div>
                            
                        </div>
                    </div>
                    <?php }?>                   
                </div>         
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="home_sidebar">
                    <div class="follow_us_side">
                        <?php echo $this->load->view('detail_sidebar')?>   
                    </div>
                </div>
            </div>            
        </div>
    </div>
</section>
<!-- VOTE DETAIL -->
<div style="clear:both"></div>