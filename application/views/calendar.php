<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12">
                <div class="cc_single_post">
                    <div class="title-list-page">
                        <h3><?php echo ($menu_title) ? ucfirst($menu_title) : "Article"?></h3>
                    </div>
                    <?php if($qp->num_rows() > 0) {?>
                    <?php foreach($qp->result_array() as $qval) {?>
                        <div class="post-<?php echo $qval['id']?> list-of-article">
                            <h4 id="post-<?php echo $qval['id']?>">
                                <a href="<?php echo site_url($controller_name.'/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" title="<?php echo $qval['title']?>">
                                    <?php echo $qval['title']?>
                                </a>
                            </h4>
                            <div class="list-of-article">
                                
                                <div class="excerptarhive-book">
                                    <p>Tanggal : <?php echo strtoupper(date('d M Y',strtotime($qval['date_calendar'])))?></p>
                                    <?php echo $qval['headline']?>
                                    ... <a href="<?php echo site_url($controller_name.'/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" rel="tag">More</a>
                                </div>
                                <div class="post_meta post-meta-list mr-grey">
                                    <!-- KRITERION META -->       
                                    <ul>
                                        <li>
                                            <i class="fa fa-user"></i>&nbsp;&nbsp;<?php echo date('M d, Y',strtotime($qval['create_date']))?>
                                        </li>
                                        <li><i class="fa fa-tag"></i>&nbsp;&nbsp;Tags:&nbsp;&nbsp;<?php echo explodetags($qval['tags']) ?></li>
                                    </ul>                                
                                </div>
                                <div class="clear"></div>
                            </div>
                            <br/>
                            
                        </div>
                        <?php } ?>
                    <?php }else{ ?>
                    <div class="post-1 post type-post">
                        <h1 id="post-1">
                            <a href="#" title="Lipsum">
                                Title Here</a>
                        </h1>
                        <div class="entry">
                            <div class="thumb-arhive">
                                <a href="#" rel="bookmark">
                                    <img width="263" height="145" src="<?php echo base_url()?>assets/theme/images/no_image.jpg" class="attachment-blog-image wp-post-image" alt="Fashion photography by Jenya Kushnir" width="150px" />
                                </a>
                            </div>
                            <div class="excerptarhive">Lorem ipsum dolor sit amet, consec tetur adipiscing elit. Quisque eu enim imperdiet, malesuada sapien ac, tempor magna. Cras bibendum adipiscing arcu, id bibendum lorem mattis et. Nulla sed tempus enim. Proin egestas nisi ultricies auctor viverra. Nunc a diam sit amet elit venenatis lacinia sed vel diam. Phasellus vestibulum magna lectus, sit amet accumsan risus ornare eu. Integer in urna sed velit sollicitudin sagittis ut ut arcu. Nam vitae fermentum mauris. Morbi arcu felis, congue vel odio vel, congue laoreet sapien. Maecenas vel pretium magna. Ut...
                            </div>
                            <div class="clear"></div>
                        </div>
                        <br/>
                        <div class="postmetadata">
                            <!-- KRITERION META -->
                            <div class="meta">
                                <span class="time">June 7th, 2013</span>
                                <em>by</em>admin | Type: Audio</div>
                            Tags:
                            <a href="#" rel="tag">arcu</a>,
                            <a href="#" rel="tag">fashion</a>,
                            <a href="#" rel="tag">golf</a>,
                            <a href="#" rel="tag">laor</a>,
                            <a href="#" rel="tag">magna</a>
                        </div>
                    </div>
                    <?php } ?>
                    <!-- KRITERION PAGINATION -->
                    <!-- KRITERION PAGINATION -->
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="news_pagination">
							<ul class="news_pagi">
								<?php echo $pagination?>	
							</ul>
						</div>	
                    </div>    				
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="home_sidebar">
                    <div class="follow_us_side">
                        <?php echo $this->load->view('detail_sidebar')?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php 
    $image_field = ($controller_name == "article") ? 'thumbnail' : 'image';
?>
<!-- CALENDER LIST -->

<div style="clear: both"></div>
<?php if($controller_name == 'kliping') { ?>
<script>
 jQuery(document).ready(function() {
    
            var config = {
              '.chzn-select'           : {},
              '.chzn-select-deselect'  : {allow_single_deselect:true},
              '.chzn-select-no-single' : {disable_search_threshold:10},
              '.chzn-select-no-results': {no_results_text:'Oops, nothing found!'},
              '.chzn-select-width'     : {width:"100%"}
            }

            for (var selector in config) {
              jQuery(selector).chosen(config[selector]);
            }

            //jQuery("#datepicker").datepicker();

        }); 
</script>
<?php } ?>