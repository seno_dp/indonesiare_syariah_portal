            <!-- KRITERION ARCHIVE -->
            <div class="flex_66">
                <div class="box">
                    <div class="listarticle-title">
                        <b>Latest blog post
                            <span class="arrows">&raquo;</span>
                        </b>
                    </div>
                </div>
                <br/>
                <br/>
                <?php if($feat->num_rows() > 0) { 
                        $vfeat = $feat->row_array();
                ?>
                <div class="post headline-blog">

                    <div class="headline-image">
                        <?php if($vfeat['image']) { ?>
                        <div class="thumb-hedline">
                                <a href="<?php echo site_url('blog/detail/'.$vfeat['id'].'/'.url_title($vfeat['title']))?>" rel="bookmark">
                                    <img style="max-width: 100%;"src="<?php echo base_url()?>uploads/<?php echo $vfeat['image']?>" class="attachment-blog-image wp-post-image" alt="<?php echo $vfeat['title']?>"/>
                                </a>
                        </div>  
                        <?php } ?>
                        <div class="clear"></div>   
                        <h1 id="post-405">
                            <a href="<?php echo site_url('blog/detail/'.$vfeat['id'].'/'.url_title($vfeat['title']))?>" title="<?php echo $vfeat['title']?>">
                                <?php echo $vfeat['title']?>
                            </a>
                        </h1>                       
                        <div class="excerptarhive-headline">
                            <?php echo $vfeat['headline']?>
                        </div>
                        <div class="clear"></div>
                        <br/>
                        <?php 
                        if($vfeat['create_user_id']) {
                            $id = $vfeat['create_user_id'];
                            $filter = array(
                                    "id"=>"where/".$id
                                );

                            $member = GetAll('kg_member',$filter);
                            if($member->num_rows() > 0){
                            $v = $member->row_array();
                        ?>

                        <div class="writer">
                            <img class="avatar avatar-60 photo" width="60" src="<?php echo base_url().'uploads/'.getThumb($v['image'])?>" alt="<?php echo $v['title']?>"></img>
                        </div>
                        <div class="writer-name">
                            <p>
                                <strong itemprop="author">
                                    Writer : <?php echo $v['title']?>
                                </strong><br/>                                                     
                                <time datetime="2014-02-10T22:36:35+00:00" itemprop="datePublished">
                                    Tanggal publish : <?php echo date('d M Y',strtotime($v['create_date']));?>
                                </time>        
                            </p>
                        </div> 
                    </div>  
                </div>
                <?php } ?>
                <?php } ?>
                <?php } ?>

                <?php if($qp->num_rows() > 0){?>
                    <?php foreach($qp->result_array() as $qval) {?>
                <div class="post-<?php echo $qval['id']?> post">
                    <h1 id="post-<?php echo $qval['id']?>">
                        <a href="<?php echo site_url('blog/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" title="<?php echo $qval['title']?>">
                            <?php echo $qval['title']?>
                        </a>
                    </h1>
                    <div class="entry">
                        <?php if($qval['image']) { ?>
                            <div class="thumb-arhive">
                                
                                <a href="<?php echo site_url('blog/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" rel="bookmark">
                                    <img width="263" src="<?php echo base_url()?>uploads/<?php echo getThumb($qval['image'])?>" class="attachment-blog-image wp-post-image" alt="<?php echo $qval['title']?>"/>
                                </a>
                                
                            </div>
                        <?php } ?>
                        <div class="excerptarhive"><?php echo $qval['headline']?></div>
                        <div class="clear"></div>
                    </div>
                    <br/>
                    <div class="postmetadata">
                        <div class="meta">
                            <span class="time"><?php echo date('M d, Y',strtotime($qval['create_date']))?></span>
                            <em>by</em> admin
                        </div>
                        <span class="categories">Filed Under:
                            <a href="#" title="" rel="category tag"><?php echo $qval['category']?></a>
                        </span>
                        Tags:
                        <?php echo explodetags($qval['tags']) ?>
                        <?php 
                        $filtercomment = array("is_publish"=>"where/publish","id_blog"=>"where/".$qval['id'],"id"=>"order/desc");
                        $comment = GetAll('kg_view_comment',$filtercomment);
                        if($comment->num_rows() > 0) { 
                            $num_comment = $comment->num_rows()." comments";
                        }else{
                            $num_comment = "No comment yet";
                        }
                        ?>
                        <a href="<?php echo site_url('blog/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" class="post-comments" title="Comment on <?php echo $qval['title']?>"><?php echo $num_comment;?></a>
                    </div>
                </div>
                <?php } ?>
                <?php }else{ ?>
                <div class="post-328 post type-post status-publish format-gallery hentry category-fashion tag-display tag-shelf tag-shoes">
                    <h1 id="post-328">
                        <a href="#" title="tidak ada posting yang tersedia">
                            Tidak ada posting yang tersedia</a>
                    </h1>
                    <div class="entry">
                        <div class="thumb-arhive">
                            <a href="#" rel="bookmark">
                                <img width="263" height="145" src="<?php echo base_url()?>uploads/2013/06/listimage2.jpg" class="attachment-blog-image wp-post-image" alt="jo" width="150px" />
                            </a>
                        </div>
                        <div class="excerptarhive">Tidak ada posting artikel atau blog yang tersedia...
                        </div>
                        <div class="clear"></div>
                    </div>
                    <br/>
                    <div class="postmetadata">
                        <!-- KRITERION META -->
                        <div class="meta">
                            <em>by </em>admin</div>
                        <span class="categories">Filed Under:
                            <a href="index.html" title="View all posts in Fashion" rel="category tag">Reindo</a>
                        </span>
                        Tags:
                        <a href="index.html" rel="tag">Reindo</a>
                        <a href="index.html#respond" class="post-comments" title="Comment on Shoes">No comments yet</a>
                    </div>
                </div>
                <?php } ?>

                <!-- KRITERION PAGINATION -->
                <!-- KRITERION PAGINATION -->
                <div class="navigation flex_66">
                    <div class="pagination">
                        <?php echo $pagination?>
                    </div>
                </div>
            </div>
            <div class="flex_33">
                <div id="sidebar">
                    <!-- begin widget sidebar -->
                    <div class="sidebar_100">
                      <script type="text/javascript">
                        jQuery(document).ready( function() {
                          jQuery('#tab-container').easytabs();
                        });
                      </script>
                    
                    <div class="box">
                        <div id="tab-container" class='tab-container'>
                            <ul class='etabs'>
                                <?php if($recent->num_rows() > 0 ) {?>
                                <li class='tab'>
                                    <a href="#tabs-recent">Recent</a>
                                </li>
                                <?php } ?>
                                <?php if($popular->num_rows() > 0 ) {?>
                                <li class='tab'>
                                    <a href="#tabs-popular">Popular</a>
                                </li>
                                <?php } ?>
                                <?php if($mostcomm->num_rows() > 0 ) {?>
                                <li class='tab'>
                                    <a href="#tabs-comment">Comments</a>
                                </li>
                                <?php } ?>
                                <!-- <li class='tab'>
                                    <a href="#tabs-comment">Comments</a>
                                </li> -->
                            </ul>
                            <div class='panel-container'>
                                <?php if($recent->num_rows() > 0 ) { ?>
                                <div id="tabs-recent">
                                    <ul>
                                        <?php foreach ($recent->result_array() as $valrec) {?>
                                            <li><a href="<?php echo site_url('blog/detail/'.$valrec['id'].'/'.url_title($valrec['title']))?>"><?php echo $valrec['title']?></a></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <?php } ?>
                                <?php if($popular->num_rows() > 0 ) { ?>
                                <div id="tabs-popular">
                                    <ul>
                                        <?php foreach ($popular->result_array() as $valpop) {?>
                                            <li><a href="<?php echo site_url('blog/detail/'.$valpop['id'].'/'.url_title($valpop['title']))?>"><?php echo $valpop['title']?></a></li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <?php } ?>
                                <?php if($mostcomm->num_rows() > 0 ) { ?>
                                <div id="tabs-comment">
                                    <ul>
                                        <?php foreach ($mostcomm->result_array() as $valcomm) {?>
                                            <li>
                                                <div class="commenters">
                                                    <img src="<?php echo base_url()?>uploads/<?php echo getThumb($valcomm['image_member'])?>" style="width: 41px; "/>
                                                </div>
                                                <a href="<?php echo site_url('blog/detail/'.$valcomm['id_blog'].'/'.url_title($valcomm['title_blog']))?>"><?php echo $valcomm['title_blog']?></a>
                                            </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    
                    </div>
                    </div>
                    <div class="clear"></div>
                    <br/>
                </div>
            </div>
            <div style="clear: both"></div>
            <!-- KRITERION FOOTER -->