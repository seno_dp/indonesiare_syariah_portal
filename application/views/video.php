<link href="<?php echo base_url()?>assets/theme/css/blue.monday/jplayer.blue.monday.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?php echo base_url()?>assets/theme/js/jquery.jplayer.min.js"></script>
<!-- START PAGE VIDEO DETAIL -->
<section>
    <div class="container">
      <div class="row">
        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
		  <div class="" id="">
			  <h1><?php echo ($menu_title) ? ucfirst($menu_title) : "Galeri Video"?></h1>
			  <div class="entry">
				  <div class="">
					   <?php if($foto_album->num_rows() > 0) {?>
						  <?php foreach($foto_album->result_array() as $qval) {?>
						  <script type="text/javascript">
						  $(document).ready(function(){

							  $("#jquery_jplayer_1").jPlayer({
								  ready: function () {
									  $(this).jPlayer("setMedia", {
										  title: "<?php echo $qval['title']?>",
										  m4v: "<?php echo base_url()?>uploads/<?php echo $qval['uploaded_file']?>",
										  /*m4v: "http://www.jplayer.org/video/m4v/Big_Buck_Bunny_Trailer.m4v",*/
										  poster: "<?php echo base_url()?>uploads/<?php echo $qval['image']?>"
									  });
								  },
								  swfPath: "js",
								  supplied: "webmv, ogv, m4v",
								  size: {
									  width: "585px",
									  height: "360px",
									  cssClass: "jp-video-270p"
								  },
								  smoothPlayBar: true,
								  keyEnabled: true,
								  remainingDuration: true,
								  toggleDuration: true
							  });

						  });
						  //]]>
						  </script>
						  <div id="jp_container_1" class="jp-video jp-video-270p">
							  <div class="jp-type-single">
								  <div id="jquery_jplayer_1" class="jp-jplayer"></div>
								  <div class="jp-gui">
									  <div class="jp-video-play">
										  <a href="javascript:;" class="jp-video-play-icon" tabindex="1">play</a>
									  </div>
									  <div class="jp-interface">
										  <div class="jp-progress">
											  <div class="jp-seek-bar">
												  <div class="jp-play-bar"></div>
											  </div>
										  </div>
										  <div class="jp-current-time"></div>
										  <div class="jp-duration"></div>
										  <div class="jp-controls-holder">
											  <ul class="jp-controls">
												  <li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
												  <li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
												  <li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
												  <li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
												  <li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
												  <li><a href="javascript:;" class="jp-volume-max" tabindex="1" title="max volume">max volume</a></li>
											  </ul>
											  <div class="jp-volume-bar">
												  <div class="jp-volume-bar-value"></div>
											  </div>
											  <ul class="jp-toggles">
												  <li><a href="javascript:;" class="jp-full-screen" tabindex="1" title="full screen">full screen</a></li>
												  <li><a href="javascript:;" class="jp-restore-screen" tabindex="1" title="restore screen">restore screen</a></li>
												  <li><a href="javascript:;" class="jp-repeat" tabindex="1" title="repeat">repeat</a></li>
												  <li><a href="javascript:;" class="jp-repeat-off" tabindex="1" title="repeat off">repeat off</a></li>
											  </ul>
										  </div>
										  <div class="jp-details">
											  <ul>
												  <li><span class="jp-title"></span></li>
											  </ul>
										  </div>
									  </div>
								  </div>
								  <div class="jp-no-solution">
									  <span>Update Required</span>
									  To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
								  </div>
							  </div>
						  </div>
						  <?php } ?>  
				  
					  <?php } ?>
				   </div>
			  </div>
			  <div class="postmetadata"></div>
		  </div>          
        </div>
        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
          <div class="home_sidebar">
              <div class="follow_us_side">
                <?php echo $this->load->view('detail_sidebar')?> 
              </div>                               
          </div>
        </div>
      </div>
    </div>
</section>
<!-- START PAGE VIDEO DETAIL -->
<div style="clear: both"></div> 