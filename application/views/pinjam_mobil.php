<!-- PINJAMAN MOBIL -->
            <div class="">
                <div class="row">
                    <?php 
                            if($member->num_rows() > 0){
                                $v = $member->row_array();
                        ?>
                        <div id="" class="">
                            <div class="cover-area-profile">
                                <?php if($v['cover']) { ?>
                                     <img class="" src="<?php echo base_url().'uploads/'.$v['cover']?>"/>
                                <?php }else{ ?>
                                    <!--<img class="" src="<?php echo base_url()?>uploads/2013/06/cover-photo-admin.jpg" />-->
                                <?php } ?>
                            </div>      
                            <div style="clear:both"></div>
                                <div class="container">
                                    <div class="nametag-admin-blog">
                                        <div class="admin-photo-profile">
                                            <?php if($v['image']) { ?>
                                                <img src="<?php echo base_url().'uploads/'.getThumb($v['image'])?>" />
                                            <?php }else{ ?>
                                                <img src="<?php echo base_url().'assets/theme/images/180x180.jpg'?>" />
                                            <?php } ?>
                                        </div>
                                        <div class="fullname-admin-blog">
                                            <h2><?php echo $v['title']?></h2>
                                            <div class="admin-edit">
                                                <?php if($this->uri->segment(4) == $this->session->userdata('user_id_sess')) { ?>
                                                    <a href="<?php echo site_url('member/profile')?>"><img width='24' height='24' src="<?php echo base_url()?>assets/theme/images/edit_icon.png" />Edit Profile</a>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>                                     
                                </div>      
                                <div class="container">
                                    <div class="row">
                                        <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                            <div class="cc_single_post">
                                                <div class="wrapper-left-profile">
                                                    <div class="top-about-administrator"></div> 
                                                    <div class="about-administrator">
                                                        <?php if($v['content']) { ?>
                                                            <p><strong>About Me : </strong><?php echo $v['content']?></p>
                                                        <?php } ?>
                                                    </div> 
													<div class="latest-activity">
														<div class="title-list-page">
															<h3>Daftar Peminjaman Mobil Dinas</h3>
														</div>
														<?php if($ma->num_rows() > 0){?>
															<ul>
																<?php foreach($ma->result_array() as $mval) {?>
																	<li class="list_pinjam_mobil">
																		<h2 class="title_mobil">
																			<?php echo $mval['keperluan']?> - <?php echo $mval['description']?> - <?php echo $mval['tujuan']?>
																		</h2>
																		<div class="name_mobil">
																			<?php 
																				$fmobil = array("id"=>"where/".$mval['mobil_id'],"id_lang"=>"where/".GetIdLang());
																				$qmobil = GetAll("kg_mobil",$fmobil);
																				if($qmobil->num_rows() > 0){
																					$amobil = $qmobil->row_array();
																					$mobil = $amobil['title'];
																				}else{
																					$mobil = "-";
																				}
																				echo 'Mobil : '.$mobil;
																			?><br/>
																			<?php 
																				$fdriver = array("id"=>"where/".$mval['driver_id'],"id_lang"=>"where/".GetIdLang());
																				$qdriver = GetAll("kg_driver",$fdriver);
																				if($qdriver->num_rows() > 0){
																					$adriver = $qdriver->row_array();
																					$driver = $adriver['title'];
																				}else{
																					$driver = "-";
																				}
																				echo 'Driver : '.$driver;
																			?><br/>
																			<?php echo 'Tanggal : '.date('d M Y H:m', strtotime($mval['date_pinjam'])).' WIB '?><br/>
																			<a href="<?php echo site_url('member/pdf_mobil/'.$mval['id'])?>" target="_BLANK"><img src="<?php echo base_url().'/assets/theme/images/pdfprint.png'?>"></a>
																		</div>
																	</li>
																<?php } ?>
															</ul>
														<?php } ?>
													</div>       													
                                                </div>                           
                                            </div>                      
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                            <div class="cc_single_post">
                                                <div class="home_sidebar">
                                                    <div class="related-bottom">
                                                        <h2><img src="http://localhost/reindo_intranet/assets/theme/images/blue-dot.png" height="20" width="20">Blog Post</h2>
														<span class="bar-blue"></span>
                                                        <?php if($this->uri->segment(4) == $this->session->userdata('user_id_sess')) { ?>
                                                            <div class="button-post-first">
                                                                <a href="<?php echo site_url('blog/post')?>"><img width="24" height="24" src="<?php echo base_url()?>assets/theme/images/post-icn.png" />Posting Blog</a>
                                                            </div>
                                                        <?php } ?>                                      
                                                    </div>
                                                    <div class="list-post-blog">
                                                        <?php if($qp->num_rows() > 0){?>
                                                            <ul>
                                                                <?php foreach($qp->result_array() as $qval) {?>
                                                                    <li><a href="<?php echo site_url('blog/detail/'.$qval['id'].'/'.url_title($qval['title']))?>"><?php echo $qval['title']?></a><span class="date-list"><?php echo date('d M Y', strtotime($qval['create_date']))?></span></li>
                                                                <?php } ?>
                                                            </ul>
                                                        <?php } ?>
                                                    </div>
                                                </div>  
                                                <!-- <div class="sidebar-thread-post">
                                                        <div class="head-blog-post">
                                                            <h4>Thread Post</h4>
                                                            <a href="#"><img src="<?php echo base_url()?>uploads/2013/06/write_icon.png" />Posting Thread</a>
                                                        </div>                              
                                                    <div class="list-post-blog">
                                                        <ul>
                                                            <li><a href="#">Update : Dept Undwriting Fakultatif Marine & Aviation Reindo Reinsurance 2014</a><span class="date-list">04 Mei 2014</span></li>
                                                            <li><a href="#">Badan Pengelola Pusat Data Asuransi Nasional (BPPDAN) adalah sebagai pusat data statistik jenis asuransi kebakaran Indonesi</a><span class="date-list">04 Mei 2014</span></li>
                                                            <li><a href="#">Special Award-Most Innovative Reinsurance Company 2006</a><span class="date-list">04 Mei 2014</span></li>
                                                        </ul>
                                                    </div>
                                                </div>   -->                                
                                            </div>
                                        </div>
                                    </div>                                     
                                </div>                                                  
                        </div>
                        <div style="clear:both"></div>

                    <?php } ?>
                </div>
            </div>
            <div style="clear: both"></div>