<div class="flex_100_1">
    <div class="post">
      <div id="slider_j">
                <ul class="ui-tabs-nav">
                    <?php if($qp->num_rows() > 0){ ?>
                    <?php foreach($qp->result_array() as $sval){ ?>
                    <li class="ui-tabs-nav-item ui-tabs-selected" id="nav-fragment-<?php echo $sval['id']?>">
                        <a href="#fragment-<?php echo $sval['id']?>">
                            <img width="86" height="57" src="<?php echo base_url()?>uploads/<?php echo getThumb($sval['image'])?>" alt="<?php echo $sval['title']?>"/>
                            <span><?php echo word_limiter($sval['title'],20)?></span><br/>
                            <!--<?php echo word_limiter(str_replace("<p>","",str_replace("</p>","",$sval['content'])),10)?>-->
                        </a>
                    </li>
                    <?php } ?>
                    <?php } ?>
                </ul>

                <?php if($qp->num_rows() > 0){ ?>
                <?php foreach($qp->result_array() as $sval){ ?>
                    <div id="fragment-<?php echo $sval['id']?>" class="ui-tabs-panel" style="">
                        <a href="<?php echo $sval['url_link']?>" rel="bookmark">
                            <img class="img_jalbum" src="<?php echo base_url()?>uploads/<?php echo $sval['image']?>" alt="<?php echo $sval['title']?>"/>
                        </a>
                        <div class="info">
                            <a class="hideshow" href="#">Hide</a>
                            <h2>
                                <a href="<?php echo $sval['url_link']?>" rel="bookmark"><?php echo $sval['title']?></a>
                            </h2>
                            <p>
                                <?php echo str_replace("<p>","",str_replace("</p>","",$sval['content']))?>
                                <a href="<?php echo $sval['url_link']?>" rel="bookmark">read more</a>
                            </p>
                        </div>
                    </div>
                <?php } ?>
                <?php } ?>
            </div>


            
            <div class="entry">

                <div class="list-gallery">
                     <?php if($qprest->num_rows() > 0) {?>
                     <h1><?php echo ($menu_title) ? ucfirst($menu_title) : "Galeri foto"?></h1>
                    <ul>
                        <?php foreach($qprest->result_array() as $qval) {?>
                        <li>
                            <a href="<?php echo $qval['url_link']?>" rel="bookmark" target="_BLANK">
                                <img width="180" height="140" src="<?php echo base_url()?>uploads/<?php echo $qval['image']?>" class="attachment-blog-image wp-post-image" alt="<?php echo $qval['title']?>"/>
                            </a>
                            <div class="title-gallery">
                                <a href="<?php echo $qval['url_link']?>" rel="bookmark" target="_BLANK">
                                    <h5><?php echo $qval['title']?></h5>
                                </a>
                            </div>
                        </li>
                        <?php } ?>  
                    </ul>
                    <?php } ?>
                </div>
            </div>

    </div>
    <!-- <div class="navigation flex_66">
        <div class="pagination">
            <?php //echo $pagination?>
        </div>
    </div> -->
</div>
<!-- <div class="flex_33">
    <div id="sidebar">
        <?php //echo $this->load->view('detail_sidebar')?>
    </div>
</div> -->
<div style="clear: both"></div>