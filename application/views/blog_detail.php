<!-- BLOG DETAIL -->
<div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
    <div class="cc_single_post">
        <?php if($blog->num_rows() > 0) {?>
        <?php $val = $blog->row_array();?>
        <div class="post-<?php echo $val['id']?> list-of-article">
            <div class="latest-activity">
                <h4>Blog Post</h4>
            </div>        
            <h4 id="post-<?php echo $val['id']?>">
                <a href="#" title="<?php echo $val['title']?>">
                    <?php echo $val['title']?></a>
            </h4>
            <div class="post_text">
                <?php if($val['image']) {?>
                    <img class="thumb-large" src="<?php echo base_url()?>uploads/<?php echo $val['image']?>" class="attachment-blog-image wp-post-image" alt="<?php echo $val['title']?>"/>
                <?php } ?>
                <div class="post_text">
                    <?php echo $val['content']?>
                </div>
                <div class="clear"></div>
            </div>
            <br/>
            <div class="postmetadatablog-comment">
                <div class="post-meta-blog">
                    <ul>
                        <li><a href=""><i class="fa fa-clock-o"></i>&nbsp;&nbsp;<?php echo date('M d, Y',strtotime($val['create_date']))?></a></li>
                        <li><a href="">by</a></li>
                        <li><?php echo '<i class="fa fa-user"></i>&nbsp;&nbsp;<a href="'.site_url('member/userprofile/'.$val['create_user_id'].'/'.url_title($val['title'])).'">'.GetUserName('kg_member','title',$val['create_user_id']).'</a>'?></li>
                    </ul>
                </div>                
                <div class="post-meta-blog">
                    <ul>
                        <li><i class="fa fa-folder"></i>&nbsp;&nbsp;Filed Under:&nbsp;&nbsp;
                            <a href="#" title="View all posts in <?php echo $val['category']?>" rel="category tag"><?php echo $val['category']?></a></li>
                        <li>
                            <a href=""><i class="fa fa-tag"></i>Tags:&nbsp;&nbsp;
                            <?php echo explodetags($val['tags']) ?>
                            <?php if($comment->num_rows() > 0) { 
                                $num_comment = $comment->num_rows()." comments";
                            }else{
                                $num_comment = "No comment yet";
                            }
                            ?></a>
                        </li>
                        <li><i class="fa fa-comment"></i>&nbsp;&nbsp;<a href="##respond" title="Comment on <?php echo $val['title']?>"><?php echo $num_comment;?> </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="area-commenters" id="area-commenters">
                <?php if($comment->num_rows() > 0) {?>
                <?php foreach ($comment->result_array() as $comm) { ?>
                <div class="single_comment">
                    <div class="single_comment_pic">
                        <?php if($comm['image_member']){ ?>
                            <img class="img-responsive" src="<?php echo base_url()?>uploads/<?php echo getThumb($comm['image_member'])?>" />
                        <?php }else{ ?>
                            <img class="img-responsive" src="<?php echo base_url()?>uploads/2013/06/commenters.png" />
                        <?php } ?> 
                        </div>
                        <div class="single_comment_text">
                            <div class="sp_title">
                                <a href=""><h4><?php echo $comm['name_member']?></h4></a>
                                <p class="name-commenters"><?php echo date('M d, Y',strtotime($comm['create_date']))?></p>
                            </div>
                            <p><?php echo $comm['content']?></p>
                        </div>       
                </div>
                <?php } ?>
                <?php }else{ ?>
                <div class="single_comment">
                    <div class="single_comment_pic">
                        <img class="img-responsive" src="<?php echo base_url()?>uploads/2013/06/commenters.png" />
                    </div>
                    <div class="single_comment_text">
                        <div class="sp_title">
                            <a href=""><h4>Anonymous</h4></a>
                            <p class="name-commenters">dd mm, yyyy</p>
                        </div>
                        <p>No comment</p>
                    </div>       
                </div>
                <?php } ?>
            </div>
            <div class="comment-form">
                <h2>Post Comment</h2>
                    <div class="comments_form">
                    <form>
                        <input type="hidden" id="blog_id" value="<?php echo $this->uri->segment(4)?>" name="blog_id">
                        <input type="hidden" id="member_id" value="<?php echo $this->session->userdata('user_id_sess')?>" name="member_id">
                        <textarea placeholder="comment" name="comment" cols="30" rows="10"></textarea>
                        <input type="button" value="Post Comment" name="submit"></input>                        
                    </form>
                    </div>
            </div>
        </div>
        <?php } ?>        
    </div>
</div>

<div class="col-lg-3 col-md-3 col-sm-12 colxs-12">
    <div class="home_sidebar">
        <div class="follow_us_side">
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#tab-container').easytabs();
                });
            </script>
            <div class="">
                <div id="tab-container" class='tab-container'>
                    <ul class='etabs'>
                        <?php if($recent->num_rows() > 0 ) {?>
                        <li class='tab'>
                            <a href="#tabs-recent">Recent</a>
                        </li>
                        <?php } ?>
                        <?php if($popular->num_rows() > 0 ) {?>
                        <li class='tab'>
                            <a href="#tabs-popular">Popular</a>
                        </li>
                        <?php } ?>
                        <?php if($mostcomm->num_rows() > 0 ) {?>
                        <li class='tab'>
                            <a href="#tabs-comment">Comments</a>
                        </li>
                        <?php } ?>
                        <!-- <li class='tab'>
                                    <a href="#tabs-comment">Comments</a>
                                </li> -->
                    </ul>
                    <div class='panel-container'>
                        <?php if($recent->num_rows() > 0 ) { ?>
                        <div id="tabs-recent">
                            <ul>
                                <?php foreach ($recent->result_array() as $valrec) {?>
                                <li>
                                    <a href="<?php echo site_url('blog/detail/'.$valrec['id'].'/'.url_title($valrec['title']))?>">
                                        <?php echo $valrec[ 'title']?>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <?php } ?>
                        <?php if($popular->num_rows() > 0 ) { ?>
                        <div id="tabs-popular">
                            <ul>
                                <?php foreach ($popular->result_array() as $valpop) {?>
                                <li>
                                    <a href="<?php echo site_url('blog/detail/'.$valpop['id'].'/'.url_title($valpop['title']))?>">
                                        <?php echo $valpop[ 'title']?>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <?php } ?>
                        <?php if($mostcomm->num_rows() > 0 ) { ?>
                        <div id="tabs-comment">
                            <ul>
                                <?php foreach ($mostcomm->result_array() as $valcomm) {?>
                                <li>
                                    <div class="commenters">
                                        <img src="<?php echo base_url()?>uploads/<?php echo getThumb($valcomm['image_member'])?>" style="width: 41px; " />
                                    </div>
                                    <a href="<?php echo site_url('blog/detail/'.$valcomm['id_blog'].'/'.url_title($valcomm['title_blog']))?>">
                                        <?php echo $valcomm[ 'title_blog']?>
                                    </a>
                                </li>
                                <?php } ?>
                            </ul>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>  
<!-- BLOG DETAIL -->
