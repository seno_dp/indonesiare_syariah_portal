<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">

<head profile="http://gmpg.org/xfn/11">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Reindo Reinsurance</title>
    <link rel="shortcut icon" href="#" />
    <link rel="stylesheet" href="<?php echo base_url()?>assets/theme/style.css" type="text/css" />
    <?php 
        $filter_background = array("is_publish"=>"where/publish","limit"=>"0/1","id"=>"order/desc");
        $query_background = GetAll("kg_background",$filter_background);
        if($query_background->num_rows() > 0){
                $val_background = $query_background->row_array(); 
                $img_background = $val_background['image'];
                $title_background = $val_background['title'];
                $url_background = base_url().'uploads/'.$img_background;
        }else{
                $url_background = base_url().'assets/theme/images/background.jpg';
        }
    ?>
    <style type='text/css'>
        body {
            background: #980613 url("<?php echo $url_background?>") ;
            background-position: center top;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }
        .logo {
            float: left;
            background: none;
        }
        
        #wrapper-menu {
            width: 960px;
            height: 45px;
            background: #252525;
        }
        #navigation li ul {
            width: auto !important;
            background: 0 0 repeat #252525;
        }
        #navigation {
            list-style: none;
            margin: 0 auto;
            padding: 0;
            border-bottom: 4px solid #D51F2B;
        }
        
        .top-nav {
            background: #fff;
            border-top: 3px solid #D51F2B;
            border-bottom: 1px solid #ececec;
            border-right: 2px solid #D51F2B;
            height: 30px;
        }
        .top-nav ul li.current-menu-item:after {
            content: " ";
            display: block;
            width: 0;
            height: 0;
            position: absolute;
            top: 0px;
            z-index: 2;
            right: 45%;
            border: 4px solid transparent;
            border-top-color: #D51F2B;
        }
        .search-block .search-button {
            background: #D51F2B url(wp-content/themes/REINDO/images/search.png) no-repeat center;
            cursor: pointer;
            float: right;
            height: 30px;
            width: 50px;
            text-indent: -999999px;
            display: block;
            border: 0 none;
        }
        
        .arrows {
            font-family: Arial;
            font-size: 27px;
            color: #D51F2B;
            text-align: left;
        }
        .arrows-tabs {
            font-family: Arial;
            font-size: 18px;
            color: #D51F2B;
            text-align: left;
        }
        #footer-border {
            height: 29px;
            width: 961px;
            position: absolute;
            left: 0px;
            top: -21px;
            background: #D51F2B;
            opacity: 0.9;
        }
        
        .scrollup:hover {
            background: url(wp-content/themes/REINDO/images/icon_top.png) center center no-repeat #D51F2B;
        }
        a {
            color: #0d0c0c;
            -moz-transition: .6s linear;
            -webkit-transition: .6s ease-out;
            transition: .6s linear;
        }
        a:focus,
        a:active,
        a:hover {
            color: #D71A1A;
            text-decoration: none;
        }
        .box .text h5 a:hover {
            color: #D71A1A;
        }
        .tags a:hover {
            background-color: #D51F2B;
            color: #FFF;
            -moz-transition: .8s linear;
            -webkit-transition: .8s ease-out;
            transition: .8s linear;
        }
        div.pagination a {
            background-color: #D51F2B;
        }
        #kr-carousel .next {
            background-color: #D51F2B;
        }
        #kr-carousel .prev {
            background-color: #D51F2B;
        }
    </style>
    <link rel="pingback" href="xmlrpc.php" />
    <link rel="alternate" type="application/rss+xml" title="Reindo Reinsurance &raquo; Feed" href="feed/index.html" />
    <link rel="alternate" type="application/rss+xml" title="Reindo Reinsurance &raquo; Comments Feed" href="comments/feed/index.html" />
    <link rel='stylesheet' id='awesome-weather-css' href='wp-content/plugins/awesome-weather/awesome-weathercd70.css?ver=3.8.3' type='text/css' media='all' />
    <link rel='stylesheet' id='opensans-googlefont-css' href='<?php echo base_url()?>assets/theme/css/fonts/opensans.css' type='text/css' media='all' />
    <!--link rel='stylesheet' id='opensans-googlefont-css' href='https://fonts.googleapis.com/css?family=Open+Sans%3A400%2C300&amp;ver=3.8.3' type='text/css' media='all' /-->
    <link rel='stylesheet' id='tp_twitter_plugin_css-css' href='wp-content/plugins/recent-tweets-widget/tp_twitter_plugin5152.css?ver=1.0' type='text/css' media='screen' />
    <link rel='stylesheet' id='woocommerce_prettyPhoto_css-css' href='<?php echo base_url()?>assets/theme/css/prettyPhotocd70.css?ver=3.8.3' type='text/css' media='all' />
    <!-- <link rel='stylesheet' id='woocommerce-layout-css' href='<?php echo base_url()?>assets/theme/css/woocommerce-layout3c94.css?ver=2.1.0' type='text/css' media='all' />
    <link rel='stylesheet' id='woocommerce-smallscreen-css' href='<?php echo base_url()?>assets/theme/css/woocommerce-smallscreen3c94.css?ver=2.1.0' type='text/css' media='only screen and (max-width: 768px)' />
    <link rel='stylesheet' id='woocommerce-general-css' href='<?php echo base_url()?>assets/theme/css/woocommerce3c94.css?ver=2.1.0' type='text/css' media='all' /> -->
    <link rel='stylesheet' href='<?php echo base_url()?>assets/theme/css/slider.css' type='text/css' media='all' />
    <!--[if IE]><link rel="stylesheet" href="<?php echo base_url()?>assets/theme/css/forIE.css" type="text/css" /></link> <![endif]-->        
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery3e5a.js?ver=1.10.2'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery-migrate.min1576.js?ver=1.2.1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery.carousel68b3.js?ver=1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery.ticker68b3.js?ver=1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/ajaxtabs68b3.js?ver=1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery-ui.min68b3.js?ver=1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/superfish68b3.js?ver=1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/supersubs68b3.js?ver=1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/custom68b3.js?ver=1'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery.prettyPhoto.minc6bd.js?ver=3.1.5'></script>
    <script type='text/javascript' src='<?php echo base_url()?>assets/theme/js/jquery.prettyPhoto.init.min3c94.js?ver=2.1.0'></script>
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="#" />
</head>
<body class="home blog">
    <div class="outer_wrap">
        <div class="inner_wrap">
            <!-- REINDO HEADER -->
            <?php echo $this->load->view($header)?>
            <!-- REINDO MENU -->
            <?php echo $this->load->view($main_menu)?>
        </div>
        <div class="shaddow"></div>
        <!-- REINDO LATEST NEWS TIKER -->
        <?php echo $this->load->view('news_ticker')?>
        
        <!-- REINDO CONTENT -->
        <div id="main-content">
            <?php echo $this->load->view($main_content)?>
        </div>
        <div style="clear: both"></div>
        <!-- REINDO FOOTER -->
        <div id="footer">
            <?php echo $this->load->view($footer);?>
        </div>
    </div>
    <a href="#" class="scrollup">Scroll</a>
</body>
<meta http-equiv="content-type" content="text/html;charset=UTF-8">
</html>