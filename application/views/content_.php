
<!-- REINDO SINGLE PAGE -->
<div class="flex_66_1">
    <?php if($content->num_rows() > 0) {?>
    <?php $val = $content->row_array();?>
    <div class="post type-post" id="">
        <!-- REINDO BREAD CRUMB -->
        <div class="bread-crumb">
            <a href="<?php echo site_url('home')?>">Home</a> / 
            <a href="<?php echo site_url('page')?>">About</a> /
            <?php echo $val['title']?>
        </div>
        <h1>
            <?php echo $val['title']?>
        </h1>
        <div class="entry">
            <?php echo $val['content']?></div>
        <div class="postmetadata">
            <div class="meta">
                <span class="time"><?php echo date('M d, Y',strtotime($val['create_date']))?></span>
                <em>by </em>
                <?php echo GetUserName('kg_admin','name',$val['create_user_id'])?>
            </div>
            Tags:
            <?php echo explodetags($val['tags']) ?>
        </div>
    </div>
    <?php }?>
    <!-- RELATED POSTS 
    <div class="box">
        <div class="related-title">
            <b>Related Posts
                <span class="arrows">&raquo;</span>
            </b>
        </div>
    </div>
    <div style="clear:both"></div>
    <div id="related-posts">
        <ul>
            <li>
                <a href="#" rel="bookmark">
                    <img width="147" height="100" src="uploads/2013/06/related1.jpg" class="attachment-related-image wp-post-image" alt="2905135770_c3c86b41d9_z" width="153px" />
                </a>
                <div class="title-car">
                    <a href="#" rel="bookmark">Fermentum mauris</a>
                </div>
            </li>
            <li>
                <a href="#" rel="bookmark">
                    <img width="147" height="100" src="uploads/2013/06/related2.jpg" class="attachment-related-image wp-post-image" alt="jo" width="153px" />
                </a>
                <div class="title-car">
                    <a href="#" rel="bookmark">Morbi arcu felis</a>
                </div>
            </li>
            <li>
                <a href="#" rel="bookmark">
                    <img width="147" height="100" src="uploads/2013/06/related3.jpg" class="attachment-related-image wp-post-image" alt="jeans-100136_640" width="153px" />
                </a>
                <div class="title-car">
                    <a href="#" rel="bookmark">Nulla sed</a>
                </div>
            </li>
            <li>
                <a href="#" rel="bookmark">
                    <img width="147" height="100" src="uploads/2013/06/related4.jpg" class="attachment-related-image wp-post-image" alt="fake-13426_640" width="153px" />
                </a>
                <div class="title-car">
                    <a href="#" rel="bookmark">Adipiscing arcu</a>
                </div>
            </li>
        </ul>
    </div>
-->
</div>
<div class="flex_33">
    <div id="sidebar">
        <?php echo $this->load->view('detail_sidebar')?>                   
    </div>
</div>
<div style="clear:both"></div>