<div class="flex_66_11">
    <div class="post" id="post-35">
        <div class="entry-thread">
            <div class="product"> 
				<div class="bread-crumb-thread">						
					Administrator : <a href="#">Edit Profile</a>
				</div>
				<div style="clear:both"></div>							
				<div class="forum-detail">												
						<!-- Here Form -->			
					<div id="respond" class="comment-respond">
						<?php 
							if($member->num_rows() > 0){
								$v = $member->row_array();
						?>
								<form action="<?php echo site_url('member/register_forum')?>" method="post" class="comment-form" enctype="multipart/form-data">

									<p class="comment-form-email">
										<label for="email">Cover Picture (960x300)
											<span class="required">:</span>
										</label>
										<?php echo form_hidden('file_old_cover',set_value('file_old_cover',$v['cover']));?>
										<input type="file" maxlength="50" allow="text/*" name="attachedfilecover"></input>
										<?php
											if($v['cover']){
												echo '<div class="thumb_image_cover">';
												echo '<img width="150px;" src="'.base_url().'uploads/'.getThumb($v['cover']).'"><br/>';
												echo '<div class="delete_cover">Delete</div></div>';
											}
										?>
									</p>

									<p class="comment-form-email">
										<label for="email">Avatar
											<span class="required">:</span>
										</label>
										<?php echo form_hidden('file_old',set_value('file_old',$v['image']));?>
										<input type="file" maxlength="50" allow="text/*" name="attachedfile"></input>
										<?php
											if($v['image']){
												echo '<div class="thumb_image_avatar">';
												echo '<img width="150px;" src="'.base_url().'uploads/'.getThumb($v['image']).'"><br/>';
												echo '<div class="delete_avatar">Delete</div></div>';
											}
										?>
									</p>

									<p class="comment-form-author">
										<label for="author">Name
											<span class="required">:</span>
										</label>
										<!--input id="post-title" name="author" type="text" value="" size="30" aria-required='true' value="<?php echo $v['title']?>"/-->
										<?php echo form_input(array('name'=>'title','aria-required'=>'true','value'=>set_value('title',$v['title'])));?>
                        				<?php echo form_error('title'); ?>
									</p> 

										<p class="comment-form-author">
										<label for="author">Email
											<span class="required">:</span>
										</label>
										<!--input id="post-title" name="author" type="text" value="" size="30" aria-required='true' value="<?php echo $v['email']?>"/-->
										<?php echo form_input(array('name'=>'email','aria-required'=>'true','value'=>set_value('email',$v['email']),'readonly'=>'true'));?>
                        				<?php echo form_error('email'); ?>
									</p> 
									<p class="comment-form-author">
										<label for="author">Password 
											<span class="required">:</span>
										</label>
										<!--input id="post-title" name="author" type="text" value="" size="30" aria-required='true' value="<?php echo $v['title']?>"/-->
										<?php echo form_password(array('name'=>'password','aria-required'=>'true','value'=>''));?>
										&nbsp;(*harus sama dengan password login portal)
                        				<?php echo form_error('password'); ?>
									</p> 
									<p class="comment-form-author">
										<label for="author">Phone
											<span class="required">:</span>
										</label>
										<?php echo form_input(array('name'=>'phone','aria-required'=>'true','value'=>set_value('phone',$v['phone'])));?>
                        				<?php echo form_error('phone'); ?>
										<!--input id="post-title" name="author" type="text" value="" size="30" aria-required='true' value="<?php echo $v['phone']?>"/-->
									</p>    
									<p class="comment-form-author">
										<label for="author">Address
											<span class="required">:</span>
										</label>
										<!--textarea id="address" name="address" cols="31" rows="2" aria-required="true"><?php echo $v['address']?></textarea-->
										<?php echo form_textarea(array('name'=>'address','aria-required'=>'true','value'=>set_value('address',$v['address']),'rows'=>'3', 'style'=>'width:43%'));?>
									</p>

									<p class="comment-form-author">
										<label for="author">Tentang saya
											<span class="required">:</span>
										</label>
										<?php echo form_textarea(array('name'=>'content','aria-required'=>'true','value'=>set_value('content',$v['content']),'rows'=>'5', 'style'=>'width:43%'));?>
									</p>                                           
									

		                            <p class="form-edit-profile" style="margin-top: 10px;">
										<input name="submit" type="submit" id="submit" value="Cancel" />
		                                <input name="submit" type="submit" id="submit" value="Save" />
		                                <?php echo form_hidden('id',set_value('id',$v['id']));?>
									</p>
								</form>
						<?php } ?>
					</div>
					<!-- #respond -->
				</div>	  
				<div style="clear:both"></div>	

				<div style="clear:both"></div>	
						
				<div style="clear:both"></div>	
				   							
            </div>
            <!-- #product-523 -->
            <div style="clear:both"></div>
        </div>
    </div>
</div>

<div style="clear: both"></div>