<!-- SEARCH -->

<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-2 col-xs-12">
                <div class="cc_single_post">
                    <div class="title-list-page">
                        <h3>Keyword
                            <?php echo $keyword;?>
                        </h3>
                    </div>
                    <?php if($search->num_rows() > 0) {?>
                    <?php foreach($search->result_array() as $qval) {?>
                        <div class="post-<?php echo $qval['id']?> list-of-article">
                            <h4 id="post-<?php echo $qval['id']?>">
                                <a href="<?php echo site_url($qval['module_detail'].$qval['id'].'/'.url_title($qval['title']))?>" title="<?php echo $qval['title']?>">
                                    <?php echo $qval['title']?>
                                </a>
                            </h4>
                            <?php if($qval['headline'] != ''){?>
                            <div class="entry">
                                <div class="excerptarhive"><?php echo $qval['headline']?></div>
                                <div class="clear"></div>
                            </div>
                            <br/>
                            <?php }?>
                            
                            <div class="post_meta">
                                <!-- KRITERION META -->
                                <ul>
                                    <li><i class="fa fa-clock-o"></i>&nbsp;<?php echo date('M d, Y',strtotime($qval['create_date']))?></li>
                                    <li><i class="fa fa-tag"></i>&nbsp;Tags:&nbsp;&nbsp;<?php echo explodetags($qval['tags']) ?></li>
                                </ul>
                            </div>
                        </div>
                        <?php } ?>
                    <?php }else{ ?>
                    <div class="post-1 list-of-article">
                        <h4 id="">
                            <a href="#" title="Lipsum">
                                No result found.
                            </a>
                        </h4>
                        <div class="post_meta">
                            <ul>
                                <li>
                                    <i class="fa fa-clock-o">June 7th, 2017</i>
                                </li>
                                <li>
                                    <i class="fa fa-tag"></i>&nbsp;Tags:&nbsp;&nbsp;<a href="#" rel="tag">tags</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <?php } ?>                     
                </div>             
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="home_sidebar">
                    <div class="follow_us_side">
                        <?php echo $this->load->view('detail_sidebar')?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- SEARCH -->
