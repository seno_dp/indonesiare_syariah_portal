<?php
    $date1 = date_create($villa['start_date']);
    $date2 = date_create($villa['end_date']);
    $interval = date_diff($date1, $date2);
    //echo 'Tanggal : '.date('d M Y', strtotime($mval['start_date'])).' WIB - '.date('d M Y', strtotime($mval['end_date'])).' WIB'
    //echo 'Lama Istirahat : '.$interval->format('%a hari')?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-US">

<head profile="http://gmpg.org/xfn/11">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>Intranet PT. Reasuransi Internasional Indonesia</title>
    <link rel="shortcut icon" href="#" />
    <!-- <link rel="stylesheet" href="<?php echo base_url()?>assets/theme/css/jquery-ui.css" type="text/css" /> -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/theme/style.css" type="text/css" />
    <style type='text/css'>
        .kolom_kiri_header{
            width: 480px;
            font-size: 11px;
        }
        .kolom_kanan_header{
            width: 210px;
            font-size: 11px;
        }
       .kolomkiri{
       	width: 365px;
        border-right: 1px solid #000;
        padding-right: 10px;
       }
       .kolomkanan{
        width: 300px;
        padding-left: 10px;

       }
       .tabel_content{
        margin-top: 30px;
        font-size: 12px;
       }
       .label_kiri{
        width: 120px;
       }

       .tabel_daftar tr td,.tabel_daftar tr th{
        border: 1px solid #000;
        text-align: center;
       }
       .label_kiri_kolom_kiri{
        width: 75px;
       }
       .label_kiri_kolom_kiri_pendek{
        width: 10px;
        text-align: right;
       }
       .align_kanan{
        text-align: right;
       }
       
       .tgl_pemohon{
        padding-top: 20px;
        
       }
       .align_tengah{
        text-align: center;
       }
       .ttd_pemohon{
        padding-top: 70px;
       }
       .catatan_no{
        width: 5px;
       }
       .catatan_text{
        width: 360px;
       }
    </style>
   
</head>
<body class="home blog">

    <div class="outer_wrap"> 
       <!--  <div class="flex_100" style="padding: 0.5%;width: 980px;border: 1px solid #000;"> -->
        	<table>
        		<tr>
        			<td style="border: 1px solid #000;">
        				<table>
        					<tr>
        						<td class="kolom_kiri_header"><img src="<?php echo base_url();?>assets/theme/images/logo.png"></td>
        						<td class="kolom_kanan_header">
        							PT Reasuransi Internasional Indonesia<br/>
        							Jl. Salemba Raya No. 30<br/>
        							Jakarta 10430, Indonesia<br/>
        							Telp : 6221-3920101, 31934208 (hunting)<br/>
        							Fax : 6221-3148776<br/>
        							Website : http://www.reindo.co.id<br/>
        							Email : cosecretary@reindo.co.id
        						</td>
        					</tr>
        				</table>
        				<table class="tabel_content">
        					<tr>
        						<td class="kolomkiri">
        							Permohonan Menginap/Mempergunakan Wisma Balai Peristirahatan Arga Sonya (BPAS) PT Reasuransi Internasional Indonesia
        						</td>
                                <td class="kolomkanan">
                                    <table>
                                        <tr>
                                            <td class="label_kiri">No. Referensi</td>
                                            <td>: <u><?php echo $villa['no_surat']?></u></td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri">Izin Penggunaan</td>
                                            <td>: <u><?php echo date('d M Y',strtotime($villa['approve_date']))?></u></td>
                                        </tr>
                                        
                                    </table>                                    
                                </td>
        					</tr>
        					<tr>
        						<td class="kolomkiri">
                                    <table>
                                        <tr>
                                            <td class="label_kiri_kolom_kiri">Nama</td>
                                            <td>: <u><?php echo $villa['name_member']?></u></td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri_kolom_kiri">Jabatan</td>
                                            <td>: </td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri_kolom_kiri">Perusahaan</td>
                                            <td>: <u>PT. ReINDO</u></td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri_kolom_kiri">Lama istirahat</td>
                                            <td>: <u><?php echo $interval->format('%a hari')?></u></td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri_kolom_kiri"></td>
                                            <td>: <u><?php echo date('d M Y', strtotime($villa['start_date'])).' s/d '.date('d M Y', strtotime($villa['end_date']))?></u></td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri_kolom_kiri">Keperluan / acara</td>
                                            <td>: <u><?php echo $villa['title']?></u></td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri_kolom_kiri">Jumlah peserta</td>
                                            <td>: <u><?php echo $villa['jumlah']?></u></td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri_kolom_kiri">Terdiri dari</td>
                                            <td>: </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <table class="tabel_daftar">
                                                    <tr>
                                                        <th>
                                                            No
                                                        </th>
                                                        <th style="width:190px">
                                                            Nama
                                                        </th>
                                                        <th>
                                                            Lk/Pr
                                                        </th>
                                                        <th>
                                                            Umur
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <td>1.</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2.</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3.</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>4.</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>5.</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>6.</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>7.</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>8.</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>9.</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>10.</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri_kolom_kiri" colspan="2">
                                                <b><u>Catatan :</u></b>
                                                <!-- <ol>
                                                    <li>Harap di isi nama / anggota keluarga yang menginap.</li>
                                                    <li>Tidak diperkenankan menggunakan wisma atas nama orang lain (sesama karyawan), karyawan ybs. harus hadir, ketidakhadiran akan menyebabkan calon pengguna wisma tidak diperbolehkan memasuki wisma dan tidak diperbolehkan memperjualbelikan izin penggunaan wisma BP. Argasonya.</li>
                                                </ol> -->
                                            </td>
                                        </tr>
                                        <tr>
                                             <td colspan="2">
                                                <table>
                                                    <tr>
                                                        <td class="catatan_no" >1.</td>
                                                        <td class="catatan_text">Harap di isi nama / anggota keluarga yang menginap.</td>
                                                    </tr>
                                                    <tr>
                                                        <td class="catatan_no">2.</td>
                                                        <td class="catatan_text">Tidak diperkenankan menggunakan wisma atas nama orang lain (sesama karyawan), karyawan ybs. harus hadir, ketidakhadiran akan menyebabkan calon pengguna wisma tidak diperbolehkan memasuki wisma dan tidak diperbolehkan memperjualbelikan izin penggunaan wisma BP. Argasonya.</td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri_kolom_kiri"></td>
                                            <td class="tgl_pemohon align_tengah">
                                                Jakarta, <?php echo date('d M Y', strtotime($villa['approve_date']))?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri_kolom_kiri"></td>
                                            <td class="align_tengah">
                                                Pemohon,
                                            </td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri_kolom_kiri"></td>
                                            <td class="ttd_pemohon align_tengah">
                                                <?php echo $villa['name_member']?>
                                            </td>
                                        </tr>
                                        
                                    </table> 
        						</td>
                                <td class="kolomkanan">
                                    <table>
                                        <tr>
                                            <td class="label_kiri" style="padding-top: 20px;" colspan="2"><u>Catatan :</u></td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri">Izin terakhir tgl</td>
                                            <td>: <u><?php echo $start_date_last?></u></td>
                                        </tr>
                                         <tr>
                                            <td class="label_kiri"></td>
                                            <td>: <u><?php echo $start_date_last.' s/d '.$end_date_last?></u></td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri">Di wisma</td>
                                            <td>: <u><?php echo $villa_title_last?></u></td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri" style="padding-top: 20px;" colspan="2"><u>Dapat diberikan izin untuk menggunakan</u></td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri">wisma</td>
                                            <td>: <u><?php echo $villa['villa_title']?></u></td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri">Check In</td>
                                            <td>: <u><?php echo date('d M Y',strtotime($villa['start_date'])).' jam '.date('H:i',strtotime($villa['start_date'])).' WIB'?></u></td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri">Check Out</td>
                                            <td>: <u><?php echo date('d M Y',strtotime($villa['end_date'])).' jam '.date('H:i',strtotime($villa['end_date'])).' WIB'?></u></td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri">Keterangan</td>
                                            <td>: <u><?php echo $villa['remark_approve']?></u></td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri"></td>
                                            <td class="align_tengah" style="padding-top:20px;">Jakarta, <u><?php echo date('d M Y',strtotime($villa['approve_date'])) ?></u></td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri"></td>
                                            <td class="align_tengah">HR & GA DIVISION</td>
                                        </tr>
                                        <tr>
                                            <td class="label_kiri"></td>
                                            <td class="align_tengah ttd_pemohon"><u>Nod A. Rachman</u></td>
                                        </tr>

                                    </table>
                                </td>
        					</tr>
        				</table>
        			</td>
        		</tr>
        	</table>
            <!-- <div class="flex_50" style="float: left;padding: 0.5%;width: 49%;"><?php echo $title;?></div>
            <div class="flex_50" style="float: left;padding: 0.5%;width: 49%;"><?php echo $message;?></div> -->
            <!-- <p><?php echo $message;?></p> -->
        <!-- </div> -->
        
    </div>
    
</body>
<meta http-equiv="content-type" content="text/html;charset=UTF-8">
</html>