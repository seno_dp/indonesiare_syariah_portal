<?php 
    $image_field = ($controller_name == "article") ? 'thumbnail' : 'image';
?>
<?php if($qp->num_rows() > 0) {?>
    
    <?php foreach($qp->result_array() as $qval) {?>
        <div class="post-<?php echo $qval['id']?> post type-post">
            <h1 id="post-<?php echo $qval['id']?>">
                <a href="<?php echo site_url($controller_name.'/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" title="<?php echo $qval['title']?>">
                    <?php echo $qval['title']?>
                </a>
            </h1>
            <div class="entry">
                <?php if($qval[$image_field]) {?>
                <div class="thumb-arhive-book">
                    <a href="<?php echo site_url($controller_name.'/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" rel="bookmark">
                        <img width="180" height="235" src="<?php echo base_url()?>uploads/<?php echo GetThumb($qval[$image_field])?>" class="attachment-blog-image wp-post-image" alt="<?php echo $qval['title']?>"/>
                    </a>
                </div>
                
                <?php } ?>
                <div class="excerptarhive-book"><?php echo $qval['headline']?>
                    ... <a href="<?php echo site_url($controller_name.'/detail/'.$qval['id'].'/'.url_title($qval['title']))?>" rel="tag">More</a>
                </div>
                <div class="postmetabook">
                    <!-- KRITERION META -->
                    <div class="meta">
                        <?php if($controller_name == 'kliping') { ?>
                            <span class="time">Tanggal koran <?php echo date('M d, Y',strtotime($qval['date_koran'])) ?></span><br/>
                            <span class="categories">Koran <?php echo $qval['title_koran'] ?></span><br/>
                        <?php } ?>
                        <span class="time"><?php echo date('M d, Y',strtotime($qval['create_date']))?></span>
                        <em>by&nbsp;</em><?php echo GetUserName('kg_admin','name',$qval['create_user_id'])?> | Type: <?php echo ExplodeNameFile($qval['uploaded_file'])['ext']?>
                    </div>
                    <?php if(($qval['uploaded_file']) || ($qval['uploaded_file'] != 0)){?>
                        <span class="categories">Download File
                            <a href="<?php echo site_url($controller_name.'/download/'.$qval['id'].'/'.url_title($qval['title']))?>" title="Download <?php echo $qval['title']?>" rel="category tag"><?php echo $qval['title']?></a>
                        </span>
                        <br/>
                    <?php } ?>
                    Tags:
                    <?php echo explodetags($qval['tags']) ?>
                </div>
                <div class="clear"></div>
            </div>
            <br/>
            
        </div>
        <?php } ?>
    
    <?php }else{ ?>
    <div class="post-1 post type-post">
        <h1 id="post-1">
            <a href="#" title="Lipsum">
                Title Here</a>
        </h1>
        <div class="entry">
            <div class="thumb-arhive">
                <a href="#" rel="bookmark">
                    <img width="263" height="145" src="<?php echo base_url()?>assets/theme/images/no_image.jpg" class="attachment-blog-image wp-post-image" alt="Fashion photography by Jenya Kushnir" width="150px" />
                </a>
            </div>
            <div class="excerptarhive">Lorem ipsum dolor sit amet, consec tetur adipiscing elit. Quisque eu enim imperdiet, malesuada sapien ac, tempor magna. Cras bibendum adipiscing arcu, id bibendum lorem mattis et. Nulla sed tempus enim. Proin egestas nisi ultricies auctor viverra. Nunc a diam sit amet elit venenatis lacinia sed vel diam. Phasellus vestibulum magna lectus, sit amet accumsan risus ornare eu. Integer in urna sed velit sollicitudin sagittis ut ut arcu. Nam vitae fermentum mauris. Morbi arcu felis, congue vel odio vel, congue laoreet sapien. Maecenas vel pretium magna. Ut...
            </div>
            <div class="clear"></div>
        </div>
        <br/>

    </div>
    <?php } ?>