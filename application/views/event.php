<!-- REINDO DETAIL EVENT PAGE -->
<section class="main_news_wrapper cc_single_post_wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="cc_single_post">
                    <div class="sp_details">
                        <?php if($event->num_rows() > 0) {?>
                        <?php $val = $event->row_array();?>
                        <div class="" id="">
                            <!-- REINDO BREAD CRUMB -->
                            <div class="breadcum_c_left">
                                <a href="<?php echo site_url('home')?>">Home</a> / 
                                <a href="<?php echo site_url('event')?>">Event</a> /
                                <?php echo $val['title']?>
                            </div>
                            <h1>
                                <?php echo $val['title']?>
                            </h1>
                            <?php if($val['image']) {?>
                            <?php if($val['is_thumbnail'] == 1) {?>
                            <div class="image-thumb">
                                <img class="img-responsive" src="<?php echo base_url()?>uploads/<?php echo $val['image']?>"/>
                            </div>
                            <?php } ?>
                            <?php } ?>
                            <div class="post_text">
                                <?php echo $val['content']?>
                            </div>
                            <div class="post_meta">
                                <ul>
                                    <li>
                                        <a><i class="fa fa-clock-o"></i><?php echo date('M d, Y',strtotime($val['create_date']))?></a>
                                    </li>
                                    <li><a href="">by </a></li>             
                                    <li>
                                        <a href=""><i class="fa fa-user"></i><?php echo GetUserName('kg_admin','name',$val['create_user_id'])?></a>
                                    </li>                   
                                </ul>
                                <div class="social_tags">
                                    <div class="social_tags_left">
                                        <p>Tags :</p>
                                        <ul>
                                            <li><a href=""><?php echo explodetags($val['tags']) ?></a></li>
                                        </ul>
                                    </div>
                                    <div class="social_tags_right">
                                        <ul>
                                            <li class="facebook"><a class="fa fa-facebook" href=""></a></li>
                                            <li class="twitter"><a class="fa fa-twitter" href=""></a></li>
                                            <li class="google-plus"><a class="fa fa-google-plus" href=""></a></li>
                                            <li class="linkedin"><a class="fa fa-linkedin" href=""></a></li>
                                        </ul>
                                    </div>
                                </div>                                  
                            </div>
                        </div>
                        <?php }?>
                        <?php if($rel_link->num_rows() > 0) {?>
                        <!-- RELATED POSTS -->
                        <div class="row">
                            <div class="follow_us_side">
                                <h2>Related Posts</h2>
                            </div>                            
                        </div>
                        <div style="clear:both"></div>
                        <!-- START RELATED ARTICLE LINK -->
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 related-column">
                                <ul>
                                    <?php foreach($rel_link->result_array() as $rval) { ?>
                                    <li>
                                        <?php if($rval['image']) {?>
                                        <a href="<?php echo site_url('event/detail/'.$rval['id'].'/'.url_title($rval['title']))?>" rel="bookmark">
                                            <img class="img-responsive" src="<?php echo base_url()?>uploads/<?php echo getThumb($rval['image'])?>" />
                                        </a>
                                        <?php } ?>
                                        <div class="title-related">
                                            <a href="<?php echo site_url('event/detail/'.$rval['id'].'/'.url_title($rval['title']))?>" rel="bookmark"><?php echo $rval['title']?>
                                            </a>
                                        </div>
                                    </li>
                                    <?php } ?>
                                </ul>
                            </div>                            
                        </div>
                        <!-- END RELATED ARTICLE LINK -->
                        <?php } ?>                      
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="home_sidebar">
                    <div class="follow_us_side">
                        <?php echo $this->load->view('detail_sidebar')?>   
                    </div>
                </div>              
            </div>
        </div>
    </div>  
</section>
<!-- REINDO DETAIL EVENT PAGE -->