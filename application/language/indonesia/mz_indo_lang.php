<?php
//Message
$lang['msg_sukses'] = "Berhasil";
$lang['msg_err_img_size'] = "File image melebihi ukuran maksimum yang diizinkan";
$lang['msg_err_file_size'] = "File upload melebihi ukuran maksimum yang diizinkan";

//Button
$lang['add'] = "Tambah";
$lang['edit'] = "Simpan dan lanjutkan";
$lang['delete'] = "Hapus";
$lang['back'] = "Kembali";
$lang['delete_file'] = "Hapus File";
$lang['delete_confirm'] = "Yakin ingin hapus data ini";//Are you sure to delete this record
$lang['page'] = "Halaman";
$lang['save'] = "Simpan";

//Title
$lang['search'] = "Cari";
$lang['list'] = "";
$lang['setting'] = "Mengatur";
$lang['log_activities'] = "Aktifitas Admin";

$lang['sa_lang'] = "Mengelola Bahasa";
$lang['sa_menu'] = "Mengelola Menu";
$lang['sa_config'] = "Mengelola Config";

$lang['home'] = "Beranda";
$lang['admin'] = "Admin";
$lang['news'] = "Berita";
$lang['news_category'] = "Kategori Berita";
$lang['menu_admin'] = "Menu Admin";
$lang['list_contents'] = "Contents";
$lang['contents'] = "Contents";