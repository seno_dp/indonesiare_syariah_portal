<div class="grid_24">
	<ul id="menu">
		<li><a href="<?php echo site_url('home')?>"><?php echo strtoupper(lang('home'));?></a></li>
		<li><a href="<?php echo site_url('content/page/9/'.url_title(lang("forest_carbon")))?>"><?php echo strtoupper(lang('reddplus'));?></a>
			<div class="dropdown_1column align_right">
                <div class="colmenu_1">
                    <ul class="simple">
                        <li><a href="<?php echo site_url('content/page/1/'.url_title(lang("forests_climate_change")))?>"><?php echo lang('forests_climate_change');?></a></li>
						<li><a href="<?php echo site_url('content/page/3/'.url_title(lang("what_is_redd")))?>"><?php echo lang('what_is_redd');?></a></li>						
						<li><a href="<?php echo site_url('content/page/2/'.url_title(lang("international_development")))?>"><?php echo lang('international_development');?></a></li>
						<li><a href="<?php echo site_url('content/page/17/'.url_title(lang("reddplus_in_indonesia")))?>"><?php echo lang('reddplus_in_indonesia');?></a></li>
						<li><a href="<?php echo site_url('content/page/9/'.url_title(lang("carbon_rich_peatlands")))?>"><?php echo lang('carbon_rich_peatlands');?></a></li>
                    </ul>
                </div>     
			</div>
		</li>
		<li><a href="<?php echo site_url('content/page/42/'.url_title(lang("about_us")))?>"><?php echo strtoupper(lang('about_us'));?></a>
			<div class="dropdown_1column align_right">
                <div class="colmenu_1">
                    <ul class="simple">
                        <li><a href="<?php echo site_url('content/page/18/'.url_title(lang("international_forest_carbon_initiative")))?>"><?php echo lang('international_forest_carbon_initiative');?></a></li>
						<li><a href="<?php echo site_url('content/page/19/'.url_title(lang("indonesia_australia_fcp")))?>"><?php echo lang('indonesia_australia_fcp');?></a></li>
						<li><a href="<?php echo site_url('content/page/22/'.url_title(lang("partners")))?>"><?php echo lang('partners');?></a></li>
						<li><a href="<?php echo site_url('content/page/23/'.url_title(lang("staff_profiles")))?>"><?php echo lang('staff_profiles');?></a></li>
                    	<li><a href="<?php echo site_url('content/page/25/'.url_title(lang("opportunities")))?>"><?php echo lang('opportunities');?></a></li>                    	
                    </ul>
                </div>     
			</div>
			
		</li>
		<li><a href="<?php echo site_url('content/page/59/'.url_title(lang("what_we_do")))?>"><?php echo strtoupper(lang('what_we_do'));?></a>
			<div class="dropdown_5columns"><!-- Begin 5 columns container -->
        
	            <div class="colmenu_1">
	                <h3><a href="<?php echo site_url('content/page/44/'.url_title(lang("kfcp")))?>"><?php echo strtoupper(lang('kfcp'));?></a></h3>
	                <ul>
	                    <li><a href="<?php echo site_url('content/page/44/'.url_title(lang("kfcp")))?>"><?php echo strtoupper(lang('kfcp'));?></a></li>
	                </ul>
	            </div>
	            
	            <div class="colmenu_1">
	                <h3><a href="<?php echo site_url('content/page/28/'.url_title(lang("activities_on_the_ground")))?>"><?php echo lang('activities_on_the_ground');?></a></h3>
	                <!--h3><?php echo lang('activities_on_the_ground');?></h3-->
	                <ul>
	                    <li><a href="<?php echo site_url('content/page/45/'.url_title(lang("community_engagement")))?>"><?php echo lang('community_engagement');?></a></li>
	                    <li><a href="<?php echo site_url('content/page/30/'.url_title(lang("alternatively_livelihoods")))?>"><?php echo lang('alternatively_livelihoods');?></a></li>
	                    <li><a href="<?php echo site_url('content/page/31/'.url_title(lang("rewetting_peatlands")))?>"><?php echo lang('rewetting_peatlands');?></a></li>
	                    <li><a href="<?php echo site_url('content/page/32/'.url_title(lang("institutional_support")))?>"><?php echo lang('institutional_support');?></a></li>
	                    <li><a href="<?php echo site_url('content/page/33/'.url_title(lang("fire_management")))?>"><?php echo lang('fire_management');?></a></li>
	                    <li><a href="<?php echo site_url('content/page/34/'.url_title(lang("nurseries_reforestation")))?>"><?php echo lang('nurseries_reforestation');?></a></li>
	                </ul>
	            </div>
	            
	            <div class="colmenu_1">
	                <h3><a href="<?php echo site_url('content/page/35/'.url_title(lang("research")))?>"><?php echo lang('research');?></a></h3>
	                <ul>
	                    <li><a href="<?php echo site_url('content/page/36/'.url_title(lang("hydrology")))?>"><?php echo lang('hydrology');?></a></li>
	                    <li><a href="<?php echo site_url('content/page/41/'.url_title(lang("vegetation_and_biomass")))?>"><?php echo lang('vegetation_and_biomass');?></a></li>
	                    <li><a href="<?php echo site_url('content/page/37/'.url_title(lang("emission_reductions")))?>"><?php echo lang('emission_reductions');?></a></li>
	                    <li><a href="<?php echo site_url('content/page/38/'.url_title(lang("reference_emission_levels")))?>"><?php echo lang('reference_emission_levels');?></a></li>
	                </ul>
	            </div>
	            
	            <div class="colmenu_1">
	                <h3><a href="<?php echo site_url('content/page/46/'.url_title(lang("cross_cutting")))?>"><?php echo lang('cross_cutting');?></a></h3>
	                <ul>
	                    <li><a href="<?php echo site_url('content/page/47/'.url_title(lang("governance")))?>"><?php echo lang('governance');?></a></li>
	                    <li><a href="<?php echo site_url('content/page/48/'.url_title(lang("gender")))?>"><?php echo lang('gender');?></a></li>
	                    <li><a href="<?php echo site_url('content/page/49/'.url_title(lang("safeguards")))?>"><?php echo lang('safeguards');?></a></li>
	                    <li><a href="<?php echo site_url('content/page/50/'.url_title(lang("indigenous_rights")))?>"><?php echo lang('indigenous_rights');?></a></li>
	                    <li><a href="<?php echo site_url('content/page/51/'.url_title(lang("conflict_and_grievance_mechanisms")))?>"><?php echo lang('conflict_and_grievance_mechanisms');?></a></li>
	                    <li><a href="<?php echo site_url('content/page/52/'.url_title(lang("risk_management")))?>"><?php echo lang('risk_management');?></a></li>
	                    <li><a href="<?php echo site_url('content/page/53/'.url_title(lang("communications")))?>"><?php echo lang('communications');?></a></li>
	                </ul></div>
	                <div class="colmenu_1">
	                <h3><a href="<?php echo site_url('content/page/54/'.url_title(lang("incas")))?>"><?php echo strtoupper(lang('incas'));?></a></h3>
	                <ul>
	                    <li><a href="<?php echo site_url('content/page/55/'.url_title(lang("overview")))?>"><?php echo lang('overview');?></a></li>
	                    <li><a href="<?php echo site_url('content/page/56/'.url_title(lang("assesing_forest_cover_change")))?>"><?php echo lang('assesing_forest_cover_change');?></a></li>
	                    <li><a href="<?php echo site_url('content/page/57/'.url_title(lang("ground_data")))?>"><?php echo lang('ground_data');?></a></li>
	                    <li><a href="<?php echo site_url('content/page/58/'.url_title(lang("carbon_quantification")))?>"><?php echo lang('carbon_quantification');?></a></li>
	                </ul></div>
	            
	            
       		</div>
		</li>
		<li><a href="<?php echo site_url('publication')?>"><?php echo strtoupper(lang('publication'));?></a></li>
		<li><a href="<?php echo site_url('media')?>"><?php echo strtoupper(lang('media'));?></a>
		<div class="dropdown_1column align_right">
            <div class="colmenu_1">
                <ul class="simple">
                    <li><a href="<?php echo site_url('news')?>"><?php echo lang('news');?></a></li>
					<li><a href="<?php echo site_url('press_release')?>"><?php echo lang('press_release');?></a></li>
					<li><a href="<?php echo site_url('content/page/10/'.url_title(lang("contact_us")))?>"><?php echo lang('contact_us');?></a></li>
				</ul>
            </div>     
		</div>
		</li>
	</ul>
</div>

<div class="clear"></div>