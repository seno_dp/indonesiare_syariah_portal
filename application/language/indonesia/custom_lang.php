<?php
/*language switch*/
$lang['lang.english'] = "English";
$lang['lang.bahasa'] = "Bahasa";

/*main menu*/
$lang['mmenu.home'] = "Beranda";
$lang['mmenu.about'] = "Tentang Kami";
$lang['mmenu.news'] = "Berita";
$lang['mmenu.researchpublication'] = "Riset & Publikasi";
$lang['mmenu.event'] = "Event";
$lang['mmenu.gallery'] = "Gallery";
$lang['mmenu.link'] = "Tautan";
$lang['mmenu.carreer'] = "Karir";
$lang['mmenu.contact'] = "Hubungi Kami";
$lang['mmenu.profile'] = "Profil Organisasi";
$lang['mmenu.visi'] = "Visi & Misi";
$lang['mmenu.management'] = "Management";
$lang['mmenu.focus'] = "Fokus Kami";
$lang['mmenu.resource'] = "Sumber Kami";
$lang['mmenu.founder'] = "Dewan";
$lang['mmenu.staff'] = "Staff";
$lang['mmenu.advisory'] = "Penasehat";
$lang['mmenu.research'] = "Asosiasi Peneliti";
$lang['mmenu.funding'] = "Sumber Pendanaan";
$lang['mmenu.report'] = "Laporan Tahunan";
$lang['mmenu.library'] = "Perpustakaan";
$lang['mmenu.news.update'] = "Terkini";
$lang['mmenu.news.choosen'] = "Pilihan";
$lang['mmenu.news.release'] = "Press Release";
$lang['mmenu.research.book'] = "Buku";
$lang['mmenu.research.paper'] = "Paper";
$lang['mmenu.research.report'] = "Laporan";
$lang['mmenu.research.multimedia'] = "Multimedia";
$lang['mmenu.book.prakarsa'] = "Produksi Prakarsa";
$lang['mmenu.book.reference'] = "Referensi";
$lang['mmenu.paper.policy'] = "Kebijakan";
$lang['mmenu.paper.brief'] = "Rangkuman Kebijakan";
$lang['mmenu.paper.sheet'] = "Lembar Fakta";
$lang['mmenu.paper.quote'] = "Kutipan";
$lang['mmenu.event.discussion'] = "Diskusi";
$lang['mmenu.discussion.public'] = "Publik";
$lang['mmenu.discussion.limited'] = "Terbatas";
$lang['mmenu.event.training'] = "Pelatihan";
$lang['mmenu.event.conference'] = "Konferensi";
$lang['mmenu.event.workshop'] = "Workshop";
$lang['mmenu.event.other'] = "Lain-lain";
$lang['mmenu.other.upcoming'] = "Akan datang";
$lang['mmenu.other.internal'] = "Internal";
$lang['mmenu.gallery.photo'] = "Foto";
$lang['mmenu.gallery.video'] = "Video";

/*tab slider*/
$lang['tab.poverty'] = "Kemiskinan & Ketidaksamaan";
$lang['tab.fiscal'] = "Kebijakan Fiskal";
$lang['tab.welfare'] = "Kebijakan kesejahteraan";
$lang['tab.sustainable'] = "Pembangunan Berkelanjutan";
$lang['tab.video'] = "Video";
$lang['tab.photo'] = "Foto";

/*home title bar*/
$lang['titbar.about'] = "Tentang Prakarsa";
$lang['titbar.news'] = "Berita Terbaru";
$lang['titbar.publications'] = "Publikasi Terbaru";
$lang['titbar.calendar'] = "Kalender Event";
$lang['titbar.followus'] = "Ikuti Kami";
$lang['titbar.subscribe'] = "Daftar Newsletter";
$lang['titbar.subscribedetail'] = "Bergabung dengan milis kami hari ini. dan dapatkan pengumuman berikutnya";

$lang['readmore'] = "Selengkapnya...";
$lang['viewonline'] = "Lihat Online";
$lang['download'] = "Unduh";
$lang['subscribe'] = "Daftar";
$lang['postedon'] = "Diposting pada";
$lang['views'] = "Dilihat";
$lang['related_link'] = "Tautan terkait";
$lang['link_gallery'] = "Tautan Gallery";
$lang['category_photo'] = "Kategori";
$lang['email_address'] = "Alamat email";
$lang['email_newsletter_exist'] = "Email sudah terdaftar";
$lang['signup_newsletter'] = "Berhasil! Email terkirim, terima kasih";
$lang['sending'] = "Mengirim...";
$lang['pages'] = "Halaman";
$lang['contact_name'] = "Nama";
$lang['contact_email'] = "Email";
$lang['contact_to'] = "Penerima";
$lang['contact_subject'] = "Subjek";
$lang['contact_message'] = "Pesan";
$lang['send'] = "Kirim";
$lang['reset'] = "Hapus";

$lang['about.gender'] = "Saya seorang laki-laki";
?>