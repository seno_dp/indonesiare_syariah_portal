<?php
/*language switch*/
$lang['lang.english'] = "English";
$lang['lang.bahasa'] = "Bahasa Indonesia";

/*main menu*/
$lang['mmenu.home'] = "Home";
$lang['mmenu.about'] = "About Us";
$lang['mmenu.news'] = "News";
$lang['mmenu.researchpublication'] = "Research & Publication";
$lang['mmenu.event'] = "Event";
$lang['mmenu.gallery'] = "Gallery";
$lang['mmenu.link'] = "Link";
$lang['mmenu.carreer'] = "Carreer";
$lang['mmenu.contact'] = "Contact Us";
$lang['mmenu.profile'] = "Profile Organization";
$lang['mmenu.visi'] = "Vision & Mision";
$lang['mmenu.management'] = "Management";
$lang['mmenu.focus'] = "Our Focus";
$lang['mmenu.resource'] = "Our Resources";
$lang['mmenu.founder'] = "Board";
$lang['mmenu.staff'] = "Staff";
$lang['mmenu.advisory'] = "Advisory";
$lang['mmenu.research'] = "Expert and Research Associate";
$lang['mmenu.funding'] = "Funding Resources";
$lang['mmenu.report'] = "Annual Report";
$lang['mmenu.library'] = "Library";
$lang['mmenu.news.update'] = "Updates";
$lang['mmenu.news.choosen'] = "Choice";
$lang['mmenu.news.release'] = "Press Release";
$lang['mmenu.research.book'] = "Books";
$lang['mmenu.research.paper'] = "Papers";
$lang['mmenu.research.report'] = "Reports";
$lang['mmenu.research.multimedia'] = "Multimedia";
$lang['mmenu.book.prakarsa'] = "Prakarsa Production";
$lang['mmenu.book.reference'] = "Reference";
$lang['mmenu.paper.policy'] = "Policy Paper";
$lang['mmenu.paper.brief'] = "Policy Brief";
$lang['mmenu.paper.sheet'] = "Fact Sheet";
$lang['mmenu.paper.quote'] = "Quote";
$lang['mmenu.event.discussion'] = "Discussion";
$lang['mmenu.discussion.public'] = "Public";
$lang['mmenu.discussion.limited'] = "Limited";
$lang['mmenu.event.training'] = "Training";
$lang['mmenu.event.conference'] = "Conference";
$lang['mmenu.event.workshop'] = "Workshop";
$lang['mmenu.event.other'] = "Others";
$lang['mmenu.other.upcoming'] = "Upcoming Events";
$lang['mmenu.other.internal'] = "Internal Gathering";
$lang['mmenu.gallery.photo'] = "Photo";
$lang['mmenu.gallery.video'] = "Video";

/*tab slider*/
$lang['tab.poverty'] = "Poverty & Inequality";
$lang['tab.fiscal'] = "Fiscal Policies";
$lang['tab.welfare'] = "Welfare Policies";
$lang['tab.sustainable'] = "Sustainable Development";
$lang['tab.video'] = "Video";
$lang['tab.photo'] = "Photo";

/*home title bar*/
$lang['titbar.about'] = "About Prakarsa";
$lang['titbar.news'] = "News Update";
$lang['titbar.publications'] = "Publications Update";
$lang['titbar.calendar'] = "Event Calendar";
$lang['titbar.followus'] = "Follow Us";
$lang['titbar.subscribe'] = "Subscribe News letter";
$lang['titbar.subscribedetail'] = "Join our mailing list today, and be ready for our next action alert";

$lang['readmore'] = "Read more...";
$lang['viewonline'] = "View Online";
$lang['download'] = "Download";
$lang['subscribe'] = "Subscribe";
$lang['postedon'] = "Posted on";
$lang['views'] = "Views";
$lang['related_link'] = "Related Link";
$lang['link_gallery'] = "Link Gallery";
$lang['category_photo'] = "Category";
$lang['email_address'] = "Email address";
$lang['email_newsletter_exist'] = "Email registered";
$lang['signup_newsletter'] = "Success! Email sent, thanks";
$lang['sending'] = "sending...";
$lang['pages'] = "Pages";
$lang['contact_name'] = "Name";
$lang['contact_email'] = "Email";
$lang['contact_to'] = "To";
$lang['contact_subject'] = "Subject";
$lang['contact_message'] = "Message";
$lang['send'] = "Send";
$lang['reset'] = "Reset";

$lang['about.gender'] = "I'm a man";
?>