<?php

//Message

$lang['msg_sukses'] = "Success";

$lang['msg_err_img_size'] = "The image file exceeds the maximum size allowed";

$lang['msg_err_file_size'] = "The uploaded file exceeds the maximum size allowed";



//Button

$lang['add'] = "Add";

$lang['edit'] = "Save and continue editing";

$lang['delete'] = "Delete";

$lang['back'] = "Back";

$lang['delete_file'] = "Delete File";

$lang['delete_confirm'] = "Are you sure to delete this record";

$lang['page'] = "Page";

$lang['save'] = "Save";



//Title

$lang['search'] = "Search";

$lang['list'] = "List";

$lang['setting'] = "Setting";

$lang['log_activities'] = "Admin Activities";



$lang['sa_lang'] = "Manage Language";

$lang['sa_menu'] = "Manage Menu";

$lang['sa_config'] = "Manage Config";



$lang['home'] = "Home";

$lang['admin'] = "Admin";

$lang['admin_auth'] = "Authorization";

$lang['news'] = "News";

$lang['news_category'] = "News Category";

$lang['menu_admin'] = "Menu Backend";

$lang['list_contents'] = "Contents";

$lang['contents'] = "Contents";

$lang['sponsorship'] = "Sponsorship";