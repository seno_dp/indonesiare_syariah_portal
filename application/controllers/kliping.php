<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class kliping extends CI_Controller {



	function index()

	{

		$this->listing();

	}



	function listing(){

		if((strpos($this->input->ip_address(),'127.0.0.1') !== false) or (strpos($this->input->ip_address(),'192.168.') !== false) or ($this->session->userdata('user_id_sess'))) 

		{

			$data = GetHeaderFooter(1);

			$data['main_content'] = 'knowledge';

			$data['menu_title'] = 'Kliping Koran';

			$data['controller_name'] = 'kliping';



			$GetAllConfig = $this->model_admin_all->GetAllConfig('kg_'.$data['controller_name']);

			if($GetAllConfig->num_rows() > 0)

			{

				$r = $GetAllConfig->row_array();

				$per_page = $r['per_page'];

			}



			$uri_segment = 4;

			$awal = $this->uri->segment($uri_segment);

			

			$filter = array("kg_kliping.id_lang"=>"where/".GetIdLang(),"kg_kliping.is_publish"=>"where/Publish","kg_kliping.id"=>"order/desc","limit"=> $awal."/".$per_page);

			//$data['qp'] = GetAll('kg_kliping',$filter);

			$data['qp'] = GetJoin('kg_kliping','kg_koran','kg_kliping.id_koran = kg_koran.id and kg_kliping.id_lang = kg_koran.id_lang','left','kg_kliping.*,kg_koran.title as title_koran',$filter);

			//die($this->db->last_query());



			$filter2 = array("kg_kliping.id_lang"=>"where/".GetIdLang(),"kg_kliping.is_publish"=>"where/Publish","kg_kliping.id"=>"order/desc");

			$q = GetJoin('kg_kliping','kg_koran','kg_kliping.id_koran = kg_koran.id and kg_kliping.id_lang = kg_koran.id_lang','left','kg_kliping.*,kg_koran.title as title_koran',$filter2);

			//$q = GetAll('kg_kliping',$filter2);



			$path_paging = site_url('kliping/listing');

			$pagination = Page($q->num_rows(),$per_page,$awal,$path_paging,$uri_segment);

			if(!$pagination) $pagination = "";

			$data['pagination'] = $pagination;



			$this->load->view('layout',$data);

		}else{

			if($this->session->userdata('user_id_sess'))

			{

				ciredirect('home');

			}else{

				ciredirect('member/login');

			}

		}

	}



	function detail($id=1)

	{

		if((strpos($this->input->ip_address(),'127.0.0.1') !== false) or (strpos($this->input->ip_address(),'192.168.') !== false) or ($this->session->userdata('user_id_sess'))) 

		{

			$data = GetHeaderFooter(1);

			

			$data['main_content'] = 'knowledge_detail';

			$data['controller_name'] = $controller_name = 'kliping';

			

			$filter = array(

					"kg_kliping.is_publish"=>"where/publish",

					"kg_kliping.id"=>"where/".$id,

					"kg_kliping.id_lang"=>"where/".GetIdLang()

			);



			//$data['knowledge'] = $knowledge = GetAll('kg_'.$controller_name,$filter);

			$data['knowledge'] = $knowledge = GetJoin('kg_'.$controller_name,'kg_koran','kg_kliping.id_koran = kg_koran.id and kg_kliping.id_lang = kg_koran.id_lang','left','kg_kliping.*,kg_koran.title as title_koran',$filter);



			if($knowledge->num_rows() > 0){

				$v = $data['knowledge']->row_array();

				

				/*$views = $r['views'];

				$this->updateviews($id,'kg_'.$controller_name,$views);*/



				$i = 0;

				$files = "";

				$tags = explode(',',$v['tags']);

				$numItems = count($tags);

				$result="";

				$files = "SELECT * FROM kg_".$controller_name." where id_lang = ".GetIdLang()." and id <> ".$v['id']. " and (";

				foreach($tags as $tag=>$val){

					if($val != ""){

						$files .= " tags like '%".$val."%'";

						if(++$i === $numItems){

					    	$files .= "";

						}else{

							$files .= " or ";

						}

					}else

					{

						$files .= " tags like '0'";

					}

				}

				$files .= ") limit 0,4";

			

				$this->session->set_userdata('detail_uri',current_url());

				$data['rel_link'] = $this->db->query($files);

			}else{

				ciredirect(site_url('error_404'));

			}

				

			$this->load->view('layout',$data);

		}else{

			if($this->session->userdata('user_id_sess'))

			{

				ciredirect('home');

			}else{

				ciredirect('member/login');

			}

		}

	}



	function download($id=NULL)

	{

		$data['controller_name'] = $controller_name = 'kliping';

		//echo $this->session->userdata('publication_uri');

		$filter = array("id"=>"where/".$id,"id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/Publish");

		$data['list'] = $list = GetAll("kg_".$controller_name,$filter);



		if($list->num_rows() > 0){

			//die('ada file');

			$r = $list->row_array();

			$downloads = $r['download'];

			$this->updatedownloads($id,'kg_'.$controller_name,$downloads);

			(strlen($r['uploaded_file']) > 0) ? ciredirect(base_url().'uploads/'.$r['uploaded_file']) : ciredirect($this->session->userdata('detail_uri'));

			

		}



	}



	function updatedownloads($id,$table,$views){

		$this->load->helper('date');

		$views_curr = $views + 1;

		$this->db->where('id', $id);

		$this->db->where('id_lang', GetIdLang());

		$this->db->update($table,array('download' => $views_curr)); 



		/*

		$data = array(

		   'id_publication' => $id,

		   'date_download' => date('Y-m-d h:i:s',now())

		);



		$this->db->insert('kg_pub_download', $data); 

		*/

	}



	function updateviews($id,$table,$views){

		$this->load->helper('date');

		$views_curr = $views + 1;

		$this->db->where('id', $id);

		$this->db->where('id_lang', GetIdLang());

		$this->db->update($table,array('views' => $views_curr)); 

	}



	function search()

	{

		

		// echo '<script>alert("Komentar anda akan dimoderasi terlebih dahulu")</script>';



			$data = GetHeaderFooter(1);

			$data['main_content'] = 'knowledge';

			$data['menu_title'] = 'Kliping Koran';

			$data['controller_name'] = 'kliping';



			$GetAllConfig = $this->model_admin_all->GetAllConfig('kg_'.$data['controller_name']);

			if($GetAllConfig->num_rows() > 0)

			{

				$r = $GetAllConfig->row_array();

				$per_page = $r['per_page'];

			}



			$uri_segment = 4;

			$awal = $this->uri->segment($uri_segment);

			

			$this->db->where("kg_kliping.is_publish", "publish");



			if($this->input->post('id_koran')){

				$this->db->where("kg_kliping.id_koran", $this->input->post('id_koran'));

			}



			if($this->input->post('date_kliping')){

				$this->db->or_where("kg_kliping.date_kliping", $this->input->post('date_kliping'));

			}



			if($this->input->post('text_kliping')){

				$this->db->or_like("kg_kliping.content", $this->input->post('text_kliping'));

			}



			$this->db->where("kg_kliping.id_lang",GetIdLang());



			$this->db->order_by("kg_kliping.id","desc");



			$this->db->select('kg_kliping.*,kg_koran.title as title_koran');

			$this->db->from('kg_kliping');

			$this->db->join('kg_koran', 'kg_kliping.id_koran = kg_koran.id and kg_kliping.id_lang = kg_koran.id_lang');



			$data['qp'] = $this->db->get();

			die($this->db->last_query());



		



			$this->load->view('search_kliping_result',$data);



			

				

	}







}



/* End of file page.php */

/* Location: ./application/controllers/page.php */