<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Page extends CI_Controller {



	function index()

	{

		ciredirect(site_url('page/detail/1'));

	}



	function detail($id=1)

	{

		if((strpos($this->input->ip_address(),'127.0.0.1') !== false) or (strpos($this->input->ip_address(),'192.168.') !== false) or ($this->session->userdata('user_id_sess'))) 

		{

			$data = GetHeaderFooter(1);

			

			$data['main_content'] = 'content';

			

			$filter = array(

					"is_publish"=>"where/publish",

					"id"=>"where/".$id,

					"id_lang"=>"where/".GetIdLang()

			);



			$data['content'] = $content = GetAll('kg_contents',$filter);

			if($content->num_rows() > 0){

				$v = $data['content']->row_array();

				$data['title_page'] = $v['title'];

			}else{

				ciredirect(site_url('error_404'));

			}

				

			$this->load->view('layout',$data);

		}else{

			if($this->session->userdata('user_id_sess'))

			{

				ciredirect('home');

			}else{

				ciredirect('member/login');

			}

		}

	}







}



/* End of file page.php */

/* Location: ./application/controllers/page.php */