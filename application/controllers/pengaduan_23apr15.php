<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Pengaduan extends CI_Controller {



	function index()

	{

		$this->main();

	}



	function main(){

		if((strpos($this->input->ip_address(),'127.0.0.1') !== false) or (strpos($this->input->ip_address(),'192.168.') !== false) or ($this->session->userdata('user_id_sess'))) 

		{
			$this->load->helper('ag_helper');
			
			$data = GetHeaderFooter(1);

			$data['main_content'] = 'pengaduan';

			$data['menu_title'] = 'Pengaduan';

			$data['controller_name'] = 'Pengaduan';
			
			$data['opt_ref_perihal'] = getPerihal();
			
			$data['opt_divisi'] = getDivisi();
			
			$data['opt_bag_pengaduan'] = getBagpengaduan();
			
			$data['info'] = '';
			
			$this->form_validation->set_rules('perihal',' ', 'required');
			$this->form_validation->set_rules('bag_pengaduan',' ', 'required');
			$this->form_validation->set_rules('isi_aduan',' ', 'required');
			$this->form_validation->set_error_delimiters(' <span style="color:#c00;font-size:11px;">', '</span>');
					
			if ($this->form_validation->run() == false) {
				$data['info'] = '';
				$this->load->view('layout', $data); 
			}
			else {
				$date_time = gmdate('Y-m-d H:i:s', time()+60*60*7);
				$nama = $this->input->post('name');
				if ($this->input->post('name') == '') { $nama = 'Anonim'; }
				$field = array (
								'name'=>$nama,
								'divisi'=>$this->input->post('divisi'),
								'ref_perihal'=>$this->input->post('perihal'),
								'bag_pengaduan'=>$this->input->post('bag_pengaduan'),
								'isi_pengaduan'=>$this->input->post('isi_aduan'),
								'create_user_id'=>$this->session->userdata('user_id_sess'),
								'is_publish'=>'Publish',
								'create_date'=>$date_time);
				$data['info'] = 'Terima kasih, pengaduan Anda telah kami terima :)';
				
				/*$process = array(
								 'tipe_alert' => 'alert alert-success',
								 'alert'      => 'Proses Input Data !&nbsp;',
								 'info_alert' => 'Input Data Berhasil',
								 'link_back'     => site_url('sumber_dana'));				 
				$data['message'] = $this->load->view('message',$process, TRUE);*/
				$this->db->insert('kg_pengaduan', $field);
				$this->load->view('layout',$data);
			}

		}else{

			if($this->session->userdata('user_id_sess'))

			{

				ciredirect('home');

			}else{

				ciredirect('member/login');

			}

		}

	}
	
	


}



/* End of file page.php */

/* Location: ./application/controllers/page.php */