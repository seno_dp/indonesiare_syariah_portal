<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*************************************
  * Created : June 2013
  * Update  : June 2013
  * Creator : Andi Galuh S
  * Email   : andi@komunigrafik.com
*************************************/

class Member extends CI_Controller  {

	public function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$this->register();
	}

	function register()
	{
		$this->form_validation->set_rules('name', 'Name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$this->form_validation->set_rules('re_password', 'Retype Password', 'required|matches[password]');

		if ($this->form_validation->run() == TRUE)
		{
			$users = array(	
				'title'	=> $this->input->post('name'),
				'email'	=> $this->input->post('email'),
				'password'	=> md5($this->input->post('password')),
				'create_date' => date('Y-m-d h:i:s',now())
			);

			$this->db->insert('kg_member',$users); 

			$this->phpbb->user_add($this->input->post('email'),$this->input->post('email'),$this->input->post('password'));

			$this->session->set_flashdata('message', '<p>You have successfully created account.</p><p><a href="'.site_url('member/login').'">Login</a></p>');
			ciredirect('member');
		}
		else
		{
			$this->load->view('register');
		}		
	}

	function login()
	{
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'required');

		if ($this->form_validation->run() == TRUE)
		{
			$email = $this->input->post('email');
			$password = md5($this->input->post('password'));

			if ($this->all_model->check_user('kg_member', $email, $password) == TRUE)
			{
				$qmember = $this->all_model->GetRow('kg_member',array('email'=>"where/".$email,'password'=>"where/".$password));
				$user_id = $qmember['id'];
				$user_name = $qmember['title'];
				$image = $qmember['image'];
				$data = array(
					'user_id_sess' => $user_id,
					'email_sess' => $email,
					'is_login_sess' => 1,
					'user_name_sess' => $user_name,
					'user_image_sess' => $image
				);

				$this->session->set_userdata($data);

				$updates = array('is_login' => 1 ,'last_login' => date('Y-m-d H:i:s',now()));

				$this->all_model->update('kg_member',$this->session->userdata('user_id_sess'), $updates);

				$this->phpbb->user_login($this->input->post('email'),$this->input->post('password'));
				ciredirect('blog/my_profile');
			}
			else
			{
				$flashmessage = $this->session->set_flashdata('message', 'Oops... the e-mail address and/or password you entered are not listed in our system.');
				ciredirect('member/login');
			}
		}
		else
		{
			$this->load->view('login');
		}		
	}


	function logout()
	{
		$user_id = $this->session->userdata('user_id_sess');
		$updates = array('is_login' => 0);
		$this->all_model->update('kg_member',$user_id, $updates);
		$this->session->sess_destroy();
		$this->phpbb->user_logout();

		ciredirect('home');
	}

	function profile()
	{
		if($this->session->userdata('user_id_sess'))
		{
			$data = GetHeaderFooter(1);
			$data['main_content'] = 'profile';
			$data['controller_name'] = $controller_name = 'member';
			$id = $this->session->userdata('user_id_sess');

			$filter = array(
					"id"=>"where/".$id,
			);

			$data['member'] = $member = GetAll('kg_'.$controller_name,$filter);
			if($member->num_rows() > 0)
			{
				$v = $data['member']->row_array();
			}else{
				ciredirect(site_url('error_404'));
			}

			$this->load->view('layout',$data);
		}else{
			ciredirect(site_url('home'));
		}
	}

	function userprofile($id=0)
	{
		if($this->session->userdata('user_id_sess')){
			$data = GetHeaderFooter(1);
			$data['main_content'] = 'my_profile';
			$data['controller_name'] = $controller_name = 'member';

			$id = $this->uri->segment(4);

			$filter = array(
					"id"=>"where/".$id,
			);

			$data['member'] = $member = GetAll('kg_'.$controller_name,$filter);
			if($member->num_rows() > 0){
				$v = $data['member']->row_array();
			}else{
				die($id);
			}

			$GetAllConfig = $this->model_admin_all->GetAllConfig('kg_blog');

			if($GetAllConfig->num_rows() > 0)
			{
				$r = $GetAllConfig->row_array();
				$per_page = $r['per_page'];
			}

			$uri_segment = 4;
			$awal = $this->uri->segment($uri_segment);

			if($this->uri->segment(4) == $this->session->userdata('user_id_sess')) { 
				$filter = array("is_publish"=>"where/Publish","create_user_id"=>"where/".$this->session->userdata('user_id_sess'),"id"=>"order/desc");
			}else{
				$filter = array("is_publish"=>"where/Publish","create_user_id"=>"where/".$this->uri->segment(4),"id"=>"order/desc");
			}

			$data['qp'] = GetAll('kg_view_blog',$filter);

			$GetAllConfig = $this->model_admin_all->GetAllConfig('kg_member_activity');

			if($GetAllConfig->num_rows() > 0)
			{
				$r = $GetAllConfig->row_array();
				$per_page = $r['per_page'];
			}

			$uri_segment = 4;

			$awal = $this->uri->segment($uri_segment);

			if($this->uri->segment(4) == $this->session->userdata('user_id_sess')) { 
				$filterm = array("id_member"=>"where/".$this->session->userdata('user_id_sess'),"create_user_id"=>"where/".$this->session->userdata('user_id_sess'),"id"=>"order/desc","limit"=> "0/20");
			}else{
				$filterm = array("id_member"=>"where/".$this->uri->segment(4),"create_user_id"=>"where/".$this->uri->segment(4),"id"=>"order/desc","limit"=> "0/20");
			}

			$data['ma'] = GetAll('kg_member_activity',$filterm);

			$this->load->view('layout',$data);
		}else{
			ciredirect(site_url('home'));
		}
	}



	function edit()
	{
		$this->form_validation->set_rules('title', 'Name', 'required');
		$id = $this->input->post('id');
		if ($this->form_validation->run() == TRUE)
		{
			if($_FILES['attachedfilecover']['name'])
	    	{
	    		if($this->input->post('file_old_cover')){
		    		$file_old_cover = $this->input->post('file_old_cover');
		    		if(file_exists("./".$this->config->item('path_upload')."/".$file_old_cover)) {
				    	unlink("./".$this->config->item('path_upload')."/".$file_old_cover);
				    }

				    $thumbcover = getThumb($file_old_cover);
				    if(file_exists("./".$this->config->item('path_upload')."/".$thumbcover)) unlink("./".$this->config->item('path_upload')."/".$thumbcover);
				}

				$data['attachedfilecover'] = input_file('attachedfilecover');
			    if($data['attachedfilecover'] == "err_img_size")
		     	{
				    $this->session->set_flashdata("message", "ukuran gambar cover picture melebihi kapasitas");
				    ciredirect('member/profile');
		     	}
		     	else if($data['attachedfilecover'] == "err_file_size")
		     	{
				    $this->session->set_flashdata("message", "ukuran file cover picture melebihi kapasitas");
				    ciredirect('member/profile');
		     	}

			    $users['cover'] = $data['attachedfilecover'];

			    $act = array(
					'id_member'	=> $this->session->userdata('user_id_sess'),
					'title' => 'Update cover profile',
					'url' => '#',
					'activity' => 'update_cover',
					'create_date' => date("Y-m-d H:i:s"),
					'create_user_id'	=> $this->session->userdata('user_id_sess')
				);
				$this->db->insert('kg_member_activity', $act);
			}

	    	if($_FILES['attachedfile']['name'])
	    	{
	    		if($this->input->post('file_old')){
		    		$file_old = $this->input->post('file_old');
		    		if(file_exists("./".$this->config->item('path_upload')."/".$file_old)) {
				    	unlink("./".$this->config->item('path_upload')."/".$file_old);
				    }

				    $thumb = getThumb($file_old);

				    if(file_exists("./".$this->config->item('path_upload')."/".$thumb)) unlink("./".$this->config->item('path_upload')."/".$thumb);
				}

				$data['attachedfile'] = input_file('attachedfile');
			    if($data['attachedfile'] == "err_img_size")
		     	{
				    $this->session->set_flashdata("message", "ukuran gambar melebihi kapasitas");
				    ciredirect('member/profile');
		     	}
		     	else if($data['attachedfile'] == "err_file_size")
		     	{
				    $this->session->set_flashdata("message", "ukuran file melebihi kapasitas");
				    ciredirect('member/profile');
		     	}

				$users['image'] = $data['attachedfile'];

				$act = array(
					'id_member'	=> $this->session->userdata('user_id_sess'),
					'title' => 'Update gambar profile',
					'url' => '#',
					'activity' => 'update_avatar',
					'create_date' => date("Y-m-d H:i:s"),
					'create_user_id'	=> $this->session->userdata('user_id_sess')
				);
				$this->db->insert('kg_member_activity', $act);
			}

			$users['title'] = $this->input->post('title');
			$users['phone'] = $this->input->post('phone');
			$users['address'] = $this->input->post('address');
			$users['modify_date'] = date('Y-m-d h:i:s',now());
			$users['modify_user_id'] = $id;
			$this->db->where('id',$id);
			$this->db->update('kg_member',$users);

			$this->session->set_flashdata('message', '<p>You have successfully created account.</p>');

			ciredirect('member/profile');
		}
		else
		{
			$this->db->where('id',$id);
			$this->db->update('kg_member',$users); 
			$this->load->view('profile');
		}		
	}

	function del_image()
	{
		if($this->session->userdata('user_id_sess'))
		{
			$id = $this->input->post('id');
			$file_old = $this->input->post('file_old');

			$GetThumb = getThumb($file_old);

			if(file_exists("./".$this->config->item('path_upload')."/".$file_old)) unlink("./".$this->config->item('path_upload')."/".$file_old);

			if(file_exists("./".$this->config->item('path_upload')."/".$GetThumb)) unlink("./".$this->config->item('path_upload')."/".$GetThumb);

			$data['image'] = "";
			$this->db->where("id", $id);
			$result = $this->db->update('kg_member', $data);

			if($result){
				$this->db->cache_delete_all();
			}
		}
	}



	function del_cover()
	{
		if($this->session->userdata('user_id_sess'))
		{
			$id = $this->input->post('id');
			$file_old_cover = $this->input->post('file_old_cover');

			$GetThumbcover = getThumb($file_old_cover);

			if(file_exists("./".$this->config->item('path_upload')."/".$file_old_cover)) unlink("./".$this->config->item('path_upload')."/".$file_old_cover);

			if(file_exists("./".$this->config->item('path_upload')."/".$GetThumbcover)) unlink("./".$this->config->item('path_upload')."/".$GetThumbcover);

			$data['cover'] = "";
			$this->db->where("id", $id);
			$result = $this->db->update('kg_member', $data);

			if($result){
				$this->db->cache_delete_all();
			}
		}
	}

	function profile_forum()
	{
		if($this->session->userdata('user_id_sess'))
		{
			$data = GetHeaderFooter(1);
			$data['main_content'] = 'profile_forum';
			$data['controller_name'] = $controller_name = 'member';
			$id = $this->session->userdata('user_id_sess');

			$filter = array(
					"id"=>"where/".$id,
			);

			$data['member'] = $member = GetAll('kg_'.$controller_name,$filter);
			if($member->num_rows() > 0)
			{
				$v = $data['member']->row_array();
			}else{
				ciredirect(site_url('error_404'));
			}

			$this->load->view('layout',$data);
		}else{
			ciredirect(site_url('home'));
		}
	}

	function register_forum()
	{
		$this->form_validation->set_rules('title', 'Name', 'required');
		$this->form_validation->set_rules('password', 'Password', 'required');
		$id = $this->input->post('id');
		if ($this->form_validation->run() == TRUE)
		{
			if($_FILES['attachedfilecover']['name'])
	    	{
	    		if($this->input->post('file_old_cover')){
		    		$file_old_cover = $this->input->post('file_old_cover');
		    		if(file_exists("./".$this->config->item('path_upload')."/".$file_old_cover)) {
				    	unlink("./".$this->config->item('path_upload')."/".$file_old_cover);
				    }

				    $thumbcover = getThumb($file_old_cover);
				    if(file_exists("./".$this->config->item('path_upload')."/".$thumbcover)) unlink("./".$this->config->item('path_upload')."/".$thumbcover);
				}

				$data['attachedfilecover'] = input_file('attachedfilecover');
			    if($data['attachedfilecover'] == "err_img_size")
		     	{
				    $this->session->set_flashdata("message", "ukuran gambar cover picture melebihi kapasitas");
				    ciredirect('member/profile');
		     	}
		     	else if($data['attachedfilecover'] == "err_file_size")
		     	{
				    $this->session->set_flashdata("message", "ukuran file cover picture melebihi kapasitas");
				    ciredirect('member/profile');
		     	}

			    $users['cover'] = $data['attachedfilecover'];

			    $act = array(
					'id_member'	=> $this->session->userdata('user_id_sess'),
					'title' => 'Update cover profile',
					'url' => '#',
					'activity' => 'update_cover',
					'create_date' => date("Y-m-d H:i:s"),
					'create_user_id'	=> $this->session->userdata('user_id_sess')
				);
				$this->db->insert('kg_member_activity', $act);
			}

	    	if($_FILES['attachedfile']['name'])
	    	{
	    		if($this->input->post('file_old')){
		    		$file_old = $this->input->post('file_old');
		    		if(file_exists("./".$this->config->item('path_upload')."/".$file_old)) {
				    	unlink("./".$this->config->item('path_upload')."/".$file_old);
				    }

				    $thumb = getThumb($file_old);

				    if(file_exists("./".$this->config->item('path_upload')."/".$thumb)) unlink("./".$this->config->item('path_upload')."/".$thumb);
				}

				$data['attachedfile'] = input_file('attachedfile');
			    if($data['attachedfile'] == "err_img_size")
		     	{
				    $this->session->set_flashdata("message", "ukuran gambar melebihi kapasitas");
				    ciredirect('member/profile');
		     	}
		     	else if($data['attachedfile'] == "err_file_size")
		     	{
				    $this->session->set_flashdata("message", "ukuran file melebihi kapasitas");
				    ciredirect('member/profile');
		     	}

				$users['image'] = $data['attachedfile'];

				$act = array(
					'id_member'	=> $this->session->userdata('user_id_sess'),
					'title' => 'Update gambar profile',
					'url' => '#',
					'activity' => 'update_avatar',
					'create_date' => date("Y-m-d H:i:s"),
					'create_user_id'	=> $this->session->userdata('user_id_sess')
				);
				$this->db->insert('kg_member_activity', $act);
			}

			$users['title'] = $this->input->post('title');
			$users['phone'] = $this->input->post('phone');
			$users['address'] = $this->input->post('address');
			$users['modify_date'] = date('Y-m-d h:i:s',now());
			$users['modify_user_id'] = $id;
			$this->db->where('id',$id);
			$this->db->update('kg_member',$users);
			
			$q_phpbb = $this->db->query('select user_email from phpbb_users where user_email = "'.strtolower($this->input->post('email')).'" limit 0,1');
			
			if($q_phpbb->num_rows() == 0){
				$post_email = $this->input->post('email');
				$this->phpbb->user_add($post_email,$post_email,$this->input->post('password'));
			}

			$this->session->set_flashdata('message', '<p>You have successfully created forum account.</p>');

			ciredirect('member/login');
		}
		else
		{
			$this->db->where('id',$id);
			$this->db->update('kg_member',$users); 
			$this->load->view('profile');
		}		
	}
}

// END Register Class