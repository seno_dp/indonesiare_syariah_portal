<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class instruksidireksi extends CI_Controller {



	function index()

	{

		$this->listing();

	}



	function listing(){

		if((strpos($this->input->ip_address(),'127.0.0.1') !== false) or (strpos($this->input->ip_address(),'192.168.') !== false) or ($this->session->userdata('user_id_sess'))) 

		{

			$data = GetHeaderFooter(1);

			$data['main_content'] = 'knowledge';

			$data['menu_title'] = 'Instruksi Direksi';

			$data['controller_name'] = 'instruksidireksi';



			$GetAllConfig = $this->model_admin_all->GetAllConfig('kg_'.$data['controller_name']);

			if($GetAllConfig->num_rows() > 0)

			{

				$r = $GetAllConfig->row_array();

				$per_page = $r['per_page'];

			}



			$uri_segment = 4;

			$awal = $this->uri->segment($uri_segment);

			

			$filter = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/Publish","id"=>"order/desc","limit"=> $awal."/".$per_page);

			$data['qp'] = GetAll('kg_'.$data['controller_name'],$filter);

			

			$filter2 = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/Publish","id"=>"order/desc");

			$q = GetAll('kg_'.$data['controller_name'],$filter2);



			$path_paging = site_url($data['controller_name'].'/listing');

			$pagination = Page($q->num_rows(),$per_page,$awal,$path_paging,$uri_segment);

			if(!$pagination) $pagination = "";

			$data['pagination'] = $pagination;



			$this->load->view('layout',$data);

		}else{

			if($this->session->userdata('user_id_sess'))

			{

				ciredirect('home');

			}else{

				ciredirect('member/login');

			}

		}

	}



	function detail($id=1)

	{



		if((strpos($this->input->ip_address(),'127.0.0.1') !== false) or (strpos($this->input->ip_address(),'192.168.') !== false) or ($this->session->userdata('user_id_sess'))) 

		{

			$data = GetHeaderFooter(1);

			

			$data['main_content'] = 'knowledge_detail';

			$data['controller_name'] = $controller_name = 'instruksidireksi';

			

			$filter = array(

					"is_publish"=>"where/publish",

					"id"=>"where/".$id,

					"id_lang"=>"where/".GetIdLang()

			);



			$data['knowledge'] = $knowledge = GetAll('kg_'.$controller_name,$filter);

			if($knowledge->num_rows() > 0){

				$v = $data['knowledge']->row_array();

				

				/*$views = $r['views'];

				$this->updateviews($id,'kg_'.$controller_name,$views);*/



				$i = 0;

				$files = "";

				$tags = explode(',',$v['tags']);

				$numItems = count($tags);

				$result="";

				$files = "SELECT * FROM kg_".$controller_name." where id_lang = ".GetIdLang()." and id <> ".$v['id']. " and (";

				foreach($tags as $tag=>$val){

					if($val != ""){

						$files .= " tags like '%".$val."%'";

						if(++$i === $numItems){

					    	$files .= "";

						}else{

							$files .= " or ";

						}

					}else

					{

						$files .= " tags like '0'";

					}

				}

				$files .= ") limit 0,4";

			

				$this->session->set_userdata('detail_uri',current_url());

				$data['rel_link'] = $this->db->query($files);

			}else{

				ciredirect(site_url('error_404'));

			}

				

			$this->load->view('layout',$data);

		}else{

			if($this->session->userdata('user_id_sess'))

			{

				ciredirect('home');

			}else{

				ciredirect('member/login');

			}

		}

	}



	function download($id=NULL)

	{

		$data['controller_name'] = $controller_name = 'instruksidireksi';

		//echo $this->session->userdata('publication_uri');

		$filter = array("id"=>"where/".$id,"id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/Publish");

		$data['list'] = $list = GetAll("kg_".$controller_name,$filter);



		if($list->num_rows() > 0){

			$r = $list->row_array();

			$downloads = $r['download'];

			$this->updatedownloads($id,'kg_'.$controller_name,$downloads);

			(strlen($r['uploaded_file']) > 0 && strlen($r['uploaded_file']) > 0) ? ciredirect(base_url().'uploads/'.$r['uploaded_file']) : ciredirect($this->session->userdata('detail_uri'));

		}

	}



	function updatedownloads($id,$table,$views){

		$this->load->helper('date');

		$views_curr = $views + 1;

		$this->db->where('id', $id);

		$this->db->where('id_lang', GetIdLang());

		$this->db->update($table,array('download' => $views_curr)); 



		/*

		$data = array(

		   'id_publication' => $id,

		   'date_download' => date('Y-m-d h:i:s',now())

		);



		$this->db->insert('kg_pub_download', $data); 

		*/

	}



	function updateviews($id,$table,$views){

		$this->load->helper('date');

		$views_curr = $views + 1;

		$this->db->where('id', $id);

		$this->db->where('id_lang', GetIdLang());

		$this->db->update($table,array('views' => $views_curr)); 

	}







}



/* End of file page.php */

/* Location: ./application/controllers/page.php */