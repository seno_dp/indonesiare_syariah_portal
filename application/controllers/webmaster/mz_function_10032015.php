<?php
class mz_function extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->session = new CI_Session();
		$this->db = $this->load->database("default",TRUE);
		$this->pagination = new CI_Pagination();
		$this->load = new CI_Loader();
		$this->load->model("model_admin_menu");
		$this->load->model("model_admin_all");
		if(!$this->session->userdata("ses_lang")) $this->session->set_userdata("ses_lang", "mz_indo");
		$this->lang->load($this->session->userdata("ses_lang"), '');
	}
	
	function getConfig()
	{
		$GetAll = $this->model_admin_all->GetAll("config_global");
		$config = $GetAll->result_array();
		return $config[0];
	}
	
	function header_footer()
	{
		if($this->session->userdata('webmaster_id'))
		{
			$data['dis_login'] = "display:'';";
			$data['nama_user'] = $this->session->userdata('admin');
		}
		else
		{
			$data['dis_login'] = "display:none;";
			$data['nama_user'] = "";
		}
		
		//Config
		$getConfig = $this->getConfig();
		$cms_name = $getConfig['cms_name'] ? $getConfig['cms_name'] : "CMS - Mazhters";
		$data['multi_lang'] = $getConfig['is_multi_lang'] == "1" ? true : false;
		$data['cms_name'] = $cms_name;
		
		//Language
		$lang="";
		$filter = array("is_active"=> "where/Active","id"=>"order/asc");
		$GetAll = $this->model_admin_all->GetAll("config_language",$filter);
		if($GetAll->num_rows > 1)
		{
			foreach($GetAll->result_array() as $r)
			{
				$ext_file = strrchr($r['flag'], '.');
				if(strtolower($ext_file) == ".jpg" || strtolower($ext_file) == ".jpeg" || strtolower($ext_file) == ".png")
				$lang .= "<img src='".base_url().$this->config->item('path_upload')."/".$this->getThumb($r['flag'])."' width='30'>";
				else
				{
					if($lang) $lang .= " | ";
					if($this->session->userdata("ses_lang") == $r['alias'])
					{
						$this->session->set_userdata("ses_id_lang",$r['id']);
						$lang .= "<a class='active a_lang' alt='".$r['id']."'>".$r['title']."</a>";
					}
					else $lang .= "<a class='a_lang' alt='".$r['id']."'>".$r['title']."</a>";
				}
			}
		}
		$data['lang'] = $lang;
		
		$data['header'] = 'webmaster/mz_header';
		$data['sidebar'] = 'webmaster/mz_sidebar';
		$data['footer'] = 'webmaster/mz_footer';
		$data['breadcrumb'] = $this->breadcrumb($data['multi_lang']);
		
		return $data;
	}
	
	function breadcrumb($multi_lang)
	{
		$breadcrumb = "<li><a href='".site_url("webmaster/mz_home")."'>".lang('home')."</a></li>";
		$sub_bread = $here = "";
		if($multi_lang) $this->db->where("id_lang",$this->session->userdata("ses_id_lang"));
		$this->db->where("filez",$this->uri->segment(2));
		$q = $this->db->get("menu_admin");
		foreach($q->result_array() as $r)
		{
			if($this->uri->segment(3) && $this->uri->segment(3) != "main")
			$here = "<li><span class='divider'>/</span><a href='".site_url("webmaster/".$r['filez'])."'>".$r['title']."</a></li><li class='active'><span class='divider'>/</span>".ucfirst($this->uri->segment(3))."</li>";
			else
			$here = "<li class='active'><span class='divider'>/</span>".$r['title']."</li>";
			if($r['id_parents'] > 0)
			{
				if($multi_lang) $this->db->where("id_lang",$this->session->userdata("ses_id_lang"));
				$this->db->where("id",$r['id_parents']);
				$q = $this->db->get("menu_admin");
				foreach($q->result_array() as $r)
				{
					if($r['filez'] == "#")
					$sub_bread .= "<li><span class='divider'>/</span><a href='#'>".$r['title']."</a></li>";
					else
					$sub_bread .= "<li><span class='divider'>/</span><a href='".site_url("webmaster/".$r['filez'])."'>".$r['title']."</a></li>";
					if($r['id_parents'] > 0)
					{
						$this->db->where("id",$r['id_parents']);
						$q = $this->db->get("menu_admin");
						foreach($q->result_array() as $r)
						{
							if($r['filez'] == "#")
							$sub_bread .= "<li><span class='divider'>/</span><a href='#'>".$r['title']."</a></li>";
							else
							$sub_bread .= "<li><span class='divider'>/</span><a href='".site_url("webmaster/".$r['filez'])."'>".$r['title']."</a></li>";
						}
					}
				}
			}
		}
		
		if($this->session->userdata("webmaster_grup") == "8910")
    {
			$setting = "<a href='#' data-controls-modal='modal-setting' class='setting'><img src='".base_url()."assets/mz_images/admin/setting.png' width='18'></a>";
		}
		else $setting = "";
		$data['breadcrumb'] = $setting.$breadcrumb.$sub_bread.$here;
		return $data['breadcrumb'];
	}
	
	function auth_menu()
	{
		if(!$this->session->userdata('webmaster_id'))
		{
			ciredirect('webmaster/mz_login');
		}
		
		$group = $this->session->userdata('webmaster_grup');
		$ref_menu = $this->uri->segment(2);
		$q_path = $this->model_admin_menu->cekAccessMenu($ref_menu);
		$jum = $q_path->num_rows();
		
		if($jum > 0){
			$row = $q_path->row();
			$id_menu_admin = $row->id;
			$this->db->where("id_admin_grup",$group);
			$this->db->where("id_menu_admin",$id_menu_admin);
			$q_menu_admin = $this->db->get("admin_auth");
			$jum_menu_admin = $q_menu_admin->num_rows();
			if($jum_menu_admin == 0 && $group != "8910")
			{
				ciredirect("webmaster/mz_forbiden");
			}
		}
	}
	
	function page($jum_record,$lmt,$pg,$path,$uri_segment)
	{
		$link = "";
		$config['base_url'] = $path;
		$config['total_rows'] = $jum_record;
		$config['per_page'] = $lmt;
		$config['num_links'] = 3;
		$config['cur_tag_open'] = '<li><strong>';
		$config['cur_tag_close'] = '</strong></li>';
		$config['num_tag_open'] = '<li>';
		$config['num_tag_close'] = '</li>';
		$config['next_tag_open'] = '<li>';
		$config['next_tag_close'] = '</li>';
		$config['prev_tag_open'] = '<li>';
		$config['prev_tag_close'] = '</li>';
		$config['first_tag_open'] = '<li>';
		$config['first_tag_close'] = '</li>';
		$config['last_tag_open'] = '<li>';
		$config['last_tag_close'] = '</li>';
		$config['uri_segment'] = $uri_segment;
		$this->pagination->initialize($config);
		$link = $this->pagination->create_links();
		return $link;
	}
	
	function get_value($field,$table,$where)
	{
		$val="";
		if(substr($table,0,3) != "kg_") $table = "kg_".$table;
		$sql = "SELECT ".$field." FROM ".$table." WHERE ".$where;
		$query = $this->db->query($sql);
		foreach($query->result_array() as $r)
		{
			return $r[$field];
		}
		return false;
	}
	
	function typeinput($table, $key, $type, $related="", $value="", $title_related="")
	{
		$typeinput = "";
		if($type == "Text")
		{
			//Text
			if($key == "userpass" || $key == "passwd") $data_input = array('name'=> $key, 'value'=> $value, 'class'=> 'span5 required', 'type'=> 'password');
			else $data_input = array('name'=> $key, 'value'=> $value, 'class'=> 'span5 required');
			$typeinput = form_input($data_input);
		}
		else if($type == "Label")
		{
			//Label
			$typeinput = "<label class='input-label'>".$value."</label>";
			//$typeinput .= "<input type='hidden' id='".$key."' name='".$key."' value='".$value."'>";
		}
		else if($type == "Dropdown")
		{
			//Dropdown
			$GetMultiLang = $this->get_value("is_multi_lang","config","tabel='".$table."'");
			
			if($related == 'kg_menu_admin' || $related == 'kg_menu'){
				$filter = $GetMultiLang ? array('id_lang'=>'where/'.$this->session->userdata("ses_id_lang"),'id_parents'=>"order/asc",'id'=>"order/asc") : array('id_parents'=>"order/asc",'id'=>"order/asc");
			}else
			{
				$filter = $GetMultiLang ? array('id_lang'=>'where/'.$this->session->userdata("ses_id_lang")) : array();
			}
			//print_r($filter);
			$opt_list = array();
			$opt_list[] = "-- Select Value --";
			if($related)
			{
				$Opt = $this->model_admin_all->GetAll($related,$filter);
				
					foreach($Opt->result_array() as $r)
					{
						if($related == 'kg_menu_admin' || $related == 'kg_menu'){
							if($r['id_parents'] != 0){
								$filter = $GetMultiLang ? array("id"=>"where/".$r['id_parents'],'id_lang'=>'where/'.$this->session->userdata("ses_id_lang")) : array("id"=>"where/".$r['id_parents']);
								$qu = GetAll($related,$filter);
								if($qu->num_rows() > 0){
									$row = $qu->row_array();

									if($title_related) $opt_list[$r['id']] = $row['title'].'&nbsp;--&nbsp;'.$r[$title_related];
									else $opt_list[$r['id']] = $row['title'].'&nbsp;--&nbsp;'.$r['title'];
								}else{
									if($title_related) $opt_list[$r['id']] = $r[$title_related];
									else $opt_list[$r['id']] = $row['title'].'&nbsp;--&nbsp;'.$r['title'];
								}

								//if($title_related) $opt_list[$r['id']] = $row['title'].'&nbsp;--&nbsp;'.$r[$title_related];
								//else $opt_list[$r['id']] = $row['title'].'&nbsp;--&nbsp;'.$r['title'];
							}else{
								if($title_related) $opt_list[$r['id']] = $r[$title_related];
								else $opt_list[$r['id']] = $r['title'];
							}
						}else{
							if($title_related) $opt_list[$r['id']] = $r[$title_related];
							else $opt_list[$r['id']] = $r['title'];
						}
					}
				}
			else 
			{
				$GetTypeData = $this->model_admin_all->GetTypeDataByField($table,$key);
				$exp = explode("'",$GetTypeData);
				for($i=1;$i< count($exp);$i+=2)
				{
					$opt_list[$exp[$i]] = $exp[$i];
				}
			}
			$typeinput = form_dropdown($key, $opt_list, $value, "class='span5'");
		}
		else if($type == "File")
		{
			//File
			$data_input = array('name'=> $key, 'type'=> 'file', 'class'=> 'span5');
			$typeinput = form_input($data_input);
			$ext_file = strrchr($value, '.');
			$img=$value;
			if(strtolower($ext_file) == ".jpg" || strtolower($ext_file) == ".jpeg" || strtolower($ext_file) == ".png")
			{
				$img = "<img src='".base_url().$this->config->item('path_upload')."/".GetThumb($value)."' class='img_detail'>";
				$typeinput .= "<div id='".$key."img' style='margin-left:100px;'>".$img."<br><a class='del_img' alt='".$key."'>".lang('delete_file')."</a></div>";
			}
			else if($img) $typeinput .= "<div id='".$key."img' style='margin-left:100px;'>".$img."<br><a class='del_img' alt='".$key."'>".lang('delete_file')."</a></div>";
			
			if(!$value) $value="-";
			$typeinput .= "<input type='hidden' id='".$key."_file' name='".$key."_file' value='".$value."'>";
		}
		else if($type == "Textarea")
		{
			//Textarea
			$data_input = array('name'=> $key, 'id'=> $key, 'value'=> $value, 'cols'=> 10, 'rows'=> 5, 'class'=> 'span5');
			$typeinput = form_textarea($data_input);
			$typeinput .= "<script>
            		CKEDITOR.replace( '".$key."',
            				{
						skin : 'office2003',
						filebrowserBrowseUrl : '".base_url()."assets/mz_ckfinder/ckfinder.html',
											 filebrowserImageBrowseUrl : '".base_url()."assets/mz_ckfinder/ckfinder.html?Type=Images',
											 filebrowserFlashBrowseUrl : '".base_url()."assets/mz_ckfinder/ckfinder.html?Type=Flash',
											 filebrowserUploadUrl : '".base_url()."assets/mz_ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
											 filebrowserImageUploadUrl : '".base_url()."assets/mz_ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
											 filebrowserFlashUploadUrl : '".base_url()."assets/mz_ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
						toolbar :
						[
							['Source','-','Preview','Templates','Cut','Copy','Paste'],['Bold','Italic','Underline','Strike','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','NumberedList','BulletedList','Subscript','Superscript','-'],
							'/',
							['Link','Unlink','-','Image','Flash','Table','HorizontalRule','Smiley','SpecialChar'],['TextColor','BGColor','-','Font','FontSize','PasteFromWord']
						]
					});
            	</script>";
            $data_input_hidden = array('name'=> $key.'_hidden', 'id'=> $key.'_hidden', 'value'=> $value, 'type'=> 'hidden', 'class'=> 'span5');
			$typeinput .= form_input($data_input_hidden);

		}
		else if($type == "Textarea-nojs")
		{
			//Textarea-nojs
			$data_input = array('name'=> $key, 'id'=> $key, 'value'=> $value, 'cols'=> 10, 'rows'=> 5, 'class'=> 'span5');
			$typeinput = form_textarea($data_input);
			
            $data_input_hidden = array('name'=> $key.'_hidden', 'id'=> $key.'_hidden', 'value'=> $value, 'type'=> 'hidden', 'class'=> 'span5');
			$typeinput .= form_input($data_input_hidden);

		}
		else if($type == "Checkbox")
		{
			//Checkbox
			if($value == 1) $checked = true;
			else $checked = false;
			$data_input = array('name'=> $key, 'value'=> 1, 'checked'=> $checked);
			$typeinput = form_checkbox($data_input);
		}
		else if($type == "Hidden")
		{
			//Hidden
			$data_input = array('name'=> $key, 'id'=> $key, 'value'=> $value, 'type'=> 'hidden', 'class'=> 'span5');
			$typeinput = form_input($data_input);
		}else if($type == "Date")
		{
			//Date
			$value = ($value == '' || $value == '0000-00-00 00:00:00') ? now() : strtotime($value);
			$data_input = array('name'=> $key, 'id'=> $key, 'value'=> date('Y-m-d',$value), 'type'=> 'text', 'class'=> 'span5');
			$typeinput = form_input($data_input)." <span style='font-size: 11px'>format : yyyy-mm-dd</span>";
			$typeinput .= "<script>
			            		$(document).ready(function() {
									$('#".$key."').DatePicker({
										format:'Y-m-d',
										date: $('#".$key."').val(),
										current: $('#".$key."').val(),
										starts: 1,
										position: 'right',
										onBeforeShow: function(){
											$('#".$key."').DatePickerSetDate($('#".$key."').val(), true);
										},
										onChange: function(formated, dates){
											$('#".$key."').val(formated);
										}
									});
								}); 
			            	</script>";
		}
		
		return $typeinput;
	}

	function buildTree(Array $data, $parent = 0) {
	    $tree = array();
	    foreach ($data as $d) {
	        if ($d['id_parents'] == $parent) {
	            $children = $this->buildTree($data, $d['id']);
	            // set a trivial key
	            if (!empty($children)) {
	                $d['_children'] = $children;
	            }
	            $tree[] = $d;
	        }
	    }
	    return $tree;
	}

	function printTree($tree, $r = 0, $p = null) {
	    foreach ($tree as $i => $t) {
	        $dash = ($t['id_parents'] == 0) ? '' : str_repeat('-', $r) .' ';
	        printf("\t<option value='%d'>%s%s</option>\n", $t['id'], $dash, $t['title']);
	        //$data = $opt_list[$r['id']] = $r['title'];
	        if ($t['parent'] == $p) {
	            // reset $r
	            $r = 0;
	        }
	        if (isset($t['_children'])) {
	            printTree($t['_children'], ++$r, $t['id_parent']);
	        }
	    }
	}


	

	
	function input_file($key)
	{
		$this->load->library('image_lib');
		$file_up = $_FILES[$key]['name'];
		$file_up = date("YmdHis").".".str_replace("-","_",url_title($file_up));
		$myfile_up	= $_FILES[$key]['tmp_name'];
		$ukuranfile_up = $_FILES[$key]['size'];
		$up_file = "./".$this->config->item('path_upload')."/".$file_up;
		
		$getConfig = $this->getConfig();
		$ext_file = strrchr($file_up, '.');
		if(strtolower($ext_file) == ".jpg" || strtolower($ext_file) == ".jpeg" || strtolower($ext_file) == ".png")
		{
			if($ukuranfile_up < ($getConfig['img_size'] * 1024))
			{
				if(copy($myfile_up, $up_file))
				{
					//Resize
					$config['image_library'] = 'gd2';
					$config['source_image'] = $up_file;
					$config['dest_image'] = "./".$this->config->item('path_upload')."/";
					$config['create_thumb'] = TRUE;
					$config['maintain_ratio'] = TRUE; //Width=Height
					$config['height'] = $getConfig['img_thumb_h'];
					$config['width'] = $getConfig['img_thumb_w'];
					
					$this->image_lib->initialize($config);
					$this->image_lib->resize();
					$this->image_lib->clear();
					
					/*//Watermark
					$config['source_image'] = $up_file;
					$config['wm_text'] = 'CMS - Mazhters';
					$config['wm_type'] = 'text';
					$config['quality'] = '20';
					$config['wm_font_path'] = './style/pencil.ttf';
					$config['wm_font_size'] = '16';
					$config['wm_font_color'] = 'ffffff';
					$config['wm_vrt_alignment'] = 'bottom';
					$config['wm_hor_alignment'] = 'center';
					$config['wm_padding'] = '0';
					$this->image_lib->initialize($config);
					$this->image_lib->watermark();*/
				}
			}
			else return "err_img_size";
		}
		else
		{
			if($ukuranfile_up < ($getConfig['file_size'] * 1024))
			{
				copy($myfile_up, $up_file);
			}
			else return "err_file_size";
		}
		
		return $file_up;
	}
	
	function explode_name_file($source)
	{
		$ext = strrchr($source, '.');
		$name = ($ext === FALSE) ? $source : substr($source, 0, -strlen($ext));

		return array('ext' => $ext, 'name' => $name);
	}
	
	function getThumb($image, $path="_thumb")
	{
		$exp = $this->explode_name_file($image);
		return $exp['name'].$path.$exp['ext'];
	}
	
	function delete_image()
	{
		$webmaster_id = $this->auth();
		$mz_function = new mz_function();
		$id = $this->input->post('del_id_img');
		$table = $this->input->post('del_table');
		$field = $this->input->post('del_field');
		
		$GetFile = $mz_function->get_value($field,$table, "id='".$id."'");
		$GetThumb = $mz_function->getThumb($GetFile);
		if(file_exists("./".$this->config->item('path_upload')."/".$GetFile)) unlink("./".$this->config->item('path_upload')."/".$GetFile);
		if(file_exists("./".$this->config->item('path_upload')."/".$GetThumb)) unlink("./".$this->config->item('path_upload')."/".$GetThumb);
		
		$data[$field] = "";
		$this->db->where("id", $id);
		$result = $this->db->update($table, $data);

		if($result){
				$this->db->cache_delete_all();
			}
	}
	
	function change_lang()
	{
		$lang = $this->input->post('lang_alias');
		$this->session->set_userdata("ses_lang",$lang);
		$this->lang->load($lang, '');
	}
}
?>