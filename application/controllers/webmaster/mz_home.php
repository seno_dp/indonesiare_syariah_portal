<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*************************************
  * Created : Dec 2011
  * Creator : Mazhters Irwan
  * Email   : irwansyah@komunigrafik.com
  * CMS ver : CI ver.2.0
*************************************/	

require_once 'mz_function.php';
class mz_home extends CI_Controller {
	
	var $title = "Home";
	var $filename = "mz_home";
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("model_admin_all");
		//$this->load->library('gapi');
	}
	
	function set_include()
	{
		$mz_function = new mz_function();
		$mz_function->auth_menu();
		$data = $mz_function->header_footer();
		
		return $data;	
	}
	
	function index()
	{
		$this->main();
	}
	
	function main($start_date = NULL, $end_date = NULL)
	{
		//Set Global
		$mz_function = new mz_function();
		$data = $this->set_include();
		$data['main_content'] = 'webmaster/mz_home';
		$data['title'] = $this->title;
		$data['filename'] = $this->filename;
		//End Global
		
		//Dashboard
		$dash=array();


		/*$this->db->select("kg_menu_admin.id,title,filez,kg_dashboard.is_active");
		$this->db->from("kg_menu_admin");
		$this->db->where("kg_dashboard.is_active","Active");
		$this->db->join("kg_dashboard","kg_dashboard.id_menu_admin=kg_menu_admin.id");
		$this->db->order_by("kg_dashboard.sort");
		$query = $this->db->get();
		die($this->db->last_query());*/
		


		$GetDashboard = $this->model_admin_all->GetDashboard();

		foreach($GetDashboard as $r)
		{
			$dash[$r['filez']] = $r['title'];
		}
		//die(print_r($GetDashboard));
		
		
		$data['list_grid'] = array();
		$i=$j=0;
		foreach($dash as $r=> $v)
		{
			//die(print_r($dash));
			$i++;
			if($i%4 == 1) $j++;
			$modify_date = $mz_function->get_value("modify_date",$r,"1 order by modify_date desc limit 1");
			
			$q = $this->model_admin_all->GetAll($r);

			$data['list_grid'][$j][$r] = array("uri"=> $r, "total"=> $q->num_rows(), "title"=> $v, "modify_date"=> $modify_date);
		}
		//End Dashboard
		
		//Activities
		$GetActivities = $this->model_admin_all->GetActivities("admin_log",10);
		$data['activities'] = $GetActivities;
		//End Activities
		//Start Google Analytics
		/*$fga = array('id'=>'where/1','is_active'=>'where/active');
		$qga = GetAll('kg_ga_api',$fga);
		$data['ga_count'] = $qga->num_rows();
		if($qga->num_rows() > 0){
			$data['vstart_date'] = $start_date = ($this->input->post('start_date')) ? $this->input->post('start_date') : date('Y-m-d',strtotime('-30 day'));
			$data['vend_date'] = $end_date = ($this->input->post('end_date')) ? $this->input->post('end_date') : date('Y-m-d',now());
			$vga = $qga->row_array();
   			$ga_profile_id = $vga['profile_id'];
   			$ga_email = $vga['email'];
   			$ga_password = $vga['passwd'];
    		$ga = $this->gapi->init($ga_email,$ga_password);
    		$this->gapi->requestReportData($ga_profile_id, array('date'),array('pageviewsPerVisit','visits','pageviews', 'uniquePageviews','newVisits','percentNewVisits'), 'date', '',$start_date,$end_date); 
    		$data['results'] = $this->gapi->getResults();
    		$data['Pageviews'] = $this->gapi->getPageviews();
    		$data['uniquePageviews'] = $this->gapi->getuniquePageviews();
    		$data['newVisits'] = $this->gapi->getnewVisits();
    		$data['Visits'] = $this->gapi->getVisits();
    		$data['percentnewVisits'] = $this->gapi->getpercentNewVisits();
    		$data['pageviewsPerVisit'] = $this->gapi->getpageviewsPerVisit();
		}*/
		//End Google Analytics
		
		$this->load->view('webmaster/template_home',$data);
	}
	
	function setting()
	{
		if($this->input->post('setting_id'))
		{
			$sort=0;
			$data = array("is_active"=> "InActive", "sort"=> 0);
			$this->db->update("dashboard",$data);
			foreach($this->input->post('setting_id') as $r)
			{
				$sort++;
				$q = $this->model_admin_all->GetSettingByIdRefMenuAdmin($r);
				if($q->num_rows() > 0)
				{
					$data = array("is_active"=> "Active", "sort"=> $sort);
					$this->db->where("id_menu_admin", $r);
					$result = $this->db->update("dashboard",$data);
				}
				else 
				{
					$data = array("id_menu_admin"=> $r, "is_active"=> "Active", "sort"=> $sort);
					$result = $this->db->insert("dashboard",$data);
				}

				if($result){
					$this->db->cache_delete_all();
				}
			}
			ciredirect('webmaster/mz_home');
		}
		else
		{
      		$data['filename'] = $this->filename;
			$q = $this->model_admin_all->GetSetting();
			$opt="";
			foreach($q as $r)
			{
				if($r['is_active'] == "Active")
				$opt .= "<option value='".$r['id']."' selected>".$r['title']."</option>";
				else
				$opt .= "<option value='".$r['id']."'>".$r['title']."</option>";
			}
			$data['opt_setting'] = $opt;
			$this->load->view('webmaster/mz_setting_dashboard',$data);
		}
	}	
function test(){
	$data = array(
              'name'        => 'username',
              'id'          => 'username',
              'value'       => 'johndoe',
              'maxlength'   => '100',
              'size'        => '50',
              'style'       => 'width:50%',
            );
  echo form_input($data);
 }
}
?>