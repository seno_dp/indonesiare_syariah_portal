<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*************************************
  * Created : Dec 2011
  * Creator : Mazhters Irwan
  * Email   : irwansyah@komunigrafik.com
  * CMS ver : CI ver.2.0
*************************************/	

require_once 'mz_function.php';
class mz_config extends CI_Controller {
	
	var $tabel = "config";
	var $title = "Config";
	var $filename = "mz_config";
	
	function __construct()
	{
		parent::__construct();
		$this->load->model("model_admin_all");
	}
	
	function index()
	{
		$this->main();
	}
	
	function main()
	{
		if($this->session->userdata("webmaster_grup") != "8910") ciredirect(site_url('webmaster/forbiden'));
		$mz_function = new mz_function();
		if($this->input->post('tabel'))
		{
			$tabel = $this->input->post('tabel');
			$filename = $this->input->post('filename');
			$q = $this->model_admin_all->GetColumns($tabel);
			foreach($q as $r)
			{
				$config[$r['Field']]['sort'] = $this->input->post('sort_'.$r['Field']);
				$config[$r['Field']]['alias'] = $this->input->post('alias_'.$r['Field']);
				$config[$r['Field']]['show'] = $this->input->post('show_'.$r['Field']);
				$config[$r['Field']]['related'] = $this->input->post('related_'.$r['Field']);
				$config[$r['Field']]['title_related'] = $this->input->post('title_related_'.$r['Field']);
				$config[$r['Field']]['search'] = $this->input->post('search_'.$r['Field']);
				$config[$r['Field']]['typeinput'] = $this->input->post('typeinput_'.$r['Field']);
			}
			
			$data = array("tabel"=> $tabel, "config"=> serialize($config), 
										"to_detail"=> $this->input->post('to_detail'),
										"is_multi_lang"=> $this->input->post('is_multi_lang'),
										"is_sort"=> $this->input->post('is_sort'),
										"per_page"=> $this->input->post('per_page'),
										"item_homepage"=> $this->input->post('item_homepage')
										);
			$cekRecord = $mz_function->get_value("tabel","config","tabel='".$tabel."'");
			if(strlen($cekRecord) > 2)
			{
				$this->db->where("tabel",$tabel);
				$result = $this->db->update($this->tabel,$data);
			}
			else $result = $this->db->insert($this->tabel,$data);

			if($result){
				$this->db->cache_delete_all();
			}
			
			//if($filename == "contents") ciredirect('webmaster/list_'.$filename);
			//else 
			ciredirect('webmaster/'.$filename);
		}
		else
		{
			$tabel = $this->uri->segment(4);
			$data['filename'] = $this->uri->segment(5);
			$data['tabel'] = $tabel;
			$data['columns'] = $this->model_admin_all->GetColumns($tabel);
			$data['config'] = $this->model_admin_all->GetConfig($tabel);
			$data['to_detail'] = $mz_function->get_value("to_detail","config","tabel='".$tabel."'");
			$data['is_multi_lang'] = $mz_function->get_value("is_multi_lang","config","tabel='".$tabel."'");
			$data['is_sort'] = $mz_function->get_value("is_sort","config","tabel='".$tabel."'");
			$data['per_page'] = $mz_function->get_value("per_page","config","tabel='".$tabel."'");
			$data['item_homepage'] = $mz_function->get_value("item_homepage","config","tabel='".$tabel."'");
			
			//Related Table
			$data['opt_table'][0] = "-- Related Table --";
			$GetListTable = $this->model_admin_all->GetListTable();
			foreach($GetListTable as $r)
			{
				$data['opt_table'][$r['Tables_in_'.$this->db->database]] = $r['Tables_in_'.$this->db->database];
			}
			
			//Sort
			$data['opt_sort'][100] = "Sort";
			for($i=1;$i<=count($data['columns']);$i++)
			{
				$data['opt_sort'][$i] = $i;
			}
			$this->load->view('webmaster/mz_config',$data);
		}
	}
}
?>