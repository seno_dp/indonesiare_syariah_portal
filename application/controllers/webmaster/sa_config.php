<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*************************************
  * Created : Dec 2011
  * Update  : Jan 2012
  * Creator : Mazhters Irwan
  * Email   : irwansyah@komunigrafik.com
  * CMS ver : CI ver.2.0
*************************************/

require_once 'mz_function.php';
class sa_config extends CI_Controller {
	
	var $filename = "sa_config";
	var $tabel = "config_global";
	var $id_primary = "id";
	var $title_table = "title";
	
	function __construct()
	{
		parent::__construct();
	}
	
	function set_include()
	{
		$mz_function = new mz_function();
		$mz_function->auth_menu();
		$data = $mz_function->header_footer();
		
		return $data;	
	}
	
	function auth()
	{
		$webmaster_id = $this->session->userdata("webmaster_id");
		if(!$webmaster_id) ciredirect('webmaster/mz_login');
		return $webmaster_id;
	}
	
	function index()
	{
		$this->main();
	}
	
	function main()
	{
		//Set Global
		$id=1;
		$mz_function = new mz_function();
		$data = $this->set_include();
		$data['val_button'] = lang("edit");
		$data['table'] = $this->tabel;
		$data['title'] = $data['val_button']." ".lang($this->filename);
		$data['filename'] = $this->filename;
		$data['main_content'] = 'webmaster/mz_detail';
		//End Global
		
		$data['list_input'] = "";
		$GetConfig = $this->model_admin_all->GetConfig($this->tabel);
		if($id > 0)
		{
			$GetList = $this->model_admin_all->GetById($this->tabel,$id);
			foreach($GetList->result_array() as $r)
			{
				//Set ID
				foreach($GetConfig as $key=> $con)
				{
					if($con['typeinput'])
					{
						$alias = !empty($con['alias']) ? $con['alias'] : $key;
						$data['list_input'] .= "<div class='clearfix'><label class='search'>".$alias."</label>";
						$data['list_input'] .= $mz_function->typeinput($this->tabel, $key, $con['typeinput'], $con['related'], $r[$key]);
						$data['list_input'] .= "</div>";
					}
					else $data['list_input'] .= $mz_function->typeinput($this->tabel, $key, "Hidden", $con['related'], $r[$key]);
				}
			}
		}		
		$this->load->view('webmaster/template',$data);
	}
	
	function update()
	{
		$mz_function = new mz_function();
		$webmaster_id = $this->auth();
		$id = $this->input->post('id');
		$GetColumns = $this->model_admin_all->GetColumns($this->tabel);
		foreach($GetColumns as $r)
		{
			if($this->input->post($r['Field']."_file"))
			{
				if($_FILES[$r['Field']]['name'])
				{
					$data[$r['Field']] = $mz_function->input_file($r['Field']);
					$file_old = $this->input->post($r['Field']."_file");
					if(file_exists("./".$this->config->item('path_upload')."/".$file_old)) unlink("./".$this->config->item('path_upload')."/".$file_old);
					
					$thumb = $mz_function->getThumb($file_old);
					if(file_exists("./".$this->config->item('path_upload')."/".$thumb)) unlink("./".$this->config->item('path_upload')."/".$thumb);
				}
			}
			else $data[$r['Field']] = $this->input->post($r['Field']);
		}
		
		$this->db->where($this->id_primary, $id);
		$result = $this->db->update($this->tabel, $data);
		
		$this->session->set_flashdata("message", lang("edit")." ".lang($this->filename)." ".lang("msg_sukses"));
		if($result){
				$this->db->cache_delete_all();
			}
		ciredirect('webmaster/'.$this->filename);
	}
}
?>