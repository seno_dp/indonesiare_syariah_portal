<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*************************************
  * Created : Dec 2011
  * Update  : Jan 2012
  * Creator : Mazhters Irwan
  * Email   : irwansyah@komunigrafik.com
  * CMS ver : CI ver.2.0
*************************************/

require_once 'mz_function.php';
class page extends CI_Controller {
	
	var $filename = "page";
	var $tabel = "contents";
	var $id_primary = "id";
	var $title_table = "title";
	
	function __construct()
	{
		parent::__construct();
	}
	
	function set_include()
	{
		$mz_function = new mz_function();
		$mz_function->auth_menu();
		$data = $mz_function->header_footer();
		
		return $data;	
	}
	
	function auth()
	{
		$webmaster_id = $this->session->userdata("webmaster_id");
		if(!$webmaster_id) ciredirect('webmaster/mz_login');
		return $webmaster_id;
	}
	
	function index()
	{
		$this->main();
	}
	
	function main($id)
	{
		//Set Global
		$mz_function = new mz_function();
		$data = $this->set_include();
		if($id > 0) $data['val_button'] = lang("edit");
		else $data['val_button'] = lang("add");
		$data['table'] = $this->tabel;
		$data['title'] = $data['val_button']." ".lang($this->filename);
		$data['filename'] = $this->filename;
		$data['main_content'] = 'webmaster/mz_detail_content';
		$GetMultiLang = $mz_function->get_value("is_multi_lang","config","tabel='".$this->tabel."'");
		$data['path_per_paging'] = "";
		//End Global
		
		$data['list_input'] = "";
		$GetConfig = $this->model_admin_all->GetConfig($this->tabel);
		if($id > 0)
		{
			$GetMultiLang = $mz_function->get_value("is_multi_lang","config","tabel='".$this->tabel."'");
			$id_lang = $GetMultiLang ? $this->session->userdata("ses_id_lang") : "";
			$GetList = $this->model_admin_all->GetById($this->tabel,$id,$id_lang);
			if($GetList->num_rows() > 0)
			{
				foreach($GetList->result_array() as $r)
				{
					//Set ID
					foreach($GetConfig as $key=> $con)
					{
						if($con['typeinput'])
						{
							$alias = !empty($con['alias']) ? $con['alias'] : $key;
							if($con['related'])
							{
								if($con['typeinput'] == "Label")
								$r[$key] = $mz_function->get_value($con['title_related'],$con['related'],"id='".$r[$key]."'");
							}
       				$data['list_input'] .= "<div class='clearfix'><label class='search'>".$alias."</label>";
							$data['list_input'] .= $mz_function->typeinput($this->tabel, $key, $con['typeinput'], $con['related'], $r[$key], $con['title_related']);
							$data['list_input'] .= "</div>";
						}
						else $data['list_input'] .= $mz_function->typeinput($this->tabel, $key, "Hidden", $con['related'], $r[$key]);
					}
					if($id_lang != "" || $id_lang != NULL){
		               $data_input = array('name'=> 'input_bahasa', 'value'=> 1, 'checked'=> false);
		               $typeinput = form_checkbox($data_input);
		               $data['list_input'] .= "<div class='clearfix'><label class='search'>".lang('update_other_language')."</label>";
		               $data['list_input'] .= $typeinput;
		               $data['list_input'] .= "</div>";
		             }
				}
			}
			else
			{
				$data['val_button'] = lang("add");
				foreach($GetConfig as $key=> $con)
				{
					if($con['typeinput'])
					{
						$alias = !empty($con['alias']) ? $con['alias'] : $key;
						$data['list_input'] .= "<div class='clearfix'><label class='search'>".$alias."</label>";
						$data['list_input'] .= $mz_function->typeinput($this->tabel, $key, $con['typeinput'], $con['related']);
						$data['list_input'] .= "</div>";
					}
					else if($key == $this->id_primary) $data['list_input'] .= $mz_function->typeinput($this->tabel, $key, "Hidden", $con['related'], $id);
					else $data['list_input'] .= $mz_function->typeinput($this->tabel, $key, "Hidden");
				}
			}
		}
		else
		{
			foreach($GetConfig as $key=> $con)
			{
				if($con['typeinput'])
				{
					$alias = !empty($con['alias']) ? $con['alias'] : $key;
					$data['list_input'] .= "<div class='clearfix'><label class='search'>".$alias."</label>";
					$data['list_input'] .= $mz_function->typeinput($this->tabel, $key, $con['typeinput'], $con['related']);
					$data['list_input'] .= "</div>";
				}
				else $data['list_input'] .= $mz_function->typeinput($this->tabel, $key, "Hidden");
			}
			if($GetMultiLang != 0){
	           $data_input = array('name'=> 'input_bahasa', 'value'=> 1, 'checked'=> false);
	           $typeinput = form_checkbox($data_input);
	           $data['list_input'] .= "<div class='clearfix'><label class='search'>".lang('input_other_language')."</label>";
	           $data['list_input'] .= $typeinput;
	           $data['list_input'] .= "</div>";
	        }
		}
				
		$this->load->view('webmaster/template',$data);
	}

function update()
  {
    $mz_function = new mz_function();
    $webmaster_id = $this->auth();
    $id = $this->input->post('id');
    $GetColumns = $this->model_admin_all->GetColumns($this->tabel);
    foreach($GetColumns as $r)
    {
      if($this->input->post($r['Field']."_file"))
      {
        if($_FILES[$r['Field']]['name'])
        {
          $data[$r['Field']] = $mz_function->input_file($r['Field']);
          if($data[$r['Field']] == "err_img_size")
          {
            $this->session->set_flashdata("message", lang('msg_err_img_size'));
            ciredirect('webmaster/'.$this->filename.'/main/'.$id);
          }
          else if($data[$r['Field']] == "err_file_size")
          {
            $this->session->set_flashdata("message", lang('msg_err_file_size'));
            ciredirect('webmaster/'.$this->filename.'/main/'.$id);
          }
          $file_old = $this->input->post($r['Field']."_file");
          if(file_exists("./".$this->config->item('path_upload')."/".$file_old)) unlink("./".$this->config->item('path_upload')."/".$file_old);
          
          $thumb = $mz_function->getThumb($file_old);
          if(file_exists("./".$this->config->item('path_upload')."/".$thumb)) unlink("./".$this->config->item('path_upload')."/".$thumb);
        }
      }
      else
      {
        $data[$r['Field']] = $this->input->post($r['Field']);
        if(!$data[$r['Field']] && $data[$r['Field']] != "0") unset($data[$r['Field']]);
      }
    }
    $data['modify_date'] = date("Y-m-d H:i:s");
    
    //Language
    $GetMultiLang = $mz_function->get_value("is_multi_lang","config","tabel='".$this->tabel."'");
    $id_lang = $GetMultiLang ? $this->session->userdata("ses_id_lang") : "";
    if($id_lang) {
        $data['id_lang'] = $id_lang;
        $available_lang = $this->model_admin_all->GetAll('kg_config_language',array("is_active"=>"where/Active","id <>"=>"where/".$id_lang));
        $lang_row = $available_lang->row_array();
        $other_lang = $lang_row['id'];
      }
    
    if($id > 0)
    {
      $GetList = $this->model_admin_all->GetById($this->tabel,$id,$id_lang);
      if($GetList->num_rows() > 0)
      {
          $data['modify_user_id'] = $webmaster_id;
          if($id_lang) $this->db->where("id_lang", $id_lang);
          $this->db->where($this->id_primary, $id);
          $result = $this->db->update($this->tabel, $data);
          //Admin Log
          $logs = $this->db->last_query();
          $this->model_admin_all->LogActivities($webmaster_id,$this->tabel,$id,$logs,lang($this->filename),$data[$this->title_table],$this->filename,"Update");

           if($this->input->post('input_bahasa') == 1){

            $data['modify_user_id'] = $webmaster_id;
            $data['id_lang'] = $other_lang;

            $this->db->where("id_lang", $other_lang);
            $this->db->where($this->id_primary, $id);
            
            $result = $this->db->update($this->tabel, $data);
            
              //Admin Log
              $logs = $this->db->last_query();
              $this->model_admin_all->LogActivities($webmaster_id,$this->tabel,$this->db->insert_id(),$logs,lang($this->filename),$data[$this->title_table],$this->filename,"Add");      
            }
        if($result){
          $this->db->cache_delete_all();
        }
        $this->session->set_flashdata("message", lang("edit")." ".lang($this->filename)." ".lang("msg_sukses"));
      }
      else
      {
        $data[$this->id_primary] = $id;
        $data['create_user_id'] = $webmaster_id;
        $data['create_date'] = $data['modify_date'];
        $result = $this->db->insert($this->tabel, $data);
        
        
        //Admin Log
        $logs = $this->db->last_query();
        $this->model_admin_all->LogActivities($webmaster_id,$this->tabel,$this->db->insert_id(),$logs,lang($this->filename),$data[$this->title_table],$this->filename,"Add");
        
        if($result){
          $this->db->cache_delete_all();
        }

        $this->session->set_flashdata("message", lang('add')." ".lang($this->filename)." ".lang('msg_sukses'));
      }
    }
    else
    {

       $data['create_user_id'] = $webmaster_id;
         $data['create_date'] = $data['modify_date'];
         $result = $this->db->insert($this->tabel, $data);

         $id = $this->db->insert_id();
         //Admin Log
         $logs = $this->db->last_query();
         $this->model_admin_all->LogActivities($webmaster_id,$this->tabel,$this->db->insert_id(),$logs,lang($this->filename),$data[$this->title_table],$this->filename,"Add");
         
        if($this->input->post('input_bahasa') == 1){
            $data['create_user_id'] = $webmaster_id;
            $data['create_date'] = $data['modify_date'];
            $data['id_lang'] = $other_lang;
            $data['id'] = $id;
            $result = $this->db->insert($this->tabel, $data);

            $id = $this->db->insert_id();
            //Admin Log
            $logs = $this->db->last_query();
            $this->model_admin_all->LogActivities($webmaster_id,$this->tabel,$this->db->insert_id(),$logs,lang($this->filename),$data[$this->title_table],$this->filename,"Add");      
        }

      if($result){
        $this->db->cache_delete_all();
      }

      $this->session->set_flashdata("message", lang('add')." ".lang($this->filename)." ".lang('msg_sukses'));
    }
    
    if($this->input->post("stay")) ciredirect('webmaster/'.$this->filename.'/main/'.$id);
    else ciredirect('webmaster/'.$this->filename);
  }
}
?>