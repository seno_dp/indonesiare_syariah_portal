<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*************************************
  * Created : Dec 2011
  * Creator : Mazhters Irwan
  * Email   : irwansyah@komunigrafik.com
  * CMS ver : CI ver.2.0
*************************************/
require_once 'mz_function.php';
class mz_forbiden extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
	
	function set_include()
	{
		$mz_function = new mz_function();
		$data = $mz_function->header_footer();
		
		return $data;	
	}
	
	function index()
	{
		$this->forbiden();
	}
	
	function forbiden()
	{
		$data = $this->set_include();
		$this->load->view('webmaster/template_forbiden',$data);
	}
	
}
?>