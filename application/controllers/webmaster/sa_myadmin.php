<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*************************************
  * Created : Dec 2011
  * Update  : Jan 2012
  * Creator : Mazhters Irwan
  * Email   : irwansyah@komunigrafik.com
  * CMS ver : CI ver.2.0
*************************************/

require_once 'mz_function.php';
class sa_myadmin extends CI_Controller {
	
	var $filename = "sa_myadmin";
	var $tabel = "config_language";
	var $id_primary = "id";
	var $title_table = "title";
	
	function __construct()
	{
		parent::__construct();
		$this->load->dbutil();
		$this->load->dbforge();
	}
	
	function set_include()
	{
		$mz_function = new mz_function();
		$mz_function->auth_menu();
		$data = $mz_function->header_footer();
		
		return $data;	
	}
	
	function auth()
	{
		$webmaster_id = $this->session->userdata("webmaster_id");
		if(!$webmaster_id) ciredirect('webmaster/mz_login');
		return $webmaster_id;
	}
	
	function index()
	{
		$this->main();
	}

	function backup()
	{
		$checked = $this->input->post('options');
		if($checked){
			$myArray = array();
			$tblname = "";
			for ($i = 0; $i < count($checked); $i++)
			{
			    $myArray[] = $checked[$i];
			}

			if($this->input->post('backupbtn')){
				$prefs = array(
		                'tables'      => $myArray,  // Array of tables to backup.
		                'ignore'      => array(),           // List of tables to omit from the backup
		                'format'      => 'txt',             // gzip, zip, txt
		                'filename'    => 'mybackup.sql',    // File name - NEEDED ONLY WITH ZIP FILES
		                'add_drop'    => TRUE,              // Whether to add DROP TABLE statements to backup file
		                'add_insert'  => TRUE,              // Whether to add INSERT data to backup file
		                'newline'     => "\n"               // Newline character used in backup file
		              ); 
		      	$backup =& $this->dbutil->backup($prefs);
				write_file(base_url().'assets/theme/img/mybackup.txt', $backup);
				$this->load->helper('download');
				force_download(date('Ymdhis',now()).'.'.$this->db->database.'.sql', $backup);
				
			}elseif($this->input->post('deletebtn')){
				
				for ($i = 0; $i < count($checked); $i++)
				{
					if($this->db->query('DROP TABLE IF EXISTS '.$checked[$i])){
						if($i != count($checked)){
					    	$tblname .= $checked[$i].',';
					    }else{
					    	$tblname .= $checked[$i];
					    }
					}else{
						$tblname .= "Failed";
					}
				}
				$this->session->set_flashdata("message", "DROP Table/tables ".$tblname);
				ciredirect('webmaster/sa_myadmin');
			}elseif($this->input->post('emptybtn')){
				for ($i = 0; $i < count($checked); $i++)
				{
					if($this->db->query('TRUNCATE '.$checked[$i])){
						if($i != count($checked)){
					    	$tblname .= $checked[$i].',';
					    }else{
					    	$tblname .= $checked[$i];
					    }
					}else{
						$tblname .= "Failed";
					}
				}
				$this->session->set_flashdata("message", "Empty Table/tables ".$tblname);
				ciredirect('webmaster/sa_myadmin');
			}else{

			}
		}else{
			$this->session->set_flashdata("message", "no selected table/tables, please try again");
			ciredirect('webmaster/sa_myadmin');
		}
	}
	
	function main()
	{
		//Set Global
		$mz_function = new mz_function();
		$data = $this->set_include();
		$data['table'] = $this->tabel;
		$data['title'] = lang($this->filename);
		$data['filename'] = $this->filename;
		$data['main_content'] = 'webmaster/mz_myadmin';
		
		if($this->dbutil->database_exists($this->db->database))
	      {
	      	$data['tables'] = $this->db->list_tables();
	      }else{
	        $data['tables'] = array('tables'=>'database tidak exist');
	      }
		
		$this->load->view('webmaster/template',$data);
	}
	
	function dosql()
	{
		//Set Global
		$mz_function = new mz_function();
		$data = $this->set_include();
		$data['val_button'] = 'Submit';
		$data['table'] = $this->tabel;
		$data['title'] = $data['val_button']." ".lang($this->filename);
		$data['filename'] = $this->filename;
		$data['main_content'] = 'webmaster/mz_sql';
		//End Global
		$data_input = array('name'=> 'dosql', 'id'=> 'dosql', 'cols'=> 20, 'rows'=> 10, 'class'=> 'span9');
		$typeinput = form_textarea($data_input);
		
		$data['list_input'] = "";
		$data['list_input'] .= "<div class='clearfix'><label class='search'>Run 1 SQL query on database ".$this->db->database."</label>";
		$data['list_input'] .= $typeinput;
		$data['list_input'] .= "</div>";
			
		$this->load->view('webmaster/template',$data);
	}
	
	function runsql()
	{
		$mz_function = new mz_function();
		$webmaster_id = $this->auth();

		$dosql = $this->input->post('dosql');
		
		if($dosql){
			if($this->db->query($dosql)){
				$this->session->set_flashdata("message", "Success run query: ".$dosql);
				ciredirect('webmaster/sa_myadmin/dosql');	
			}else{
				$this->session->set_flashdata("message", "Failed");
				ciredirect('webmaster/sa_myadmin/dosql');
			}

		}else{
			$this->session->set_flashdata("message", "No code to run");
			ciredirect('webmaster/sa_myadmin/dosql');
		}

		
	}
	
	
}
?>