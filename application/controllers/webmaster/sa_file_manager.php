<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*************************************
  * Created : Dec 2011
  * Update  : Jan 2012
  * Creator : Mazhters Irwan
  * Email   : irwansyah@komunigrafik.com
  * CMS ver : CI ver.2.0
*************************************/

require_once 'mz_function.php';
class sa_file_manager extends CI_Controller {
	
	function __construct()
	{
		parent::__construct();
	}
	
	function set_include()
	{
		$mz_function = new mz_function();
		$mz_function->auth_menu();
		$data = $mz_function->header_footer();
		
		return $data;	
	}
	
	function auth()
	{
		$webmaster_id = $this->session->userdata("webmaster_id");
		if(!$webmaster_id) ciredirect('webmaster/mz_login');
		return $webmaster_id;
	}
	
	function index()
	{
		$this->main();
	}
	
	function main($flag=1)
	{
		//Set Global
		$mz_function = new mz_function();
		$data = $this->set_include();
		$data['flag'] = $flag;
		$data['table'] = $data['filename'] = "";
		
		$sub_folder = "";
		if($this->uri->segment(5)) $sub_folder .= $this->uri->segment(5)."/";
		if($this->uri->segment(6)) $sub_folder .= $this->uri->segment(6)."/";
		if($this->uri->segment(7)) $sub_folder .= $this->uri->segment(7)."/";
		$data['sub_folder'] = $sub_folder;
		$data['main_content'] = 'webmaster/mz_file_manager';
		//End Global
		$this->load->view('webmaster/template_file_manager',$data);
	}
}
?>