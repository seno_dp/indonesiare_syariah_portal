<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*************************************
  * Created : Dec 2011
  * Update  : Jan 2012
  * Creator : Mazhters Irwan
  * Email   : irwansyah@komunigrafik.com
  * CMS ver : CI ver.2.0
*************************************/

require_once 'mz_function.php';
class article extends CI_Controller {
 
 var $filename = "article";
 var $tabel = "kg_article";
 var $id_primary = "id";
 var $title_table = "title";
 
 function __construct()
 {
  parent::__construct();
 }
 
 function set_include()
 {
  $mz_function = new mz_function();
  $mz_function->auth_menu();
  $data = $mz_function->header_footer();
  
  return $data; 
 }
 
 function auth()
 {
  $webmaster_id = $this->session->userdata("webmaster_id");
  if(!$webmaster_id) ciredirect('webmaster/mz_login');
  return $webmaster_id;
 }
 
 function index()
 {
  $this->main();
 }
 
 function main()
 {
  //Set Global
  $mz_function = new mz_function();
  $data = $this->set_include();
  $data['table'] = $this->tabel;
  $data['title'] = lang($this->filename);
  $data['filename'] = $this->filename;
  $data['main_content'] = 'webmaster/mz_grid';
  $GetConfig = $this->model_admin_all->GetConfig($this->tabel);
  asort($GetConfig);
  $GetAllConfig = $this->model_admin_all->GetAllConfig($this->tabel);
  if($GetAllConfig->num_rows() > 0)
  {
   foreach($GetAllConfig->result_array() as $r)
   {
    $GetToDetail = $r['to_detail'];
    $GetMultiLang = $r['is_multi_lang'];
    $GetSort = $r['is_sort'];
   }
  }
  else $GetToDetail=$GetMultiLang=$GetSort="";
  $data['sortable'] = $GetSort ? "sortable":"";
  //End Global
  
  //Search
  $path_paging = site_url("webmaster/".$this->filename."/main");
  $uri = 4;
  $arr_search=array();
  if($GetMultiLang) $arr_search["id_lang"] = "where/".$this->session->userdata("ses_id_lang");
  if($GetSort) $arr_search["sort"] = "order/asc";
  $data['search']="";
  foreach($GetConfig as $key=> $con)
  {
   if($con['search'] == 1)
   {
    //Cek Type Data Field
    $GetTypeData = $this->model_admin_all->GetTypeDataByField($this->tabel,$key);
    if(preg_match("/enum/i",$GetTypeData))
    {
     $data['opt_'.$key][] = "-- Select Value --";
     $exp = explode("'",$GetTypeData);
     for($i=1;$i< count($exp);$i+=2)
     {
      $data['opt_'.$key][$exp[$i]] = $exp[$i];
     }
     $condition = "where";
    }
    else if(preg_match("/int/i",$GetTypeData)) $condition = "where";
    else $condition = "like";
    
    //Set Element Search
    $data['s_'.$key] = (strlen($this->uri->segment($uri)) > 1) ? urldecode(substr($this->uri->segment($uri),1)) : '';
    $path_paging .= "/s".substr($this->uri->segment($uri++),1);
    $arr_search[$key] = $condition."/".$data['s_'.$key];
    
    //Set Form Search
    $alias = !empty($con['alias']) ? $con['alias'] : $key;
    $data['search'] .= "<div class='clearfix'><label class='search'>".$alias."</label>";
    if(preg_match("/enum/i", $GetTypeData))
    {
     $data['search'] .= form_dropdown('s_'.$key, $data['opt_'.$key], isset($data['s_'.$key]) ? $data['s_'.$key] : '', "class='span4'"); 
    }
    else if($con['related'])
    {
     $opt_list[] = "-- Select Value --";
     if($GetMultiLang){
      $Opt = $this->model_admin_all->GetAll($con['related'],array('id_lang'=>"where/".$this->session->userdata('ses_id_lang')));
     }else{
      $Opt = $this->model_admin_all->GetAll($con['related']);
     }
     
     foreach($Opt->result_array() as $as_opt)
     {
      $opt_list[$as_opt['id']] = $as_opt[$con['title_related']];
     }
     $data['search'] .= form_dropdown('s_'.$key, $opt_list, isset($data['s_'.$key]) ? $data['s_'.$key] : '', "class='span4'");
    }
    else
    {
     $data_s = array('name'=> 's_'.$key, 'value'=> isset($data['s_'.$key]) ? $data['s_'.$key] : '', 'class'=> 'span4');
     $data['search'] .= form_input($data_s);
    }
    $data['search'] .= "</div>";
   }
  }
  $page_uri = $uri++;
  $uri_segment = $uri;
  $pg = $this->uri->segment($uri);
  if(!$this->uri->segment($page_uri) || !intval($this->uri->segment($page_uri))) $per_page=10;
  else $per_page=$this->uri->segment($page_uri);
  $data['pg'] = $pg;
  $data['per_page'] = $per_page;
  $data['path_per_paging'] = $path_paging;
  $path_paging .= "/".$per_page;
  //End Search
  
  //Grid
  $grid[] = "#";
  foreach($GetConfig as $key=> $con)
  {
   if($con['show'] == 1)
   {
    $grid[] = !empty($con['alias']) ? $con['alias'] : $key;
   }
  }
  $data['grid'] = $grid;
  //End Grid
  
  
  // List
  $arr_search["id"] = "order/desc"; 
  $GetQuery = $this->model_admin_all->GetAll($this->tabel,$arr_search);
  $AllRecord = $GetQuery->num_rows();
  
  $GetList = $this->model_admin_all->GetList($this->tabel,$arr_search,$pg,$per_page);
  $i=$k=$flag_width=0;$list="";
  foreach($GetList as $r)
  {
   $list .= "<li id='sort-".$r['id']."'><table><tr id='listz-".$r['id']."'>";
   $list .= "<td class='box_delete'><input type='checkbox' value='".$r['id']."' id='del".$r['id']."' class='delete'></td>";
   foreach($GetConfig as $key=> $con)
   {
    if($con['show'] == 1)
    {
     if($con['related'])
     {
      //$GetFieldTitle = $mz_function->get_value("field_title","config","tabel='".str_replace("kg_","",$con['related'])."'");
      //if(!$GetFieldTitle) $GetFieldTitle = "title";
      $related_val = $mz_function->get_value($con['title_related'],$con['related'],"id='".$r[$key]."'");
      if(!$related_val) $related_val = "#";
      if($GetToDetail == $key) $list .= "<td><a href='".site_url("webmaster/".$this->filename."/detail/".$r[$this->id_primary])."'>".$related_val."</a></td>";
      else $list .= "<td>".$related_val."</td>";
     }
     else
     {
      if($GetToDetail == $key) $list .= "<td><a href='".site_url("webmaster/".$this->filename."/detail/".$r[$this->id_primary])."'>".$r[$key]."</a></td>";
      else $list .= "<td>".$r[$key]."</td>";
     }
     if(!$flag_width) $k++;
    }
   }
   $list .= "</tr></table></li>";
   $i++;
   $flag_width=1;
  }
  if(!$k) $k=1;
  $w = 95/$k;
  $list .= "<style>.is_sort li table tr td, table.gridz tr th{width:$w%;}</style>";
  $data['list'] = $list;
  // End List 
  
  //Page
  $pagination = $mz_function->page($AllRecord,$per_page,$pg,$path_paging,$uri_segment);
  if(!$pagination) $pagination = "<strong>1</strong>";
  $data['pagination'] = $pagination;
  //End Page
  
  $this->load->view('webmaster/template',$data);
 }

 function detail($id=0)
  {
    //Set Global
    $mz_function = new mz_function();
    $data = $this->set_include();
    if($id > 0) $data['val_button'] = lang("edit");
    else $data['val_button'] = lang("add");
    $data['table'] = $this->tabel;
    $data['title'] = $data['val_button']." ".lang($this->filename);
    $data['filename'] = $this->filename;
    $data['main_content'] = 'webmaster/mz_detail';
    $GetMultiLang = $mz_function->get_value("is_multi_lang","config","tabel='".$this->tabel."'");
    $data['path_per_paging'] = "";
    //End Global
    
    $data['list_input'] = "";
    $GetConfig = $this->model_admin_all->GetConfig($this->tabel);
    if($id > 0)
    {
      $GetMultiLang = $mz_function->get_value("is_multi_lang","config","tabel='".$this->tabel."'");
      $id_lang = $GetMultiLang ? $this->session->userdata("ses_id_lang") : "";
      $GetList = $this->model_admin_all->GetById($this->tabel,$id,$id_lang);
      if($GetList->num_rows() > 0)
      {
        foreach($GetList->result_array() as $r)
        {
          //Set ID
          foreach($GetConfig as $key=> $con)
          {
            if($con['typeinput'])
            {
              $alias = !empty($con['alias']) ? $con['alias'] : $key;
              if($con['related'])
              {
                if($con['typeinput'] == "Label")
                $r[$key] = $mz_function->get_value($con['title_related'],$con['related'],"id='".$r[$key]."'");
              }
              $data['list_input'] .= "<div class='clearfix'><label class='search'>".$alias."</label>";
              $data['list_input'] .= $mz_function->typeinput($this->tabel, $key, $con['typeinput'], $con['related'], $r[$key], $con['title_related']);
              $data['list_input'] .= "</div>";
            }
            else $data['list_input'] .= $mz_function->typeinput($this->tabel, $key, "Hidden", $con['related'], $r[$key]);
          }
           if($id_lang != "" || $id_lang != NULL){
               $data_input = array('name'=> 'input_bahasa', 'value'=> 1, 'checked'=> false);
               $typeinput = form_checkbox($data_input);
               $data['list_input'] .= "<div class='clearfix'><label class='search'>".lang('update_other_language')."</label>";
               $data['list_input'] .= $typeinput;
               $data['list_input'] .= "</div>";
             }
        }
      }
      else
      {
        $data['val_button'] = lang("add");
        foreach($GetConfig as $key=> $con)
        {
          if($con['typeinput'])
          {
            $alias = !empty($con['alias']) ? $con['alias'] : $key;
            $data['list_input'] .= "<div class='clearfix'><label class='search'>".$alias."</label>";
            $data['list_input'] .= $mz_function->typeinput($this->tabel, $key, $con['typeinput'], $con['related']);
            $data['list_input'] .= "</div>";
          }
          else if($key == $this->id_primary) $data['list_input'] .= $mz_function->typeinput($this->tabel, $key, "Hidden", $con['related'], $id);
          else $data['list_input'] .= $mz_function->typeinput($this->tabel, $key, "Hidden");
        }
      }
    }
    else
    {
      foreach($GetConfig as $key=> $con)
      {
        if($con['typeinput'])
        {
          $alias = !empty($con['alias']) ? $con['alias'] : $key;
          $data['list_input'] .= "<div class='clearfix'><label class='search'>".$alias."</label>";
          $data['list_input'] .= $mz_function->typeinput($this->tabel, $key, $con['typeinput'], $con['related']);
          $data['list_input'] .= "</div>";
        }
        else $data['list_input'] .= $mz_function->typeinput($this->tabel, $key, "Hidden");
      }
        if($GetMultiLang != 0){
           $data_input = array('name'=> 'input_bahasa', 'value'=> 1, 'checked'=> false);
           $typeinput = form_checkbox($data_input);
           $data['list_input'] .= "<div class='clearfix'><label class='search'>".lang('input_other_language')."</label>";
           $data['list_input'] .= $typeinput;
           $data['list_input'] .= "</div>";
        }
    }
        
    $this->load->view('webmaster/template',$data);
  }
  
  function update()
  {
    $mz_function = new mz_function();
    $webmaster_id = $this->auth();
    $id = $this->input->post('id');
    $GetColumns = $this->model_admin_all->GetColumns($this->tabel);
    foreach($GetColumns as $r)
    {
      if($this->input->post($r['Field']."_file"))
      {
        if($_FILES[$r['Field']]['name'])
        {
          $data[$r['Field']] = $mz_function->input_file($r['Field']);
          if($data[$r['Field']] == "err_img_size")
          {
            $this->session->set_flashdata("message", lang('msg_err_img_size'));
            ciredirect('webmaster/'.$this->filename.'/detail/'.$id);
          }
          else if($data[$r['Field']] == "err_file_size")
          {
            $this->session->set_flashdata("message", lang('msg_err_file_size'));
            ciredirect('webmaster/'.$this->filename.'/detail/'.$id);
          }
          $file_old = $this->input->post($r['Field']."_file");
          if(file_exists("./".$this->config->item('path_upload')."/".$file_old)) unlink("./".$this->config->item('path_upload')."/".$file_old);
          
          $thumb = $mz_function->getThumb($file_old);
          if(file_exists("./".$this->config->item('path_upload')."/".$thumb)) unlink("./".$this->config->item('path_upload')."/".$thumb);
        }
      }
      else
      {
        if($this->input->post($r['Field']."_hidden"))
          {
            $textarea_old = $this->input->post($r['Field']."_hidden");
            $textarea_new = $this->input->post($r['Field']);
            
            if($id > 0){
              if(strlen($textarea_old) > 0 && strlen($textarea_new) == 0){
                $data[$r['Field']] = $textarea_new;          
              }elseif(strlen($textarea_old) > 0 && strlen($textarea_new) > 0){
                $data[$r['Field']] = $textarea_new;
              }else{
                $data[$r['Field']] = $textarea_old;
              }
            }

          }else{
            $data[$r['Field']] = $this->input->post($r['Field']);
            if(!$data[$r['Field']] && $data[$r['Field']] != "0") unset($data[$r['Field']]);  
          }

            /*$data[$r['Field']] = $this->input->post($r['Field']);
            if(!$data[$r['Field']] && $data[$r['Field']] != "0") unset($data[$r['Field']]);*/
      }
    }
    $data['modify_date'] = date("Y-m-d H:i:s");
    
    //Language
    $GetMultiLang = $mz_function->get_value("is_multi_lang","config","tabel='".$this->tabel."'");
    $id_lang = $GetMultiLang ? $this->session->userdata("ses_id_lang") : "";
    if($id_lang) {
        $data['id_lang'] = $id_lang;
        $available_lang = $this->model_admin_all->GetAll('kg_config_language',array("is_active"=>"where/Active","id <>"=>"where/".$id_lang));
        $lang_row = $available_lang->row_array();
        $other_lang = $lang_row['id'];
      }
    
    if($id > 0)
    {
      $GetList = $this->model_admin_all->GetById($this->tabel,$id,$id_lang);
      if($GetList->num_rows() > 0)
      {
          $data['modify_user_id'] = $webmaster_id;
          if($id_lang) $this->db->where("id_lang", $id_lang);
          $this->db->where($this->id_primary, $id);
          $result = $this->db->update($this->tabel, $data);
          //Admin Log
          $logs = $this->db->last_query();
          $this->model_admin_all->LogActivities($webmaster_id,$this->tabel,$id,$logs,lang($this->filename),$data[$this->title_table],$this->filename,"Update");

           if($this->input->post('input_bahasa') == 1){

            $data['modify_user_id'] = $webmaster_id;
            $data['id_lang'] = $other_lang;

            $this->db->where("id_lang", $other_lang);
            $this->db->where($this->id_primary, $id);
            
            $result = $this->db->update($this->tabel, $data);
            
              //Admin Log
              $logs = $this->db->last_query();
              $this->model_admin_all->LogActivities($webmaster_id,$this->tabel,$this->db->insert_id(),$logs,lang($this->filename),$data[$this->title_table],$this->filename,"Add");      
            }
        if($result){
          $this->db->cache_delete_all();
        }
        $this->session->set_flashdata("message", lang("edit")." ".lang($this->filename)." ".lang("msg_sukses"));
      }
      else
      {
        $data[$this->id_primary] = $id;
        $data['create_user_id'] = $webmaster_id;
        $data['create_date'] = $data['modify_date'];
        $result = $this->db->insert($this->tabel, $data);
        
        
        //Admin Log
        $logs = $this->db->last_query();
        $this->model_admin_all->LogActivities($webmaster_id,$this->tabel,$this->db->insert_id(),$logs,lang($this->filename),$data[$this->title_table],$this->filename,"Add");
        
        if($result){
          $this->db->cache_delete_all();
        }

        $this->session->set_flashdata("message", lang('add')." ".lang($this->filename)." ".lang('msg_sukses'));
      }
    }
    else
    {

       $data['create_user_id'] = $webmaster_id;
         $data['create_date'] = $data['modify_date'];
         $result = $this->db->insert($this->tabel, $data);

         $id = $this->db->insert_id();
         //Admin Log
         $logs = $this->db->last_query();
         $this->model_admin_all->LogActivities($webmaster_id,$this->tabel,$this->db->insert_id(),$logs,lang($this->filename),$data[$this->title_table],$this->filename,"Add");
         
        if($this->input->post('input_bahasa') == 1){
            $data['create_user_id'] = $webmaster_id;
            $data['create_date'] = $data['modify_date'];
            $data['id_lang'] = $other_lang;
            $data['id'] = $id;
            $result = $this->db->insert($this->tabel, $data);

            $id = $this->db->insert_id();
            //Admin Log
            $logs = $this->db->last_query();
            $this->model_admin_all->LogActivities($webmaster_id,$this->tabel,$this->db->insert_id(),$logs,lang($this->filename),$data[$this->title_table],$this->filename,"Add");      
        }

      if($result){
        $this->db->cache_delete_all();
      }

      $this->session->set_flashdata("message", lang('add')." ".lang($this->filename)." ".lang('msg_sukses'));
    }
    
    if($this->input->post("stay")) ciredirect('webmaster/'.$this->filename.'/detail/'.$id);
    else ciredirect('webmaster/'.$this->filename);
  }
 
 /*function detail($id=0)
 {
  //Set Global
  $mz_function = new mz_function();
  $data = $this->set_include();
  if($id > 0) $data['val_button'] = lang("edit");
  else $data['val_button'] = lang("add");
  $data['table'] = $this->tabel;
  $data['title'] = $data['val_button']." ".lang($this->filename);
  $data['filename'] = $this->filename;
  $data['main_content'] = 'webmaster/mz_detail';
  $GetMultiLang = $mz_function->get_value("is_multi_lang","config","tabel='".$this->tabel."'");
  $data['path_per_paging'] = "";
  //End Global
  
  $data['list_input'] = "";
  $GetConfig = $this->model_admin_all->GetConfig($this->tabel);
  if($id > 0)
  {
   $GetMultiLang = $mz_function->get_value("is_multi_lang","config","tabel='".$this->tabel."'");
   $id_lang = $GetMultiLang ? $this->session->userdata("ses_id_lang") : "";
   $GetList = $this->model_admin_all->GetById($this->tabel,$id,$id_lang);
   if($GetList->num_rows() > 0)
   {
    foreach($GetList->result_array() as $r)
    {
     //Set ID
     foreach($GetConfig as $key=> $con)
     {
      if($con['typeinput'])
      {
       $alias = !empty($con['alias']) ? $con['alias'] : $key;
       if($con['related'])
       {
        if($con['typeinput'] == "Label")
        $r[$key] = $mz_function->get_value($con['title_related'],$con['related'],"id='".$r[$key]."'");
       }
           $data['list_input'] .= "<div class='clearfix'><label class='search'>".$alias."</label>";
       $data['list_input'] .= $mz_function->typeinput($this->tabel, $key, $con['typeinput'], $con['related'], $r[$key], $con['title_related']);
       $data['list_input'] .= "</div>";
      }
      else $data['list_input'] .= $mz_function->typeinput($this->tabel, $key, "Hidden", $con['related'], $r[$key]);
     }
    }
   }
   else
   {
    $data['val_button'] = lang("add");
    foreach($GetConfig as $key=> $con)
    {
     if($con['typeinput'])
     {
      $alias = !empty($con['alias']) ? $con['alias'] : $key;
      $data['list_input'] .= "<div class='clearfix'><label class='search'>".$alias."</label>";
      $data['list_input'] .= $mz_function->typeinput($this->tabel, $key, $con['typeinput'], $con['related']);
      $data['list_input'] .= "</div>";
     }
     else if($key == $this->id_primary) $data['list_input'] .= $mz_function->typeinput($this->tabel, $key, "Hidden", $con['related'], $id);
     else $data['list_input'] .= $mz_function->typeinput($this->tabel, $key, "Hidden");
    }
   }
  }
  else
  {
   foreach($GetConfig as $key=> $con)
   {
    if($con['typeinput'])
    {
     $alias = !empty($con['alias']) ? $con['alias'] : $key;
     $data['list_input'] .= "<div class='clearfix'><label class='search'>".$alias."</label>";
     $data['list_input'] .= $mz_function->typeinput($this->tabel, $key, $con['typeinput'], $con['related']);
     $data['list_input'] .= "</div>";
    }
    else $data['list_input'] .= $mz_function->typeinput($this->tabel, $key, "Hidden");
   }
  }
    
  $this->load->view('webmaster/template',$data);
 }
 
 function update()
 {
  $mz_function = new mz_function();
  $webmaster_id = $this->auth();
  $id = $this->input->post('id');
  $GetColumns = $this->model_admin_all->GetColumns($this->tabel);
  foreach($GetColumns as $r)
  {
   if($this->input->post($r['Field']."_file"))
   {
    if($_FILES[$r['Field']]['name'])
    {
     $data[$r['Field']] = $mz_function->input_file($r['Field']);
     if($data[$r['Field']] == "err_img_size")
     {
      $this->session->set_flashdata("message", lang('msg_err_img_size'));
      ciredirect('webmaster/'.$this->filename.'/detail/'.$id);
     }
     else if($data[$r['Field']] == "err_file_size")
     {
      $this->session->set_flashdata("message", lang('msg_err_file_size'));
      ciredirect('webmaster/'.$this->filename.'/detail/'.$id);
     }
     $file_old = $this->input->post($r['Field']."_file");
     if(file_exists("./".$this->config->item('path_upload')."/".$file_old)) unlink("./".$this->config->item('path_upload')."/".$file_old);
     
     $thumb = $mz_function->getThumb($file_old);
     if(file_exists("./".$this->config->item('path_upload')."/".$thumb)) unlink("./".$this->config->item('path_upload')."/".$thumb);
    }
   }
   else
   {
    $data[$r['Field']] = $this->input->post($r['Field']);
    if(!$data[$r['Field']] && $data[$r['Field']] != "0") unset($data[$r['Field']]);
   }
  }
  $data['modify_date'] = date("Y-m-d H:i:s");
  
  //Language
  $GetMultiLang = $mz_function->get_value("is_multi_lang","config","tabel='".$this->tabel."'");
  $id_lang = $GetMultiLang ? $this->session->userdata("ses_id_lang") : "";
  if($id_lang) $data['id_lang'] = $id_lang;
  
  if($id > 0)
  {
   $GetList = $this->model_admin_all->GetById($this->tabel,$id,$id_lang);
   if($GetList->num_rows() > 0)
   {
    $data['modify_user_id'] = $webmaster_id;
    if($id_lang) $this->db->where("id_lang", $id_lang);
    $this->db->where($this->id_primary, $id);
    $result = $this->db->update($this->tabel, $data);
    //Admin Log
    $logs = $this->db->last_query();
    $this->model_admin_all->LogActivities($webmaster_id,$this->tabel,$id,$logs,lang($this->filename),$data[$this->title_table],$this->filename,"Update");
    
    if($result){
     $this->db->cache_delete_all();
    }
    $this->session->set_flashdata("message", lang("edit")." ".lang($this->filename)." ".lang("msg_sukses"));
   }
   else
   {
    $data[$this->id_primary] = $id;
    $data['create_user_id'] = $webmaster_id;
    $data['create_date'] = $data['modify_date'];
    $result = $this->db->insert($this->tabel, $data);
    
    
    //Admin Log
    $logs = $this->db->last_query();
    $this->model_admin_all->LogActivities($webmaster_id,$this->tabel,$this->db->insert_id(),$logs,lang($this->filename),$data[$this->title_table],$this->filename,"Add");
    
    if($result){
     $this->db->cache_delete_all();
    }

    $this->session->set_flashdata("message", lang('add')." ".lang($this->filename)." ".lang('msg_sukses'));
   }
  }
  else
  {
   $data['create_user_id'] = $webmaster_id;
   $data['create_date'] = $data['modify_date'];
   $result = $this->db->insert($this->tabel, $data);

   $id = $this->db->insert_id();
   //Admin Log
   $logs = $this->db->last_query();
   $this->model_admin_all->LogActivities($webmaster_id,$this->tabel,$this->db->insert_id(),$logs,lang($this->filename),$data[$this->title_table],$this->filename,"Add");
   
   if($result){
    $this->db->cache_delete_all();
   }

   $this->session->set_flashdata("message", lang('add')." ".lang($this->filename)." ".lang('msg_sukses'));
  }
  
  if($this->input->post("stay")) ciredirect('webmaster/'.$this->filename.'/detail/'.$id);
  else ciredirect('webmaster/'.$this->filename);
 }*/


 
 function search()
 {
  $path_search = 'webmaster/'.$this->filename.'/main';
  $GetConfig = $this->model_admin_all->GetConfig($this->tabel);
  asort($GetConfig);
  foreach($GetConfig as $key=> $con)
  {
   if($con['search'] == 1)
   {
    $path_search .= "/s".$this->input->post('s_'.$key);
   }
  }
  ciredirect($path_search);
 }
 
 function delete()
 {
  $mz_function = new mz_function();
  $webmaster_id = $this->auth();
  $data=array();
  
  $exp = explode("-",$this->input->post('del_id'));
  foreach($exp as $r)
  {
   if($r)
   {
    $data[]=$r;
    //Admin Log
    $logs = "DELETE from ".$this->tabel." where id='".$r."'";
    $this->model_admin_all->LogActivities($webmaster_id,$this->tabel,$r,$logs,lang($this->filename),'',$this->filename,"Delete");
   }
  }
  
  $GetMultiLang = $mz_function->get_value("is_multi_lang","config","tabel='".$this->tabel."'");
  $id_lang = $GetMultiLang ? $this->session->userdata("ses_id_lang") : "";
  if($id_lang) $this->db->where("id_lang",$this->session->userdata("ses_id_lang"));
  
  $this->db->where_in($this->id_primary, $data);
  $result = $this->db->delete($this->tabel);
  if($result){
   $this->db->cache_delete_all();
  }
  $this->session->set_flashdata("message", lang('delete')." ".count($data)." ".lang($this->filename)." ".lang('msg_sukses'));
 }
}
?>