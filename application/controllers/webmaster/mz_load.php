<?php
require_once 'mz_function.php';
class mz_load extends CI_Controller {

	function __construct()
	{
		parent::__construct();	
		$this->session = new CI_Session();
		$this->db = $this->load->database("default",TRUE);
		$this->pagination = new CI_Pagination();
		$this->load = new CI_Loader();
	}
	
	function auth()
	{
		$webmaster_id = $this->session->userdata("webmaster_id");
		if(!$webmaster_id) ciredirect('webmaster/login');
		return $webmaster_id;
	}
	
	function list_column()
	{
		$tbl = $this->input->post("tbl");
		$field = $this->input->post("field");
		$query = $this->model_admin_all->GetColumns($tbl);
		$list_column = "<select name='title_related_".$field."' class='span4'>";
		foreach($query as $r)
		{
			$val = $r['Field'];
			$list_column .= "<option value='".$val."'>".$val."</option>";
		}
		$list_column .= "</select>";
		echo $list_column;
	}
	
	function delete_image()
	{
		$webmaster_id = $this->auth();
		$mz_function = new mz_function();
		$id = $this->input->post('del_id_img');
		$table = $this->input->post('del_table');
		$field = $this->input->post('del_field');
		
		$GetMultiLang = $mz_function->get_value("is_multi_lang","config","tabel='".$table."'");
		$id_lang = $GetMultiLang ? $this->session->userdata("ses_id_lang") : "";
		if($id_lang) $GetFile = $mz_function->get_value($field,$table, "id='".$id."' AND id_lang='".$id_lang."'");
		else $GetFile = $mz_function->get_value($field,$table, "id='".$id."'");
		$GetThumb = $mz_function->getThumb($GetFile);
		if(file_exists("./".$this->config->item('path_upload')."/".$GetFile)) unlink("./".$this->config->item('path_upload')."/".$GetFile);
		if(file_exists("./".$this->config->item('path_upload')."/".$GetThumb)) unlink("./".$this->config->item('path_upload')."/".$GetThumb);
		
		$data[$field] = "";
		if($id_lang) $this->db->where("id_lang", $id_lang);
		$this->db->where("id", $id);
		$result = $this->db->update($table, $data);

		if($result){
				$this->db->cache_delete_all();
			}
	}
	
	function change_lang()
	{
		$mz_function = new mz_function();
		$id_lang = $this->input->post('id_lang');
		$getLang = $mz_function->get_value("alias","config_language","id='".$id_lang."'");
		$this->session->set_userdata("ses_id_lang",$id_lang);
		$this->session->set_userdata("ses_lang",$getLang);
	}
	
	function sortz($tbl)
	{
		$urutan = $this->input->post('sortz');
		$pg = $this->input->post('pg');
		$exp = explode(",",$urutan);
		foreach($exp as $r)
		{
			$pg++;
			if($r)
			{
				$id = str_replace("sort-","",$r);
				$data = array("sort"=> $pg);
				$this->db->where("id", $id);
				$result = $this->db->update($tbl, $data);
			}
		}
		if($result){
				$this->db->cache_delete_all();
			}
	}
	
	function load_ace_frame($nmfile)
	{
		$data['nmfile'] = $nmfile;
		$this->load->view('webmaster/mz_ace_frame',$data);
	}
	
	function load_ace($nmfile)
	{
		$dir = $this->session->userdata("path_filemanager");
		$filez = fopen($dir.$nmfile,'r');
		$size = filesize($dir.$nmfile);
		$baca = fread($filez,$size);
		
		$data['nmfile'] = $nmfile;
		$data['content'] = htmlentities($baca);
		$this->load->view('webmaster/mz_ace',$data);
	}
	
	function save_ace($nmfile)
	{
		$dir = $this->session->userdata("path_filemanager");
		$open = fopen($dir.$nmfile,'w');
		$contents = $_POST['contents'];
		$contents = html_entity_decode($contents);
		$contents = htmlentities($contents);
		//$contents = htmlentities($contents);
		//$contents = str_replace("&Acirc;","",$contents);
		//$contents = str_replace("&nbsp;","",$contents);
		$contents = html_entity_decode($contents);
		//$contents = str_replace("&lt;","<",$contents);
		//$contents = str_replace("&gt;",">",$contents);
		fwrite($open,$contents);
		fclose($open);
	}
	
	function rename_file()
	{
		$nmfile = $this->input->post("nm_file");
		$nmfile_old = $this->input->post("nm_file_old");
		$dir = $this->session->userdata("path_filemanager");
		rename($dir.$nmfile_old, $dir.$nmfile);
	}
	
	function copy_file()
	{
		$nmfile = $this->input->post("nm_file");
		$nmfile_old = $this->input->post("nm_file_old");
		$dir = $this->session->userdata("path_filemanager");
		copy($dir.$nmfile_old, $dir.$nmfile);
	}
	
	function delete_file()
	{
		$nmfile = $this->input->post("nm_file");
		$dir = $this->session->userdata("path_filemanager");
		if(file_exists($dir.$nmfile)) unlink($dir.$nmfile);
	}
}
?>