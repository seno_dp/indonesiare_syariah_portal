<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*************************************
  * Created             : Dec 2011
  * Update              : Jan 2012
  * Creator             : Mazhters Irwan
  * Colaborator         : Andi Galuh Sutrisno
  * Email               : irwansyah@komunigrafik.com
  * Email Colaborator   : andi@komunigrafik.com
  * CMS ver             : CI ver.3.0
*************************************/

require_once 'mz_function.php';
class multiupload_foto extends CI_Controller {
 
 var $filename = "multiupload_foto";
 var $tabel = "table_default";
 var $id_primary = "id";
 var $title_table = "title";
 
 function __construct()
 {
  parent::__construct();
 }
 
 function set_include()
 {
  $mz_function = new mz_function();
  $mz_function->auth_menu();
  $data = $mz_function->header_footer();
  
  return $data; 
 }
 
 function auth()
 {
  $webmaster_id = $this->session->userdata("webmaster_id");
  if(!$webmaster_id) ciredirect('webmaster/mz_login');
  return $webmaster_id;
 }
 
 function index()
 {
  ciredirect('webmaster/multiupload_foto/detail/0');
  //$this->detail(0);
 }
 
 function detail($id=0,$id_album=1)
 {
  //Set Global
  $mz_function = new mz_function();
  $data = $this->set_include();
  if($id > 0) $data['val_button'] = lang("edit");
  else $data['val_button'] = lang("add");
  $data['table'] = $this->tabel;
  $data['title'] = $data['val_button']." ".lang($this->filename);
  $data['filename'] = $this->filename;
  $data['main_content'] = 'webmaster/multiupload';
      
  $this->load->view('webmaster/template',$data);
 }
 
 public function do_upload()
    {

      if(is_array($_FILES['files']['name'])){
        
        $this->load->library('upload');

        $this->upload->initialize(
          array(
            "upload_path"   => "./".$this->config->item('path_upload')."/",
            "allowed_types" => '*',
            "max_size" => '10000000000'
          ));

       // die($this->upload->do_multi_upload("files"));
        
        //Perform upload.
        if($this->upload->do_multi_upload("files")){
            //Code to run upon successful upload.
            //print_r($this->upload->get_multi_upload_data());
            $data = $this->upload->get_multi_upload_data();
            foreach ($data as $key0=>$value0) {
              //echo '<br/>';
              foreach ($value0 as $key1 => $value1) {
                $caption = $this->input->post('caption');
                //die($caption[$key0]);
                //die(print_mz($this->input->post('caption')));
                //echo $key1.' = '.$value1;
                //echo '<br/>';
                /*$this->load->library('image_lib');
                $file_up = $_FILES[$key]['name'];
                $file_up = date("YmdHis").".".str_replace("-","_",url_title($file_up));
                $myfile_up  = $_FILES[$key]['tmp_name'];
                $ukuranfile_up = $_FILES[$key]['size'];
                $up_file = "./".$this->config->item('path_upload')."/".$file_up;*/


                $GetAll = $this->model_admin_all->GetAll("config_global");
                $config = $GetAll->result_array();
                $getConfig = $config[0];
                
                $this->load->library('image_lib');
                $config['image_library'] = 'gd2';
                $config['source_image'] = "./".$this->config->item('path_upload')."/".$value0['file_name'];
                $config['dest_image'] = "./".$this->config->item('path_upload')."/";
                $config['create_thumb'] = TRUE;
                $config['maintain_ratio'] = TRUE; //Width=Height
                $config['height'] = $getConfig['img_thumb_h'];
                $config['width'] = $getConfig['img_thumb_w'];
                
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $this->image_lib->clear();


              }

              $data = array(
                   'id_foto_album' => $this->input->post('id_album'),
                   'image' => $value0['file_name'],
                   'title' => $caption[$key0],
                   //'title' => $value0['file_name'],
                   'is_publish' => 'publish',
                   'create_date' => date('Y-m-d H:i:s'),
                   'create_user_id' => $this->session->userdata("webmaster_id")
                );

                $this->db->insert('kg_foto', $data);
              
            }
            
            //print_r($data);
            //die('data ke satu : '.$data[0])
          $this->session->set_flashdata("message", "file berhasil terupload");
          ciredirect('webmaster/foto_album');
        }else{
             //die($this->upload->display_errors('<p>', '</p>'));
            $this->session->set_flashdata("message", $this->upload->display_errors());
            ciredirect('webmaster/foto_album');
        }
      }
    
  }

 
}
?>