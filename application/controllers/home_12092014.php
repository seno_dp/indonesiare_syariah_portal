<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*************************************

  * Created : June 2013

  * Update  : June 2013

  * Creator : Andi Galuh S

  * Email   : andi@komunigrafik.com

*************************************/



class Home extends CI_Controller  {

	

	public function __construct()

	{

		parent::__construct();

	}


//die('error');
	function index()

	{

		if((strpos($this->input->ip_address(),'127.0.0.1') !== false) or (strpos($this->input->ip_address(),'192.168.') !== false) or ($this->session->userdata('user_id_sess'))) 

		{

    

			$data = GetHeaderFooter(1);

			$data['main_content'] = 'home';



			$slide_config = tableconfig("kg_slider");

			$slide_limit = $slide_config['item_homepage'];

			$filterslide = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/publish","limit"=>"0/".$slide_limit,"id"=>"order/desc");

			$data['slide'] = GetAll("kg_slider",$filterslide);



			$analisis_config = tableconfig("kg_analisis");

			$analisis_limit = $analisis_config['item_homepage'];

			$filteranalisis = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/publish","limit"=>"0/".$analisis_limit,"id"=>"order/desc");

			$data['analisis'] = GetAll("kg_analisis",$filteranalisis);



			$event_config = tableconfig("kg_event");

			$event_limit = $event_config['item_homepage'];

			$filterevent = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/publish","limit"=>"0/".$event_limit,"id"=>"order/desc");

			$data['event'] = GetAll("kg_event",$filterevent);



			$article_config = tableconfig("kg_article");

			$article_limit = $article_config['item_homepage'];

			$module_detail = "absence/download/";

			$filterarticle = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/publish","limit"=>"0/".$article_limit,"create_date"=>"order/desc");

			$data['article'] = GetAll("kg_view_latest",$filterarticle);



			$calendar_config = tableconfig("kg_calendar");

			$calendar_limit = $calendar_config['item_homepage'];

			$filtercalendar = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/publish","date_calendar >="=>"where/".date('Y-m-d',now()),"limit"=>"0/".$calendar_limit,"date_calendar"=>"order/asc");

			$data['calendar'] = GetAll("kg_calendar",$filtercalendar);



			$announcement_config = tableconfig("kg_announcement");

			$announcement_limit = $announcement_config['item_homepage'];

			$filterannouncement = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/publish","limit"=>"0/".$announcement_limit,"id"=>"order/desc");

			$data['announcement'] = GetAll("kg_announcement",$filterannouncement);



			$meeting_config = tableconfig("kg_meeting_room");

			$meeting_limit = $meeting_config['item_homepage'];

			$filtermeeting_room = array("kg_meeting_room.id_lang"=>"where/".GetIdLang(),"kg_meeting_room.is_publish"=>"where/publish","kg_meeting_room.date_meeting >="=>"where/".date('Y-m-d',now()),"limit"=>"0/".$meeting_limit,"kg_meeting_room.date_meeting"=>"order/desc");

			$data['meeting'] = GetJoin("kg_meeting_room","kg_ref_meeting_room","kg_ref_meeting_room.id = kg_meeting_room.ref_meeting_room","","kg_meeting_room.*,kg_ref_meeting_room.*",$filtermeeting_room);

			//echo $this->db->last_query();

			$absence_config = tableconfig("kg_absence");

			$absence_limit = $absence_config['item_homepage'];



			$date = date('Y-m-d H:i:s',now());

			$threedaysago = date('Y-m-d H:i:s',strtotime($date . "-30 days"));



			$filterabsence = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/publish","date_absence >="=>"where/".$threedaysago,"limit"=>"0/".$absence_limit,"id"=>"order/desc");

			$data['absence'] = GetAll("kg_absence",$filterabsence);



			$blog_config = tableconfig("kg_blog");

			$blog_limit = $blog_config['item_homepage'];

			$filterblog = array("is_publish"=>"where/Publish","id"=>"order/desc","limit"=>"0/".$blog_limit,);

			$data['blog'] = GetAll('kg_view_blog',$filterblog);

			

			$this->load->view('layout',$data);

		}else{



			if($this->session->userdata('user_id_sess')){

				ciredirect('home');

			}else{

				ciredirect('member/login');

			}



		}

	}



	function keyword(){

		if((strpos($this->input->ip_address(),'127.0.0.1') !== false) or (strpos($this->input->ip_address(),'192.168.') !== false) or ($this->session->userdata('user_id_sess'))) 

		{

    		$keyword = $this->input->post('s');

			//ciredirect(site_url('searching/keyword/'.url_title($keyword,'underscore')));

			ciredirect(site_url('home/search/'.url_title($keyword,'underscore')));

		}else{



			if($this->session->userdata('user_id_sess')){

				ciredirect('home');

			}else{

				ciredirect('member/login');

			}



		}

	}



	function search($keyword){

		if((strpos($this->input->ip_address(),'127.0.0.1') !== false) or (strpos($this->input->ip_address(),'192.168.') !== false) or ($this->session->userdata('user_id_sess'))) 

		{

    

			$data = GetHeaderFooter(1);

			$data['main_content'] = 'search';



			$data['keyword'] = $keyword = str_replace("_"," ", $keyword);



			$q_search = "SELECT * FROM kg_view_searching where is_publish = 'publish' and id_lang = ".GetIdLang()." and ( tags like '%".$keyword."%' or title like '%".$keyword."%') order by create_date desc ";

			$data['search'] = $this->db->query($q_search);



			/*$filter = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/publish","create_date"=>"order/desc");

			$data['search'] = GetAll("kg_view_searching",$filter);

			die($this->db->last_query());*/

			

			

			$this->load->view('layout',$data);

		}else{



			if($this->session->userdata('user_id_sess')){

				ciredirect('home');

			}else{

				ciredirect('member/login');

			}



		}

	}

}

// END Home Class