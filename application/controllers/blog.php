<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Blog extends CI_Controller {

	function index()
	{
		$this->listing();
	}

	function listing(){

		if((strpos($this->input->ip_address(),'192.168.') !== false) or ($this->session->userdata('user_id_sess'))) 
		{
			$data = GetHeaderFooter(1);
			$data['main_content'] = 'blogroll';
			$data['menu_title'] = 'Blog';
			$data['controller_name'] = 'blog';

			$GetAllConfig = $this->model_admin_all->GetAllConfig('kg_'.$data['controller_name']);
			if($GetAllConfig->num_rows() > 0)
			{
				$r = $GetAllConfig->row_array();
				$per_page = $r['per_page'];
			}

			$uri_segment = 4;

			$awal = $this->uri->segment($uri_segment);

			$filterfeat = array("is_publish"=>"where/Publish","id"=>"order/desc","limit"=> "0/1");

			$data['feat'] = $feat = GetAll('kg_view_'.$data['controller_name'],$filterfeat);

			if($feat->num_rows() > 0){
				$vfeat = $feat->row_array();
				$idfeat = $vfeat['id'];
			}else{
				$idfeat = 0;
			}

			$filter = array("is_publish"=>"where/Publish","id !="=>"where/".$idfeat,"id"=>"order/desc","limit"=> $awal."/".$per_page);
			$data['qp'] = GetAll('kg_view_'.$data['controller_name'],$filter);

			$filterrecent = array("is_publish"=>"where/Publish","id"=>"order/desc","limit"=> "0/5");
			$data['recent'] = GetAll('kg_view_'.$data['controller_name'],$filterrecent);

			$filterpopular = array("is_publish"=>"where/Publish","views"=>"order/desc","limit"=> "0/5");
			$data['popular'] = GetAll('kg_view_'.$data['controller_name'],$filterpopular);

			$filtermostcomm = array("is_publish"=>"where/publish","id"=>"order/desc");
			$data['mostcomm'] = GetAll('kg_view_comment',$filtermostcomm);

			$filter2 = array("is_publish"=>"where/Publish","id !="=>"where/".$idfeat,"id"=>"order/desc");
			$q = GetAll('kg_view_'.$data['controller_name'],$filter2);

			$path_paging = site_url($data['controller_name'].'/listing');

			$pagination = Page($q->num_rows(),$per_page,$awal,$path_paging,$uri_segment);

			if(!$pagination) $pagination = "";

			$data['pagination'] = $pagination;

			$this->load->view('layout',$data);
		}else{
			if($this->session->userdata('user_id_sess')){
				ciredirect('home');
			}else{
				ciredirect('member/login');
			}
		}

	}



	function my_profile(){

		/*if($this->session->userdata('user_id_sess')) {*/

		if((strpos($this->input->ip_address(),'192.168.') !== false) or ($this->session->userdata('user_id_sess'))) 

		{

			$data = GetHeaderFooter(1);

			$data['main_content'] = 'blog';

			$data['menu_title'] = 'Blog';

			$data['controller_name'] = 'blog';



			$GetAllConfig = $this->model_admin_all->GetAllConfig('kg_'.$data['controller_name']);

			if($GetAllConfig->num_rows() > 0)

			{

				$r = $GetAllConfig->row_array();

				$per_page = $r['per_page'];

			}



			$uri_segment = 4;

			$awal = $this->uri->segment($uri_segment);

			

			$filter = array("is_publish"=>"where/Publish","create_user_id"=>"where/".$this->session->userdata('user_id_sess'),"id"=>"order/desc","limit"=> $awal."/".$per_page);

			$data['qp'] = GetAll('kg_view_'.$data['controller_name'],$filter);



			$filterrecent = array("is_publish"=>"where/Publish","id"=>"order/desc","limit"=> "0/5");

			$data['recent'] = GetAll('kg_view_'.$data['controller_name'],$filterrecent);



			$filterpopular = array("is_publish"=>"where/Publish","views"=>"order/desc","limit"=> "0/5");

			$data['popular'] = GetAll('kg_view_'.$data['controller_name'],$filterpopular);



			$filtermostcomm = array("is_publish"=>"where/publish","id"=>"order/desc");

			$data['mostcomm'] = GetAll('kg_view_comment',$filtermostcomm);

			

			$filter2 = array("is_publish"=>"where/Publish","create_user_id"=>"where/".$this->session->userdata('user_id_sess'),"id"=>"order/desc");

			$q = GetAll('kg_view_'.$data['controller_name'],$filter2);



			$path_paging = site_url($data['controller_name'].'/listing');

			$pagination = Page($q->num_rows(),$per_page,$awal,$path_paging,$uri_segment);

			if(!$pagination) $pagination = "";

			$data['pagination'] = $pagination;



			$this->load->view('layout_blog',$data);

		}else{



			if($this->session->userdata('user_id_sess')){

				ciredirect('home');

			}else{

				ciredirect('member/login');

			}



		}

	}



	function post(){

		if($this->session->userdata('user_id_sess')) 

		{

		

			$data = GetHeaderFooter(1);

			$data['main_content'] = 'input_blog';

			$data['menu_title'] = 'Posting Blog';

			$data['controller_name'] = 'blog';



			$this->load->view('layout_blog',$data);

		

		}else{

			ciredirect('home');

		}

		

	}



	function edit($id=0){

		if($this->session->userdata('user_id_sess')) 

		{

			

			$data = GetHeaderFooter(1);

			$data['main_content'] = 'edit_blog';

			$data['menu_title'] = 'Edit Blog';

			$data['controller_name'] = 'blog';



			$filter = array("create_user_id"=>"where/".$this->session->userdata('user_id_sess'),"id"=>"where/".$id);

			$data['qp'] = GetAll('kg_view_'.$data['controller_name'],$filter);



			$this->load->view('layout_blog',$data);

		

		}else{

			ciredirect('home');

		}

		

	}



	function submit()

	{

		if($this->session->userdata('user_id_sess'))

		{

			$this->form_validation->set_rules('title', 'title', 'required');

			$this->form_validation->set_rules('tags', 'tags', 'required');

			

			if ($this->form_validation->run() == TRUE)

			{

				/*if($this->input->post('attachedfile'))

   				{*/

			    	if($_FILES['attachedfile']['name'])

			    	{

					    $data['attachedfile'] = input_file('attachedfile');

					    if($data['attachedfile'] == "err_img_size")

				     	{

						    $this->session->set_flashdata("message", "ukuran gambar melebihi kapasitas");

						    ciredirect('blog/post');

				     	}

				     	else if($data['attachedfile'] == "err_file_size")

				     	{

						    $this->session->set_flashdata("message", "ukuran file melebihi kapasitas");

						    ciredirect('blog/post');

				     	}





					    $file_old = $this->input->post('file_old');					   

					    if(file_exists(base_url().$this->config->item('path_upload')."/".$file_old)) unlink(base_url().$this->config->item('path_upload')."/".$file_old);

				    	

					    $thumb = getThumb($file_old);

					    if(file_exists(base_url().$this->config->item('path_upload')."/".$thumb)) unlink(base_url().$this->config->item('path_upload')."/".$thumb);

				    /*}*/

				}else{

					$data['attachedfile'] = "";

				}



				$datas = array(

					'id_member'	=> $this->session->userdata('user_id_sess'),	

					'id_blog_category'	=> 1,

					'image'	=> $data['attachedfile'],

					'title'	=> $this->input->post('title'),

					'headline'	=> $this->input->post('headline'),

					'content'	=> $this->input->post('editor1'),

					'tags'	=> $this->input->post('tags'),

					'is_publish'	=> 'publish',

					'create_date'	=> date("Y-m-d H:i:s"),

					'create_user_id'	=> $this->session->userdata('user_id_sess')

				);

				

				$this->db->insert('kg_blog', $datas);

				$id_last = $this->db->insert_id();



				$act = array(

					'id_member'	=> $this->session->userdata('user_id_sess'),

					'title' => 'Posting blog : '.$this->input->post('title'),

					'url' => 'blog/detail/'.$id_last.'/'.url_title($this->input->post('title')),

					'activity' => 'posting_blog',

					'create_date' => date("Y-m-d H:i:s"),

					'create_user_id'	=> $this->session->userdata('user_id_sess')

				);

				$this->db->insert('kg_member_activity', $act);



				$this->session->set_flashdata('message', '<p class="alert-message-success">Posting blog berhasil.</p>');

				ciredirect(site_url('blog/post'));

			}

			else

			{		

				$data = GetHeaderFooter(1);

				$data['main_content'] = 'input_blog';

				$data['menu_title'] = 'Posting Blog';

				$data['controller_name'] = 'blog';



				$this->load->view('layout_blog',$data);

			}

		}

		else

		{

			ciredirect(site_url('login'));

		}

	}



	function update()

	{

		if($this->session->userdata('user_id_sess'))

		{

			$this->form_validation->set_rules('title', 'title', 'required');

			$this->form_validation->set_rules('tags', 'tags', 'required');

			

			if ($this->form_validation->run() == TRUE)

			{

		    	if($_FILES['attachedfile']['name'])

		    	{

		    		if($this->input->post('file_old')){

			    		$file_old = $this->input->post('file_old');

			    		//if(file_exists("./".$this->config->item('path_upload')."/".$file_old)) unlink("./".$this->config->item('path_upload')."/".$file_old);

					    if(file_exists("./".$this->config->item('path_upload')."/".$file_old)) {

					    	unlink("./".$this->config->item('path_upload')."/".$file_old);

					    	//die('hapus file '.$this->input->post('file_old').' di '.base_url().$this->config->item('path_upload')."/".$file_old);

					    }//else{

					    	//die('gagal hapus file '.$this->input->post('file_old').' di '.base_url().$this->config->item('path_upload')."/".$file_old);

					    //}

				    	

					    $thumb = getThumb($file_old);

					    if(file_exists("./".$this->config->item('path_upload')."/".$thumb)) unlink("./".$this->config->item('path_upload')."/".$thumb);

					}



					    $data['attachedfile'] = input_file('attachedfile');

					    if($data['attachedfile'] == "err_img_size")

				     	{

						    $this->session->set_flashdata("message", "ukuran gambar melebihi kapasitas");

						    ciredirect('blog/edit/'.$this->input->post('id_blog'));

				     	}

				     	else if($data['attachedfile'] == "err_file_size")

				     	{

						    $this->session->set_flashdata("message", "ukuran file melebihi kapasitas");

						    ciredirect('blog/edit/'.$this->input->post('id_blog'));

				     	}



					    $datas = array(

							'id_member'	=> $this->session->userdata('user_id_sess'),	

							'id_blog_category'	=> 1,

							'image'	=> $data['attachedfile'],

							'title'	=> $this->input->post('title'),

							'headline'	=> $this->input->post('headline'),

							'content'	=> $this->input->post('editor1'),

							'tags'	=> $this->input->post('tags'),

							'is_publish'	=> 'publish',

							'modify_date'	=> date("Y-m-d H:i:s"),

							'modify_user_id'	=> $this->session->userdata('user_id_sess')

						);



				}else{

					$datas = array(

						'id_member'	=> $this->session->userdata('user_id_sess'),	

						'id_blog_category'	=> 1,

						'title'	=> $this->input->post('title'),

						'headline'	=> $this->input->post('headline'),

						'content'	=> $this->input->post('editor1'),

						'tags'	=> $this->input->post('tags'),

						'is_publish'	=> 'publish',

						'modify_date'	=> date("Y-m-d H:i:s"),

						'modify_user_id'	=> $this->session->userdata('user_id_sess')

					);

				}



				

				$this->db->where('id', $this->input->post('id_blog'));

				$this->db->update('kg_blog', $datas);



				$act = array(

					'id_member'	=> $this->session->userdata('user_id_sess'),

					'title' => 'Update blog : '.$this->input->post('title'),

					'url' => 'blog/detail/'.$this->input->post('id_blog').'/'.url_title($this->input->post('title')),

					'activity' => 'update_blog',

					'create_date' => date("Y-m-d H:i:s"),

					'create_user_id'	=> $this->session->userdata('user_id_sess')

				);

				$this->db->insert('kg_member_activity', $act);



				$this->session->set_flashdata('message', '<p class="alert-message-success">Edit blog berhasil.</p>');

				ciredirect(site_url('blog/edit/'.$this->input->post('id_blog')));

			}

			else

			{		

				$data = GetHeaderFooter(1);

				$data['main_content'] = 'input_blog';

				$data['menu_title'] = 'Posting Blog';

				$data['controller_name'] = 'blog';



				$this->load->view('layout_blog',$data);

			}

		}

		else

		{

			ciredirect(site_url('login'));

		}

	}



	function detail($id=1)

	{

		$data = GetHeaderFooter(1);

		

		$data['main_content'] = 'blog_detail';

		$data['controller_name'] = $controller_name = 'blog';

		

		$filter = array(

				"is_publish"=>"where/publish",

				"id"=>"where/".$id

		);



		$data['blog'] = $blog = GetAll('kg_view_'.$controller_name,$filter);



		$filtercomment = array("is_publish"=>"where/publish","id_blog"=>"where/".$id,"id"=>"order/desc");

		$data['comment'] = GetAll('kg_view_comment',$filtercomment);



		$filterrecent = array("is_publish"=>"where/Publish","id"=>"order/desc","limit"=> "0/5");

		$data['recent'] = GetAll('kg_view_'.$data['controller_name'],$filterrecent);



		$filterpopular = array("is_publish"=>"where/Publish","views"=>"order/desc","limit"=> "0/5");

		$data['popular'] = GetAll('kg_view_'.$data['controller_name'],$filterpopular);



		$filtermostcomm = array("is_publish"=>"where/publish","id"=>"order/desc");

		$data['mostcomm'] = GetAll('kg_view_comment',$filtermostcomm);



		if($blog->num_rows() > 0){

			$v = $data['blog']->row_array();



			$i = 0;

			$files = "";

			$tags = explode(',',$v['tags']);

			$numItems = count($tags);

			$result="";

			$files = "SELECT * FROM kg_view_".$controller_name." where id <> ".$v['id']. " and (";

			foreach($tags as $tag=>$val){

				if($val != ""){

					$files .= " tags like '%".$val."%'";

					if(++$i === $numItems){

				    	$files .= "";

					}else{

						$files .= " or ";

					}

				}else

				{

					$files .= " tags like '0'";

				}

			}

			$files .= ") limit 0,4";

		

			$this->session->set_userdata('detail_uri',current_url());

			$data['rel_link'] = $this->db->query($files);

		}else{

			ciredirect(site_url('error_404'));

		}

			

		$this->load->view('layout_blog',$data);

	}



	function comment()

	{

		

		if($this->input->post('comment') != NULL){

			$data = array(	

				'id_member'	=> $this->input->post('member_id'),

				'id_blog'	=> $this->input->post('blog_id'),

				'content'	=> $this->input->post('comment'),

				'is_publish' => 'publish',

				'create_user_id'	=> $this->input->post('member_id'),

				'create_date' => date('Y-m-d h:i:s',now())

			);



			$this->db->insert('kg_comment',$data);

			$id_last = $this->db->insert_id();



			$filterblog = array("id"=>"where/".$this->input->post('blog_id'));

			$dblog = GetAll('kg_view_blog',$filterblog);

			$vblog = $dblog->row_array();



			$act = array(

				'id_member'	=> $this->input->post('member_id'),

				'title' => 'Memberikan komentar pada '.$vblog['title'],

				'url' => 'blog/detail/'.$vblog['id'].'/'.url_title($vblog['title']),

				'activity' => 'comment',

				'create_date' => date("Y-m-d H:i:s"),

				'create_user_id'	=> $this->session->userdata('user_id_sess')

			);

			$this->db->insert('kg_member_activity', $act);



			//echo '<script>alert("Komentar anda akan dimoderasi terlebih dahulu")</script>';

			$filtercomment = array("is_publish"=>"where/publish","id_blog"=>"where/".$this->input->post('blog_id'),"id"=>"order/desc");

			$data['comment'] = GetAll('kg_view_comment',$filtercomment);



			$this->load->view('area_commenters',$data);

		}else{

			echo '<script>alert("silahkan masukkan komentar anda")</script>';

			$filtercomment = array("is_publish"=>"where/publish","id_blog"=>"where/".$this->input->post('blog_id'),"id"=>"order/desc");

			$data['comment'] = GetAll('kg_view_comment',$filtercomment);



			$this->load->view('area_commenters',$data);

		}

				

	}



	function download($id=NULL)

	{

		$data['controller_name'] = $controller_name = 'absence';

		//echo $this->session->userdata('publication_uri');

		$filter = array("id"=>"where/".$id,"id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/Publish");

		$data['list'] = $list = GetAll("kg_".$controller_name,$filter);



		if($list->num_rows() > 0){

			$r = $list->row_array();

			$downloads = $r['download'];

			$this->updatedownloads($id,'kg_'.$controller_name,$downloads);

			(strlen($r['uploaded_file']) > 0 && strlen($r['uploaded_file']) > 0) ? ciredirect(base_url().'uploads/'.$r['uploaded_file']) : ciredirect($this->session->userdata('detail_uri'));

		}

	}



	function updatedownloads($id,$table,$views){

		$this->load->helper('date');

		$views_curr = $views + 1;

		$this->db->where('id', $id);

		$this->db->where('id_lang', GetIdLang());

		$this->db->update($table,array('download' => $views_curr)); 



	}



	function updateviews($id,$table,$views){

		$this->load->helper('date');

		$views_curr = $views + 1;

		$this->db->where('id', $id);

		$this->db->where('id_lang', GetIdLang());

		$this->db->update($table,array('views' => $views_curr)); 

	}



	function del_image()

	{

		if($this->session->userdata('user_id_sess'))

		{

			$id = $this->input->post('id_blog');

			$file_old = $this->input->post('file_old');

			

			//GetValue($field,$table,$filter=array(),$order=NULL)

			$GetThumb = getThumb($file_old);

			if(file_exists("./".$this->config->item('path_upload')."/".$file_old)) unlink("./".$this->config->item('path_upload')."/".$file_old);

			if(file_exists("./".$this->config->item('path_upload')."/".$GetThumb)) unlink("./".$this->config->item('path_upload')."/".$GetThumb);

			

			$data['image'] = "";

			$this->db->where("id", $id);

			$result = $this->db->update('kg_blog', $data);



			if($result){

				$this->db->cache_delete_all();

			}

		}

	}



	function delete($id)

	{

		if($this->session->userdata('user_id_sess'))

		{

			$filterblog = array("id"=>"where/".$id);

			$dblog = GetAll('kg_blog',$filterblog);

			$vblog = $dblog->row_array();

			$file_old = $vblog['image'];

			if($file_old){

				$GetThumb = getThumb($file_old);

				if(file_exists("./".$this->config->item('path_upload')."/".$file_old)) unlink("./".$this->config->item('path_upload')."/".$file_old);

				if(file_exists("./".$this->config->item('path_upload')."/".$GetThumb)) unlink("./".$this->config->item('path_upload')."/".$GetThumb);

			}



			$act = array(

				'id_member'	=> $this->session->userdata('user_id_sess'),

				'title' => 'Menghapus blog : '.$vblog['title'],

				'url' => '#',

				'activity' => 'delete_blog',

				'create_date' => date("Y-m-d H:i:s"),

				'create_user_id'	=> $this->session->userdata('user_id_sess')

			);

			$this->db->insert('kg_member_activity', $act);



			$this->db->where("id", $id);

			$result = $this->db->delete('kg_blog');



			if($result){

				$this->db->cache_delete_all();

			}



			$this->session->set_flashdata('message', '<p class="alert-message-success">Blog berhasil dihapus.</p>');

			ciredirect(site_url('blog/my_profile'));



		}

	}



	







}



/* End of file page.php */

/* Location: ./application/controllers/page.php */