<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class jadwal_closing extends CI_Controller {



	function index()

	{

		$this->listing();

	}



	function listing(){

		if((strpos($this->input->ip_address(),'127.0.0.1') !== false) or (strpos($this->input->ip_address(),'192.168.') !== false) or ($this->session->userdata('user_id_sess'))) 

		{

			$data = GetHeaderFooter(1);

			$data['main_content'] = 'list';

			$data['menu_title'] = 'Jadwal Closing';

			$data['controller_name'] = 'jadwal_closing';



			$GetAllConfig = $this->model_admin_all->GetAllConfig('kg_'.$data['controller_name']);

			if($GetAllConfig->num_rows() > 0)

			{

				$r = $GetAllConfig->row_array();

				$per_page = $r['per_page'];

			}



			$uri_segment = 4;

			$awal = $this->uri->segment($uri_segment);

			

			$filter = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/Publish","id"=>"order/desc","limit"=> $awal."/".$per_page);

			$data['qp'] = GetAll('kg_'.$data['controller_name'],$filter);

			

			$filter2 = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/Publish","id"=>"order/desc");

			$q = GetAll('kg_'.$data['controller_name'],$filter2);



			$path_paging = site_url($data['controller_name'].'/listing');

			$pagination = Page($q->num_rows(),$per_page,$awal,$path_paging,$uri_segment);

			if(!$pagination) $pagination = "";

			$data['pagination'] = $pagination;



			$this->load->view('layout',$data);

		}else{

			if($this->session->userdata('user_id_sess'))

			{

				ciredirect('home');

			}else{

				ciredirect('member/login');

			}

		}

	}



	function detail($id=1)

	{

		if((strpos($this->input->ip_address(),'127.0.0.1') !== false) or (strpos($this->input->ip_address(),'192.168.') !== false) or ($this->session->userdata('user_id_sess'))) 

		{

			$data = GetHeaderFooter(1);

			

			$data['main_content'] = 'article';

			$data['controller_name'] = 'jadwal_closing';



			$filter = array(

					"is_publish"=>"where/publish",

					"id"=>"where/".$id,

					"id_lang"=>"where/".GetIdLang()

			);



			$data['article'] = $article = GetAll('kg_'.$data['controller_name'],$filter);

			if($article->num_rows() > 0){

				$v = $data['article']->row_array();

				$i = 0;

				$files = "";

				$tags = explode(',',$v['tags']);

				$numItems = count($tags);

				$result="";

				$files = "SELECT * FROM kg_".$data['controller_name']." where id_lang = ".GetIdLang()." and id <> ".$v['id']. " and (";

				foreach($tags as $tag=>$val){

					if($val != ""){

						$files .= " tags like '%".$val."%'";

						if(++$i === $numItems){

					    	$files .= "";

						}else{

							$files .= " or ";

						}

					}else

					{

						$files .= " tags like '0'";

					}

				}

				$files .= ") limit 0,4";

			



				$data['rel_link'] = $this->db->query($files);

			}else{

				ciredirect(site_url('error_404'));

			}

				

			$this->load->view('layout',$data);

		}else{

			if($this->session->userdata('user_id_sess'))

			{

				ciredirect('home');

			}else{

				ciredirect('member/login');

			}

		}

	}

}



/* End of file page.php */

/* Location: ./application/controllers/page.php */