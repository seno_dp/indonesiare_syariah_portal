<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*************************************
  * Created : June 2013
  * Update  : June 2013
  * Creator : Andi Galuh S
  * Email   : andi@komunigrafik.com
*************************************/

class Home extends CI_Controller  {
	
	public function __construct()
	{
		parent::__construct();
	}

	function index()
	{
		$data = GetHeaderFooter(1);
		$data['main_content'] = 'home';

		$slide_config = tableconfig("kg_slider");
		$slide_limit = $slide_config['item_homepage'];
		$filterslide = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/publish","limit"=>"0/".$slide_limit,"id"=>"order/desc");
		$data['slide'] = GetAll("kg_slider",$filterslide);

		$analisis_config = tableconfig("kg_analisis");
		$analisis_limit = $analisis_config['item_homepage'];
		$filteranalisis = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/publish","limit"=>"0/".$analisis_limit,"id"=>"order/desc");
		$data['analisis'] = GetAll("kg_analisis",$filteranalisis);

		$event_config = tableconfig("kg_event");
		$event_limit = $event_config['item_homepage'];
		$filterevent = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/publish","limit"=>"0/".$event_limit,"id"=>"order/desc");
		$data['event'] = GetAll("kg_event",$filterevent);

		$article_config = tableconfig("kg_article");
		$article_limit = $article_config['item_homepage'];
		$filterarticle = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/publish","limit"=>"0/".$article_limit,"id"=>"order/desc");
		$data['article'] = GetAll("kg_article",$filterarticle);

		$calendar_config = tableconfig("kg_calendar");
		$calendar_limit = $calendar_config['item_homepage'];
		$filtercalendar = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/publish","limit"=>"0/".$calendar_limit,"date_calendar"=>"order/desc");
		$data['calendar'] = GetAll("kg_calendar",$filtercalendar);

		$announcement_config = tableconfig("kg_announcement");
		$announcement_limit = $announcement_config['item_homepage'];
		$filterannouncement = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/publish","limit"=>"0/".$announcement_limit,"id"=>"order/desc");
		$data['announcement'] = GetAll("kg_announcement",$filterannouncement);

		$meeting_config = tableconfig("kg_meeting_room");
		$meeting_limit = $meeting_config['item_homepage'];
		$filtermeeting_room = array("kg_meeting_room.id_lang"=>"where/".GetIdLang(),"kg_meeting_room.is_publish"=>"where/publish","limit"=>"0/".$meeting_limit,"kg_meeting_room.date_meeting"=>"order/desc");
		$data['meeting'] = GetJoin("kg_meeting_room","kg_ref_meeting_room","kg_ref_meeting_room.id = kg_meeting_room.ref_meeting_room","","kg_meeting_room.*,kg_ref_meeting_room.*",$filtermeeting_room);
		
		$absence_config = tableconfig("kg_absence");
		$absence_limit = $absence_config['item_homepage'];
		$filterabsence = array("id_lang"=>"where/".GetIdLang(),"is_publish"=>"where/publish","limit"=>"0/".$absence_limit,"id"=>"order/desc");
		$data['absence'] = GetAll("kg_absence",$filterabsence);
		
		$this->load->view('layout',$data);
	}
}
// END Home Class